#######################################################################
#
# pgAdmin III - PostgreSQL Tools
# $Id$
# Copyright (C) 2002 - 2009, The pgAdmin Development Team
# This software is released under the BSD Licence
#
# module.mk - xtra/pgscript/doc/ Makefile fragment
#
#######################################################################

EXTRA_DIST += \
	$(srcdir)/doc/module.mk \
	$(srcdir)/doc/developers.html \
	$(srcdir)/doc/readme.html
