#######################################################################
#
# pgAdmin III - PostgreSQL Tools
# $Id$
# Copyright (C) 2002 - 2009, The pgAdmin Development Team
# This software is released under the BSD Licence
#
# module.mk - xtra/pgscript/file/test/dictionary/ Makefile fragment
#
#######################################################################

EXTRA_DIST += \
	$(srcdir)/file/test/dictionary/module.mk \
	$(srcdir)/file/test/dictionary/dict_iso8859.txt \
	$(srcdir)/file/test/dictionary/dict_utf8.txt
