#######################################################################
#
# pgAdmin III - PostgreSQL Tools
# $Id$
# Copyright (C) 2002 - 2009, The pgAdmin Development Team
# This software is released under the BSD Licence
#
# module.mk - xtra/pgscript/file/input/ Makefile fragment
#
#######################################################################

EXTRA_DIST += \
	$(srcdir)/file/input/module.mk \
	$(srcdir)/file/input/test.pgs
