
#include "pgAdmin3.h"
#include "utils/pgodbc.h"

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)

const SQLWCHAR* const pgODBC::ODBCINFONAMES[] = {
    _T("SQL_MAX_CATALOG_NAME_LEN"),
    _T("SQL_MAX_SCHEMA_NAME_LEN"),
    _T("SQL_MAX_TABLE_NAME_LEN"),
    _T("SQL_MAX_COLUMN_NAME_LEN"),
    _T("SQL_SQL_CONFORMANCE"),
    _T("SQL_IDENTIFIER_CASE"),
    _T("SQL_QUOTED_IDENTIFIER_CASE"),
    _T("SQL_IDENTIFIER_QUOTE_CHAR"),
    _T("SQL_SPECIAL_CHARACTERS"),
    _T("SQL_CATALOG_NAME"),
    _T("SQL_CATALOG_LOCATION"),
    _T("SQL_CATALOG_NAME_SEPARATOR"),
    _T("SQL_MAX_STATEMENT_LEN"),
    _T("SQL_MAX_TABLES_IN_SELECT"),
    _T("SQL_MAX_COLUMNS_IN_SELECT"),
    _T("SQL_MAX_COLUMNS_IN_ORDER_BY"),
    _T("SQL_MAX_COLUMNS_IN_GROUP_BY"),
    NULL
};

const SQLWCHAR* const pgODBC::ODBCTYPENAMES[] = {
    _T("SQL_UNKNOWN_TYPE"),
    _T("SQL_CHAR"),
    _T("SQL_NUMERIC"),
    _T("SQL_DECIMAL"),
    _T("SQL_INTEGER"),
    _T("SQL_SMALLINT"),
    _T("SQL_FLOAT"),
    _T("SQL_REAL"),
    _T("SQL_DOUBLE"),
    _T("SQL_DATETIME"),
    _T("SQL_INTERVAL"),
    _T("SQL_TIMESTAMP"),
    _T("SQL_VARCHAR"),
    _T("SQL_LONGVARCHAR"),
    _T("SQL_BINARY"),
    _T("SQL_VARBINARY"),
    _T("SQL_LONGVARBINARY"),
    _T("SQL_BIGINT"),
    _T("SQL_TINYINT"),
    _T("SQL_BIT"),
    _T("SQL_WCHAR"),
    _T("SQL_WVARCHAR"),
    _T("SQL_WLONGVARCHAR"),
    NULL
};

SQLWCHAR **pgODBC::FetchDataSources(SQLWCHAR *errmsg)
{
    // The return value must be deleted out of this function.
    SQLHANDLE odbchenv;
    SQLRETURN sqlrc;

    sqlrc = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &odbchenv);
    if (sqlrc != SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO ) {
        if (errmsg)
            wcscpy(errmsg, _T("Can't allocate ODBC environment handle."));
        //SQLFreeHandle( SQL_HANDLE_ENV, odbchenv);
        return NULL;
    }

    sqlrc = SQLSetEnvAttr(odbchenv, SQL_ATTR_ODBC_VERSION,
            (SQLPOINTER)SQL_OV_ODBC3, SQL_IS_INTEGER);
    if (sqlrc != SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO ) {
        if (errmsg)
            wcscpy(errmsg, _T("Can't set ODBC environment attribute(SQL_ATTR_ODBC_VERSION=SQL_OV_ODBC3)."));
        SQLFreeHandle( SQL_HANDLE_ENV, odbchenv);
        return NULL;
    }

    size_t namelen, dsnidx = 0;
    SQLWCHAR *dsnnameptr;
    SQLWCHAR dsnname[SQL_MAX_DSN_LENGTH + 1];
    SQLWCHAR **rtndsnlist = NULL, **tmpdsnlist;

    while ( ( sqlrc = SQLDataSources( odbchenv, SQL_FETCH_NEXT,
                    dsnname, SQL_MAX_DSN_LENGTH + 1, NULL,NULL, 0, NULL))==SQL_SUCCESS
                || sqlrc==SQL_SUCCESS_WITH_INFO)
    {
        namelen = wcslen(dsnname) + 1;
        dsnnameptr = new SQLWCHAR[namelen];
        memcpy(dsnnameptr, dsnname, sizeof(SQLWCHAR)*namelen);

        tmpdsnlist = rtndsnlist;
        rtndsnlist = new SQLWCHAR *[dsnidx+1];
        memcpy(rtndsnlist, tmpdsnlist, sizeof(SQLWCHAR*)*dsnidx);
        rtndsnlist[dsnidx] = dsnnameptr;
        if (tmpdsnlist)
            delete[] tmpdsnlist;

        dsnidx++;
    }

    tmpdsnlist = rtndsnlist;
    rtndsnlist = new SQLWCHAR *[dsnidx+1];
    memcpy(rtndsnlist, tmpdsnlist, sizeof(SQLWCHAR*)*dsnidx);
    rtndsnlist[dsnidx] = NULL;
    if (tmpdsnlist)
        delete[] tmpdsnlist;

    return rtndsnlist;
}

pgODBC::pgODBC(const SQLWCHAR *dsnname, const SQLWCHAR *dsnuser, const SQLWCHAR *dsnpwd)
{
    memcpy(m_dsnnamewc, dsnname, (wcslen(dsnname)+1)*sizeof(SQLWCHAR));
    memcpy(m_dsnuserwc, dsnuser, (wcslen(dsnuser)+1)*sizeof(SQLWCHAR));
    memcpy(m_dsnpwdwc, dsnpwd, (wcslen(dsnpwd)+1)*sizeof(SQLWCHAR));

    Init();
}
/*
pgODBC::pgODBC(const wxString dsnname, const wxString dsnuser, const wxString dsnpwd)
{
    const wxChar* dsnnamewc = dsnname.wc_str();
    const wxChar* dsnuserwc = dsnuser.wc_str();
    const wxChar* dsnpwdwc = dsnpwd.wc_str();

    memcpy(m_dsnnamewc, dsnname, (wcslen(dsnnamewc)+1)*sizeof(wxChar));
    memcpy(m_dsnuserwc, dsnuser, (wcslen(dsnuserwc)+1)*sizeof(wxChar));
    memcpy(m_dsnpwdwc, dsnpwd, (wcslen(dsnpwdwc)+1)*sizeof(wxChar));

    Init();
}
*/
pgODBC::~pgODBC()
{
    this->Disconnect();

    SQLFreeHandle(SQL_HANDLE_DBC, m_odbchdbc);
    SQLFreeHandle(SQL_HANDLE_ENV, m_odbchenv);

    delete[] m_odbcinfovals;
}

void pgODBC::Init()
{
    m_connected = false;
    m_tablenum = 0;
    m_tablelist = NULL;
    m_tableinfo = NULL;

    m_odbcinfovals = new SQLWCHAR *[ODBCINFONUM+1];
    size_t infoidx;
    for (infoidx=0; infoidx<ODBCINFONUM+1; infoidx++)
        m_odbcinfovals[infoidx] = NULL;

    m_curhstmt = NULL;
    m_curstmttext = NULL;
    m_curstmtcolsnum = 0;
    m_curstmttypes = NULL;
    m_curstmtlabels = NULL;
}

bool pgODBC::Connect()
{
    wxASSERT(!m_connected);

    SQLRETURN sqlrc;

    m_oerrmsg[0] = '\0';

    if (!wcslen(m_dsnnamewc))
    {
        wcscpy(m_oerrmsg, _T("Please specify DSN name."));
        return false;
    }

    sqlrc = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &m_odbchenv);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO )
    {
        wcscpy(m_oerrmsg, _T("Can't allocate ODBC environment handle."));
        //SQLFreeHandle( SQL_HANDLE_ENV, m_odbchenv);
        return false;
    }

    sqlrc = SQLSetEnvAttr(m_odbchenv, SQL_ATTR_ODBC_VERSION,
            (SQLPOINTER)SQL_OV_ODBC3, SQL_IS_INTEGER);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO )
    {
        wcscpy(m_oerrmsg, _T("Can't set ODBC environment attribute(SQL_ATTR_ODBC_VERSION=SQL_OV_ODBC3)."));
        SQLFreeHandle( SQL_HANDLE_ENV, m_odbchenv);
        return false;
    }

    sqlrc = SQLAllocHandle( SQL_HANDLE_DBC, m_odbchenv, &m_odbchdbc);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO )
    {
        wcscpy(m_oerrmsg, _T("Can't allocate ODBC connection handle."));
        //SQLFreeHandle( SQL_HANDLE_DBC, m_odbchdbc);
        SQLFreeHandle( SQL_HANDLE_ENV, m_odbchenv);
        return false;
    }

    sqlrc = SQLSetConnectAttr(m_odbchdbc, SQL_ATTR_ACCESS_MODE, (SQLPOINTER)SQL_MODE_READ_ONLY, SQL_IS_UINTEGER);
    sqlrc = SQLSetConnectAttr(m_odbchdbc, SQL_ATTR_AUTOCOMMIT, (SQLPOINTER)SQL_AUTOCOMMIT_OFF, SQL_IS_UINTEGER);

    sqlrc = SQLConnect(m_odbchdbc, m_dsnnamewc, SQL_NTS, m_dsnuserwc, SQL_NTS, m_dsnpwdwc, SQL_NTS);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO )
    {
        wcscpy(m_oerrmsg, _T("Can't connect to database."));
        SQLWCHAR errmsg[ODBCNAMEBUFFLEN];
        sqlrc = SQLGetDiagRec( SQL_HANDLE_DBC, m_odbchdbc, 1, NULL, 0, errmsg, ODBCNAMEBUFFLEN, NULL);
        if ((sqlrc==SQL_SUCCESS || sqlrc==SQL_SUCCESS_WITH_INFO) && errmsg[0])
        {
            wcscat(m_oerrmsg, END_OF_LINE);
            wcscat(m_oerrmsg, errmsg);
        }
        SQLFreeHandle( SQL_HANDLE_DBC, m_odbchdbc);
        SQLFreeHandle( SQL_HANDLE_ENV, m_odbchenv);
        return false;
    }

    m_connected = true;

    FetchODBCInfo();

    return true;
}

bool pgODBC::Connect(const SQLWCHAR *dsnname, const SQLWCHAR *dsnuser, const SQLWCHAR *dsnpwd)
{
    wxASSERT(!m_connected);

    memcpy(m_dsnnamewc, dsnname, (wcslen(dsnname)+1)*sizeof(SQLWCHAR));
    memcpy(m_dsnuserwc, dsnuser, (wcslen(dsnuser)+1)*sizeof(SQLWCHAR));
    memcpy(m_dsnpwdwc, dsnpwd, (wcslen(dsnpwd)+1)*sizeof(SQLWCHAR));

    return this->Connect();
}
/*
bool pgODBC::Connect(const wxString dsnname, const wxString dsnuser, const wxString dsnpwd)
{
    wxASSERT(!m_connected);

    const wxChar* dsnnamewc = dsnname.wc_str();
    const wxChar* dsnuserwc = dsnuser.wc_str();
    const wxChar* dsnpwdwc = dsnpwd.wc_str();

    memcpy(m_dsnnamewc, dsnname, (wcslen(dsnnamewc)+1)*sizeof(wxChar));
    memcpy(m_dsnuserwc, dsnuser, (wcslen(dsnuserwc)+1)*sizeof(wxChar));
    memcpy(m_dsnpwdwc, dsnpwd, (wcslen(dsnpwdwc)+1)*sizeof(wxChar));

    return this->Connect();
}
*/
void pgODBC::Disconnect()
{
    if (!m_connected)
        return;

    CloseCurrentStmt();

    ClearTableInfo();
    ClearTableList();

    ClearODBCInfo();

    SQLEndTran(SQL_HANDLE_DBC, m_odbchdbc, SQL_ROLLBACK);
    SQLDisconnect(m_odbchdbc);

    m_connected = false;
    m_oerrmsg[0] = '\0';
}

void pgODBC::FetchODBCInfo()
{
    ClearODBCInfo();

    if (!m_connected)
        return;

    SQLGetInfo(m_odbchdbc, SQL_MAX_CATALOG_NAME_LEN, &m_dsnmaxcatnamelen, 0, NULL);
    SQLGetInfo(m_odbchdbc, SQL_MAX_SCHEMA_NAME_LEN, &m_dsnmaxschemnamelen, 0, NULL);
    SQLGetInfo(m_odbchdbc, SQL_MAX_TABLE_NAME_LEN, &m_dsnmaxtablenamelen, 0, NULL);
    SQLGetInfo(m_odbchdbc, SQL_MAX_COLUMN_NAME_LEN, &m_dsnmaxcolnamelen, 0, NULL);

    SQLGetInfo(m_odbchdbc, SQL_SQL_CONFORMANCE, &m_sqlconformance, 0, NULL);
    SQLGetInfo(m_odbchdbc, SQL_IDENTIFIER_CASE, &m_idcase, 0, NULL);
    SQLGetInfo(m_odbchdbc, SQL_QUOTED_IDENTIFIER_CASE, &m_quotedidcase, 0, NULL);
    SQLGetInfo(m_odbchdbc, SQL_IDENTIFIER_QUOTE_CHAR, &m_idquotechars, sizeof(SQLWCHAR)*ODBCNAMEBUFFLEN, NULL);
    SQLGetInfo(m_odbchdbc, SQL_SPECIAL_CHARACTERS, &m_idspecchars, sizeof(SQLWCHAR)*ODBCNAMEBUFFLEN, NULL);

    SQLGetInfo(m_odbchdbc, SQL_CATALOG_LOCATION, &m_catalogloc, 0, NULL);
    SQLGetInfo(m_odbchdbc, SQL_CATALOG_NAME, &m_catalogname, sizeof(SQLWCHAR)*2, NULL);
    SQLGetInfo(m_odbchdbc, SQL_CATALOG_NAME_SEPARATOR, &m_catalognameseparator, sizeof(SQLWCHAR)*ODBCNAMEBUFFLEN, NULL);
    SQLGetInfo(m_odbchdbc, SQL_MAX_STATEMENT_LEN, &m_maxstatementlen, 0, NULL);
    SQLGetInfo(m_odbchdbc, SQL_MAX_TABLES_IN_SELECT, &m_maxtabsinselect, 0, NULL);
    SQLGetInfo(m_odbchdbc, SQL_MAX_COLUMNS_IN_SELECT, &m_maxcolsinselect, 0, NULL);
    SQLGetInfo(m_odbchdbc, SQL_MAX_COLUMNS_IN_ORDER_BY, &m_maxcolsinorderby, 0, NULL);
    SQLGetInfo(m_odbchdbc, SQL_MAX_COLUMNS_IN_GROUP_BY, &m_maxcolsingroupby, 0, NULL);

    m_odbcinfovals[0] = new SQLWCHAR[10];
    _itow_s(m_dsnmaxcatnamelen, m_odbcinfovals[0], 10, 10);
    m_odbcinfovals[1] = new SQLWCHAR[10];
    _itow_s(m_dsnmaxschemnamelen, m_odbcinfovals[1], 10, 10);
    m_odbcinfovals[2] = new SQLWCHAR[10];
    _itow_s(m_dsnmaxtablenamelen, m_odbcinfovals[2], 10, 10);
    m_odbcinfovals[3] = new SQLWCHAR[10];
    _itow_s(m_dsnmaxcolnamelen, m_odbcinfovals[3], 10, 10);

    m_odbcinfovals[4] = new SQLWCHAR[30];
    switch(m_sqlconformance)
    {
    case SQL_SC_SQL92_ENTRY:
        wcscpy(m_odbcinfovals[4], _T("SQL_SC_SQL92_ENTRY"));
        break;
    case SQL_SC_FIPS127_2_TRANSITIONAL:
        wcscpy(m_odbcinfovals[4], _T("SQL_SC_FIPS127_2_TRANSITIONAL"));
        break;
    case SQL_SC_SQL92_FULL:
        wcscpy(m_odbcinfovals[4], _T("SQL_SC_SQL92_FULL"));
        break;
    case SQL_SC_SQL92_INTERMEDIATE:
        wcscpy(m_odbcinfovals[4], _T("SQL_SC_SQL92_INTERMEDIATE"));
        break;
    default:
        wcscpy(m_odbcinfovals[4], _T(""));
        break;
    }

    m_odbcinfovals[5] = new SQLWCHAR[20];
    switch(m_idcase)
    {
    case SQL_IC_UPPER:
        wcscpy(m_odbcinfovals[5], _T("SQL_IC_UPPER"));
        break;
    case SQL_IC_LOWER:
        wcscpy(m_odbcinfovals[5], _T("SQL_IC_LOWER"));
        break;
    case SQL_IC_SENSITIVE:
        wcscpy(m_odbcinfovals[5], _T("SQL_IC_SENSITIVE"));
        break;
    case SQL_IC_MIXED:
        wcscpy(m_odbcinfovals[5], _T("SQL_IC_MIXED"));
        break;
    default:
        wcscpy(m_odbcinfovals[5], _T(""));
        break;
    }

    m_odbcinfovals[6] = new SQLWCHAR[20];
    switch(m_quotedidcase)
    {
    case SQL_IC_UPPER:
        wcscpy(m_odbcinfovals[6], _T("SQL_IC_UPPER"));
        break;
    case SQL_IC_LOWER:
        wcscpy(m_odbcinfovals[6], _T("SQL_IC_LOWER"));
        break;
    case SQL_IC_SENSITIVE:
        wcscpy(m_odbcinfovals[6], _T("SQL_IC_SENSITIVE"));
        break;
    case SQL_IC_MIXED:
        wcscpy(m_odbcinfovals[6], _T("SQL_IC_MIXED"));
        break;
    default:
        wcscpy_s(m_odbcinfovals[6], 20, _T(""));
        break;
    }

    m_odbcinfovals[7] = new SQLWCHAR[wcslen(m_idquotechars)+1];
    wcscpy(m_odbcinfovals[7], m_idquotechars);
    m_odbcinfovals[8] = new SQLWCHAR[wcslen(m_idspecchars)+1];
    wcscpy(m_odbcinfovals[8], m_idspecchars);

    m_odbcinfovals[9] = new SQLWCHAR[wcslen(m_catalogname)+1];
    wcscpy(m_odbcinfovals[9], m_catalogname);

    m_odbcinfovals[10] = new SQLWCHAR[13];
    switch(m_catalogloc)
    {
    case SQL_CL_START:
        wcscpy(m_odbcinfovals[10], _T("SQL_CL_START"));
        break;
    case SQL_CL_END:
        wcscpy(m_odbcinfovals[10], _T("SQL_CL_END"));
        break;
    default:
        wcscpy(m_odbcinfovals[10], _T(""));
        break;
    }

    m_odbcinfovals[11] = new SQLWCHAR[wcslen(m_catalognameseparator)+1];
    wcscpy(m_odbcinfovals[11], m_catalognameseparator);

    m_odbcinfovals[12] = new SQLWCHAR[10];
    _itow_s(m_maxstatementlen, m_odbcinfovals[12], 10, 10);
    m_odbcinfovals[13] = new SQLWCHAR[10];
    _itow_s(m_maxtabsinselect, m_odbcinfovals[13], 10, 10);
    m_odbcinfovals[14] = new SQLWCHAR[10];
    _itow_s(m_maxcolsinselect, m_odbcinfovals[14], 10, 10);
    m_odbcinfovals[15] = new SQLWCHAR[10];
    _itow_s(m_maxcolsinorderby, m_odbcinfovals[15], 10, 10);
    m_odbcinfovals[16] = new SQLWCHAR[10];
    _itow_s(m_maxcolsingroupby, m_odbcinfovals[16], 10, 10);

}

void pgODBC::ClearODBCInfo()
{
    size_t infoidx;
    for (infoidx=0; infoidx<ODBCINFONUM; infoidx++)
        if (m_odbcinfovals[infoidx])
        {
            delete[] m_odbcinfovals[infoidx];
            m_odbcinfovals[infoidx] = NULL;
        }
}

const SQLWCHAR **pgODBC::GetTableList()
{
    if (!m_connected)
        return NULL;

    if (m_tablelist)
        return (const SQLWCHAR **)m_tablelist;
    //ClearTableList();

    SQLRETURN sqlrc;

    SQLHSTMT tableshstmt;
    sqlrc = SQLAllocHandle(SQL_HANDLE_STMT, m_odbchdbc, &tableshstmt);
    if (sqlrc != SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO ) {
        wcscpy_s(m_oerrmsg, _T("Can't allocate statement handle."));
        //SQLFreeHandle( SQL_HANDLE_STMT, tableshstmt);
        return NULL;
    }

    sqlrc = SQLTables(tableshstmt, NULL, 0, NULL, 0, NULL, 0, NULL, 0);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO )
    {
        SQLFreeHandle(SQL_HANDLE_STMT, tableshstmt);
        wcscpy_s(m_oerrmsg, _T("Can't fetch table list."));
        return NULL;
    }

    SQLWCHAR *catname = NULL, *schemname = NULL, *tablename = NULL;
    if (m_dsnmaxcatnamelen)
        catname = new SQLWCHAR[m_dsnmaxcatnamelen];
    if (m_dsnmaxschemnamelen)
        schemname = new SQLWCHAR[m_dsnmaxschemnamelen];
    tablename = new SQLWCHAR[m_dsnmaxtablenamelen];
    SQLWCHAR *catnameptr, *schemnameptr, *tablenameptr;

    SQLWCHAR /***tabledata, */**tablelist;

    while (true)
    {
        sqlrc = SQLFetch(tableshstmt);
        if (sqlrc != SQL_SUCCESS && sqlrc != SQL_SUCCESS_WITH_INFO)
            break;

        if (m_dsnmaxcatnamelen)
        {
            SQLGetData(tableshstmt, 1, SQL_C_WCHAR, catname, sizeof(SQLWCHAR)*m_dsnmaxcatnamelen, NULL);
            catnameptr = new SQLWCHAR[wcslen(catname)+1];
            wcscpy(catnameptr, catname);
        }
        else
            catnameptr = NULL;

        if (m_dsnmaxschemnamelen)
        {
            SQLGetData(tableshstmt, 2, SQL_C_WCHAR, schemname, sizeof(SQLWCHAR)*m_dsnmaxschemnamelen, NULL);
            schemnameptr = new SQLWCHAR[wcslen(schemname)+1];
            wcscpy(schemnameptr, schemname);
        }
        else
            schemnameptr = NULL;

        SQLGetData(tableshstmt, 3, SQL_C_WCHAR, tablename, sizeof(SQLWCHAR)*m_dsnmaxtablenamelen, NULL);
        tablenameptr = new SQLWCHAR[wcslen(tablename)+1];
        wcscpy(tablenameptr, tablename);
/*
        tabledata = new SQLWCHAR *[3];
        tabledata[0] = catnameptr;
        tabledata[1] = schemnameptr;
        tabledata[2] = tablenameptr;
*/
        tablelist = m_tablelist;
        m_tablelist = new SQLWCHAR *[(m_tablenum+1)*3];
        if (m_tablenum)
            memcpy(m_tablelist, tablelist, sizeof(SQLWCHAR*)*m_tablenum*3);
        m_tablelist[m_tablenum*3] = catnameptr;
        m_tablelist[m_tablenum*3+1] = schemnameptr;
        m_tablelist[m_tablenum*3+2] = tablenameptr;
        if (tablelist)
            delete[] tablelist;

        m_tablenum++;
    }

    SQLFreeHandle(SQL_HANDLE_STMT, tableshstmt);

    if (catname)
        delete[] catname;
    if (schemname)
        delete[] schemname;
    delete[] tablename;
/*
    tablelist = m_tablelist;
    m_tablelist = new SQLWCHAR **[tablecount+1];
    if (tablecount)
        memcpy(m_tablelist, tablelist, sizeof(SQLWCHAR**)*tablecount);
    m_tablelist[tablecount] = NULL;
    if (tablelist)
        delete[] tablelist;
*/
    return (const SQLWCHAR **)m_tablelist;
/*
    unsigned int tableidx = 0;
    bool noaccesspriv;
    SQLHSTMT privshstmt;
    SQLWCHAR *catnamewc, *schemnamewc, *tablenamewc;
    SQLWCHAR tablepriv[IMPSQLNAMELEN];

    for (; tableidx>0; tableidx--)
    {
        tabledata = (SQLWCHAR**)cbODBCTableNames->GetClientData(tableidx);
        catnamewc = tabledata[0];
        schemnamewc = tabledata[1];
        tablenamewc = tabledata[2];

        sqlrc = SQLAllocHandle(SQL_HANDLE_STMT, imphdbc, &privshstmt);
        if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO )
        {
            SQLFreeHandle( SQL_HANDLE_STMT, privshstmt);
            continue;
        }
        noaccesspriv = true;
        sqlrc = SQLTablePrivileges(privshstmt, catnamewc, wcslen(catnamewc),
            schemnamewc, wcslen(schemnamewc), tablenamewc, wcslen(tablenamewc));
        if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO )
            noaccesspriv = false;
        else
        {
            while (true)
            {
                sqlrc = SQLFetch(privshstmt);
                if (sqlrc != SQL_SUCCESS && sqlrc != SQL_SUCCESS_WITH_INFO)
                    break;

                SQLGetData(privshstmt, 6, SQL_C_WCHAR, tablepriv, IMPSQLNAMELEN, NULL);
                if (!wcscmp(tablepriv, wxT("SELECT")))
                {
                    noaccesspriv = false;
                    break;
                }
            }
        }

        if (noaccesspriv)
        {
            delete[] tabledata[0];
            delete[] tabledata[1];
            delete[] tabledata[2];
            delete[] tabledata;
            cbODBCTableNames->Delete(tableidx);
        }

        SQLFreeHandle( SQL_HANDLE_STMT, privshstmt);
    }
*/
}

void pgODBC::ClearTableList()
{
    if (!m_tablelist)
        return;

    SQLINTEGER tableidx = 0;
    for (; tableidx<m_tablenum; tableidx++)
    {
        if (m_tablelist[tableidx*3])
            delete[] m_tablelist[tableidx*3];

        if (m_tablelist[tableidx*3+1])
            delete[] m_tablelist[tableidx*3+1];

        delete[] m_tablelist[tableidx*3+2];
    }

/*
    SQLWCHAR ***tablelist;
    for (tablelist=m_tablelist; *tablelist; tablelist++)
    {
        if ((*tablelist)[0])
            delete[] (*tablelist)[0];
        if ((*tablelist)[1])
            delete[] (*tablelist)[1];
        delete[] (*tablelist)[2];

        delete[] (*tablelist);
    }
*/
    delete[] m_tablelist;
    m_tablelist = NULL;
    m_tablenum = 0;
}

/*
void pgODBC::ClearODBCInfo()
{
    if (!m_odbcinfo)
        return;

    SQLWCHAR ***odbcinfo;
    for (odbcinfo=m_odbcinfo; *odbcinfo; odbcinfo++)
    {
        delete (*odbcinfo)[0];
        delete (*odbcinfo)[1];

        delete[] (*m_odbcinfo);
    }

    delete[] m_odbcinfo;
    m_odbcinfo = NULL;
}
*/

const SQLWCHAR ***pgODBC::GetTableInfo(SQLWCHAR *catname, SQLWCHAR *schemname, SQLWCHAR *tablename)
{
    if (!m_connected)
        return NULL;

    SQLRETURN sqlrc;

    SQLHSTMT colshstmt;
    sqlrc = SQLAllocHandle(SQL_HANDLE_STMT, m_odbchdbc, &colshstmt);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO )
    {
        wcscpy(m_oerrmsg, _T("Can't allocate statement handle."));
        //SQLFreeHandle( SQL_HANDLE_STMT, colshstmt);
        return NULL;
    }

    sqlrc = SQLColumns(colshstmt, catname, SQL_NTS, schemname, SQL_NTS, tablename, SQL_NTS, NULL, 0);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO )
    {
        wcscpy(m_oerrmsg, _T("Can't retrieve columns information"));
        SQLFreeHandle( SQL_HANDLE_STMT, colshstmt);
        return NULL;
    }

    ClearTableInfo();

    SQLWCHAR *colname, odbctypename[ODBCNAMEBUFFLEN];
    SQLSMALLINT coltype;
    //SQLINTEGER ordpos;

    if (m_dsnmaxcolnamelen)
        colname = new SQLWCHAR[m_dsnmaxcolnamelen+1];
    else
        colname = new SQLWCHAR[ODBCNAMEBUFFLEN];

    size_t namelen, colsnum = 0;
    SQLWCHAR **colinfoarr, ***tableinfoarr;
    const SQLWCHAR *coltypenameptr;

    while(true)
    {
        if (colsnum>99)
        {
            colinfoarr = new SQLWCHAR *[3];
            colinfoarr[0] = new SQLWCHAR[4];
            wcscpy(colinfoarr[0], _T("..."));
            colinfoarr[1] = new SQLWCHAR[4];
            wcscpy(colinfoarr[1], _T("..."));
            colinfoarr[2] = new SQLWCHAR[4];
            wcscpy(colinfoarr[2], _T("..."));

            tableinfoarr = m_tableinfo;
            m_tableinfo = new SQLWCHAR **[colsnum+2];
            memcpy(m_tableinfo, tableinfoarr, sizeof(SQLWCHAR**)*colsnum);
            m_tableinfo[colsnum] = colinfoarr;
            m_tableinfo[colsnum+1] = NULL;
            delete[] tableinfoarr;

            break;
        }

        sqlrc = SQLFetch(colshstmt);
        if (sqlrc != SQL_SUCCESS && sqlrc != SQL_SUCCESS_WITH_INFO)
            break;

        //memset(colname, 0, sizeof(SQLWCHAR)*ODBCNAMEBUFFLEN);
        SQLGetData(colshstmt, 4, SQL_C_WCHAR, colname, sizeof(SQLWCHAR)*ODBCNAMEBUFFLEN, NULL);
        SQLGetData(colshstmt, 5, SQL_C_SSHORT, &coltype, 0, NULL);
        SQLGetData(colshstmt, 6, SQL_C_WCHAR, odbctypename, sizeof(SQLWCHAR)*ODBCNAMEBUFFLEN, NULL);
        //SQLGetData(colshstmt,17, SQL_C_SLONG, &ordpos, 0, NULL);

        colinfoarr = new SQLWCHAR *[3];
        namelen = wcslen(colname) + 1;
        colinfoarr[0] = new SQLWCHAR[namelen];
        wcscpy(colinfoarr[0], colname);
        coltypenameptr = SQLTypeToName(coltype);
        namelen = wcslen(coltypenameptr) + 1;
        colinfoarr[1] = new SQLWCHAR[namelen];
        wcscpy(colinfoarr[1], coltypenameptr);
        namelen = wcslen(odbctypename) + 1;
        colinfoarr[2] = new SQLWCHAR[namelen];
        wcscpy(colinfoarr[2], odbctypename);

        if (m_tableinfo)
        {
            tableinfoarr = m_tableinfo;
            m_tableinfo = new SQLWCHAR **[colsnum+2];
            memcpy(m_tableinfo, tableinfoarr, sizeof(SQLWCHAR**)*colsnum);
            m_tableinfo[colsnum] = colinfoarr;
            m_tableinfo[colsnum+1] = NULL;
            delete[] tableinfoarr;
        }
        else
        {
            m_tableinfo = new SQLWCHAR **[2];
            m_tableinfo[0] = colinfoarr;
            m_tableinfo[1] = NULL;
        }

        colsnum++;
    }

    SQLFreeHandle( SQL_HANDLE_STMT, colshstmt);

    delete[] colname;

    return (const SQLWCHAR***)m_tableinfo;
}

void pgODBC::ClearTableInfo()
{
    if (!m_tableinfo)
        return;

    SQLWCHAR ***tableinfo;
    for (tableinfo=m_tableinfo; *tableinfo; tableinfo++)
    {
        if ((*tableinfo)[0])
            delete[] (*tableinfo)[0];
        if ((*tableinfo)[1])
            delete[] (*tableinfo)[1];
        if ((*tableinfo)[2])
            delete[] (*tableinfo)[2];

        delete[] *tableinfo;
    }

    delete[] m_tableinfo;
    m_tableinfo = NULL;
}

SQLWCHAR *pgODBC::GetSelectStmt(const SQLWCHAR *qualifiedname,
                                           const SQLWCHAR ***colsinfo,
                                           const SQLWCHAR *wherecl)
{
    if (!m_connected || !colsinfo || !colsinfo[0])
        return NULL;

    size_t stmtbuffsz = 1024, stmtbuffof = 0, objnamelen;
    SQLWCHAR *stmtbuff = new SQLWCHAR[stmtbuffsz], *tmpbuff;
    const SQLWCHAR ***colinfo, *quotedname;

    wcscpy(stmtbuff, _T("SELECT "));
    stmtbuffof = 7;

    for (colinfo=colsinfo; *colinfo; colinfo++)
    {
        if (!(*colinfo)[0][0])
            continue;

        quotedname = GetQuotedName((*colinfo)[0]);
        if (quotedname)
            objnamelen = wcslen(quotedname);
        else
            objnamelen = wcslen((*colinfo)[0]);

        if (*(colinfo+1))
            objnamelen += 2;

        if (objnamelen>=stmtbuffsz-stmtbuffof)
        {
            stmtbuffsz += (objnamelen>1024?objnamelen:1024);
            tmpbuff = new SQLWCHAR[stmtbuffsz];
            wcscpy(tmpbuff, stmtbuff);
            delete[] stmtbuff;
            stmtbuff = tmpbuff;
        }
        if (quotedname)
            wcscat(stmtbuff, quotedname);
        else
            wcscat(stmtbuff, (*colinfo)[0]);

        stmtbuffof += objnamelen;

        if (quotedname)
            delete[] quotedname;

        if (*(colinfo+1))
            wcscat(stmtbuff, _T(", "));
    }

    objnamelen = wcslen(qualifiedname) + (wherecl&&wherecl[0]?wcslen(wherecl)+1:0) + 6;
    if (objnamelen>=stmtbuffsz-stmtbuffof)
    {
        stmtbuffsz += (objnamelen>1024?objnamelen:1024);
        tmpbuff = new SQLWCHAR[stmtbuffsz];
        wcscpy(tmpbuff, stmtbuff);
        delete[] stmtbuff;
        stmtbuff = tmpbuff;
    }
    wcscat(stmtbuff, _T(" FROM "));
    wcscat(stmtbuff, qualifiedname);
    if (wherecl&&wherecl[0])
    {
        wcscat(stmtbuff, _T(" "));
        wcscat(stmtbuff, wherecl);
    }

    return stmtbuff;
}

bool pgODBC::ExecuteQueryStmt(SQLWCHAR *stmttext)
{
    wxASSERT (m_curhstmt==NULL);

    size_t stmtlen = wcslen(stmttext);
    if (stmtlen<7)
    {
        wcscpy(m_oerrmsg, _T("Invalid statement."));
        return false;
    }

    if (stmttext[0]!='s' && stmttext[0]!='S' &&
        stmttext[0]!='s' && stmttext[0]!='E' &&
        stmttext[0]!='s' && stmttext[0]!='L' &&
        stmttext[0]!='s' && stmttext[0]!='E' &&
        stmttext[0]!='s' && stmttext[0]!='C' &&
        stmttext[0]!='s' && stmttext[0]!='T')
    {
        wcscpy(m_oerrmsg, _T("The statement must be a query statement."));
        return false;
    }

    if (m_maxstatementlen && m_maxstatementlen<stmtlen)
    {
        wcscpy(m_oerrmsg, _T("The length of statement exceed ODBC driver's limit."));
        return false;
    }

    SQLRETURN sqlrc = SQLAllocHandle(SQL_HANDLE_STMT, m_odbchdbc, &m_curhstmt);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO)
    {
        wcscpy(m_oerrmsg, _T("Can't allocate statement handle."));
        m_curhstmt = NULL;
        return false;
    }

    SQLWCHAR errmsg[ODBCNAMEBUFFLEN];

    sqlrc = SQLPrepare(m_curhstmt, stmttext, SQL_NTS);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO)
    {
        wcscpy(m_oerrmsg, _T("Can't prepare statement."));
        sqlrc = SQLGetDiagRec( SQL_HANDLE_STMT, m_curhstmt, 1, NULL, 0, errmsg, ODBCNAMEBUFFLEN, NULL);
        if ((sqlrc==SQL_SUCCESS || sqlrc==SQL_SUCCESS_WITH_INFO) && errmsg[0])
        {
            wcscat(m_oerrmsg, END_OF_LINE);
            wcscat(m_oerrmsg, errmsg);
        }
        SQLFreeHandle( SQL_HANDLE_STMT, m_curhstmt);
        m_curhstmt = NULL;
        return false;
    }

    sqlrc = SQLExecute(m_curhstmt);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO)
    {
        wcscpy(m_oerrmsg, _T("Can't execute statement."));
        sqlrc = SQLGetDiagRec( SQL_HANDLE_STMT, m_curhstmt, 1, NULL, 0, errmsg, ODBCNAMEBUFFLEN, NULL);
        if ((sqlrc==SQL_SUCCESS || sqlrc==SQL_SUCCESS_WITH_INFO) && errmsg[0])
        {
            wcscat(m_oerrmsg, END_OF_LINE);
            wcscat(m_oerrmsg, errmsg);
        }
        SQLFreeHandle( SQL_HANDLE_STMT, m_curhstmt);
        m_curhstmt = NULL;
        return false;
    }

    ClearCurrentStmtInfo();

    sqlrc = SQLNumResultCols(m_curhstmt, &m_curstmtcolsnum);
    if ((sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO) || !m_curstmtcolsnum)
    {
        wcscpy(m_oerrmsg, _T("Can't retrieve the number of columns."));
        SQLFreeHandle( SQL_HANDLE_STMT, m_curhstmt);
        m_curhstmt = NULL;
        return false;
    }

    SQLSMALLINT colidx, labellen;
    m_curstmttypes = new SQLSMALLINT[m_curstmtcolsnum];
    m_curstmtlabels = new SQLWCHAR *[m_curstmtcolsnum];
    SQLINTEGER coltype;
    SQLWCHAR collabel[ODBCNAMEBUFFLEN];
    bool hasemptylabel = false;
    for (colidx=0; colidx<m_curstmtcolsnum; colidx++)
    {
        sqlrc = SQLColAttribute(m_curhstmt, colidx+1, SQL_DESC_TYPE, NULL, SQL_SMALLINT, NULL, &coltype);
        if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO)
            m_curstmttypes[colidx] = SQL_UNKNOWN_TYPE;
        else
            m_curstmttypes[colidx] = coltype;

        sqlrc = SQLColAttribute(m_curhstmt, colidx+1, SQL_DESC_LABEL, collabel, ODBCNAMEBUFFLEN*sizeof(SQLWCHAR), &labellen, NULL);
        if ((sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO) || !(labellen=wcslen(collabel)))
        {
            m_curstmtlabels[colidx] = NULL;
            hasemptylabel = true;
        }
        else
        {
            m_curstmtlabels[colidx] = new SQLWCHAR[labellen+1];
            wcscpy(m_curstmtlabels[colidx], collabel);
        }
    }

    SQLSMALLINT minoridx;
    for (colidx=0; colidx+1<m_curstmtcolsnum; colidx++)
    {
        if (!m_curstmtlabels[colidx] || !m_curstmtlabels[colidx][0])
            continue;

        for (minoridx=colidx+1; minoridx<m_curstmtcolsnum; minoridx++)
            if (!wcscmp(m_curstmtlabels[colidx], m_curstmtlabels[minoridx]))
            {
                wcscpy(m_oerrmsg, _T("The columns must have distinct name."));
                SQLFreeHandle( SQL_HANDLE_STMT, m_curhstmt);
                m_curhstmt = NULL;
                return false;
            }
    }

    if (hasemptylabel)
    {
        SQLWCHAR autolabel[20];
        short minornum;
        bool hassamename;
        for (colidx=0; colidx<m_curstmtcolsnum; colidx++)
        {
            if (m_curstmtlabels[colidx])
                continue;

            minornum = 1;
            swprintf_s(autolabel, _T("Column #%d"), colidx+1);
            while(true)
            {
                hassamename = false;
                for (minoridx=0; minoridx<m_curstmtcolsnum; minoridx++)
                {
                    if (!m_curstmtlabels[minoridx])
                        continue;

                    if (wcscmp(m_curstmtlabels[minoridx], autolabel))
                        continue;
                    else
                    {
                        hassamename = true;
                        break;
                    }
                }

                if (hassamename)
                {
                    swprintf_s(autolabel, _T("Column #%d_%d"), colidx+1, minornum);
                    continue;
                }
                else
                    break;
            }
            m_curstmtlabels[colidx] = new SQLWCHAR[wcslen(autolabel)+1];
            wcscpy(m_curstmtlabels[colidx], autolabel);
        }
    }

    m_curstmttext = new SQLWCHAR[stmtlen+1];
    wcscpy(m_curstmttext, stmttext);

    return true;
}

bool pgODBC::ReexecPrevQuery()
{
    wxASSERT (m_curhstmt);

    SQLWCHAR errmsg[ODBCNAMEBUFFLEN];

    m_oerrmsg[0] = '\0';

    SQLFreeHandle( SQL_HANDLE_STMT, m_curhstmt);
    m_curhstmt = NULL;

    SQLRETURN sqlrc = SQLAllocHandle(SQL_HANDLE_STMT, m_odbchdbc, &m_curhstmt);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO)
    {
        wcscpy(m_oerrmsg, _T("Can't allocate statement handle."));
        m_curhstmt = NULL;
        return false;
    }

    sqlrc = SQLPrepare(m_curhstmt, m_curstmttext, SQL_NTS);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO)
    {
        wcscpy(m_oerrmsg, _T("Can't prepare statement."));
        sqlrc = SQLGetDiagRec( SQL_HANDLE_STMT, m_curhstmt, 1, NULL, 0, errmsg, ODBCNAMEBUFFLEN, NULL);
        if ((sqlrc==SQL_SUCCESS || sqlrc==SQL_SUCCESS_WITH_INFO) && errmsg[0])
        {
            wcscat(m_oerrmsg, END_OF_LINE);
            wcscat(m_oerrmsg, errmsg);
        }
        SQLFreeHandle( SQL_HANDLE_STMT, m_curhstmt);
        m_curhstmt = NULL;
        return false;
    }

    sqlrc = SQLExecute(m_curhstmt);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO)
    {
        wcscpy(m_oerrmsg, _T("Can't execute statement."));
        sqlrc = SQLGetDiagRec( SQL_HANDLE_STMT, m_curhstmt, 1, NULL, 0, errmsg, ODBCNAMEBUFFLEN, NULL);
        if ((sqlrc==SQL_SUCCESS || sqlrc==SQL_SUCCESS_WITH_INFO) && errmsg[0])
        {
            wcscat(m_oerrmsg, END_OF_LINE);
            wcscat(m_oerrmsg, errmsg);
        }
        SQLFreeHandle( SQL_HANDLE_STMT, m_curhstmt);
        m_curhstmt = NULL;
        return false;
    }

    return true;
}

SQLLEN pgODBC::GetResultRowsNum()
{
    if (!m_curhstmt)
        return -1;

    SQLLEN rowcount;
    SQLRETURN sqlrc = SQLRowCount(m_curhstmt, &rowcount);
    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO)
        return -1;

    return rowcount;
}

SQLSMALLINT pgODBC::GetResultColType(SQLUSMALLINT colnumber)
{
    wxASSERT (m_curhstmt);

    if (colnumber<1 || colnumber>m_curstmtcolsnum)
        return SQL_UNKNOWN_TYPE;

    return m_curstmttypes[colnumber];
}

SQLWCHAR* pgODBC::GetResultLabel(SQLUSMALLINT colnumber)
{
    wxASSERT (m_curhstmt);

    if (colnumber<1 || colnumber>m_curstmtcolsnum)
        return SQL_UNKNOWN_TYPE;

    return m_curstmtlabels[colnumber];
}

SQLUSMALLINT pgODBC::GetResultColNr(SQLWCHAR* collabel)
{
    wxASSERT (m_curhstmt);

    if (!collabel || !collabel[0])
        return 0;

    SQLSMALLINT colidx;
    for (colidx=0; colidx<m_curstmtcolsnum; colidx++)
        if (!wcscmp(m_curstmtlabels[colidx], collabel))
            return colidx+1;

    return 0;
}

SQLRETURN pgODBC::NextResultRow()
{
    wxASSERT (m_curhstmt);

    SQLRETURN sqlrc = SQLFetch(m_curhstmt);

    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO && sqlrc!=SQL_NO_DATA)
    {
        SQLWCHAR errmsg[ODBCNAMEBUFFLEN];
        wcscpy(m_oerrmsg, _T("Can't fetch next row."));
        sqlrc = SQLGetDiagRec(SQL_HANDLE_STMT, m_curhstmt, 1, NULL, 0, errmsg, ODBCNAMEBUFFLEN, NULL);
        if (errmsg[0])
        {
            wcscat(m_oerrmsg, END_OF_LINE);
            wcscat(m_oerrmsg, errmsg);
        }
    }

    return sqlrc;
}

void pgODBC::ClearCurrentStmtInfo()
{
    if (m_curstmttypes)
    {
        delete[] m_curstmttypes;
        m_curstmttypes = NULL;
    }

    if (m_curstmtlabels)
    {
        SQLSMALLINT colidx;
        for (colidx=0; colidx<m_curstmtcolsnum; colidx++)
            if (m_curstmtlabels[colidx])
                delete[] m_curstmtlabels[colidx];

        delete[] m_curstmtlabels;
        m_curstmtlabels = NULL;
    }

    if (m_curstmttext)
    {
        delete[] m_curstmttext;
        m_curstmttext = NULL;
    }

    m_curstmtcolsnum = 0;
}

// databufflen: Length of the *databuffptr buffer in bytes.
SQLRETURN pgODBC::GetResultData(SQLUSMALLINT colnumber,
                                SQLWCHAR *databuff, size_t bufflen, SQLINTEGER *indptr)
{
    wxASSERT (m_curhstmt!=NULL);

    if (colnumber<=0 || colnumber>m_curstmtcolsnum)
        return SQL_ERROR;

    SQLRETURN sqlrc;
    *indptr = SQL_DATA_AT_EXEC;
    size_t datalen;

    SQLSMALLINT coltype = m_curstmttypes[colnumber-1];
    if (coltype==SQL_CHAR || coltype==SQL_VARCHAR || coltype==SQL_LONGVARCHAR
        || coltype==SQL_WCHAR || coltype==SQL_WVARCHAR || coltype==SQL_WLONGVARCHAR)
    {
        sqlrc = SQLGetData(m_curhstmt, colnumber, SQL_C_WCHAR, databuff, bufflen*sizeof(SQLWCHAR), indptr);
    }
    else if (coltype==SQL_NUMERIC)
    {
        SQL_NUMERIC_STRUCT numericdata;
        sqlrc = SQLGetData(m_curhstmt, colnumber, SQL_C_NUMERIC, &numericdata, 0, NULL);
        if (sqlrc==SQL_SUCCESS || sqlrc==SQL_SUCCESS_WITH_INFO)
        {
            NumericToWChar(numericdata, databuff, bufflen);
            if (wcschr(databuff, _T('.')))
            {
                datalen = wcslen(databuff);
                while (databuff[datalen-1]==_T('.') || databuff[datalen-1]==_T('0'))
                {
                    databuff[datalen-1] = _T('\0');
                    datalen--;
                }
            }
        }
    }
    else if (coltype==SQL_FLOAT || coltype==SQL_REAL || coltype==SQL_DOUBLE)
    {
        double doubleval;
        sqlrc = SQLGetData(m_curhstmt, colnumber, SQL_C_DOUBLE, &doubleval, 0, NULL);
        if (sqlrc==SQL_SUCCESS || sqlrc==SQL_SUCCESS_WITH_INFO)
        {
            swprintf_s(databuff, bufflen, _T("%f"), doubleval);
            if (wcschr(databuff, _T('.')))
            {
                datalen = wcslen(databuff);
                while (databuff[datalen-1]==_T('0'))
                {
                    databuff[datalen-1] = _T('\0');
                    datalen--;
                }
                if (databuff[datalen-1]==_T('.'))
                    databuff[datalen-1] = _T('\0');
            }
        }
    }
    else if (coltype==SQL_INTEGER || coltype==SQL_SMALLINT)
    {
        int intval;
        sqlrc = SQLGetData(m_curhstmt, colnumber, SQL_C_SLONG, &intval, 0, NULL);
        if (sqlrc==SQL_SUCCESS || sqlrc==SQL_SUCCESS_WITH_INFO)
            swprintf_s(databuff, bufflen, _T("%d"), intval);
    }
    else if (coltype==SQL_INTERVAL)
    {
        SQL_INTERVAL_STRUCT intvval;
        sqlrc = SQLGetData(m_curhstmt, colnumber, SQL_ARD_TYPE, &intvval, 0, NULL);
        if (sqlrc==SQL_SUCCESS || sqlrc==SQL_SUCCESS_WITH_INFO)
            IntervalToWChar(intvval, databuff, bufflen);
    }
    else if (coltype==SQL_TIMESTAMP || coltype==SQL_DATETIME)
    {
        SQL_TIMESTAMP_STRUCT tsval;
        sqlrc = SQLGetData(m_curhstmt, colnumber, SQL_C_TYPE_TIMESTAMP, &tsval, 0, NULL);
        if (sqlrc==SQL_SUCCESS || sqlrc==SQL_SUCCESS_WITH_INFO)
        {
            swprintf_s(databuff, bufflen, _T("%d/%d/%d %d:%d:%d.%d"), tsval.year, tsval.month, tsval.day,
                tsval.hour, tsval.minute, tsval.second, tsval.fraction);
            if (wcschr(databuff, _T('.')))
            {
                datalen = wcslen(databuff);
                while (databuff[datalen-1]==_T('0'))
                {
                    databuff[datalen-1] = _T('\0');
                    datalen--;
                }
                if (databuff[datalen-1]==_T('.'))
                    databuff[datalen-1] = _T('\0');
            }
        }
    }
    else
    {
        swprintf_s(m_oerrmsg, _T("Unsupported type(%d)."), coltype);
        return SQL_ERROR;
    }

    if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO)
    {
        SQLWCHAR errmsg[ODBCNAMEBUFFLEN];
        wcscpy(m_oerrmsg, _T("Can't fetch data."));
        sqlrc = SQLGetDiagRec(SQL_HANDLE_STMT, m_curhstmt, 1, NULL, 0, errmsg, ODBCNAMEBUFFLEN, NULL);
        if (errmsg[0])
        {
            wcscat(m_oerrmsg, END_OF_LINE);
            wcscat(m_oerrmsg, errmsg);
        }
    }

    return sqlrc;
/*
SQL_UNKNOWN_TYPE://0
*SQL_CHAR://1
*SQL_NUMERIC://2
SQL_DECIMAL://3
*SQL_INTEGER://4
*SQL_SMALLINT://5
*SQL_FLOAT://6
*SQL_REAL://7
*SQL_DOUBLE://8
*SQL_DATETIME://9
*SQL_INTERVAL://10
*SQL_TIMESTAMP://11
*SQL_VARCHAR://12
*SQL_LONGVARCHAR://-1
SQL_BINARY://-2
SQL_VARBINARY://-3
SQL_LONGVARBINARY://-4
SQL_BIGINT://-5
SQL_TINYINT://-6
SQL_BIT://-7
*SQL_WCHAR://-8
*SQL_WVARCHAR://-9
*SQL_WLONGVARCHAR://-10
*/
}

bool pgODBC::CloseCurrentStmt()
{
    if (m_curhstmt==NULL)
        return false;

    ClearCurrentStmtInfo();

    SQLRETURN sqlrc = SQLFreeHandle(SQL_HANDLE_STMT, m_curhstmt);

    if (sqlrc==SQL_SUCCESS || sqlrc==SQL_SUCCESS_WITH_INFO)
    {
        SQLEndTran(SQL_HANDLE_DBC, m_odbchdbc, SQL_ROLLBACK);
        m_curhstmt = NULL;
        return true;
    }

    SQLWCHAR errmsg[ODBCNAMEBUFFLEN];
    wcscpy(m_oerrmsg, _T("Can't close statement."));
    sqlrc = SQLGetDiagRec(SQL_HANDLE_STMT, m_curhstmt, 1, NULL, 0, errmsg, ODBCNAMEBUFFLEN, NULL);
    if (errmsg[0])
    {
        wcscat(m_oerrmsg, END_OF_LINE);
        wcscat(m_oerrmsg, errmsg);
    }

    return false;
}
/*
bool pgODBC::CancelCurrentStmt()
{
    if (!HasStmtRunning())
        return false;

    SQLRETURN sqlrc = SQLCancel(m_curhstmt);

    if (sqlrc==SQL_SUCCESS || sqlrc==SQL_SUCCESS_WITH_INFO)
    {
        SQLFreeHandle(SQL_HANDLE_STMT, m_curhstmt);
        SQLEndTran(SQL_HANDLE_DBC, m_odbchdbc, SQL_ROLLBACK);
        m_curhstmt = NULL;
        return true;
    }

    return false;
}
*/
const SQLWCHAR *pgODBC::GetQuotedName(const SQLWCHAR *objname)
{
    if (!m_connected || !m_idquotechars[0] || !objname || !objname[0])
        return NULL;

    size_t quotecharlen = wcslen(m_idquotechars);
    size_t objnamelen = wcslen(objname);

    bool notneedsquoting = true;
    if (wcschr(objname, _T(' '))!=NULL)
        notneedsquoting = false;

    const SQLWCHAR *nextchr;
    size_t chridx = 0, quotecharidx;

    if (notneedsquoting)
        while (nextchr=wcschr(objname+chridx, m_idquotechars[0]))
        {
            quotecharidx = 1;
            while(m_idquotechars[quotecharidx])
            {
                if (!objname[chridx+quotecharidx])
                    break;
                if (m_idquotechars[quotecharidx]!=objname[chridx+quotecharidx])
                    break;
                quotecharidx++;
            }
            if (m_idquotechars[quotecharidx])
                chridx+=quotecharidx;
            else
            {
                notneedsquoting = false;
                break;
            }
        }

    chridx = 0;
    if (notneedsquoting)
        while (m_idspecchars[chridx])
            if (nextchr=wcschr(objname, m_idspecchars[chridx++]))
            {
                notneedsquoting = false;
                break;
            }

    SQLWCHAR *quotedname;
    if (notneedsquoting)
    {
        quotedname = new SQLWCHAR[objnamelen+1];
        wcscpy(quotedname, objname);
        return quotedname;
    }

    SQLWCHAR *quotedobjname;
    size_t namecharidx = 0, quotednameidx = 0;

    quotedname = new SQLWCHAR[(objnamelen+2)*quotecharlen*2];
    wcscpy(quotedname+quotednameidx, m_idquotechars);
    quotednameidx += quotecharlen;

    while (objname[namecharidx])
    {
        if (objname[namecharidx]!=m_idquotechars[0])
        {
            quotedname[quotednameidx] = objname[namecharidx];
            quotednameidx++;
            namecharidx++;
            continue;
        }

        quotecharidx = 1;
        while (m_idquotechars[quotecharidx])
        {
            if (!objname[namecharidx+quotecharidx])
                break;
            if (m_idquotechars[quotecharidx]!=objname[namecharidx+quotecharidx])
                break;

            quotecharidx++;
        }

        if (m_idquotechars[quotecharidx])
        {
            memcpy(quotedname+quotednameidx, m_idquotechars, sizeof(SQLWCHAR)*quotecharidx);
            quotednameidx += quotecharidx;
        }
        else
        {
            wcscpy(quotedname+quotednameidx, m_idquotechars);
            quotednameidx += quotecharlen;
            wcscpy(quotedname+quotednameidx, m_idquotechars);
            quotednameidx += quotecharlen;
        }

        namecharidx += quotecharidx;
    }

    wcscpy(quotedname+quotednameidx, m_idquotechars);
    quotednameidx += quotecharlen;

    quotedobjname = new SQLWCHAR[wcslen(quotedname)+1];
    wcscpy(quotedobjname, quotedname);
    delete[] quotedname;

    return quotedobjname;
}

const SQLWCHAR *pgODBC::GetQualifiedName(const SQLWCHAR *catname,
                                         const SQLWCHAR *schemname,
                                         const SQLWCHAR *tablename)
{
    if (!m_connected)
        return NULL;

    if (!tablename)
        return NULL;

    const SQLWCHAR *quotedcatname = NULL;
    size_t qualifiedlen = 0;
    if (m_catalogname[0]==_T('Y') && catname)
    {
        quotedcatname = GetQuotedName(catname);
        qualifiedlen = wcslen(quotedcatname) + wcslen(m_catalognameseparator);
    }

    const SQLWCHAR *quotedschemname = NULL;
    if (schemname)
    {
        quotedschemname = GetQuotedName(schemname);
        qualifiedlen += wcslen(quotedschemname);
    }

    const SQLWCHAR *quotedtablename = GetQuotedName(tablename);
    qualifiedlen += wcslen(quotedtablename);
    qualifiedlen++;

    SQLWCHAR *qualifiedname = new SQLWCHAR[qualifiedlen+2];
    size_t qualifiedoffset = 0;
    if (quotedcatname && m_catalogloc==SQL_CL_START)
    {
        wcscpy(qualifiedname, quotedcatname);
        qualifiedoffset = wcslen(quotedcatname);
        wcscpy(qualifiedname + qualifiedoffset, m_catalognameseparator);
        qualifiedoffset += wcslen(m_catalognameseparator);
    }
    if (quotedschemname)
    {
        wcscpy(qualifiedname + qualifiedoffset, quotedschemname);
        qualifiedoffset += wcslen(quotedschemname);
        wcscpy(qualifiedname + qualifiedoffset, _T("."));
        qualifiedoffset++;
    }
    wcscpy(qualifiedname + qualifiedoffset, quotedtablename);
    qualifiedoffset += wcslen(quotedtablename);

    if (m_catalogloc==SQL_CL_END)
    {
        wcscpy(qualifiedname + qualifiedoffset, m_catalognameseparator);
        qualifiedoffset += wcslen(m_catalognameseparator);
        wcscpy(qualifiedname + qualifiedoffset, quotedcatname);
        qualifiedoffset = wcslen(quotedcatname);
    }

    if (quotedcatname)
        delete quotedcatname;
    if (quotedschemname)
        delete quotedschemname;
    delete quotedtablename;

    return qualifiedname;
}

const SQLWCHAR* pgODBC::SQLTypeToName(int sqltype)
{
    switch(sqltype)
    {
    case SQL_UNKNOWN_TYPE://0
        return ODBCTYPENAMES[0];
    case SQL_CHAR://1
        return ODBCTYPENAMES[1];
    case SQL_NUMERIC://2
        return ODBCTYPENAMES[2];
    case SQL_DECIMAL://3
        return ODBCTYPENAMES[3];
    case SQL_INTEGER://4
        return ODBCTYPENAMES[4];
    case SQL_SMALLINT://5
        return ODBCTYPENAMES[5];
    case SQL_FLOAT://6
        return ODBCTYPENAMES[6];
    case SQL_REAL://7
        return ODBCTYPENAMES[7];
    case SQL_DOUBLE://8
        return ODBCTYPENAMES[8];
    case SQL_DATETIME://9
        return ODBCTYPENAMES[9];
    case SQL_INTERVAL://10
        return ODBCTYPENAMES[10];
    case SQL_TIMESTAMP://11
        return ODBCTYPENAMES[11];
    case SQL_VARCHAR://12
        return ODBCTYPENAMES[12];
    case SQL_LONGVARCHAR://-1
        return ODBCTYPENAMES[13];
    case SQL_BINARY://-2
        return ODBCTYPENAMES[14];
    case SQL_VARBINARY://-3
        return ODBCTYPENAMES[15];
    case SQL_LONGVARBINARY://-4
        return ODBCTYPENAMES[16];
    case SQL_BIGINT://-5
        return ODBCTYPENAMES[17];
    case SQL_TINYINT://-6
        return ODBCTYPENAMES[18];
    case SQL_BIT://-7
        return ODBCTYPENAMES[19];
    case SQL_WCHAR://-8
        return ODBCTYPENAMES[20];
    case SQL_WVARCHAR://-9
        return ODBCTYPENAMES[21];
    case SQL_WLONGVARCHAR://-10
        return ODBCTYPENAMES[22];
    default:
        return ODBCTYPENAMES[0];
    }
}

int pgODBC::NumericToWChar(SQL_NUMERIC_STRUCT numericval, SQLWCHAR *strbuff, size_t bufflen)
{
    double numval = 0;
    int validx, lastradix = 1, curval;
    int lsdval, msdval;

    for(validx=0; validx<SQL_MAX_NUMERIC_LEN; validx++)
    {
	    curval = (int) numericval.val[validx];
		lsdval = curval % 16; //Obtain LSD
		msdval = curval / 16; //Obtain MSD

		numval += lastradix * lsdval;
		lastradix = lastradix * 16;
		numval += lastradix * msdval;
		lastradix = lastradix * 16;
	}

    if(numericval.scale)
    {
        long numdivisor = 1;
        for (validx=0; validx< numericval.scale; validx++)	
            numdivisor = numdivisor * 10;

        numval =  numval / numdivisor;
    }

    if (!numericval.sign)
        numval *= -1;

    return swprintf_s(strbuff, bufflen, _T("%f"), numval);
}

int pgODBC::IntervalToWChar(SQL_INTERVAL_STRUCT intvval, SQLWCHAR *strbuff, size_t bufflen)
{
    size_t datalen;

    switch(intvval.interval_type)
    {
    case SQL_IS_YEAR:
    case SQL_IS_MONTH:
    case SQL_IS_YEAR_TO_MONTH:
        return swprintf_s(strbuff, bufflen, intvval.interval_sign==SQL_TRUE?_T("P-%dY%dM"):_T("P%dY%dM"),
                intvval.intval.year_month.year, intvval.intval.year_month.month);
    case SQL_IS_DAY:
    case SQL_IS_HOUR:
    case SQL_IS_MINUTE:
    case SQL_IS_SECOND:
    case SQL_IS_DAY_TO_HOUR:
    case SQL_IS_DAY_TO_MINUTE:
    case SQL_IS_DAY_TO_SECOND:
    case SQL_IS_HOUR_TO_MINUTE:
    case SQL_IS_HOUR_TO_SECOND:
    case SQL_IS_MINUTE_TO_SECOND:
        swprintf_s(strbuff, bufflen, _T("P%dD%dH%dM%d.%d"),
                intvval.intval.day_second.day, intvval.intval.day_second.hour,
                intvval.intval.day_second.minute, intvval.intval.day_second.second,
                intvval.intval.day_second.fraction);

        datalen = wcslen(strbuff);
        while (strbuff[datalen-1]==_T('0'))
        {
            strbuff[datalen-1] = _T('\0');
            datalen--;
        }
        if (strbuff[datalen-1]==_T('.'))
        {
            strbuff[datalen-1] = _T('\0');
            datalen--;
        }
        strbuff[datalen] = _T('S');

        return datalen*sizeof(SQLWCHAR);
    default:
        return -1;
    }
}

#endif
