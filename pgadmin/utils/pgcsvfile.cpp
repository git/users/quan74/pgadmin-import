
#include "pgAdmin3.h"
#include "utils/pgcsvfile.h"

const wxString pgCSVFile::DEFAULTCHARDELIMITER(wxT(","));

const wxRegEx pgCSVFile::m_widthdlmregex(wxT("^([0-9]{1,6}|[Nn][Un][Ll][Ll])?(,([0-9]{1,6}|[Nn][Un][Ll][Ll])?)*(,?)$"));


pgCSVFile::pgCSVFile()
{
    m_fileencoding = wxFONTENCODING_SYSTEM;
    m_fileconv = new wxCSConv(wxFONTENCODING_SYSTEM);
    m_fencisok = false;

    m_rawfile = NULL;
    m_fileisok = false;
    m_ferrmsg = _("Please specify a file.");
    m_settingisok = false;

    m_delimitedbychar = true;
    m_chardlm = DEFAULTCHARDELIMITER;
    m_chardlmvals = m_chardlm.GetData();
    m_widthdlm = NULL;
    m_widthdlmsnum = (size_t)0;
    m_widthdlmisok = false;
    m_quotedby = DEFAULTQUOTEDBY;

    m_firstrowisheader = false;
    m_backslashquote = false;
    m_pvrowsnum = DEFAULTPREVIEWROWSNUM;
    m_forcelinebreak = true;

#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
    m_parentinstrm = NULL;
    m_entriesnum = 0;
#if wxUSE_ZIPSTREAM
    m_zipinstrm = NULL;
    m_zipentrylist = NULL;
#endif
#if wxUSE_TARSTREAM
    m_tarinstrm = NULL;
    m_tarentrylist = NULL;
#endif
    m_curarchentry = NULL;
    m_entryencoding = wxFONTENCODING_SYSTEM;
    m_entryconv = NULL;
#endif

    m_linenumber = (size_t)0;
    m_csvrownr = (size_t)0;

    m_pvbufoffset = (size_t)0;
    m_pvdecoffset = (size_t)0;

    m_pvparsedoffset = (size_t)0;

    m_pvparsedrowsnum = (size_t)0;
    m_pvcsvlines = NULL;
    m_pvparsedcolsnum = NULL;
    m_pvparsedrowvals = NULL;
    m_maxcolsnum = (size_t)0;

    m_prevcolvals = NULL;
    m_prevcolnum = 0;
    m_filelinenr = 0;
    m_csvlinenr = 0;

    m_filesize = 0;
    m_baseoffset = 0;
    m_fileoffset = 0;
    m_charavglen = 0;

    m_settingisok = true;
    //Init();
}

/*
pgCSVFile::pgCSVFile(const wxString fpath, wxFontEncoding fileencoding,
                     const wxString chardlm, const wxChar quotedby)
{
    m_fileencoding = fileencoding;
    m_fileconv = new wxCSConv(fileencoding);
    m_fencisok = false;

    m_delimitedbychar = true;
    m_chardlm = chardlm;
    m_chardlmvals = m_chardlm.GetData();
    m_widthdlm = NULL;
    m_widthdlmsnum = (size_t)0;
    m_widthdlmisok = false;
    m_firstrowisheader = false;
    m_backslashquote = false;
    m_quotedby = quotedby;
    m_pvrowsnum = DEFAULTPREVIEWROWSNUM;
    m_forcelinebreak = true;

    Init();

    m_settingisok = false;
    ValidateSetting();

    m_fileisok = false;
    m_ferrmsg = _("Please specify a file.");
    m_filesize = 0;

    //Open(fpath);
}
*/
pgCSVFile::~pgCSVFile()
{
    Close();

    if (m_widthdlm)
        delete []m_widthdlm;

    if (m_fileconv)
        delete m_fileconv;

    if (m_prevcolnum)
    {
        size_t tmpcolidx;
        for (tmpcolidx=(size_t)0; tmpcolidx<m_prevcolnum; tmpcolidx++)
            if (m_prevcolvals[tmpcolidx])
                delete m_prevcolvals[tmpcolidx];

        delete[] m_prevcolvals;

        m_prevcolvals = NULL;
        m_prevcolnum = 0;
    }

}
/*
void pgCSVFile::Init()
{
    m_linenumber = (size_t)0;
    m_csvrownr = (size_t)0;

    m_pvbufoffset = (size_t)0;
    m_pvdecoffset = (size_t)0;

    m_pvparsedoffset = (size_t)0;

    m_pvparsedrowsnum = (size_t)0;
    m_pvcsvlines = NULL;
    m_pvparsedcolsnum = NULL;
    m_pvparsedrowvals = NULL;
    m_maxcolsnum = (size_t)0;

    m_prevcolvals = NULL;
    m_prevcolnum = 0;
    m_filelinenr = 0;
    m_csvlinenr = 0;

    m_filesize = 0;
    m_settingisok = true;
}
*/

bool pgCSVFile::Open(const wxString fpath, bool isarch)
{
    Close();

#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
#else
    if (isarch)
    {
        m_fileisok = false;
        m_ferrmsg = _("Can't support archive file.");
        return false;
    }
#endif

    m_filepath = fpath;

    if (fpath.IsEmpty())
    {
        m_fileisok = false;
        m_ferrmsg = _("Please specify a file.");
        return false;
    }

    if (!wxFile::Exists(fpath))
    {
        m_fileisok = false;
        m_ferrmsg = _("File not exists.");
        return false;
    }

    if (!wxFile::Access(fpath, wxFile::read))
    {
        m_fileisok = false;
        m_ferrmsg = _("Failed to open the file.");
        return false;
    }

#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
    if (isarch)
    {
        int archfmt;

        ClearEntryList();

        wxFFileInputStream *ffinstrm = new wxFFileInputStream(fpath, wxT("rb"));
        if (!ffinstrm->IsOk())
        {
            m_fileisok = false;
            m_ferrmsg = _("Failed to open the file.");
            delete ffinstrm;
            return false;
        }
        if (!ffinstrm->IsSeekable())
        {
            m_fileisok = false;
            m_ferrmsg = _("Can't handle non-seekable file.");
            delete ffinstrm;
            return false;
        }

        if (fpath.EndsWith(wxT(".zip")))
        {
            archfmt = TestStream(*ffinstrm, ARCHZIP, *m_fileconv);
            if (archfmt==ARCHUNKNOWN)
                archfmt = TestStream(*ffinstrm, ARCHGZIP | ARCHBZIP2 | ARCHTAR, *m_fileconv);
        }
        else if (fpath.EndsWith(wxT(".gz")) || fpath.EndsWith(wxT(".bz2")))
        {
            archfmt = TestStream(*ffinstrm, ARCHGZIP | ARCHBZIP2, *m_fileconv);
            if (archfmt==ARCHUNKNOWN)
                archfmt = TestStream(*ffinstrm, ARCHZIP | ARCHTAR, *m_fileconv);
        }
        else if (fpath.EndsWith(wxT(".tar")))
        {
            archfmt = TestStream(*ffinstrm, ARCHTAR, *m_fileconv);
            if (archfmt==ARCHUNKNOWN)
                archfmt = TestStream(*ffinstrm, ARCHZIP | ARCHGZIP | ARCHBZIP2, *m_fileconv);
        }
        else
            archfmt = TestStream(*ffinstrm, ARCHUNKNOWN, *m_fileconv);

        if (archfmt==ARCHUNKNOWN)
        {
            m_fileisok = false;
            m_ferrmsg = _("Unsupported archive format.");
            delete ffinstrm;
            return false;
        }

        ffinstrm->SeekI(0);

        if (!m_entryconv)
            m_entryconv = new wxCSConv(m_entryencoding);

#if wxUSE_ZLIB
        if (archfmt&ARCHGZIP || archfmt&ARCHBZIP2)
        {
            m_parentinstrm = new wxZlibInputStream(ffinstrm);
            m_parentinstrm->SeekI(0);
#if wxUSE_TARSTREAM
            if (archfmt&ARCHTAR)
                m_tarinstrm = new wxTarInputStream(*m_parentinstrm, *m_fileconv);
#endif
#if wxUSE_ZIPSTREAM
            if (archfmt&ARCHZIP)
                m_zipinstrm = new wxZipInputStream(*m_parentinstrm, *m_fileconv);
#endif
        }
        else
        {
#endif // wxUSE_ZLIB

            m_parentinstrm = ffinstrm;
#if wxUSE_TARSTREAM
            if (archfmt&ARCHTAR)
                m_tarinstrm = new wxTarInputStream(*m_parentinstrm, *m_fileconv);
#endif

#if wxUSE_ZIPSTREAM
            if (archfmt&ARCHZIP)
                m_zipinstrm = new wxZipInputStream(*m_parentinstrm, *m_fileconv);
#endif

#if wxUSE_ZLIB
        }
#endif

        LoadEntryList();

        m_fileisok = false;
        m_ferrmsg = _("Please specify a entry.");
        return true;
    }
    else
    {
#endif

    m_fileisok = true;
    m_ferrmsg = wxEmptyString;

    if (!m_rawfile)
        m_rawfile = new wxFile();

    if (!m_rawfile->Open(m_filepath, wxFile::read))
    {
        m_fileisok = false;
        m_ferrmsg = _("Failed to open the file.");
        return false;
    }

    m_filesize = m_rawfile->Length();
    if (!m_filesize)
    {
        m_fileisok = false;
        m_ferrmsg = _("The file contains nothing.");
        return false;
    }

    m_fencisok = true;

    while (m_pvbufoffset<INITPREVIEWSZ)
        if (GenMoreFilePreview(NULL)==wxInvalidOffset)
            break;

    return true;
#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
    }
#endif
}

bool pgCSVFile::IsOpened()
{
    if (m_rawfile)
        return m_rawfile->IsOpened();

#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
    if (m_parentinstrm)
        return true;
#endif

    return false;
}

#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
int pgCSVFile::TestStream(wxInputStream& testinstrm, int strmtype, wxMBConv& strmconv)
{

#if wxUSE_ZLIB
    if (strmtype==ARCHUNKNOWN || strmtype&ARCHGZIP || strmtype&ARCHBZIP2)
    {
        wxZlibInputStream zlibfinstrm(testinstrm);

#if wxUSE_TARSTREAM
        wxTarInputStream tarfinstrm(zlibfinstrm, strmconv);
        wxTarEntry* ftarentry = tarfinstrm.GetNextEntry();
        if (ftarentry)
        {
            delete ftarentry;
            return strmtype | ARCHTAR;
        }
#endif

#if wxUSE_ZIPSTREAM
        wxZipInputStream zipfinstrm(zlibfinstrm, strmconv);
        if (zipfinstrm.GetTotalEntries())
            return strmtype | ARCHZIP;
#endif

    }
#endif // wxUSE_ZLIB

#if wxUSE_TARSTREAM
    if (strmtype==ARCHUNKNOWN || strmtype&ARCHTAR)
    {
        wxTarInputStream tarfinstrm(testinstrm, strmconv);
        wxTarEntry* ftarentry = tarfinstrm.GetNextEntry();
        if (ftarentry)
        {
            delete ftarentry;
            return ARCHTAR;
        }
    }
#endif // wxUSE_TARSTREAM

#if wxUSE_ZIPSTREAM
    if (strmtype==ARCHUNKNOWN || strmtype&ARCHZIP)
    {
        wxZipInputStream zipfinstrm(testinstrm, strmconv);
        if (zipfinstrm.GetTotalEntries())
            return ARCHZIP;
    }
#endif //wxUSE_ZIPSTREAM

    return ARCHUNKNOWN;
}

void pgCSVFile::LoadEntryList(const wxString* origentryname)
{
    if (!m_parentinstrm)
        return;

    size_t entriesnum;
#if wxUSE_TARSTREAM
    if (m_tarinstrm)
    {
        entriesnum = 0;
        wxTarEntry *tmptarentry;
        m_tarentrylist = new wxTarEntry*[MAXENTRYNUM+1];
        while ((tmptarentry=m_tarinstrm->GetNextEntry()))
        {
            if (tmptarentry->IsDir())
            {
                delete tmptarentry;
                continue;
            }
            m_tarentrylist[entriesnum] = tmptarentry;
            if (origentryname && !origentryname->Cmp(tmptarentry->GetInternalName()))
            {
                m_curarchentry = tmptarentry;
                origentryname = NULL;
                m_filesize = m_curarchentry->GetSize();
            }
            entriesnum++;
            if (entriesnum>=MAXENTRYNUM)
                break;
        }

        if (entriesnum)
            m_tarentrylist[entriesnum] = NULL;
        else
        {
            delete m_tarentrylist;
            m_tarentrylist = NULL;
        }
        m_entriesnum = entriesnum;

        return;
    }
#endif

#if wxUSE_ZIPSTREAM
    if (m_zipinstrm)
    {
        entriesnum = m_zipinstrm->GetTotalEntries();
        if (!entriesnum)
        {
            m_entriesnum = 0;
            return;
        }

        if (entriesnum<=MAXENTRYNUM)
            m_zipentrylist = new wxZipEntry*[entriesnum+1];
        else
            m_zipentrylist = new wxZipEntry*[MAXENTRYNUM+1];

        entriesnum = 0;
        wxZipEntry *tmpzipentry;
        while ((tmpzipentry=m_zipinstrm->GetNextEntry()))
        {
            if (tmpzipentry->IsDir())
            {
                delete tmpzipentry;
                continue;
            }
            m_zipentrylist[entriesnum] = tmpzipentry;
            if (origentryname && !origentryname->Cmp(tmpzipentry->GetInternalName()))
            {
                m_curarchentry = tmpzipentry;
                origentryname = NULL;
            }
            entriesnum++;
            if (entriesnum>=MAXENTRYNUM)
                break;
        }

        if (entriesnum)
            m_zipentrylist[entriesnum] = NULL;
        else
        {
            delete m_zipentrylist;
            m_zipentrylist = NULL;
        }
        m_entriesnum = entriesnum;

        return;
    }
#endif
}

void pgCSVFile::ClearEntryList()
{
    CloseArchiveEntry();

    m_entriesnum = 0;

#if wxUSE_ZIPSTREAM
    if (m_zipinstrm)
    {
        wxZipEntry **tmpzipentry = m_zipentrylist;
        while (tmpzipentry&&*tmpzipentry)
        {
            delete *tmpzipentry;
            tmpzipentry++;
        }
        delete []m_zipentrylist;
        m_zipentrylist = NULL;
    }
#endif // wxUSE_ZIPSTREAM

#if wxUSE_TARSTREAM
    if (m_tarinstrm)
    {
        wxTarEntry **tmptarentry = m_tarentrylist;
        while (tmptarentry&&*tmptarentry)
        {
            delete *tmptarentry;
            tmptarentry++;
        }
        delete []m_tarentrylist;
        m_tarentrylist = NULL;
    }
#endif // wxUSE_TARSTREAM
}

wxString pgCSVFile::GetEntryName(size_t entryidx)
{
    if (entryidx>=m_entriesnum)
        return *wxEmptyString;

#if wxUSE_ZIPSTREAM
    if (m_zipentrylist)
        return m_zipentrylist[entryidx]->GetInternalName();
#endif

#if wxUSE_TARSTREAM
    if (m_tarentrylist)
        return m_tarentrylist[entryidx]->GetInternalName();
#endif

    return *wxEmptyString;
}

void* pgCSVFile::GetEntry(size_t entryidx)
{
    if (entryidx>=m_entriesnum)
        return NULL;

#if wxUSE_ZIPSTREAM
    if (m_zipentrylist)
        return m_zipentrylist[entryidx];
#endif

#if wxUSE_TARSTREAM
    if (m_tarentrylist)
        return m_tarentrylist[entryidx];
#endif

    return NULL;
}

bool pgCSVFile::OpenArchiveEntry(size_t entryidx)
{
    if (entryidx>=m_entriesnum)
        return false;

    bool openres = false;
#if wxUSE_ZIPSTREAM
    m_curarchentry = NULL;
    if (m_zipentrylist)
    {
        openres = m_zipinstrm->OpenEntry(*m_zipentrylist[entryidx]);
        if (openres)
            m_curarchentry = m_zipentrylist[entryidx];
    }
#endif

#if wxUSE_TARSTREAM
    m_curarchentry = NULL;
    if (m_tarentrylist)
    {
        openres = m_tarinstrm->OpenEntry(*m_tarentrylist[entryidx]);
        if (openres)
            m_curarchentry = m_tarentrylist[entryidx];
    }
#endif

    ClearPreview();

    m_fileisok = openres;
    if (openres)
    {
        m_filesize = m_curarchentry->GetSize();

        m_fencisok = true;
        while (m_pvbufoffset<INITPREVIEWSZ)
            if (GenMoreFilePreview(NULL)==wxInvalidOffset)
                break;
    }
    else
    {
        m_filesize = 0;
        m_ferrmsg = _("Can't open selected entry.");
    }
    //m_fileoffset = 0;

    return openres;
}

bool pgCSVFile::OpenArchiveEntry(void* archentry)
{
    bool openres = false;
    m_curarchentry = NULL;

#if wxUSE_ZIPSTREAM
    if (m_zipinstrm)
    {
        if (archentry)
        {
            openres = m_zipinstrm->OpenEntry(*(wxZipEntry*)archentry);
            if (openres)
                m_curarchentry = (wxZipEntry*)archentry;
        }
        else
            m_zipinstrm->CloseEntry();

    }
#endif

#if wxUSE_TARSTREAM
    if (m_tarinstrm)
    {
        if (archentry)
        {
            openres = m_tarinstrm->OpenEntry(*(wxTarEntry*)archentry);
            if (openres)
                m_curarchentry = (wxTarEntry*)archentry;
        }
        else
            m_tarinstrm->CloseEntry();
    }
#endif

    ClearPreview();

    m_fileisok = openres;
    if (openres)
    {
        m_filesize = m_curarchentry->GetSize();

        m_fencisok = true;
        while (m_pvbufoffset<INITPREVIEWSZ)
            if (GenMoreFilePreview(NULL)==wxInvalidOffset)
                break;
    }
    else if (archentry)
    {
        m_filesize = 0;
        m_ferrmsg = _("Can't open selected entry.");
    }
    else
    {
        m_filesize = 0;
        m_ferrmsg = _("Please specify a entry.");
    }
    //m_fileoffset = 0;

    return openres;
}

bool pgCSVFile::CloseArchiveEntry()
{
    m_curarchentry = NULL;
    m_filesize = 0;
    //m_fileoffset = 0;
    m_fileisok = false;
    m_ferrmsg = _("Please specify a entry.");

    ClearPreview();

#if wxUSE_ZIPSTREAM
    if (m_zipinstrm)
        return m_zipinstrm->CloseEntry();
#endif

#if wxUSE_ZIPSTREAM
    if (m_tarinstrm)
        return m_tarinstrm->CloseEntry();
#endif

    return false;
}

wxString pgCSVFile::GetCurrentEntryName()
{
    if (m_curarchentry)
        return m_curarchentry->GetInternalName();

    return *wxEmptyString;
}

#endif

void pgCSVFile::Close()
{
    ClearPreview();

    if (m_rawfile)
    {
        if (m_rawfile->IsOpened())
            m_rawfile->Close();

        delete m_rawfile;
        m_rawfile = NULL;
    }

#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
    ClearEntryList();
#if wxUSE_ZIPSTREAM
    if (m_zipinstrm)
    {
        delete m_zipinstrm;
        m_zipinstrm = NULL;
    }
#endif // wxUSE_ZIPSTREAM

#if wxUSE_TARSTREAM
    if (m_tarinstrm)
    {
        delete m_tarinstrm;
        m_tarinstrm = NULL;
    }
#endif // wxUSE_TARSTREAM
    if (m_entryconv)
    {
        delete m_entryconv;
        m_entryconv = NULL;
    }

    if (m_parentinstrm)
    {
        delete m_parentinstrm;
        m_parentinstrm = NULL;
    }
#endif

    m_filesize = 0;
    //m_fileoffset = 0;

    m_fileisok = false;
    m_ferrmsg = _("File closed.");
}

wxFileOffset pgCSVFile::TellI()
{
#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
#if wxUSE_ZIPSTREAM
    if (m_zipinstrm)
        return m_zipinstrm->TellI();
#endif

#if wxUSE_TARSTREAM
    if (m_tarinstrm)
        return m_tarinstrm->TellI();
#endif
#endif

    if (m_rawfile)
        return m_rawfile->Tell();

    return wxInvalidOffset;
}

bool pgCSVFile::Eof()
{
#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
#if wxUSE_ZIPSTREAM
    if (m_zipinstrm)
        return m_zipinstrm->Eof();
#endif

#if wxUSE_TARSTREAM
    if (m_tarinstrm)
        return m_tarinstrm->Eof();
#endif
#endif

    if (m_rawfile)
        return m_rawfile->Eof();

    return true;
}

void pgCSVFile::SetFileEncoding(wxFontEncoding fileencoding)
{
    if (m_fileencoding==fileencoding)
        return;

    delete m_fileconv;
    m_fileencoding = fileencoding;
    m_fileconv = new wxCSConv(fileencoding);

    ClearFilePreview();

#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
    if (m_parentinstrm)
    {
        m_parentinstrm->SeekI(0);
#if wxUSE_ZIPSTREAM
        if (m_zipinstrm)
        {
            delete m_zipinstrm;
            m_zipinstrm = new wxZipInputStream(*m_parentinstrm, *m_fileconv);
        }
#endif

#if wxUSE_TARSTREAM
        if (m_tarinstrm)
        {
            delete m_tarinstrm;
            m_tarinstrm = new wxTarInputStream(*m_parentinstrm, *m_fileconv);
        }
#endif

        wxString *preventryname = NULL;
        if (m_curarchentry)
            preventryname = new wxString(m_curarchentry->GetInternalName());
        ClearEntryList();
        LoadEntryList(preventryname);
        if (preventryname)
            delete preventryname;

        if (m_curarchentry)
            OpenArchiveEntry(m_curarchentry);
        else
        {
            m_fileisok = false;
            m_ferrmsg = _("Please specify a entry.");
            return;
        }

    }
#endif

    if (m_rawfile)
    {
        m_fencisok = true;

        while (m_fileisok && m_pvbufoffset<INITPREVIEWSZ)
            if (GenMoreFilePreview(NULL)==wxInvalidOffset)
                break;
    }
}

#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
void pgCSVFile::SetEntryEncoding(wxFontEncoding entryencoding)
{
    if (!m_entryconv)
    {
        m_entryencoding = entryencoding;
        m_entryconv = new wxCSConv(m_entryencoding);
        return;
    }
    if (m_entryencoding==entryencoding)
        return;

    ClearFilePreview();

    delete m_entryconv;
    m_entryencoding = entryencoding;
    m_entryconv = new wxCSConv(m_entryencoding);

    if (!m_curarchentry)
        return;

    m_fencisok = true;
    while (m_pvbufoffset<INITPREVIEWSZ)
        if (GenMoreFilePreview(NULL)==wxInvalidOffset)
            break;
}
#endif

void pgCSVFile::SetDelimitedByChar(bool delimitedbychar)
{
    if ((m_delimitedbychar&&delimitedbychar) || (!m_delimitedbychar&&!delimitedbychar))
        return;

    m_delimitedbychar = delimitedbychar;

    ValidateSetting();

    ClearCSVPreview();
    GenCSVPreview();
}

void pgCSVFile::SetCharDelimiter(wxString chardlm)
{
    if (!m_chardlm.Cmp(chardlm))
        return;

    m_chardlm = chardlm;
    m_chardlmvals = m_chardlm.GetData();

    ValidateSetting();

    if (!m_delimitedbychar)
        return;

    ClearCSVPreview();
    GenCSVPreview();
}

const wxString pgCSVFile::GetWidthDelimiterStr()
{
    if (!m_widthdlm)
        return wxEmptyString;

    wxString widthdlm;
    size_t dlmidx;
    for (dlmidx=0; dlmidx<m_widthdlmsnum; dlmidx++)
    {
        if (dlmidx)
            widthdlm << wxT(", ");
        if (m_widthdlm[dlmidx]==NULLWIDTHDELIMITER)
            widthdlm << wxT("NULL");
        else
            widthdlm << m_widthdlm[dlmidx];
    }

    return widthdlm;
}

bool pgCSVFile::SetWidthDelimiter(wxString widthdelimiter)
{
    if (m_widthdlm)
    {
        delete []m_widthdlm;
        m_widthdlm = NULL;
    }

    if (widthdelimiter.IsEmpty())
    {
        m_widthdlm = NULL;
        m_widthdlmsnum = (size_t)0;
        m_widthdlmisok = true;
        return true;
    }

    if (!m_widthdlmregex.Matches(widthdelimiter))
    {
        m_widthdlmisok = false;
        m_ferrmsg = _("Invalid width delimiter.");
        return false;
    }

    long tkval;

    int c_delimiter = 0;
    int si=0, ei;
    for (si=0, ei=widthdelimiter.Length(); si<ei; si++)
        if (widthdelimiter.GetChar(si)==wxT(','))
            c_delimiter++;

    m_widthdlmsnum = c_delimiter + 1;

    m_widthdlm = new int[m_widthdlmsnum];

    int commaPos, arrIdx = 0;
    wxString wdtk;
    do
    {
        commaPos = widthdelimiter.Find(wxT(','));
        if (commaPos==wxNOT_FOUND)
        {
            wdtk = widthdelimiter.Trim().Trim(false);
            if (!wdtk.CmpNoCase(wxT("NULL")))
                m_widthdlm[arrIdx] = NULLWIDTHDELIMITER;
            else
            {
                if (wdtk.ToLong(&tkval))
                    m_widthdlm[arrIdx] = (int)tkval;
                else
                    m_widthdlm[arrIdx] = 0;
            }

            break;
        }

        if (commaPos)
        {
            wdtk = widthdelimiter.Mid(0, commaPos).Trim().Trim(false);
            if (!wdtk.CmpNoCase(wxT("NULL")))
                m_widthdlm[arrIdx++] = NULLWIDTHDELIMITER;
            else
            {
                if (wdtk.ToLong(&tkval))
                   m_widthdlm[arrIdx++] = (int)tkval;
                else
                    m_widthdlm[arrIdx++] = 0;
            }

            widthdelimiter = widthdelimiter.Mid(commaPos+1);
        }
        else
        {
            m_widthdlm[arrIdx++] = 0;
            widthdelimiter = widthdelimiter.Mid(1);
        }

        if (widthdelimiter.IsEmpty())
        {
            m_widthdlm[arrIdx] = 0;
            break;
        }
    } while(true);

    m_widthdlmisok = true;

    ValidateSetting();

    if (m_delimitedbychar)
        return m_settingisok;

    ClearCSVPreview();
    GenCSVPreview();

    return true;
}

void pgCSVFile::SetFirstRowIsHeader(bool firstrowisheader)
{
    if ((m_firstrowisheader && firstrowisheader) || (!m_firstrowisheader && !firstrowisheader))
        return;

    m_firstrowisheader = firstrowisheader;

    GenCSVPreview();
    GenHeader();
}

void pgCSVFile::SetBackSlashQuote(bool backslashquote)
{
    if ((m_backslashquote && backslashquote) || (!m_backslashquote && !backslashquote))
        return;

    m_backslashquote = backslashquote;

    ValidateSetting();

    ClearCSVPreview();
    GenCSVPreview();
}

void pgCSVFile::SetQuotedBy(wxChar quotedby)
{
    if (m_quotedby==quotedby)
        return;

    m_quotedby = quotedby;

    if (!m_fileisok)
        return;

    ValidateSetting();

    if (!m_delimitedbychar)
        return;

    ClearCSVPreview();
    GenCSVPreview();
}

void pgCSVFile::SetForceLineBreak(bool forcelinebreak)
{
    if ((m_forcelinebreak && forcelinebreak) || (!m_forcelinebreak && !forcelinebreak))
        return;

    m_forcelinebreak = forcelinebreak;

    ClearCSVPreview();
    GenCSVPreview();
}

void pgCSVFile::SetPreviewRowsNum(const size_t rowsnum)
{
    wxASSERT(rowsnum>=0);

    if (rowsnum<MINPREVIEWROWSNUM)
        m_pvrowsnum = MINPREVIEWROWSNUM;
    else if (rowsnum>MAXPREVIEWROWSNUM)
        m_pvrowsnum = MAXPREVIEWROWSNUM;
    else
        m_pvrowsnum = rowsnum;

    GenCSVPreview(m_pvparsedrowsnum==(size_t)0);
}

wxFileOffset pgCSVFile::GenMoreFilePreview(wxString *pvstr)
{
    size_t readlen = (size_t)0, buflen = (size_t)0, convlen, pvcurbufsz;
    char readbuf[READBUFSZ];

    pvcurbufsz = m_pvmembuf.GetDataLen();

    char* bufdata;
    if (pvcurbufsz>m_pvbufoffset)
    {
        bufdata = (char*)m_pvmembuf.GetData();
        if (pvcurbufsz-m_pvbufoffset>=READBLKSZ)
        {
            memcpy(readbuf+MAXWORDSZ, bufdata + m_pvbufoffset, READBLKSZ);
            buflen = READBLKSZ;
        }
        else
        {
            memcpy(readbuf+MAXWORDSZ, bufdata + m_pvbufoffset, pvcurbufsz-m_pvbufoffset);

            if (Eof())
                buflen = pvcurbufsz-m_pvbufoffset;
            else
            {
                // should never happen
                /*if (m_rawfile)
                    readlen = m_rawfile->Read(readbuf+MAXWORDSZ+pvcurbufsz-m_pvbufoffset,
                        READBLKSZ-(pvcurbufsz-m_pvbufoffset));
                if (readlen!=wxInvalidOffset)
                {
                    m_pvmembuf.AppendData(readbuf+MAXWORDSZ+pvcurbufsz-m_pvbufoffset, readlen);
                    buflen = readlen + pvcurbufsz-m_pvbufoffset;
                }*/
            }
        }
    }
    else
    {
        if (Eof())
            return wxInvalidOffset;

#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
#if wxUSE_ZIPSTREAM
        if (m_zipinstrm)
        {
            m_zipinstrm->Read(readbuf + MAXWORDSZ, READBLKSZ);
            readlen = m_zipinstrm->LastRead();
        }
#endif
#if wxUSE_TARSTREAM
        if (m_tarinstrm)
        {
            m_tarinstrm->Read(readbuf + MAXWORDSZ, READBLKSZ);
            readlen = m_tarinstrm->LastRead();
        }
#endif
#endif
        if (m_rawfile)
            readlen = m_rawfile->Read(readbuf + MAXWORDSZ, READBLKSZ);

        m_pvmembuf.AppendData(readbuf + MAXWORDSZ, readlen);
        buflen = readlen;
        bufdata = (char*)m_pvmembuf.GetData();
    }
    memset(readbuf, 0, MAXWORDSZ);
    memset(readbuf + MAXWORDSZ + buflen, 0, READBUFSZ - MAXWORDSZ - buflen);

    wxString *genstr = pvstr;
    if (!genstr)
        genstr = new wxString;

    if (buflen)
    {
        readlen = buflen;

        int leftbyte = m_pvbufoffset - m_pvdecoffset;
        if (leftbyte)
            memcpy(readbuf+MAXWORDSZ-leftbyte, bufdata + m_pvdecoffset, leftbyte);

        wxCSConv *txtconv = NULL;
#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
#if wxUSE_ZIPSTREAM
        if (m_zipinstrm)
            txtconv = m_entryconv;
#endif
#if wxUSE_TARSTREAM
        if (m_tarinstrm)
            txtconv = m_entryconv;
#endif
#endif
        if (m_rawfile)
            txtconv = m_fileconv;

        convlen = MB2str(txtconv, genstr, readbuf, MAXWORDSZ-leftbyte, readlen);
        if (convlen == wxCONV_FAILED)
        {
            m_fencisok = false;
            m_ferrmsg = _("Can't convert specified encoding to Unicode.");

            return wxInvalidOffset;
        }

        m_pvdecoffset += readlen + leftbyte;
        m_pvbufoffset += buflen;
        m_pvfilecont.Append(*genstr);
    }

    if (!pvstr)
        delete genstr;

    return (wxFileOffset)buflen;
}

void pgCSVFile::ReparseFilePreview()
{
    ClearFilePreview();

    if (m_fileisok)
        while (m_pvdecoffset<INITPREVIEWSZ)
            if (GenMoreFilePreview(NULL)==wxInvalidOffset)
                break;
}

size_t pgCSVFile::GenCSVPreview(const bool isinit)
{
    if (!m_fileisok || !m_fencisok || !m_settingisok)
        return wxInvalidOffset;

    if (!isinit && m_pvparsedrowsnum>=(m_firstrowisheader?m_pvrowsnum+1:m_pvrowsnum))
        return m_pvparsedrowsnum;

    size_t pvrowsnum;
    if (m_firstrowisheader)
        pvrowsnum = m_pvrowsnum + 1;
    else
        pvrowsnum = m_pvrowsnum;

    if (m_pvfilecont.IsEmpty())
        if (GenMoreFilePreview(NULL)==wxInvalidOffset)
            return wxInvalidOffset;

    const wxChar *filedata = m_pvfilecont.GetData();
    wxFileOffset parsedrs;
    size_t curcolnr, parsedoffset = m_pvparsedoffset, filelinenum;
    bool filehasmore = !Eof(), incolparsing, inquotedblk;
    size_t *tmpcolsnums;
    wxString *fileline, **tmpcsvlines, **parsedvals, ***tmpcolsvals;
    while (m_pvparsedrowsnum<pvrowsnum)
    {
        curcolnr = (size_t)0;
        filelinenum = (size_t)0;
        incolparsing = false;
        inquotedblk = false;
        parsedvals = NULL;
        fileline = new wxString;

        while (true)
        {
            if (!filedata[parsedoffset])
            {
                if (isinit && m_pvparsedrowsnum>(m_firstrowisheader?(size_t)1:(size_t)0))
                {
                    parsedrs = wxInvalidOffset;
                    break;
                }
                if (GenMoreFilePreview(NULL)==wxInvalidOffset)
                {
                    GenHeader();

                    if (isinit)
                        m_pvrowsnum = m_firstrowisheader?m_pvparsedrowsnum-1:m_pvparsedrowsnum;

                    return m_pvparsedrowsnum;
                }
                filedata = m_pvfilecont.GetData();
                filehasmore = !Eof();
            }
            if (m_delimitedbychar)
            {
                parsedvals = ParseLine(filedata, &parsedoffset, filehasmore, parsedvals, &curcolnr,
                        &incolparsing, &inquotedblk, &parsedrs, &filelinenum, fileline);
            }
            else
            {
                parsedvals = ParseLine(filedata, &parsedoffset, filehasmore,
                        parsedvals, &curcolnr, &parsedrs, fileline);
            }
            if (parsedrs==wxInvalidOffset)
            {
                if (isinit && m_pvparsedrowsnum>(m_firstrowisheader?(size_t)1:(size_t)0))
                    break;
                else if (GenMoreFilePreview(NULL)==wxInvalidOffset)
                    break;
                filedata = m_pvfilecont.GetData();
            }
            else
                break;
        }
        if (parsedrs==wxInvalidOffset)
        {
            size_t tmpcolindx;
            for (tmpcolindx=(size_t)0; tmpcolindx<curcolnr; tmpcolindx++)
                if (parsedvals[tmpcolindx])
                    delete parsedvals[tmpcolindx];
            if (curcolnr)
                delete[] parsedvals;
            delete fileline;
            break;
        }

        if (m_pvparsedrowsnum)
        {
            tmpcolsnums = m_pvparsedcolsnum;
            m_pvparsedcolsnum = new size_t[m_pvparsedrowsnum+1];
            memcpy(m_pvparsedcolsnum, tmpcolsnums, sizeof(tmpcolsnums)*m_pvparsedrowsnum);
            delete[] tmpcolsnums;

            tmpcolsvals = m_pvparsedrowvals;
            m_pvparsedrowvals = new wxString **[m_pvparsedrowsnum+1];
            memcpy(m_pvparsedrowvals, tmpcolsvals, sizeof(tmpcolsvals)*m_pvparsedrowsnum);
            delete[] tmpcolsvals;

            tmpcsvlines = m_pvcsvlines;
            m_pvcsvlines = new wxString *[m_pvparsedrowsnum+1];
            memcpy(m_pvcsvlines, tmpcsvlines, sizeof(tmpcsvlines)*m_pvparsedrowsnum);
            delete[] tmpcsvlines;
        }
        else
        {
            m_pvcsvlines = new wxString *[1];
            m_pvparsedcolsnum = new size_t[1];
            m_pvparsedrowvals = new wxString **[1];
        }
        m_pvcsvlines[m_pvparsedrowsnum] = fileline;
        if (m_delimitedbychar)
            m_pvparsedcolsnum[m_pvparsedrowsnum] = curcolnr + 1;
        else
            m_pvparsedcolsnum[m_pvparsedrowsnum] = m_widthdlmsnum;
        m_pvparsedrowvals[m_pvparsedrowsnum] = parsedvals;
        m_pvparsedrowsnum++;
        m_pvparsedoffset = parsedoffset;
        if (m_delimitedbychar && m_maxcolsnum<curcolnr + 1)
            m_maxcolsnum = curcolnr + 1;
    }

    if (!m_delimitedbychar)
        m_maxcolsnum = m_widthdlmsnum;

    GenHeader();

    if (isinit)
        m_pvrowsnum = m_firstrowisheader?m_pvparsedrowsnum-1:m_pvparsedrowsnum;

    return m_pvparsedrowsnum;
}

void pgCSVFile::ClearPreview()
{
    m_pvmembuf.SetDataLen(0);

    m_pvbufoffset = (size_t)0;
    m_pvdecoffset = (size_t)0;

    ClearFilePreview();
}

void pgCSVFile::ClearFilePreview()
{
    m_pvfilecont.Clear();
    m_pvdecoffset = (size_t)0;
    m_pvbufoffset = (size_t)0;

    ClearCSVPreview();
}

void pgCSVFile::ClearCSVPreview()
{
    if (m_pvparsedrowsnum)
    {
        size_t pvrowidx, pvcolidx;

        m_colheaders.Clear();

        for (pvrowidx=(size_t)0; pvrowidx<m_pvparsedrowsnum; pvrowidx++)
            if (*(m_pvcsvlines + pvrowidx))
                delete *(m_pvcsvlines + pvrowidx);

        delete[] m_pvcsvlines;
        m_pvcsvlines = NULL;

        for (pvrowidx=(size_t)0; pvrowidx<m_pvparsedrowsnum; pvrowidx++)
        {
            for (pvcolidx=(size_t)0; pvcolidx<m_pvparsedcolsnum[pvrowidx]; pvcolidx++)
            {
                if (*(m_pvparsedrowvals[pvrowidx] + pvcolidx))
                    delete *(m_pvparsedrowvals[pvrowidx] + pvcolidx);
            }
            if (pvcolidx)
                delete[] m_pvparsedrowvals[pvrowidx];
        }

        delete[] m_pvparsedrowvals;
        m_pvparsedrowvals = NULL;

        delete[] m_pvparsedcolsnum;
        m_pvparsedcolsnum = NULL;

        m_pvparsedrowsnum = (size_t)0;
    }

    m_pvparsedoffset = (size_t)0;
    m_maxcolsnum = (size_t)0;
}

wxFileOffset pgCSVFile::MB2str(wxCSConv *txtconv, wxString *bufstr, char* inbuf, const size_t bufos, size_t& datlen)
{
    wxFileOffset convlen;

    int decr;
    for (decr=0 ; decr < 4 ; decr++)
    {
        convlen = txtconv->MB2WC(NULL, inbuf + bufos, 0);
        if ( convlen != wxCONV_FAILED )
            break;
        datlen--;
        inbuf[READBUFSZ-decr-1] = inbuf[datlen + MAXWORDSZ];
        inbuf[datlen + MAXWORDSZ] = 0;
    }

    if (convlen == wxCONV_FAILED )
        return wxCONV_FAILED;

    txtconv->MB2WC((wchar_t*)(wxChar*)wxStringBuffer(*bufstr, convlen+1),
                        inbuf + bufos, convlen+1);

    return convlen;
}

/**
parse line delimited by width-delimiter

return value:
    parsed char number
    wxInvalidOffset mean need more data
*/
wxString **pgCSVFile::ParseLine(const wxChar *csvdata, size_t *parsedoffset, bool hasmore,
                wxString **parsedvals, size_t *curcolnr, wxFileOffset *rsval, wxString *linedata)
{
    wxASSERT(csvdata[*parsedoffset]);
/*
    if (csvdata[*parsedoffset]==LF)
    {
        *rsval = ++(*parsedoffset);
        return parsedvals;
    }
    else if (csvdata[*parsedoffset]==CR)
    {
        if (csvdata[(*parsedoffset)+1] || !hasmore)
        {
            (*parsedoffset)++;
            if (csvdata[*parsedoffset]==LF)
                (*parsedoffset)++;

            *rsval = *parsedoffset;
            return parsedvals;
        }
        else
        {
            *rsval = wxInvalidOffset;
            return parsedvals;
        }
    }
*/
    int curvallen = 0;
    wxString *curval = NULL;

    if (parsedvals)
        curval = parsedvals[*curcolnr];
    else if (m_widthdlmsnum)
    {
        *curcolnr = (size_t)0;
        parsedvals = new wxString *[m_widthdlmsnum];
        memset(parsedvals, 0, sizeof(parsedvals)*m_widthdlmsnum);
    }

    if (curval)
        curvallen = curval->Length();

    while ((*curcolnr)<m_widthdlmsnum)
    {
        if (m_widthdlm[*curcolnr]==NULLWIDTHDELIMITER);
        else if (m_widthdlm[*curcolnr])
        {
            if (!curval)
                curval = new wxString;
            break;
        }
        else
            parsedvals[*curcolnr] = new wxString;
        (*curcolnr)++;
    }

    while (csvdata[*parsedoffset])
    {
        if (csvdata[*parsedoffset]==LF)
        {
            (*parsedoffset)++;
            if (curval)
            {
                parsedvals[*curcolnr] = curval;
                (*curcolnr)++;
            }

            while ((*curcolnr)<m_widthdlmsnum)
            {
                if (m_widthdlm[*curcolnr]==NULLWIDTHDELIMITER);
                else if (!m_widthdlm[*curcolnr])
                    parsedvals[*curcolnr] = new wxString;
                (*curcolnr)++;
            }

            *rsval = *parsedoffset;
            return parsedvals;
        }
        else if (csvdata[*parsedoffset]==CR)
        {
            if (csvdata[(*parsedoffset)+1] || !hasmore)
            {
                (*parsedoffset)++;
                if (csvdata[*parsedoffset]==LF)
                    (*parsedoffset)++;

                if (curval)
                {
                    parsedvals[*curcolnr] = curval;
                    (*curcolnr)++;
                }

                while ((*curcolnr)<m_widthdlmsnum)
                {
                    if (m_widthdlm[*curcolnr]==NULLWIDTHDELIMITER);
                    else if (!m_widthdlm[*curcolnr])
                        parsedvals[*curcolnr] = new wxString;
                    (*curcolnr)++;
                }

                *rsval = *parsedoffset;
                return parsedvals;
            }
            else
            {
                if (curval)
                    parsedvals[*curcolnr] = curval;

                *rsval = wxInvalidOffset;
                return parsedvals;
            }
        }
        else if ((*curcolnr)<m_widthdlmsnum)
        {
            if (m_backslashquote && csvdata[*parsedoffset]==wxT('\\'))
            {
                if (csvdata[(*parsedoffset)+1])
                {
                    if (linedata)
                        linedata->Append(csvdata[*parsedoffset]);
                    switch (csvdata[(*parsedoffset)+1])
                    {
                    case wxT('\\'):
                        curval->Append(wxT('\\'));
                        break;
                    case wxT('r'):
                        curval->Append(wxT('\r'));
                        break;
                    case wxT('n'):
                        curval->Append(wxT('\r'));
                        break;
                    case wxT('"'):
                        curval->Append(wxT('"'));
                        break;
                    default:
                        curval->Append(csvdata[*parsedoffset]).Append(csvdata[(*parsedoffset)+1]);
                        break;
                    }

                    (*parsedoffset)++;
                    if (linedata)
                        linedata->Append(csvdata[*parsedoffset]).Append(csvdata[(*parsedoffset)+1]);
                }
                else if (hasmore)
                {
                    parsedvals[*curcolnr] = curval;

                    *rsval = wxInvalidOffset;
                    return parsedvals;
                }
                else
                {
                    if (linedata)
                        linedata->Append(csvdata[*parsedoffset]);
                    curval->Append(csvdata[*parsedoffset]);
                    (*parsedoffset)++;

                    *rsval = *parsedoffset;
                    return parsedvals;
                }
            }
            else
            {
                curval->Append(csvdata[*parsedoffset]);
                if (linedata)
                    linedata->Append(csvdata[*parsedoffset]);
            }
            curvallen++;
            (*parsedoffset)++;
            if (curvallen==m_widthdlm[*curcolnr])
            {
                parsedvals[*curcolnr] = curval;
                (*curcolnr)++;
                curval = NULL;
                curvallen = 0;

                while ((*curcolnr)<m_widthdlmsnum)
                {
                    if (m_widthdlm[*curcolnr]==NULLWIDTHDELIMITER);
                    else if (m_widthdlm[*curcolnr])
                    {
                        if (!curval)
                            curval = new wxString;
                        break;
                    }
                    else
                        parsedvals[*curcolnr] = new wxString;
                    (*curcolnr)++;
                }
            }
        }
        else
        {
            if (linedata)
                linedata->Append(csvdata[*parsedoffset]);
            (*parsedoffset)++;
        }
    }

    if (curval)
        parsedvals[*curcolnr] = curval;

    if (hasmore)
        *rsval = wxInvalidOffset;
    else
        *rsval = *parsedoffset;

    return parsedvals;
}

/**
parse line delimited by char-delimiter
resval: result value
    parsed char offset
    wxInvalidOffset mean need more data
filelinenum: how much file lines that be contained in csv row
    (when force break line, it is always one.)
*/
wxString **pgCSVFile::ParseLine(const wxChar *csvdata, size_t *parsedoffset, const bool hasmore,
                                wxString **parsedvals, size_t *curcolnr, bool *incolparsing, bool *inquotedblk,
                                wxFileOffset *rsval, size_t *filelinenum, wxString *linedata)
{
    wxASSERT(csvdata[*parsedoffset]);

    wxString **parsedarr = parsedvals, **tmparr;
    wxString *colval = NULL;
    if (parsedvals && *incolparsing)
        colval = parsedvals[*curcolnr];

    while (csvdata[*parsedoffset])
    {
        if (csvdata[*parsedoffset]==LF)
        {
            if (m_forcelinebreak || !(*inquotedblk))
            {
                (*parsedoffset)++;
                (*filelinenum)++;
                if (!colval)
                    colval = new wxString;
                if (parsedarr)
                {
                    if (*incolparsing)
                        *incolparsing = false;
                    else
                    {
                        tmparr = parsedarr;
                        parsedarr = new wxString *[(*curcolnr)+2];
                        memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                        delete []tmparr;
                        (*curcolnr)++;
                        parsedarr[*curcolnr] = colval;
                    }
                }
                else
                {
                    parsedarr = new wxString *[1];
                    *curcolnr = (size_t)0;
                    parsedarr[*curcolnr] = colval;
                }
                *inquotedblk = false;
                *rsval = *parsedoffset;
                return parsedarr;
            }
            else
            {
                if (linedata)
                    linedata->Append(csvdata[*parsedoffset]);
                if (!colval)
                    colval = new wxString;
                colval->Append(csvdata[*parsedoffset]);
                (*parsedoffset)++;
                *filelinenum++;
            }
        }
        else if (csvdata[*parsedoffset]==CR)
        {
            if (csvdata[(*parsedoffset)+1] || !hasmore)
            {
                *filelinenum++;

                if (m_forcelinebreak || !(*inquotedblk) || !hasmore)
                {
                    (*parsedoffset)++;
                    if (csvdata[*parsedoffset]==LF)
                        (*parsedoffset)++;

                    if (!colval)
                        colval = new wxString;
                    if (parsedarr)
                    {
                        if (*incolparsing)
                            *incolparsing = false;
                        else
                        {
                            tmparr = parsedarr;
                            parsedarr = new wxString *[(*curcolnr)+2];
                            memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                            delete []tmparr;
                            (*curcolnr)++;
                            parsedarr[*curcolnr] = colval;
                        }
                    }
                    else
                    {
                        parsedarr = new wxString *[1];
                        *curcolnr = (size_t)0;
                        parsedarr[*curcolnr] = colval;
                    }
                    *inquotedblk = false;
                    *rsval = *parsedoffset;
                    return parsedarr;
                }
                else
                {
                    if (linedata)
                        linedata->Append(csvdata[*parsedoffset]);
                    if (!colval)
                        colval = new wxString;
                    colval->Append(csvdata[*parsedoffset]);
                    (*parsedoffset)++;
                    if (csvdata[*parsedoffset]==LF)
                    {
                        if (linedata)
                            linedata->Append(csvdata[*parsedoffset]);
                        colval->Append(csvdata[*parsedoffset]);
                        (*parsedoffset)++;
                    }
                }
            }
            else
            {
                if (!colval)
                    colval = new wxString;
                if (parsedarr)
                {
                    if (!(*incolparsing))
                    {
                        tmparr = parsedarr;
                        parsedarr = new wxString *[(*curcolnr)+2];
                        memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                        delete []tmparr;
                        (*curcolnr)++;
                        parsedarr[*curcolnr] = colval;
                        *incolparsing = true;
                    }
                }
                else
                {
                    parsedarr = new wxString *[1];
                    *curcolnr = (size_t)0;
                    parsedarr[*curcolnr] = colval;
                    *incolparsing = true;
                }
                *rsval = wxInvalidOffset;
                return parsedvals;
            }
        }
        else if (m_backslashquote && csvdata[*parsedoffset]==wxT('\\'))
        {
            if (csvdata[(*parsedoffset)+1])
            {
                if (linedata)
                    linedata->Append(csvdata[*parsedoffset]);
                if (!colval)
                    colval = new wxString;
                switch (csvdata[(*parsedoffset)+1])
                {
                case wxT('\\'):
                    (*parsedoffset)++;
                    colval->Append(wxT('\\'));
                    break;
                case wxT('r'):
                    (*parsedoffset)++;
                    colval->Append(wxT('\r'));
                    break;
                case wxT('n'):
                    (*parsedoffset)++;
                    colval->Append(wxT('\n'));
                    break;
                case wxT('"'):
                    (*parsedoffset)++;
                    colval->Append(wxT('\"'));
                    break;
                default:
                    colval->Append(csvdata[*parsedoffset]);
                    (*parsedoffset)++;
                    continue;
                }
                if (linedata)
                    linedata->Append(csvdata[*parsedoffset]);
                (*parsedoffset)++;
            }
            else if (hasmore)
            {
                if (!colval)
                    colval = new wxString;
                if (parsedarr)
                {
                    if (!(*incolparsing))
                    {
                        tmparr = parsedarr;
                        parsedarr = new wxString *[(*curcolnr)+2];
                        memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                        delete []tmparr;
                        (*curcolnr)++;
                        parsedarr[*curcolnr] = colval;
                        *incolparsing = true;
                    }
                }
                else
                {
                    parsedarr = new wxString *[1];
                    *curcolnr = (size_t)0;
                    parsedarr[*curcolnr] = colval;
                    *incolparsing = true;
                }
                *rsval = wxInvalidOffset;
                return parsedarr;
            }
            else
            {
                if (linedata)
                    linedata->Append(csvdata[*parsedoffset]);
                if (!colval)
                    colval = new wxString;
                colval->Append(csvdata[*parsedoffset]);
                (*parsedoffset)++;
            }
        }
        else if (m_quotedby && (*inquotedblk))
        {
            if (csvdata[*parsedoffset]==m_quotedby)
            {
                if (csvdata[(*parsedoffset)+1])
                {
                    if (linedata)
                        linedata->Append(csvdata[*parsedoffset]);
                    (*parsedoffset)++;
                    if (csvdata[*parsedoffset]==m_quotedby)
                    {
                        if (linedata)
                            linedata->Append(csvdata[*parsedoffset]);
                        if (!colval)
                            colval = new wxString;
                        colval->Append(csvdata[*parsedoffset]);
                        (*parsedoffset)++;
                    }
                    else
                        *inquotedblk = false;
                }
                else if (hasmore)
                {
                    if (!colval)
                        colval = new wxString;
                    if (parsedarr)
                    {
                        if (!(*incolparsing))
                        {
                            tmparr = parsedarr;
                            parsedarr = new wxString *[(*curcolnr)+2];
                            memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                            delete []tmparr;
                            (*curcolnr)++;
                            *incolparsing = true;
                            parsedarr[*curcolnr] = colval;
                        }
                    }
                    else
                    {
                        parsedarr = new wxString *[1];
                        *curcolnr = (size_t)0;
                        *incolparsing = true;
                        parsedarr[*curcolnr] = colval;
                    }
                    *rsval = wxInvalidOffset;
                    return parsedarr;
                }
                else
                {
                    if (linedata)
                        linedata->Append(csvdata[*parsedoffset]);
                    (*parsedoffset)++;
                    *inquotedblk = false;
                }
            }
            else
            {
                if (linedata)
                    linedata->Append(csvdata[*parsedoffset]);
                if (!colval)
                    colval = new wxString;
                colval->Append(csvdata[*parsedoffset]);
                (*parsedoffset)++;
            }
        }
        else if (m_quotedby && csvdata[*parsedoffset]==m_quotedby)
        {
            if (csvdata[(*parsedoffset)+1]==m_quotedby)
            {
                if (csvdata[(*parsedoffset)+2])
                {
                    if (m_chardlmvals && csvdata[(*parsedoffset)+2]==m_chardlmvals[0])
                    {
                        size_t dlmidx = (size_t)1;
                        while (m_chardlmvals[dlmidx])
                        {
                            if (csvdata[(*parsedoffset)+dlmidx+2]!=m_chardlmvals[dlmidx])
                                break;
                            dlmidx++;
                        }
                        if (m_chardlmvals[dlmidx])
                        {
                            if (csvdata[(*parsedoffset)+dlmidx+2])
                            {
                                if (linedata)
                                    linedata->Append(csvdata[*parsedoffset], 2);
                                if (!colval)
                                    colval = new wxString;
                                colval->Append(csvdata[*parsedoffset]);
                                (*parsedoffset)++;
                                (*parsedoffset)++;
                                while (dlmidx--)
                                {
                                    if (linedata)
                                        linedata->Append(csvdata[*parsedoffset]);
                                    colval->Append(csvdata[*parsedoffset]);
                                    (*parsedoffset)++;
                                }
                            }
                            else if (hasmore)
                            {
                                if (!colval)
                                    colval = new wxString;
                                if (parsedarr)
                                {
                                    if (!(*incolparsing))
                                    {
                                        tmparr = parsedarr;
                                        parsedarr = new wxString *[(*curcolnr)+2];
                                        memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                                        delete []tmparr;
                                        (*curcolnr)++;
                                        parsedarr[*curcolnr] = colval;
                                        *incolparsing = true;
                                    }
                                }
                                else
                                {
                                    parsedarr = new wxString *[1];
                                    *curcolnr = (size_t)0;
                                    parsedarr[*curcolnr] = colval;
                                    *incolparsing = true;
                                }
                                *rsval = wxInvalidOffset;

                                return parsedarr;
                            }
                            else
                            {
                                if (linedata)
                                    linedata->Append(csvdata[*parsedoffset], 2);
                                if (!colval)
                                    colval = new wxString;
                                colval->Append(csvdata[*parsedoffset]);
                                (*parsedoffset)++;
                                (*parsedoffset)++;
                                if (linedata)
                                    linedata->Append(csvdata+(*parsedoffset));
                                colval->Append(csvdata+(*parsedoffset));

                                if (parsedarr)
                                {
                                    if (*incolparsing)
                                        *incolparsing = false;
                                    else
                                    {
                                        tmparr = parsedarr;
                                        parsedarr = new wxString *[(*curcolnr)+2];
                                        memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                                        delete []tmparr;
                                        (*curcolnr)++;
                                        parsedarr[*curcolnr] = colval;
                                    }
                                }
                                else
                                {
                                    parsedarr = new wxString *[1];
                                    *curcolnr = (size_t)0;
                                    parsedarr[*curcolnr] = colval;
                                }

                                *parsedoffset += dlmidx;
                                *rsval = *parsedoffset;

                                return parsedarr;
                            }
                        }
                        else
                        {
                            if (!colval)
                                colval = new wxString;

                            if (parsedarr)
                            {
                                if (*incolparsing)
                                    *incolparsing = false;
                                else
                                {
                                    tmparr = parsedarr;
                                    parsedarr = new wxString *[(*curcolnr)+2];
                                    memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                                    delete []tmparr;
                                    (*curcolnr)++;
                                    parsedarr[*curcolnr] = colval;
                                }
                            }
                            else
                            {
                                parsedarr = new wxString *[1];
                                *curcolnr = (size_t)0;
                                parsedarr[*curcolnr] = colval;
                            }
                            colval = NULL;
                            if (linedata)
                            {
                                linedata->Append(csvdata[*parsedoffset], 2);
                                linedata->Append(m_chardlmvals);
                            }
                            *parsedoffset += dlmidx + 2;
                        }
                    }
                    else
                    {
                        if (linedata)
                            linedata->Append(csvdata[*parsedoffset], 2);
                        if (!colval)
                            colval = new wxString;
                        colval->Append(csvdata[*parsedoffset]);
                        (*parsedoffset)++;
                        (*parsedoffset)++;
                    }
                }
                else if (hasmore)
                {
                    if (!colval)
                        colval = new wxString;
                    if (parsedarr)
                    {
                        if (!(*incolparsing))
                        {
                            tmparr = parsedarr;
                            parsedarr = new wxString *[(*curcolnr)+2];
                            memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                            delete []tmparr;
                            (*curcolnr)++;
                            parsedarr[*curcolnr] = colval;
                            *incolparsing = true;
                        }
                    }
                    else
                    {
                        parsedarr = new wxString *[1];
                        *curcolnr = (size_t)0;
                        parsedarr[*curcolnr] = colval;
                        *incolparsing = true;
                    }
                    *rsval = wxInvalidOffset;
                    return parsedarr;
                }
                else
                {
                    if (linedata)
                        linedata->Append(csvdata[*parsedoffset], 2);
                    if (!colval)
                        colval = new wxString;
                    colval->Append(csvdata[*parsedoffset]);
                    (*parsedoffset)++;
                    (*parsedoffset)++;
                    if (parsedarr)
                    {
                        if (!(*incolparsing))
                        {
                            tmparr = parsedarr;
                            parsedarr = new wxString *[(*curcolnr)+2];
                            memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                            delete []tmparr;
                            (*curcolnr)++;
                            parsedarr[*curcolnr] = colval;
                        }
                    }
                    else
                    {
                        parsedarr = new wxString *[1];
                        *curcolnr = (size_t)0;
                        parsedarr[*curcolnr] = colval;
                    }
                    *incolparsing = false;
                    *rsval = *parsedoffset;
                    return parsedarr;
                }
            }
            else if (csvdata[(*parsedoffset)+1])
            {
                *inquotedblk = true;
                (*parsedoffset)++;
            }
            else if (hasmore)
            {
                if (!colval)
                    colval = new wxString;
                if (parsedarr)
                {
                    if (!(*incolparsing))
                    {
                        tmparr = parsedarr;
                        parsedarr = new wxString *[(*curcolnr)+2];
                        memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                        delete []tmparr;
                        (*curcolnr)++;
                        parsedarr[*curcolnr] = colval;
                        *incolparsing = true;
                    }
                }
                else
                {
                    parsedarr = new wxString *[1];
                    *curcolnr = (size_t)0;
                    parsedarr[*curcolnr] = colval;
                    *incolparsing = true;
                }
                *rsval = wxInvalidOffset;
                return parsedarr;
            }
            else
            {
                if (linedata)
                    linedata->Append(csvdata[*parsedoffset]);
                if (!colval)
                    colval = new wxString;
                colval->Append(csvdata[*parsedoffset]);
                (*parsedoffset)++;
                if (parsedarr)
                {
                    if (*incolparsing)
                        *incolparsing = false;
                    else
                    {
                        tmparr = parsedarr;
                        parsedarr = new wxString *[(*curcolnr)+2];
                        memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                        delete []tmparr;
                        (*curcolnr)++;
                        parsedarr[*curcolnr] = colval;
                    }
                }
                else
                {
                    parsedarr = new wxString *[1];
                    *curcolnr = (size_t)0;
                    parsedarr[*curcolnr] = colval;
                }
                *rsval = *parsedoffset;
                return parsedarr;
            }
        }
        else if (m_chardlmvals && csvdata[*parsedoffset]==m_chardlmvals[0])
        {
            size_t dlmidx = (size_t)1;
            while (m_chardlmvals[dlmidx])
            {
                if (csvdata[(*parsedoffset)+dlmidx]!=m_chardlmvals[dlmidx])
                    break;
                dlmidx++;
            }
            if (m_chardlmvals[dlmidx])
            {
                if (csvdata[(*parsedoffset)+dlmidx])
                {
                    if (!colval)
                        colval = new wxString;
                    while (dlmidx--)
                    {
                        if (linedata)
                            linedata->Append(csvdata[*parsedoffset]);
                        colval->Append(csvdata[*parsedoffset]);
                        (*parsedoffset)++;
                    }
                }
                else if (hasmore)
                {
                    if (colval)
                    {
                        if (parsedarr)
                        {
                            if (!(*incolparsing))
                            {
                                tmparr = parsedarr;
                                parsedarr = new wxString *[(*curcolnr)+2];
                                memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                                delete []tmparr;
                                (*curcolnr)++;
                                parsedarr[*curcolnr] = colval;
                                *incolparsing = true;
                            }
                        }
                        else
                        {
                            parsedarr = new wxString *[1];
                            *curcolnr = (size_t)0;
                            parsedarr[*curcolnr] = colval;
                            *incolparsing = true;
                        }
                    }

                    *rsval = wxInvalidOffset;
                    return parsedarr;
                }
                else
                {
                    if (!colval)
                        colval = new wxString;
                    while (dlmidx--)
                    {
                        if (linedata)
                            linedata->Append(csvdata[*parsedoffset]);
                        colval->Append(csvdata[*parsedoffset]);
                        (*parsedoffset)++;
                    }
                    if (parsedarr)
                    {
                        if (!(*incolparsing))
                        {
                            tmparr = parsedarr;
                            parsedarr = new wxString *[(*curcolnr)+2];
                            memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                            delete []tmparr;
                            (*curcolnr)++;
                            parsedarr[*curcolnr] = colval;
                        }
                    }
                    else
                    {
                        parsedarr = new wxString *[1];
                        *curcolnr = (size_t)0;
                        parsedarr[*curcolnr] = colval;
                    }

                    *incolparsing = false;
                    *rsval = *parsedoffset;
                    return parsedarr;
                }
            }
            else
            {
                if (parsedarr)
                {
                    if (*incolparsing)
                        *incolparsing = false;
                    else
                    {
                        tmparr = parsedarr;
                        parsedarr = new wxString *[(*curcolnr)+2];
                        memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
                        delete []tmparr;
                        (*curcolnr)++;
                        parsedarr[*curcolnr] = colval;
                    }
                }
                else
                {
                    parsedarr = new wxString *[1];
                    *curcolnr = (size_t)0;
                    parsedarr[*curcolnr] = colval;
                }
                colval = NULL;
                *parsedoffset += dlmidx;
                if (linedata)
                    linedata->Append(m_chardlmvals);
            }
        }
        else
        {
            if (linedata)
                linedata->Append(csvdata[*parsedoffset]);
            if (!colval)
                colval = new wxString;
            colval->Append(csvdata[*parsedoffset]);
            (*parsedoffset)++;
        }
    }

    if (parsedarr)
    {
        if (!(*incolparsing))
        {
            tmparr = parsedarr;
            parsedarr = new wxString *[(*curcolnr)+2];
            memcpy(parsedarr, tmparr, sizeof(parsedarr)*((*curcolnr)+1));
            delete []tmparr;
            (*curcolnr)++;
            parsedarr[*curcolnr] = colval;
        }
    }
    else
    {
        parsedarr = new wxString *[1];
        *curcolnr = (size_t)0;
        parsedarr[*curcolnr] = colval;
    }

    if (hasmore)
    {
        *rsval = wxInvalidOffset;
        *incolparsing = true;
    }
    else
    {
        *rsval = *parsedoffset;
        *incolparsing = false;
    }

    return parsedarr;
}

bool pgCSVFile::ValidateSetting()
{
    if (m_quotedby && m_chardlm.Find(m_quotedby)!=wxNOT_FOUND)
    {
        m_settingisok = false;
        m_ferrmsg = _("The quoted mark can't be inclued in delimiter.");
        return false;
    }
    if (m_backslashquote && m_chardlm.Find(wxT('\\'))!=wxNOT_FOUND)
    {
        m_settingisok = false;
        m_ferrmsg = _("The backslash can't be inclued in delimiter.");
        return false;
    }
    if (m_backslashquote && m_quotedby==wxT('\\'))
    {
        m_settingisok = false;
        m_ferrmsg = _("The quoted mark can't be escape char when back slash quote.");
        return false;
    }

    m_settingisok = true;
    return true;
}

void pgCSVFile::GenHeader()
{
    wxASSERT(m_pvparsedrowsnum);

    m_colheaders.Clear();
    wxString colheader;

    size_t colidx;
    if (!m_firstrowisheader)
    {
        for (colidx=(size_t)0; colidx<m_maxcolsnum; colidx++)
        {
            colheader.Printf(wxT("Column #%d"), colidx+1);
            m_colheaders.Add(colheader);
        }
        return;
    }

    int triednum;
    size_t colsnum = *m_pvparsedcolsnum;
    wxString **headerarr = *m_pvparsedrowvals;
    wxString headerminor;
    for (colidx=(size_t)0; colidx<m_maxcolsnum; colidx++)
    {
        if (colidx>=colsnum)
            colheader.Printf(wxT("Column #%d"), colidx+1);
        else
        {
            if (headerarr[colidx])
            {
                colheader = headerarr[colidx]->Trim().Trim(false);
                if (colheader.IsEmpty())
                    colheader.Printf(wxT("Column #%d"), colidx+1);
            }
            else
                colheader.Printf(wxT("Column #%d"), colidx+1);
        }

        if (m_colheaders.Index(colheader)==wxNOT_FOUND)
        {
            m_colheaders.Add(colheader);
            continue;
        }

        triednum = 1;
        while (true)
        {
            headerminor.Printf(wxT("_%d"), triednum);
            if (m_colheaders.Index(colheader + headerminor)==wxNOT_FOUND)
            {
                m_colheaders.Add(colheader + headerminor);
                break;
            }
            triednum++;
        }

    }

}

bool pgCSVFile::EndPreview()
{
    size_t pvcurbufsz, readlen = 0;

    m_parsedoffset = 0;
    size_t parsedoffset = (size_t)0;
    if (m_firstrowisheader)
    {
        wxFileOffset parsedrs;
        size_t curcolnr = (size_t)0;
        wxString **parsedvals;
        if (m_delimitedbychar)
        {
            bool incolparsing=false, inquotedblk=false;
            parsedvals = ParseLine(m_pvfilecont.GetData(), &parsedoffset, false, NULL, &curcolnr,
                    &incolparsing, &inquotedblk, &parsedrs, &m_filelinenr, NULL);
        }
        else
        {
            parsedvals = ParseLine(m_pvfilecont.GetData(), &parsedoffset, false,
                    NULL, &curcolnr, &parsedrs, NULL);
            m_filelinenr++;
        }

        m_curfilecont.Append(m_pvfilecont.GetData() + parsedoffset);

        size_t tmpcolidx;
        for (tmpcolidx=(size_t)0; tmpcolidx<curcolnr; tmpcolidx++)
            if (parsedvals[tmpcolidx])
                delete parsedvals[tmpcolidx];
        if (curcolnr)
            delete[] parsedvals;
    }
    else
        m_curfilecont.Append(m_pvfilecont);

    pvcurbufsz = m_pvmembuf.GetDataLen();
    char* pvbufdata = (char*)m_pvmembuf.GetData();

    memset(m_leftbuf, 0, MAXWORDSZ);

    if (pvcurbufsz>m_pvbufoffset)
    {
        wxString convstr;
        size_t convlen;
        readlen = pvcurbufsz - m_pvbufoffset;

        convlen = MB2str(m_fileconv, &convstr, pvbufdata, m_pvdecoffset, readlen);
        if (convlen==wxCONV_FAILED)
        {
            m_fencisok = false;
            m_ferrmsg = _("Can't convert specified encoding to Unicode.");

            return false;
        }

        m_curfilecont.Append(convstr);
        m_pvdecoffset = m_pvbufoffset + readlen;
    }

    int leftbyte = pvcurbufsz - m_pvdecoffset - readlen;
    if (leftbyte)
        memcpy(m_leftbuf+MAXWORDSZ-leftbyte, pvbufdata+m_pvdecoffset+readlen, leftbyte);

    m_charavglen = TellI()*1.0/(parsedoffset+m_curfilecont.Len());
    if (parsedoffset)
        m_fileoffset = (wxFileOffset)parsedoffset*m_charavglen;

    ClearPreview();

    return true;
}

bool pgCSVFile::NextLine(wxString& linedata, wxString**& linevals, int &valsnum)
{
    if (m_prevcolnum)
    {
        size_t tmpcolidx;
        for (tmpcolidx=(size_t)0; tmpcolidx<m_prevcolnum; tmpcolidx++)
            if (m_prevcolvals[tmpcolidx])
                delete m_prevcolvals[tmpcolidx];

        delete[] m_prevcolvals;

        m_prevcolvals = NULL;
        m_prevcolnum = 0;
    }

    const wxChar* filedata = m_curfilecont.GetData();
    if (!filedata[m_parsedoffset] && Eof())
        return true;

    wxFileOffset parsedrs;
    size_t curcolnr = (size_t)0, parsedoffset = m_parsedoffset, convlen;
    wxString **parsedvals = NULL;
    bool incolparsing = false, inquotedblk = false;
    wxString convstr, tmpstr;
    char* readbuf = NULL;
    size_t bufoffs = 0, readlen;
    wxCSConv *txtconv = NULL;

    while (true)
    {
        if (!filedata[m_parsedoffset])
        {
            parsedrs = wxInvalidOffset;
            m_curfilecont.Empty();
            m_parsedoffset = 0;
            parsedoffset = 0;
            m_baseoffset = TellI();
        }
        else if (m_delimitedbychar)
            parsedvals = ParseLine(filedata, &parsedoffset, !Eof(), parsedvals, &curcolnr,
                    &incolparsing, &inquotedblk, &parsedrs, &m_filelinenr, &linedata);
        else
        {
            parsedvals = ParseLine(filedata, &parsedoffset, !Eof(),
                    parsedvals, &curcolnr, &parsedrs, &linedata);
            m_filelinenr++;
        }

        if (parsedrs==wxInvalidOffset)
        {
            if (!readbuf)
                readbuf = new char[READBUFSZ];
            memcpy(readbuf, m_leftbuf, MAXWORDSZ);
            while (!readbuf[bufoffs] && bufoffs<MAXWORDSZ)
                bufoffs++;

#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
#if wxUSE_ZIPSTREAM
            if (m_zipinstrm)
            {
                m_zipinstrm->Read(readbuf + MAXWORDSZ, READBLKSZ);
                readlen = m_zipinstrm->LastRead();
                txtconv = m_entryconv;
            }
#endif
#if wxUSE_TARSTREAM
            if (m_tarinstrm)
            {
                m_tarinstrm->Read(readbuf + MAXWORDSZ, READBLKSZ);
                readlen = m_tarinstrm->LastRead();
                txtconv = m_entryconv;
            }
#endif
#endif
            if (m_rawfile)
            {
                readlen = m_rawfile->Read(readbuf + MAXWORDSZ, READBLKSZ);
                txtconv = m_fileconv;
            }

            memset(readbuf + MAXWORDSZ + readlen, 0, READBUFSZ - MAXWORDSZ - readlen);
            convlen = MB2str(txtconv, &convstr, readbuf, bufoffs, readlen);
            if (convlen==wxCONV_FAILED)
            {
                m_fencisok = false;
                m_ferrmsg = _("Can't convert specified encoding to Unicode.");
                m_prevcolnum = curcolnr;
                m_prevcolvals = parsedvals;

                delete[] readbuf;
                return false;
            }
            memcpy(m_leftbuf, readbuf + READBUFSZ - MAXWORDSZ, MAXWORDSZ);

            if (m_parsedoffset)
            {
                m_baseoffset = TellI() - (m_curfilecont.Len()-m_parsedoffset)*m_charavglen;
                m_charavglen = ((m_curfilecont.Len()-m_parsedoffset)+convstr.Len())/
                        ((m_curfilecont.Len()-m_parsedoffset)*m_charavglen + (MAXWORDSZ-bufoffs) + readlen);
                wxString tmpfilecont(m_curfilecont.GetData()+m_parsedoffset);
                m_curfilecont.Empty();
                m_curfilecont.Append(tmpfilecont);
                parsedoffset -= m_parsedoffset;
                m_parsedoffset = 0;
            }
            else
                m_charavglen = (m_curfilecont.Len()*m_charavglen+(MAXWORDSZ-bufoffs) + readlen)/convstr.Len();

            m_curfilecont.Append(convstr);
            filedata = m_curfilecont.GetData();
        }
        else
            break;
    }

    m_prevcolvals = linevals = parsedvals;
    m_prevcolnum = valsnum = curcolnr+1;
    m_parsedoffset = parsedoffset;
    m_fileoffset = m_baseoffset + (wxFileOffset)m_parsedoffset*m_charavglen;
    m_csvlinenr++;

    if (readbuf)
        delete[] readbuf;

    return true;
}
