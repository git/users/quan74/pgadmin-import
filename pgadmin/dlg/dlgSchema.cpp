//////////////////////////////////////////////////////////////////////////
//
// pgAdmin III - PostgreSQL Tools
// RCS-ID:      $Id$
// Copyright (C) 2002 - 2009, The pgAdmin Development Team
// This software is released under the BSD Licence
//
// dlgSchema.cpp - PostgreSQL Schema Property
//
//////////////////////////////////////////////////////////////////////////

// wxWindows headers
#include <wx/wx.h>

// App headers
#include "pgAdmin3.h"
#include "utils/misc.h"
#include "dlg/dlgSchema.h"
#include "schema/pgSchema.h"


// pointer to controls

BEGIN_EVENT_TABLE(dlgSchema, dlgSecurityProperty)
END_EVENT_TABLE();

dlgProperty *pgSchemaBaseFactory::CreateDialog(frmMain *frame, pgObject *node, pgObject *parent)
{
    return new dlgSchema(this, frame, (pgSchema*)node);
}

dlgSchema::dlgSchema(pgaFactory *f, frmMain *frame, pgSchema *node)
: dlgSecurityProperty(f, frame, node, wxT("dlgSchema"), wxT("USAGE,CREATE"), "UC")
{
    schema=node;
}


pgObject *dlgSchema::GetObject()
{
    return schema;
}


int dlgSchema::Go(bool modal)
{
    if (!schema)
        cbOwner->Append(wxT(""));

    AddGroups();
    AddUsers(cbOwner);
    if (schema)
    {
        // edit mode

        if (!connection->BackendMinimumVersion(7, 5))
            cbOwner->Disable();

        if (schema->GetMetaType() == PGM_CATALOG)
        {
            cbOwner->Disable();
            txtName->Disable();
        }
    }
    else
    {
        // create mode
    }

    return dlgSecurityProperty::Go(modal);
}


pgObject *dlgSchema::CreateObject(pgCollection *collection)
{
    wxString name=GetName();

    pgObject *obj=schemaFactory.CreateObjects(collection, 0, wxT(" WHERE nspname=") + qtDbString(name) + wxT("\n"));
    return obj;
}


#ifdef __WXMAC__
void dlgSchema::OnChangeSize(wxSizeEvent &ev)
{
    SetPrivilegesLayout();
    if (GetAutoLayout())
    {
        Layout();
    }
}
#endif


void dlgSchema::CheckChange()
{
    wxString name=GetName();
    if (schema)
    {
        EnableOK(name != schema->GetName() || txtComment->GetValue() != schema->GetComment()
            || cbOwner->GetValue() != schema->GetOwner());
    }
    else
    {

        bool enable=true;
        CheckValid(enable, !name.IsEmpty(), _("Please specify name."));

        EnableOK(enable);
    }
}



wxString dlgSchema::GetSql()
{
    wxString sql, name;
    name=GetName();

    if (schema)
    {
        // edit mode
        AppendNameChange(sql);
        AppendOwnerChange(sql, wxT("SCHEMA ") + qtIdent(name));
    }
    else
    {
        // create mode
        sql = wxT("CREATE SCHEMA ") + qtIdent(name);
        AppendIfFilled(sql, wxT("\n       AUTHORIZATION "), qtIdent(cbOwner->GetValue()));
        sql += wxT(";\n");

    }
    AppendComment(sql, wxT("SCHEMA"), 0, schema);

    sql += GetGrant(wxT("UC"), wxT("SCHEMA ") + qtIdent(name));

    return sql;
}


