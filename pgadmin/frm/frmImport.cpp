
// App headers
#include <wx/file.h>

#include "pgAdmin3.h"
#include "frm/frmImport.h"
#include "utils/sysSettings.h"

// wizard & pages
//#define frmImport              XRCCTRL(*this, "frmImport", wxWizard)
#define wzdTargetPage         CTRL_WIZARDPAGE("wzdTargetPage")
#define wzdSrcTypePage        CTRL_WIZARDPAGE("wzdSrcTypePage")
#define wzdTextFilePage       CTRL_WIZARDPAGE("wzdTextFilePage")
#define wzdArchFilePage       CTRL_WIZARDPAGE("wzdArchFilePage")
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
#define wzdSrcODBCPage        CTRL_WIZARDPAGE("wzdSrcODBCPage")
#endif
#define wzdSrcPgSQLPage       CTRL_WIZARDPAGE("wzdSrcPgSQLPage")
#define wzdFileSettingPage    CTRL_WIZARDPAGE("wzdFileSettingPage")
#define wzdFileOptsPage       CTRL_WIZARDPAGE("wzdFileOptsPage")
#define wzdColsSettingPage    CTRL_WIZARDPAGE("wzdColsSettingPage")
#define wzdDestLogPage        CTRL_WIZARDPAGE("wzdDestLogPage")
#define wzdProcessPage        CTRL_WIZARDPAGE("wzdProcessPage")
// Target page

// Datasource type page
#define rdbSrcTextFile        CTRL_RADIOBUTTON("rdbSrcTextFile")
#define rdbSrcArchFile        CTRL_RADIOBUTTON("rdbSrcArchFile")
#define rdbSrcODBC            CTRL_RADIOBUTTON("rdbSrcODBC")
#define rdbSrcPgSQL           CTRL_RADIOBUTTON("rdbSrcPgSQL")
// Text source file page
#define txtTextFilePath             CTRL_TEXT("txtTextFilePath")
#define btnTextFileBrowse           CTRL_BUTTON("btnTextFileBrowse")
#define chcTextFileEncoding         CTRL_CHOICE("chcTextFileEncoding")
#define btnTextFilePreviewMore      CTRL_BUTTON("btnTextFilePreviewMore")
#define txtTextFileCont             CTRL_TEXT("txtTextFileCont")
// Archive source file page
#define txtArchFilePath            CTRL_TEXT("txtArchFilePath")
#define btnArchFileBrowse          CTRL_BUTTON("btnArchFileBrowse")
#define chcArchFileEncoding        CTRL_CHOICE("chcArchFileEncoding")
#define chcArchEntryName           CTRL_CHOICE("chcArchEntryName")
#define chcArchEntryEncoding       CTRL_CHOICE("chcArchEntryEncoding")
#define btnArchFilePreviewMore     CTRL_BUTTON("btnArchFilePreviewMore")
#define txtArchFileCont            CTRL_TEXT("txtArchFileCont")
//wzdODBCPage
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
#define cbODBCDSNNames           CTRL_COMBOBOX("cbODBCDSNNames")
#define btnODBCReset             CTRL_BUTTON("btnODBCReset")
#define txtODBCUsername          CTRL_TEXT("txtODBCUsername")
#define txtODBCPassword          CTRL_TEXT("txtODBCPassword")
#define btnODBCConnect           CTRL_BUTTON("btnODBCConnect")
#define cbODBCTableNames         CTRL_COMBOBOX("cbODBCTableNames")
#define chkODBCCustomizedSQL     CTRL_CHECKBOX("chkODBCCustomizedSQL")
#define txtODBCWhereCl           CTRL_TEXT("txtODBCWhereCl")
#define txtODBCSQLStmt           CTRL_TEXT("txtODBCSQLStmt")
#define btnODBCPreview           CTRL_BUTTON("btnODBCPreview")
#define spinODBCPreviewRows      CTRL_SPIN("spinODBCPreviewRows")
#define gridODBCPreview          CTRL_GRID("gridODBCPreview")
#define lstODBCResultAttribs     CTRL_LISTVIEW("lstODBCResultAttribs")
#define lstODBCConnInfo          CTRL_LISTVIEW("lstODBCConnInfo")
#define lstODBCTableCols         CTRL_LISTVIEW("lstODBCTableCols")
#endif
// File setting page
#define rdbFileCharDelimiter      CTRL_RADIOBUTTON("rdbFileCharDelimiter")
#define cbFileCharDelimiter       CTRL_COMBOBOX("cbFileCharDelimiter")
#define rdbFileWidthDelimiter     CTRL_RADIOBUTTON("rdbFileWidthDelimiter")
#define txtFileWidthDelimiter     CTRL_TEXT("txtFileWidthDelimiter")
#define cbFileQuotedBy            CTRL_COMBOBOX("cbFileQuotedBy")
#define chkFileFirstRowIsHeader   CTRL_CHECKBOX("chkFileFirstRowIsHeader")
#define chkFileBackSlashQuote     CTRL_CHECKBOX("chkFileBackSlashQuote")
#define chkFileForceLineBreak     CTRL_CHECKBOX("chkFileForceLineBreak")
#define spinFilePreviewRows       CTRL_SPIN("spinFilePreviewRows")
#define gridFilePreview           CTRL_GRID("gridFilePreview")
//wzdFileOptsPage
#define chkFileSkipEmptyRow       CTRL_CHECKBOX("chkFileSkipEmptyRow")
#define txtFileOptImpRegEx        CTRL_TEXT("txtFileOptImpRegEx")
#define txtFileOptStartWith       CTRL_TEXT("txtFileOptStartWith")
#define spinFileSkipFirstRows     CTRL_SPIN("spinFileSkipFirstRows")
#define rdbFileImpAllRows         CTRL_RADIOBUTTON("rdbFileImpAllRows")
#define rdbFileImpOnlyRows        CTRL_RADIOBUTTON("rdbFileImpOnlyRows")
#define spinFileImpOnlyRows       CTRL_SPIN("spinFileImpOnlyRows")
#define chkFileCheckTrueFirst     CTRL_CHECKBOX("chkFileCheckTrueFirst")
#define txtFileOptTrueVals        CTRL_TEXT("txtFileOptTrueVals")
#define txtFileOptFalseVals       CTRL_TEXT("txtFileOptFalseVals")
//wzdColumnsPage
#define chcImportMode          CTRL_CHOICE("chcImportMode")
#define chcFormatStyle         CTRL_CHOICE("chcFormatStyle")
#define gridColSetting         CTRL_GRID("gridColSetting")
#define btnImpPreview          CTRL_BUTTON("btnImpPreview")
#define chkTestImport          CTRL_CHECKBOX("chkTestImport")
#define chkValidateExpr        CTRL_CHECKBOX("chkValidateExpr")
#define nbColsPreview          CTRL_NOTEBOOK("nbColsPreview")
#define gridColsSampleData     CTRL_GRID("gridColsSampleData")
#define txtColsSettingLog      CTRL_TEXT("txtColsSettingLog")
#define txtColsImpLog          CTRL_TEXT("txtColsImpLog")
//wzdDestLogPage
#define chcImpDest             CTRL_CHOICE("chcImpDest")
#define txtDestFilePath        CTRL_TEXT("txtDestFilePath")
#define btnDestFileBrowse      CTRL_BUTTON("btnDestFileBrowse")
#define chkDestFileAppend      CTRL_CHECKBOX("chkDestFileAppend")
#define spinCommitEvery        CTRL_SPIN("spinCommitEvery")
#define spinStopOnError        CTRL_SPIN("spinStopOnError")
#define rdbLogToScreen         CTRL_RADIOBUTTON("rdbLogToScreen")
#define rdbLogToFile           CTRL_RADIOBUTTON("rdbLogToFile")
#define rdbLogToScrFile        CTRL_RADIOBUTTON("rdbLogToScrFile")
#define txtLogFilePath         CTRL_TEXT("txtLogFilePath")
#define btnLogFileBrowse       CTRL_BUTTON("btnLogFileBrowse")
#define chkLogFileAppend       CTRL_CHECKBOX("chkLogFileAppend")
#define rdbLogVerbose          CTRL_RADIOBUTTON("rdbLogVerbose")
#define rdbLogSummary          CTRL_RADIOBUTTON("rdbLogSummary")
#define rdbLogSilent           CTRL_RADIOBUTTON("rdbLogSilent")
#define chkLogError            CTRL_CHECKBOX("chkLogError")
#define chkLogWarn             CTRL_CHECKBOX("chkLogWarn")
#define chkLogStmt             CTRL_CHECKBOX("chkLogStmt")
#define chkLogInfo             CTRL_CHECKBOX("chkLogInfo")
#define chkLogProc             CTRL_CHECKBOX("chkLogProc")
//wzdProcessPage
#define btnImpStart            CTRL_BUTTON("btnImpStart")
#define btnImpStop             CTRL_BUTTON("btnImpStop")
#define gaugeImp               CTRL_GAUGE("gaugeImp")
#define stxtInsertedRows       CTRL_STATIC("stxtInsertedRows")
#define stxtElapsedTime        CTRL_STATIC("stxtElapsedTime")
#define stxtRemainingTime      CTRL_STATIC("stxtRemainingTime")
#define stxtAvgRowTime         CTRL_STATIC("stxtAvgRowTime")
#define txtLogMessages         CTRL_TEXT("txtLogMessages")

BEGIN_EVENT_TABLE(frmImport, wxWizard)
    // wizard event
    EVT_WIZARD_PAGE_CHANGING(XRCID("frmImport"),        frmImport::OnPageChanging)
    EVT_WIZARD_CANCEL(XRCID("frmImport"),               frmImport::OnWizardCancel)
    // TODO: check whether finish button has bug that can't be disabled?
    // if or not add a new method to catch finish wizard event.
    // Source Type page
    EVT_RADIOBUTTON(XRCID("rdbSrcTextFile"),            frmImport::OnSourceType)
    EVT_RADIOBUTTON(XRCID("rdbSrcArchFile"),            frmImport::OnSourceType)
    EVT_RADIOBUTTON(XRCID("rdbSrcODBC"),                frmImport::OnSourceType)
    EVT_RADIOBUTTON(XRCID("rdbSrcPgSQL"),               frmImport::OnSourceType)
    // Text file page
    EVT_BUTTON(XRCID("btnTextFileBrowse"),              frmImport::OnTextFileBrowse)
    EVT_CHOICE(XRCID("chcTextFileEncoding"),            frmImport::OnTextFileEncoding)
    EVT_BUTTON(XRCID("btnTextFilePreviewMore"),         frmImport::OnTextFilePreviewMore)
    // Archive file page
    EVT_BUTTON(XRCID("btnArchFileBrowse"),              frmImport::OnArchFileBrowse)
    EVT_CHOICE(XRCID("chcArchFileEncoding"),            frmImport::OnArchFileEncoding)
    EVT_CHOICE(XRCID("chcArchEntryName"),               frmImport::OnArchEntryName)
    EVT_CHOICE(XRCID("chcArchEntryEncoding"),           frmImport::OnArchEntryEncoding)
    EVT_BUTTON(XRCID("btnArchFilePreviewMore"),         frmImport::OnArchFilePreviewMore)
    // ODBC page
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    //EVT_COMBOBOX(XRCID("cbODBCDSNNames"),               frmImport::OnODBCDSNNames)
    EVT_COMBOBOX(XRCID("cbODBCTableNames"),             frmImport::OnODBCTableNames)
    EVT_BUTTON(XRCID("btnODBCReset"),                   frmImport::OnODBCReset)
    EVT_BUTTON(XRCID("btnODBCConnect"),                 frmImport::OnODBCConnect)
    EVT_CHECKBOX(XRCID("chkODBCCustomizedSQL"),         frmImport::OnODBCCustomizedSQL)
    EVT_TEXT(XRCID("txtODBCWhereCl"),                   frmImport::OnODBCWhereCl)
    EVT_BUTTON(XRCID("btnODBCPreview"),                 frmImport::OnODBCPreview)
#endif
    // File setting page
    EVT_RADIOBUTTON(XRCID("rdbFileCharDelimiter"),      frmImport::OnFileDelimiterType)
    EVT_RADIOBUTTON(XRCID("rdbFileWidthDelimiter"),     frmImport::OnFileDelimiterType)
    EVT_COMBOBOX(XRCID("cbFileCharDelimiter"),          frmImport::OnFileCharDelimiter)
    EVT_TEXT(XRCID("cbFileCharDelimiter"),              frmImport::OnFileCharDelimiter)
    EVT_COMBOBOX(XRCID("cbFileQuotedBy"),               frmImport::OnFileQuotedBy)
    EVT_CHECKBOX(XRCID("chkFileFirstRowIsHeader"),      frmImport::OnFileFirstRowIsHeader)
    EVT_CHECKBOX(XRCID("chkFileForceLineBreak"),        frmImport::OnFileForceLineBreak)
    EVT_CHECKBOX(XRCID("chkFileBackSlashQuote"),        frmImport::OnFileBackSlashQuote)
    EVT_SPINCTRL(XRCID("spinFilePreviewRows"),          frmImport::OnFilePreviewRows)
    // File options page
    EVT_RADIOBUTTON(XRCID("rdbFileImpAllRows"),         frmImport::OnFileOptsImpRows)
    EVT_RADIOBUTTON(XRCID("rdbFileImpOnlyRows"),        frmImport::OnFileOptsImpRows)
    EVT_CHECKBOX(XRCID("chkFileCheckTrueFirst"),        frmImport::OnFileOptsCheckTrueFirst)
    // Column page
    EVT_CHOICE(XRCID("chcImportMode"),                  frmImport::OnColSettingImportMode)
    EVT_CHOICE(XRCID("chcFormatStyle"),                 frmImport::OnColSettingFormatStyle)
    EVT_GRID_CMD_CELL_CHANGE(XRCID("gridColSetting"),   frmImport::OnColSettingCellChange)
    EVT_BUTTON(XRCID("btnImpPreview"),                  frmImport::OnColSettingImpPreview)
    EVT_CHECKBOX(XRCID("chkTestImport"),                frmImport::OnColSettingTestImp)
    // Destination&Log page
    EVT_CHOICE(XRCID("chcImpDest"),                     frmImport::OnDestImpDest)
    EVT_BUTTON(XRCID("btnDestFileBrowse"),              frmImport::OnDestFileBrowse)
    EVT_RADIOBUTTON(XRCID("rdbLogToScreen"),            frmImport::OnDestLogTo)
    EVT_RADIOBUTTON(XRCID("rdbLogToFile"),              frmImport::OnDestLogTo)
    EVT_RADIOBUTTON(XRCID("rdbLogToScrFile"),           frmImport::OnDestLogTo)
    EVT_BUTTON(XRCID("btnLogFileBrowse"),               frmImport::OnLogFileBrowse)
    EVT_RADIOBUTTON(XRCID("rdbLogVerbose"),             frmImport::OnLogTypeChange)
    EVT_RADIOBUTTON(XRCID("rdbLogSummary"),             frmImport::OnLogTypeChange)
    EVT_RADIOBUTTON(XRCID("rdbLogSilent"),              frmImport::OnLogTypeChange)
    // Process page
    EVT_BUTTON(XRCID("btnImpStart"),                    frmImport::OnProcImpStart)
    EVT_BUTTON(XRCID("btnImpStop"),                     frmImport::OnProcImpStop)
END_EVENT_TABLE()

// copy from wxWidgets src/common/fmapbase.cpp
// encodings supported by GetEncodingDescription
static const wxFontEncoding gs_encodings[] =
{
    wxFONTENCODING_SYSTEM,
    wxFONTENCODING_ISO8859_1,
    wxFONTENCODING_ISO8859_2,
    wxFONTENCODING_ISO8859_3,
    wxFONTENCODING_ISO8859_4,
    wxFONTENCODING_ISO8859_5,
    wxFONTENCODING_ISO8859_6,
    wxFONTENCODING_ISO8859_7,
    wxFONTENCODING_ISO8859_8,
    wxFONTENCODING_ISO8859_9,
    wxFONTENCODING_ISO8859_10,
    wxFONTENCODING_ISO8859_11,
    wxFONTENCODING_ISO8859_12,
    wxFONTENCODING_ISO8859_13,
    wxFONTENCODING_ISO8859_14,
    wxFONTENCODING_ISO8859_15,
    wxFONTENCODING_KOI8,
    wxFONTENCODING_KOI8_U,
    wxFONTENCODING_CP874,
    wxFONTENCODING_CP932,
    wxFONTENCODING_CP936,
    wxFONTENCODING_CP949,
    wxFONTENCODING_CP950,
    wxFONTENCODING_CP1250,
    wxFONTENCODING_CP1251,
    wxFONTENCODING_CP1252,
    wxFONTENCODING_CP1253,
    wxFONTENCODING_CP1254,
    wxFONTENCODING_CP1255,
    wxFONTENCODING_CP1256,
    wxFONTENCODING_CP1257,
    wxFONTENCODING_CP437,
    wxFONTENCODING_UTF7,
    wxFONTENCODING_UTF8,
    wxFONTENCODING_UTF16BE,
    wxFONTENCODING_UTF16LE,
    wxFONTENCODING_UTF32BE,
    wxFONTENCODING_UTF32LE,
    wxFONTENCODING_EUC_JP,
    wxFONTENCODING_ISO8859_1,//wxFONTENCODING_DEFAULT, (Because it raise assert.)
};

// reference from wxWidgets src/common/fmapbase.cpp
// the descriptions for them
static const wxChar* gs_encodingdescs[] =
{
    wxTRANSLATE( "[Default]" ),
    wxTRANSLATE( "Western European (ISO-8859-1)" ),
    wxTRANSLATE( "Central European (ISO-8859-2)" ),
    wxTRANSLATE( "Esperanto (ISO-8859-3)" ),
    wxTRANSLATE( "Baltic (old) (ISO-8859-4)" ),
    wxTRANSLATE( "Cyrillic (ISO-8859-5)" ),
    wxTRANSLATE( "Arabic (ISO-8859-6)" ),
    wxTRANSLATE( "Greek (ISO-8859-7)" ),
    wxTRANSLATE( "Hebrew (ISO-8859-8)" ),
    wxTRANSLATE( "Turkish (ISO-8859-9)" ),
    wxTRANSLATE( "Nordic (ISO-8859-10)" ),
    wxTRANSLATE( "Thai (ISO-8859-11)" ),
    wxTRANSLATE( "Indian (ISO-8859-12)" ),
    wxTRANSLATE( "Baltic (ISO-8859-13)" ),
    wxTRANSLATE( "Celtic (ISO-8859-14)" ),
    wxTRANSLATE( "Western European with Euro (ISO-8859-15)" ),
    wxTRANSLATE( "KOI8-R" ),
    wxTRANSLATE( "KOI8-U" ),
    wxTRANSLATE( "Windows Thai (CP 874)" ),
    wxTRANSLATE( "Windows Japanese (CP 932/SHIFT-JIS)" ),
    wxTRANSLATE( "Windows Chinese Simplified (CP 936/GB-2312)" ),
    wxTRANSLATE( "Windows Korean (CP 949)" ),
    wxTRANSLATE( "Windows Chinese Traditional (CP 950/BIG5)" ),
    wxTRANSLATE( "Windows Central European (CP 1250)" ),
    wxTRANSLATE( "Windows Cyrillic (CP 1251)" ),
    wxTRANSLATE( "Windows Western European (CP 1252)" ),
    wxTRANSLATE( "Windows Greek (CP 1253)" ),
    wxTRANSLATE( "Windows Turkish (CP 1254)" ),
    wxTRANSLATE( "Windows Hebrew (CP 1255)" ),
    wxTRANSLATE( "Windows Arabic (CP 1256)" ),
    wxTRANSLATE( "Windows Baltic (CP 1257)" ),
    wxTRANSLATE( "Windows/DOS OEM (CP 437)" ),
    wxTRANSLATE( "Unicode 7 bit (UTF-7)" ),
    wxTRANSLATE( "Unicode 8 bit (UTF-8)" ),
#ifdef WORDS_BIGENDIAN
    wxTRANSLATE( "Unicode 16 bit (UTF-16)" ),
    wxTRANSLATE( "Unicode 16 bit Little Endian (UTF-16LE)" ),
    wxTRANSLATE( "Unicode 32 bit (UTF-32)" ),
    wxTRANSLATE( "Unicode 32 bit Little Endian (UTF-32LE)" ),
#else // WORDS_BIGENDIAN
    wxTRANSLATE( "Unicode 16 bit Big Endian (UTF-16BE)" ),
    wxTRANSLATE( "Unicode 16 bit (UTF-16)" ),
    wxTRANSLATE( "Unicode 32 bit Big Endian (UTF-32BE)" ),
    wxTRANSLATE( "Unicode 32 bit (UTF-32)" ),
#endif // WORDS_BIGENDIAN
    wxTRANSLATE( "Extended Unix Codepage for Japanese (EUC-JP)" ),
    wxTRANSLATE( "US-ASCII" ),
    NULL
};

const wxString frmImport::IMPTABMARK = wxT("$(TAB)"), frmImport::IMPNONEQUOTEDBY = wxT("$(none)");
const wxString frmImport::IMPERRORDISP = _("<Error>"), frmImport::IMPNULLDISP = wxT("<NULL>"), frmImport::IMPDEFAULTDISP = _("<DEFAULT>");
const wxString frmImport::IMPEMPTYDISP = _("Empty"), frmImport::IMPCONSTDISP = _("Constant"), frmImport::IMPEXPRDISP = _("Expression"), frmImport::IMPIGNORERDISP = _("<IGNORE>");
const wxString frmImport::IMPCURTSDISP = wxT("current_timestamp"), frmImport::IMPCURDATEDISP = wxT("current_date"), frmImport::IMPCURTIMEDISP = wxT("current_time");
const wxString frmImport::IMPLOCALTSDISP = wxT("localtimestamp"), frmImport::IMPLOCALTIMEDISP = wxT("localtime"), frmImport::IMPCLIENTTIMEDISP = wxT("client time");
const wxString frmImport::IMPZERONUMBERDISP = wxT("0"), frmImport::IMPZEROINTERVALDISP = wxT("00:00:00");
const wxString frmImport::IMPBOOLTRUEDISP = wxT("true"), frmImport::IMPBOOLFALSEDISP = wxT("false");
const wxString frmImport::TRIMNONEDISP = _("None"), frmImport::TRIMALLDISP = _("All"), frmImport::TRIMLEFTDISP = _("Left"), frmImport::TRIMRIGHTDISP = _("Right");
const wxString frmImport::ANSICTIMETZFMT = wxT("%H:%M:%S %Z"), frmImport::ANSICTIMESTAMPFMT = wxT("%Y-%m-%d %H:%M:%S"), frmImport::ANSICTIMESTAMPTZFMT = wxT("%Y-%m-%d %H:%M:%S %Z");
const wxColour frmImport::YELLOW_COLOR = wxColour(255, 255, 0);
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
const wxString frmImport::BINDATADISP = _("<<Binary data>>"), frmImport::UNKNOWNTYPEDISP = _("<<Unknown type>>");
const wxString frmImport::UNSUPTYPEDISP = _("<<Unsupported type>>"), frmImport::MOREDATADISP = _("...<<more>>");
#endif
const wxString frmImport::IMPDESTCOPYDISP = _("COPY and commit"), frmImport::IMPDESTFCOPYDISP = _("COPY and commit, Generate file");
const wxString frmImport::IMPDESTINSDISP = _("INSERT and commit"), frmImport::IMPDESTFINSDISP = _("INSERT and commit, Generate file");
const wxString frmImport::IMPDESTTESTDISP = _("Test and rollback"), frmImport::IMPDESTFTESTDISP = _("Test and rollback, Generate file");
const wxString frmImport::IMPDESTFILEDISP = _("Generate file");

frmImport::frmImport(frmMain *_form, pgTable *_table)
{
    m_form = _form;
    m_table = _table;

    Init();
}
/*
frmImport::frmImport(frmMain *_form, pgConn *_conn, wxString *schemaname, wxString *tableoidstr, wxString *tablename)
{
    m_form = _form;
    m_conn = _conn;
    m_impschemaname = *schemaname;
    m_imptableoidstr = *tableoidstr;
    m_imptablename = *tablename;

    Init();
}
*/
frmImport::~frmImport()
{
    if (m_srcfile)
    {
        m_srcfile->Close();
        delete m_srcfile;
    }

    if (m_fileoptimpregex)
        delete m_fileoptimpregex;
    if (m_fileoptstartwith)
        delete m_fileoptstartwith;

    if (m_booltruevals)
        delete m_booltruevals;
    if (m_boolfalsevals)
        delete m_boolfalsevals;

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    if (m_srcodbc)
        delete m_srcodbc;

    wxString **odbcstmt = (wxString**)txtODBCSQLStmt->GetClientData();
    if (odbcstmt)
    {
        if (odbcstmt[0])
            delete odbcstmt[0];
        if (odbcstmt[1])
            delete odbcstmt[1];

        delete[] odbcstmt;
    }

#endif

    ClearTargetColsInfo();

    if (m_impconn)
        delete m_impconn;

    if (m_destfile)
    {
        m_destfile->Close();
        delete m_destfile;
    }

    if (m_logfile)
    {
        m_logfile->Close();
        delete m_logfile;
    }
}

void frmImport::Init()
{
    if (m_table)
    {
        m_conn = m_table->GetDatabase()->connection();//->Duplicate();
        m_impschemaname = m_table->GetSchema()->GetQuotedFullIdentifier();
        m_imptableoidstr = m_table->GetOidStr();
        m_imptablename = m_table->GetQuotedFullIdentifier();
    }

    wxWindowBase::SetFont(settings->GetSystemFont());
    wxXmlResource::Get()->LoadObject(this, (wxWindow*)m_form, wxT("frmImport"), wxT("wxWizard"));

    FitToPage(wzdTargetPage);

    m_txtfilepginit = m_archfilepginit = m_srcodbcpginit = m_srcpgsqlpginit = false;
    m_filesettingpginit = m_fileoptspginit = m_colsettingpginit = m_destlogpginit = false;

    m_impconn = NULL;

    m_tgtcolsnum = 0;
    m_tgtcolsnames = NULL;
    m_tgtcolstypes = NULL;
    m_tgtcolsisnotnull = NULL;
    m_tgtcolshasdef = NULL;
    m_tgtcolincopy = NULL;

    m_colsrccolnr = NULL;
    m_coltrimmode = NULL;
    m_colonemptyact = NULL;
    m_colfmtarr = NULL;
    m_colexprarr = NULL;
    m_srcrefnum = NULL;

    if (m_conn)
    {
        m_firstpagenr = SRCTYPEPAGE;
        wzdSrcTypePage->SetPrev(NULL);
        FetchTargetColsInfo();
    }
    else
        m_firstpagenr = TARGETPAGE;

    m_curpagenr = m_firstpagenr;
    m_origsrctype = m_datasrctype = INVALIDPAGE;

    wzdTextFilePage->SetNext(wzdFileSettingPage);
    wzdArchFilePage->SetPrev(wzdSrcTypePage);
    wzdArchFilePage->SetNext(wzdFileSettingPage);
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    wzdSrcODBCPage->SetPrev(wzdSrcTypePage);
    wzdSrcODBCPage->SetNext(wzdColsSettingPage);
    m_srcodbc = NULL;
    m_srcodbcisok = false;
#else
    rdbSrcODBC->Disable();
#endif
    wzdSrcPgSQLPage->SetPrev(wzdSrcTypePage);
    wzdSrcPgSQLPage->SetNext(wzdColsSettingPage);

    rdbSrcTextFile->SetValue(true);
    wzdSrcTypePage->SetNext(wzdTextFilePage);
    wzdFileSettingPage->SetPrev(wzdTextFilePage);
    wzdColsSettingPage->SetPrev(wzdFileOptsPage);

    if (pgCSVFile::CanHandleArchive())
        rdbSrcArchFile->Enable();
    else
        rdbSrcArchFile->Disable();

    rdbSrcPgSQL->Show(false);

    m_srcfile = NULL;

    m_fileoptimpregex = NULL;
    m_fileoptstartwith = NULL;
    m_booltruevals = NULL;
    m_boolfalsevals = NULL;

    m_colsettingerrnum = -1;
    m_colsettingiserr = NULL;

    gridColSetting->CreateGrid(0, 0);
    gridColSetting->SetRowLabelSize(40);
    gridColsSampleData->CreateGrid(0, 0);
    gridColsSampleData->SetRowLabelSize(40);
    gridColsSampleData->EnableEditing(false);

    m_procstatus = IMPPREPARED;
    m_destfile = NULL;
    m_logfile = NULL;

    Show();
}

bool frmImport::Go(bool modal)
{
    bool returncode;
    wxWizardPage *firstpage;

    if (m_firstpagenr==SRCTYPEPAGE)
        firstpage = wzdSrcTypePage;
    else
        firstpage = wzdTargetPage;

    if (modal)
        returncode = RunWizard(firstpage);
    else
        returncode = ShowPage(firstpage);

    SetSize(550, 600);
    CentreOnParent();

    return returncode;
}

void frmImport::OnPageChanging(wxWizardEvent& ev)
{
    bool isforward = ev.GetDirection();

    if (isforward && !CheckWizardPage())
    {
        ev.Veto();
        return;
    }

    wxButton* wzdctrlbtn;
    switch(m_curpagenr)
    {
    case TARGETPAGE:
        if (isforward)
            m_curpagenr = SRCTYPEPAGE;

        break;
    case SRCTYPEPAGE:
        if (!isforward)
        {
            m_curpagenr = TARGETPAGE;
            break;
        }

        m_origsrctype = m_datasrctype;
        if (rdbSrcArchFile->GetValue())
        {
            m_datasrctype = ARCHFILEPAGE;
            m_curpagenr = ARCHFILEPAGE;
        }
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
        else if (rdbSrcODBC->GetValue())
        {
            m_datasrctype = SRCODBCPAGE;
            m_curpagenr = SRCODBCPAGE;
        }
#endif
        else if (rdbSrcPgSQL->GetValue())
        {
            m_datasrctype = SRCPGSQLPAGE;
            m_curpagenr = SRCPGSQLPAGE;
        }
        else
        {
            m_datasrctype = TEXTFILEPAGE;
            m_curpagenr = TEXTFILEPAGE;
        }

        if (m_datasrctype!=m_origsrctype)
        {
            m_colsettingpginit = false;
            if (m_datasrctype!=TEXTFILEPAGE && m_datasrctype!=ARCHFILEPAGE)
            {
                if (m_srcfile)
                {
                    m_srcfile->Close();
                    delete m_srcfile;
                    m_srcfile = NULL;
                }
                if (m_fileoptimpregex)
                {
                    delete m_fileoptimpregex;
                    m_fileoptimpregex = NULL;
                }
                if (m_fileoptstartwith)
                {
                    delete m_fileoptstartwith;
                    m_fileoptstartwith = NULL;
                }
            }

            if ((m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE))
            {
                if (m_srcfile)
                    m_srcfile->Close();
                else
                    m_srcfile = new pgCSVFile;
            }

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
            if (m_datasrctype!=SRCODBCPAGE && m_srcodbc)
            {
                delete m_srcodbc;
                m_srcodbc = NULL;
            }
#endif

            InitWizardPage(m_datasrctype);
            ResetWizardPage(m_datasrctype);
        }

        break;
    case TEXTFILEPAGE:
        if (isforward)
        {
            m_curpagenr = FILESETTINGPAGE;

            m_srcfile->SetPreviewRowsNum(spinFilePreviewRows->GetValue());
            InitWizardPage(FILESETTINGPAGE);
            ResetWizardPage(FILESETTINGPAGE);
        }
        else
            m_curpagenr = SRCTYPEPAGE;

        break;
    case ARCHFILEPAGE:
        if (isforward)
        {
            m_curpagenr = FILESETTINGPAGE;

            m_srcfile->SetPreviewRowsNum(spinFilePreviewRows->GetValue());
            InitWizardPage(FILESETTINGPAGE);
            ResetWizardPage(FILESETTINGPAGE);
        }
        else
            m_curpagenr = SRCTYPEPAGE;

        break;
    case SRCPGSQLPAGE:
        if (isforward)
            m_curpagenr = COLSETTINGPAGE;
        else
            m_curpagenr = SRCTYPEPAGE;

        if (m_colsettingpginit)
            ResetColSettingPage();
        else
            InitColSettingPage();

        break;
    case SRCODBCPAGE:
        if (isforward)
            m_curpagenr = COLSETTINGPAGE;
        else
        {
            m_curpagenr = SRCTYPEPAGE;
            break;
        }

        if (m_colsettingpginit)
            ResetColSettingPage();
        else
            InitColSettingPage();

        break;
    case FILESETTINGPAGE:
        if (isforward)
        {
            m_curpagenr = FILEOPTSPAGE;
            InitWizardPage(FILEOPTSPAGE);
        }
        else
        {
            m_curpagenr = m_datasrctype;
            GenFilePreview();
        }

        break;
    case FILEOPTSPAGE:
        if (isforward)
            m_curpagenr = COLSETTINGPAGE;
        else
        {
            m_curpagenr = FILESETTINGPAGE;
            break;
        }

        if (m_colsettingpginit)
            ResetColSettingPage();
        else
            InitColSettingPage();

        break;
    case COLSETTINGPAGE:
        if (isforward)
        {
            m_curpagenr = DESTLOGPAGE;
            ResetWizardPage(DESTLOGPAGE);
        }
        else
        {
            if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
                m_curpagenr = FILEOPTSPAGE;
            else if (m_datasrctype==SRCODBCPAGE)
                m_curpagenr = SRCODBCPAGE;
            else
                m_curpagenr = m_datasrctype;
        }

        break;
    case DESTLOGPAGE:
        if (isforward)
        {
            m_curpagenr = PROCESSPAGE;

            wzdctrlbtn = (wxButton*)FindWindowById(wxID_FORWARD);
            if (m_procstatus==IMPSTOPED || m_procstatus==IMPFINISHED)
                wzdctrlbtn->Enable();
            else
                wzdctrlbtn->Disable();
        }
        else
            m_curpagenr = COLSETTINGPAGE;

        break;
    case PROCESSPAGE:
        if (isforward)
            m_curpagenr = PROCESSPAGE;
        else
        {
            m_curpagenr = DESTLOGPAGE;

            wzdctrlbtn = (wxButton*)FindWindowById(wxID_FORWARD);
            wzdctrlbtn->Enable();
        }

        break;
    default:
        break;
    }
}

void frmImport::OnWizardCancel(wxWizardEvent& ev)
{
    if (m_procstatus==IMPPREPARED || m_procstatus==IMPSTOPED ||
            m_procstatus==IMPERRORSTOP || m_procstatus==IMPFINISHED)
        return;
    else if (m_procstatus==IMPRUNNING)
    {
        DispStatusMsg(_("Stop import, please."));
        ev.Veto();
    }
    else if (m_procstatus==IMPPAUSING)
    {
        DispStatusMsg(_("Pausing import, please wait."));
        ev.Veto();
    }
    else if (m_procstatus==IMPSTOPING)
    {
        DispStatusMsg(_("Stoping import, please wait."));
        ev.Veto();
    }
    else if (m_procstatus==IMPPAUSED)
        OnProcImpStop(ev);
    else
        ev.Veto();
}

void frmImport::OnSourceType(wxCommandEvent &ev)
{
    if (rdbSrcArchFile->GetValue())
    {
        wzdSrcTypePage->SetNext(wzdArchFilePage);
        wzdFileSettingPage->SetPrev(wzdArchFilePage);
        wzdColsSettingPage->SetPrev(wzdFileOptsPage);
    }
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    else if (rdbSrcODBC->GetValue())
    {
        wzdSrcTypePage->SetNext(wzdSrcODBCPage);
        wzdColsSettingPage->SetPrev(wzdSrcODBCPage);
    }
#endif
    else if (rdbSrcPgSQL->GetValue())
    {
        wzdSrcTypePage->SetNext(wzdSrcPgSQLPage);
        wzdColsSettingPage->SetPrev(wzdSrcPgSQLPage);
    }
    else
    {
        wzdSrcTypePage->SetNext(wzdTextFilePage);
        wzdFileSettingPage->SetPrev(wzdTextFilePage);
        wzdColsSettingPage->SetPrev(wzdFileOptsPage);
    }
}

void frmImport::OnTextFileBrowse(wxCommandEvent &ev)
{
    wxFileDialog fileDlg(this, _("Select a text file"), wxGetHomeDir(), txtTextFilePath->GetValue(), 
        _("Text files (*.csv,*.txt)|*.csv;*.txt|CSV files (*.csv)|*.csv|Text files (*.txt)|*.txt|Data files (*.dat)|*.dat|All Files(*.*)|*.*"),
        wxFD_FILE_MUST_EXIST | wxFD_CHANGE_DIR);

    if (fileDlg.ShowModal() == wxID_OK)
    {
        wxString fpath = fileDlg.GetPath();
        if (fpath.CompareTo(txtTextFilePath->GetValue()))
        {
            txtTextFilePath->SetValue(fpath);
            OnTextFilePathChange(fpath);
            txtTextFilePath->SetModified(false);
        }
    }
}

void frmImport::OnTextFilePathKillFocus(wxFocusEvent &ev)
{
    if (txtTextFilePath->IsModified())
    {
        OnTextFilePathChange(txtTextFilePath->GetValue());
        txtTextFilePath->SetModified(false);
    }
}

void frmImport::OnTextFilePathChange(const wxString& fpath)
{
    if (fpath==m_srcfile->GetFilePath())
        return;

    m_srcfile->Open(fpath);

    GenFilePreview();
}

void frmImport::OnTextFileEncoding(wxCommandEvent &ev)
{
    m_srcfile->SetFileEncoding(gs_encodings[chcTextFileEncoding->GetSelection()]);
    GenFilePreview();
}

void frmImport::OnTextFilePreviewMore(wxCommandEvent &ev)
{
    wxString filepvcont;
    if (m_srcfile->GenMoreFilePreview(&filepvcont)!=wxInvalidOffset)
    {
        txtTextFileCont->Freeze();
        txtTextFileCont->AppendText(filepvcont);
        txtTextFileCont->Thaw();
    }
}

void frmImport::OnArchFileBrowse(wxCommandEvent &ev)
{
    wxFileDialog fileDlg(this, _("Select archive file"), wxGetHomeDir(), txtTextFilePath->GetValue(), 
        _("Archive files (*.zip,*.tar)|*.zip;*.tar|Zip files (*.zip)|*.zip|Tar files (*.tar)|*.tar|All Files(*.*)|*.*"),
        wxFD_FILE_MUST_EXIST | wxFD_CHANGE_DIR);

    if (fileDlg.ShowModal()!=wxID_OK)
        return;

    wxString fpath = fileDlg.GetPath();
    if (fpath.CompareTo(txtArchFilePath->GetValue()))
    {
        txtArchFilePath->SetValue(fpath);
        OnArchFileChange(&fpath);
        txtArchFilePath->SetModified(false);
    }

}

void frmImport::OnArchFilePathKillFocus(wxFocusEvent &ev)
{
    if (txtArchFilePath->IsModified())
    {
        wxString archfilepath = txtArchFilePath->GetValue();
        OnArchFileChange(&archfilepath);
        txtArchFilePath->SetModified(false);
    }
}

void frmImport::OnArchFileChange(const wxString* fpath)
{
    wxLog::GetActiveTarget()->SetLogLevel(LOG_NONE);

    chcArchEntryName->Clear();
    chcArchEntryName->Append(wxT(""), (void*)NULL);

    if (fpath)
        m_srcfile->Open(*fpath, true);

    size_t entriesnum = m_srcfile->GetEntriesNum();
    size_t entriesnr = 0;
    for (; entriesnr<entriesnum; entriesnr++)
    {
        chcArchEntryName->Append(m_srcfile->GetEntryName(entriesnr), m_srcfile->GetEntry(entriesnr));
        if (m_srcfile->GetEntry(entriesnr)==m_srcfile->GetCurrentEntry())
            chcArchEntryName->SetSelection(entriesnr+1);
    }

    wxLog::GetActiveTarget()->SetLogLevel(settings->GetLogLevel());

    GenFilePreview();
}

void frmImport::OnArchFileEncoding(wxCommandEvent &ev)
{
    if (gs_encodings[chcArchFileEncoding->GetSelection()]==m_srcfile->GetFileEncoding())
        return;

    m_srcfile->SetFileEncoding(gs_encodings[chcArchFileEncoding->GetSelection()]);
    OnArchFileChange();
}

void frmImport::OnArchEntryName(wxCommandEvent &ev)
{
    if (m_srcfile->GetCurrentEntry()==chcArchEntryName->GetClientData(ev.GetSelection()))
        return;

    m_srcfile->OpenArchiveEntry(chcArchEntryName->GetClientData(ev.GetSelection()));

    GenFilePreview();
}

void frmImport::OnArchEntryEncoding(wxCommandEvent &ev)
{
    m_srcfile->SetEntryEncoding(gs_encodings[chcArchEntryEncoding->GetSelection()]);
    GenFilePreview();
}

void frmImport::OnArchFilePreviewMore(wxCommandEvent &ev)
{
    wxString filepvcont;
    if (m_srcfile->GenMoreFilePreview(&filepvcont)!=wxInvalidOffset)
    {
        txtArchFileCont->Freeze();
        txtArchFileCont->AppendText(filepvcont);
        txtArchFileCont->Thaw();
    }
}

void frmImport::OnFileDelimiterType(wxCommandEvent &ev)
{
    if (rdbFileCharDelimiter->GetValue())
    {
        cbFileCharDelimiter->Enable();
        txtFileWidthDelimiter->Disable();
        m_srcfile->SetDelimitedByChar();
    }
    else
    {
        cbFileCharDelimiter->Disable();
        txtFileWidthDelimiter->Enable();
        m_srcfile->SetDelimitedByWidth();
        if (txtFileWidthDelimiter->IsModified())
        {
            m_srcfile->SetWidthDelimiter(txtFileWidthDelimiter->GetValue());
            txtFileWidthDelimiter->SetModified(false);
        }
    }

    GenCSVPreview();
}

void frmImport::OnFileCharDelimiter(wxCommandEvent &ev)
{
    wxString chardlm = cbFileCharDelimiter->GetValue();

    if (!chardlm.Cmp(IMPTABMARK))
    {
        m_srcfile->SetCharDelimiter(wxT("\t"));
        GenCSVPreview();
        return;
    }

    wxString parseddml;
    const wxChar *dmldata = chardlm.GetData();
    size_t dlmcidx = 0;
    while (dmldata[dlmcidx])
    {
        if (dmldata[dlmcidx]==wxT('$'))
        {
            if (dmldata[dlmcidx+1])
            {
                if (dmldata[dlmcidx+1]==wxT('$'))
                    parseddml.Append(dmldata[dlmcidx]);
                else if (dmldata[dlmcidx+1]==wxT(')'))
                    parseddml.Append(dmldata[dlmcidx+1]);
                else
                    parseddml.Append(dmldata[dlmcidx]).Append(dmldata[dlmcidx+1]);

                dlmcidx++;
            }
            else
                parseddml.Append(dmldata[dlmcidx]);
        }
        else
            parseddml.Append(dmldata[dlmcidx]);

        dlmcidx++;
    }

    if (parseddml.Cmp(m_srcfile->GetCharDelimiter()))
    {
        m_srcfile->SetCharDelimiter(parseddml);
        GenCSVPreview();
    }
}

void frmImport::OnFileFixedWidthKillFocus(wxFocusEvent &ev)
{
    if (txtFileWidthDelimiter->IsModified())
    {
        m_srcfile->SetWidthDelimiter(txtFileWidthDelimiter->GetValue());
        GenCSVPreview();
        txtFileWidthDelimiter->SetModified(false);
    }
}

void frmImport::OnFileQuotedBy(wxCommandEvent &ev)
{
    wxChar quotedchar;
    if (cbFileQuotedBy->GetValue()==IMPNONEQUOTEDBY)
        quotedchar = wxT('\0');
    else
        quotedchar = cbFileQuotedBy->GetValue().GetChar(0);

    m_srcfile->SetQuotedBy(quotedchar);

    GenCSVPreview();
}

void frmImport::OnFileFirstRowIsHeader(wxCommandEvent &ev)
{
    m_srcfile->SetFirstRowIsHeader(ev.IsChecked());
    GenCSVPreview();
}

void frmImport::OnFileForceLineBreak(wxCommandEvent &ev)
{
    m_srcfile->SetForceLineBreak(chkFileForceLineBreak->GetValue());
    GenCSVPreview();
}

void frmImport::OnFileBackSlashQuote(wxCommandEvent &ev)
{
    m_srcfile->SetBackSlashQuote(chkFileBackSlashQuote->GetValue());
    GenCSVPreview();
}

void frmImport::OnFilePreviewRows(wxSpinEvent &ev)
{
    size_t previewnum = (size_t)spinFilePreviewRows->GetValue();

    m_srcfile->SetPreviewRowsNum(previewnum);
    GenCSVPreview();
}

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)

void frmImport::OnODBCReset(wxCommandEvent &ev)
{
    ResetWizardPage(SRCODBCPAGE);

    if (m_srcodbc)
        if (m_srcodbc->IsConnected())
            m_srcodbc->Disconnect();

    FetchODBCInfo();
}

void frmImport::OnODBCConnect(wxCommandEvent &ev)
{
    
    if (m_srcodbc && m_srcodbc->IsConnected())
    {
        m_srcodbc->Disconnect();

        ResetWizardPage(SRCODBCPAGE);
        FetchODBCInfo();

        return;
    }

    bool connected = false;
    if (m_srcodbc)
    {
        connected = m_srcodbc->Connect(cbODBCDSNNames->GetValue(),
                txtODBCUsername->GetValue(), txtODBCPassword->GetValue());
    }
    else
    {
        m_srcodbc = new pgODBC(cbODBCDSNNames->GetValue(),
                txtODBCUsername->GetValue(), txtODBCPassword->GetValue());

        connected = m_srcodbc->Connect();
    }

    if (!connected)
    {
        FetchODBCInfo();
        DispStatusMsg(m_srcodbc->GetErrorMsg());
        return;
    }

    cbODBCDSNNames->Disable();
    txtODBCUsername->Disable();
    txtODBCPassword->Disable();
    btnODBCConnect->SetLabel(_("Disconnect"));

    FetchODBCInfo();

    cbODBCTableNames->Freeze();
    cbODBCTableNames->Clear();
    cbODBCTableNames->Append(wxEmptyString);

    SQLINTEGER tableidx = 0;
    const SQLWCHAR **tablelist = m_srcodbc->GetTableList();
    const SQLINTEGER tablenum = m_srcodbc->GetTableNum();
    const SQLWCHAR *qualifiedname;
    int itemidx;
    for (; tableidx<tablenum; tableidx++)
    {
        qualifiedname = m_srcodbc->GetQualifiedName(tablelist + tableidx*3);
        itemidx = cbODBCTableNames->Append(qualifiedname);
        cbODBCTableNames->SetClientData(itemidx, tablelist + tableidx*3);
        delete[] qualifiedname;
    }

    cbODBCTableNames->FitInside();
    cbODBCTableNames->Thaw();
}

void frmImport::OnODBCTableNames(wxCommandEvent &ev)
{
    int tableidx = cbODBCTableNames->GetSelection();

    const SQLWCHAR **tableinfo = NULL;
    if (tableidx)
        tableinfo = (const SQLWCHAR**)cbODBCTableNames->GetClientData(tableidx);
    if (cbODBCTableNames->GetClientData()==tableinfo)
        return;

    cbODBCTableNames->SetClientData(tableinfo);

    lstODBCTableCols->DeleteAllItems();
    if (!tableinfo)
        return;

    const SQLWCHAR*** colsinfo = m_srcodbc->GetTableInfo((SQLWCHAR**)tableinfo);
    const SQLWCHAR*** colsinf;

    if (colsinfo)
        for (colsinf=colsinfo; *colsinf; colsinf++)
            lstODBCTableCols->AppendItem((*colsinf)[0], (*colsinf)[1], (*colsinf)[2]);

    GenODBCSelectStmt(colsinfo);
    if (!chkODBCCustomizedSQL->GetValue())
        ClearODBCPreview();
}

void frmImport::OnODBCCustomizedSQL(wxCommandEvent &ev)
{
    wxString **odbcstmt;
    if (chkODBCCustomizedSQL->GetValue())
    {
        btnODBCPreview->Enable(true);
        txtODBCWhereCl->Enable(false);
        txtODBCSQLStmt->SetEditable(true);

        odbcstmt = (wxString**)txtODBCSQLStmt->GetClientData();
        if (!odbcstmt[1]->IsEmpty())
        {
            txtODBCSQLStmt->ChangeValue(*odbcstmt[1]);
            txtODBCSQLStmt->SetModified(false);
        }
    }
    else
    {
        wxString **odbcstmt = (wxString**)txtODBCSQLStmt->GetClientData();
        if (txtODBCSQLStmt->IsEmpty())
            odbcstmt[1]->Clear();
        else
        {
            if (txtODBCSQLStmt->IsModified())
            {
                odbcstmt[1]->Clear();
                odbcstmt[1]->Append(txtODBCSQLStmt->GetValue());
            }
            txtODBCSQLStmt->Clear();
        }

        if (odbcstmt[0]->IsEmpty())
            btnODBCPreview->Enable(false);
        else
        {
            // TODO: use pgODBC class to check if the statement is valid.(length)
            txtODBCSQLStmt->AppendText(*odbcstmt[0]);
            if (!txtODBCWhereCl->IsEmpty())
            {
                txtODBCSQLStmt->AppendText(wxT(" "));
                txtODBCSQLStmt->AppendText(txtODBCWhereCl->GetValue());
            }

            btnODBCPreview->Enable(true);
        }

        txtODBCWhereCl->Enable(true);
        txtODBCSQLStmt->SetEditable(false);
    }
}

void frmImport::OnODBCWhereCl(wxCommandEvent &ev)
{
    if (txtODBCWhereCl->IsEmpty())
        return;

    wxString **clientdata = (wxString**)txtODBCSQLStmt->GetClientData();
    if (!clientdata)
        return;

    txtODBCSQLStmt->ChangeValue(*clientdata[0]);
    txtODBCSQLStmt->AppendText(wxT(" WHERE "));
    txtODBCSQLStmt->AppendText(txtODBCWhereCl->GetValue());
}

void frmImport::OnODBCPreview(wxCommandEvent &ev)
{
    GenODBCPreview();
}
#endif

void frmImport::OnFileOptsImpRows(wxCommandEvent &ev)
{
    if (rdbFileImpOnlyRows->GetValue())
        spinFileImpOnlyRows->Enable();
    else
        spinFileImpOnlyRows->Disable();
}

void frmImport::OnFileOptsCheckTrueFirst(wxCommandEvent &ev)
{
    if (chkFileCheckTrueFirst->GetValue())
    {
        txtFileOptTrueVals->Enable();
        txtFileOptFalseVals->Disable();
    }
    else
    {
        txtFileOptTrueVals->Disable();
        txtFileOptFalseVals->Enable();
    }
}

void frmImport::OnColSettingImportMode(wxCommandEvent &ev)
{
    ClearGrid(gridColsSampleData);
    txtColsSettingLog->ChangeValue(wxEmptyString);
    txtColsImpLog->ChangeValue(wxEmptyString);

    gridColSetting->Freeze();

    ResetColSettingSrcColEditor(false);
    ResetColSettingOnEmptyColEditor();

    ResetColSettingFmtColEditor();

    ParseColsImpSetting();

    gridColSetting->Thaw();
}

void frmImport::OnColSettingFormatStyle(wxCommandEvent &ev)
{
    ClearGrid(gridColsSampleData);
    txtColsSettingLog->ChangeValue(wxEmptyString);
    txtColsImpLog->ChangeValue(wxEmptyString);

    gridColSetting->Freeze();

    ResetColSettingFmtColEditor();

    ParseColsImpSetting();

    gridColSetting->Thaw();
}

void frmImport::OnColSettingCellChange(wxGridEvent &ev)
{
    if (m_colsettingerrnum==-1)
        return;

    int evrownr = ev.GetRow();

    ResetColSettingOnEmptyColEditor(evrownr);
    ParseColsImpSetting(evrownr);
}

void frmImport::OnColSettingImpPreview(wxCommandEvent &ev)
{
    if (m_colsettingerrnum==-1)
        ParseColsImpSetting();

    if (!m_colsettingerrnum)
        GenColsPreview();
}

void frmImport::OnColSettingTestImp(wxCommandEvent &ev)
{
    if (ev.IsChecked())
    {
        chkValidateExpr->SetValue(true);
        chkValidateExpr->Disable();
    }
    else
        chkValidateExpr->Enable();
}


void frmImport::OnDestImpDest(wxCommandEvent &ev)
{
    int impdest = (int)chcImpDest->GetClientData(chcImpDest->GetSelection());
    if (impdest==IMPDESTFCOPY || impdest==IMPDESTFINS || impdest==IMPDESTFTEST || impdest==IMPDESTFILE)
    {
        txtDestFilePath->Enable();
        btnDestFileBrowse->Enable();
        chkDestFileAppend->Enable();
    }
    else
    {
        txtDestFilePath->Disable();
        btnDestFileBrowse->Disable();
        chkDestFileAppend->Disable();
    }
}

void frmImport::OnDestFileBrowse(wxCommandEvent &ev)
{
    wxFileDialog fileDlg(this, _("Choose a output file"), wxGetHomeDir(), txtTextFilePath->GetValue(), 
        wxT("All Files(*.*)|*.*"), wxFD_SAVE | wxFD_CHANGE_DIR);

    if (fileDlg.ShowModal()==wxID_OK)
    {
        wxString fpath = fileDlg.GetPath();

        if (fpath.CompareTo(txtDestFilePath->GetValue()) && OnDestFilePathChange(fpath))
        {
            txtDestFilePath->ChangeValue(fpath);
            txtDestFilePath->SetModified(false);
        }
    }
}

bool frmImport::OnDestFilePathChange(const wxString& filepath)
{
    if (!chkDestFileAppend->GetValue() && wxFileExists(filepath))
    {
        wxString msg;
        msg.Printf(_("The output file: \n\n%s\n\nalready exists. Do you want to overwrite it?"), filepath.c_str());

        if (wxMessageBox(msg, _("Overwrite file?"), wxYES_NO | wxICON_QUESTION) == wxNO)
            return false;
    }

    return true;
}

void frmImport::OnDestLogTo(wxCommandEvent &ev)
{
    if (rdbLogToScreen->GetValue())
    {
        txtLogFilePath->Disable();
        btnLogFileBrowse->Disable();
        chkLogFileAppend->Disable();
    }
    else
    {
        txtLogFilePath->Enable();
        btnLogFileBrowse->Enable();
        chkLogFileAppend->Enable();
    }
}

void frmImport::OnLogFileBrowse(wxCommandEvent &ev)
{
    wxFileDialog fileDlg(this, _("Choose a log file"), wxGetHomeDir(), txtTextFilePath->GetValue(), 
        wxT("All Files(*.*)|*.*"), wxFD_SAVE | wxFD_CHANGE_DIR);

    if (fileDlg.ShowModal() == wxID_OK)
    {
        wxString fpath = fileDlg.GetPath();

        if (fpath.CompareTo(txtLogFilePath->GetValue()) && OnLogFilePathChange(fpath))
        {
            txtLogFilePath->ChangeValue(fpath);
            txtLogFilePath->SetModified(false);
        }
    }
}

bool frmImport::OnLogFilePathChange(const wxString& filepath)
{
    if (!chkLogFileAppend->GetValue() && wxFileExists(filepath))
    {
        wxString msg;
        msg.Printf(_("The log file: \n\n%s\n\nalready exists. Do you want to overwrite it?"), filepath.c_str());

        if (wxMessageBox(msg, _("Overwrite file?"), wxYES_NO | wxICON_QUESTION) == wxNO)
            return false;
    }

    return true;
}

void frmImport::OnLogTypeChange(wxCommandEvent &ev)
{
    if (rdbLogVerbose->GetValue())
    {
        chkLogError->SetValue(true);
        chkLogWarn->SetValue(true);
        chkLogStmt->SetValue(true);
        chkLogInfo->SetValue(true);
        chkLogProc->SetValue(true);
    }
    else if (rdbLogSummary->GetValue())
    {
        chkLogError->SetValue(true);
        chkLogWarn->SetValue(true);
        chkLogStmt->SetValue(false);
        chkLogInfo->SetValue(false);
        chkLogProc->SetValue(true);
    }
    else
    {
        chkLogError->SetValue(false);
        chkLogWarn->SetValue(false);
        chkLogStmt->SetValue(false);
        chkLogInfo->SetValue(false);
        chkLogProc->SetValue(true);
    }
}

void frmImport::OnProcImpStart(wxCommandEvent &ev)
{
    if (m_procstatus==IMPPAUSED || m_procstatus==IMPPREPARED)
        ImportData();
    else if (m_procstatus==IMPRUNNING)
    {
        m_procstatus = IMPPAUSING;
        btnImpStart->Disable();
        btnImpStop->Disable();
    }
}

void frmImport::OnProcImpStop(wxCommandEvent &ev)
{
    if (m_procstatus==IMPRUNNING || m_procstatus==IMPPAUSED)
    {
        if (wxMessageBox(_("Stop import?"), _("Stop import?"), wxYES_NO | wxICON_QUESTION)==wxYES)
        {
            btnImpStart->Disable();
            btnImpStop->Disable();
            if (m_procstatus==IMPPAUSED)
            {
                m_procstatus = IMPSTOPING;
                ImportData();
            }
            else
                m_procstatus = IMPSTOPING;
        }
        else if (ev.GetEventType()==wxEVT_WIZARD_CANCEL)
            ((wxWizardEvent*)&ev)->Veto();
    }
}

void frmImport::ImportData()
{
    wxString errmsg;
    wxButton* wzdctrlbtn;

    bool logtoscr, logtofile;
    if (rdbLogToScrFile->GetValue())
    {
        logtoscr = true;
        logtofile = true;
    }
    else if (rdbLogToFile->GetValue())
    {
        logtoscr = false;
        logtofile = true;
    }
    else
    {
        logtoscr = true;
        logtofile = false;
    }

    bool logerror = chkLogError->GetValue();
    //bool logwarn = chkLogWarn->GetValue();
    bool logstmt = chkLogStmt->GetValue();
    bool loginfo = chkLogInfo->GetValue();
    bool logproc = chkLogProc->GetValue();

    int impmode = (int)chcImportMode->GetClientData(chcImportMode->GetSelection());
    int impdest = (int)chcImpDest->GetClientData(chcImpDest->GetSelection());

    bool fileopened;
    if (m_procstatus==IMPPREPARED)
    {
        txtLogMessages->Clear();

        if (!m_destfile && (impdest==IMPDESTFCOPY || impdest==IMPDESTFINS || impdest==IMPDESTFTEST || impdest==IMPDESTFILE))
        {
            wxString destfilename = txtDestFilePath->GetValue();
            if (wxFile::Exists(destfilename) && !wxFile::Access(destfilename, wxFile::write))
            {
                errmsg.Printf(_("Can't access file: %s"), destfilename.c_str());
                txtLogMessages->AppendText(errmsg);
                txtLogMessages->AppendText(END_OF_LINE);

                return;
            }
            m_destfile = new wxFile;
            if (chkDestFileAppend->GetValue())
                fileopened = m_destfile->Open(destfilename, wxFile::write_append);
            else
                fileopened = m_destfile->Open(destfilename, wxFile::write);
            if (!fileopened)
            {
                delete m_destfile;
                m_destfile = NULL;
                errmsg.Printf(_("Can't open file: %s"), destfilename.c_str());
                txtLogMessages->AppendText(errmsg);
                txtLogMessages->AppendText(END_OF_LINE);

                return;
            }
        }

        if (logtofile && !m_logfile)
        {
            wxString logfilename = txtLogFilePath->GetValue();
            if (wxFile::Exists(logfilename) && !wxFile::Access(logfilename, wxFile::write))
            {
                errmsg.Printf(_("Can't access file: %s"), logfilename.c_str());
                txtLogMessages->AppendText(errmsg);
                txtLogMessages->AppendText(END_OF_LINE);

                return;
            }
            m_logfile = new wxFile;
            if (chkLogFileAppend->GetValue())
                fileopened = m_logfile->Open(logfilename, wxFile::write_append);
            else
                fileopened = m_logfile->Open(logfilename, wxFile::write);
            if (!fileopened)
            {
                delete m_logfile;
                m_logfile = NULL;
                errmsg.Printf(_("Can't open file: %s"), logfilename.c_str());
                txtLogMessages->AppendText(errmsg);
                txtLogMessages->AppendText(END_OF_LINE);

                return;
            }
        }

        if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
        {
            if (!m_srcfile->IsOk())
                return;

            m_srcsize = m_srcfile->Length();

            if (!m_srcfile->EndPreview())
            {
                if (logerror)
                {
                    txtLogMessages->AppendText(m_srcfile->GetErrorMsg());
                    txtLogMessages->AppendText(END_OF_LINE);
                    if (m_logfile)
                    {
                        m_logfile->Write(m_srcfile->GetErrorMsg());
                        m_logfile->Write(END_OF_LINE);
                    }
                }

                return;
            }
        }
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
        else if (m_datasrctype==SRCODBCPAGE)
        {
            if (m_srcodbc->ReexecPrevQuery())
            {
                m_srcsize = m_srcodbc->GetResultRowsNum();
                if (m_srcsize==-1)
                    gaugeImp->SetRange(100);
            }
            else
            {
                txtLogMessages->AppendText(m_srcodbc->GetErrorMsg());
                txtLogMessages->AppendText(END_OF_LINE);

                return;
            }
        }
#endif
        else
            return;

        if (impdest!=IMPDESTFILE && !m_impconn)
        {
            m_impconn = m_conn->Duplicate();
            if (m_impconn)
            {
                txtLogMessages->AppendText(_("Import connection duplicated."));
                txtLogMessages->AppendText(END_OF_LINE);
                if (loginfo && m_logfile)
                {
                    m_logfile->Write(_("Import connection duplicated."));
                    m_logfile->Write(END_OF_LINE);
                }
            }
            else
            {
                if (logerror)
                {
                    txtLogMessages->AppendText(_("Couldn't create a connection object!"));
                    txtLogMessages->AppendText(END_OF_LINE);
                    if (m_logfile)
                    {
                        m_logfile->Write(_("Couldn't create a connection object!"));
                        m_logfile->Write(END_OF_LINE);
                    }
                }

                return;
            }
            if (m_impconn->GetStatus()!=PGCONN_OK)
            {
                if (logerror)
                {
                    if (logtoscr)
                        txtLogMessages->AppendText(m_impconn->GetLastError());
                    if (m_logfile)
                        m_logfile->Write(m_impconn->GetLastError());
                }

                delete m_impconn;
                m_impconn = NULL;
                return;
            }
            m_impconn->SetNonblocking();
        }

        if (m_impconn && m_impconn->ExecuteVoid(wxT("BEGIN TRANSACTION;"), false))
        {
            if (logstmt)
            {
                if (logtoscr)
                {
                    txtLogMessages->AppendText(wxT("BEGIN TRANSACTION;"));
                    txtLogMessages->AppendText(END_OF_LINE);
                }
                if (m_logfile)
                {
                    m_logfile->Write(wxT("BEGIN TRANSACTION;"));
                    m_logfile->Write(END_OF_LINE);
                }
            }

            txtLogMessages->AppendText(_("Transaction begin."));
            txtLogMessages->AppendText(END_OF_LINE);
            if (loginfo && m_logfile)
            {
                m_logfile->Write(_("Transaction begin."));
                m_logfile->Write(END_OF_LINE);
            }
        }
        else
        {
            if (logerror)
            {
                if (logtoscr)
                    txtLogMessages->AppendText(m_impconn->GetLastError());
                if (m_logfile)
                    m_logfile->Write(m_impconn->GetLastError());
            }

            return;
        }

        if (impmode==IMPORTMODECOPY)
        {
            if (m_destfile)
            {
                m_destfile->Write(m_impsqlstmt);
                m_destfile->Write(END_OF_LINE);
            }
            if (impdest!=IMPDESTFILE)
            {
                if (m_impconn->PutCopyBegin(m_impsqlstmt))
                {
                    if (logstmt)
                    {
                        if (logtoscr)
                        {
                            txtLogMessages->AppendText(m_impsqlstmt);
                            txtLogMessages->AppendText(END_OF_LINE);
                        }
                        if (m_logfile)
                        {
                            m_logfile->Write(m_impsqlstmt);
                            m_logfile->Write(END_OF_LINE);
                        }
                    }

                    txtLogMessages->AppendText(_("COPY Begin."));
                    txtLogMessages->AppendText(END_OF_LINE);
                    if (loginfo && m_logfile)
                    {
                        m_logfile->Write(_("COPY Begin."));
                        m_logfile->Write(END_OF_LINE);
                    }
                }
                else
                {
                    if (logerror)
                    {
                        if (logtoscr)
                            txtLogMessages->AppendText(m_impconn->GetLastError());
                        if (m_logfile)
                            m_logfile->Write(m_impconn->GetLastError());
                    }
                    m_impconn->ExecuteVoid(wxT("ROLLBACK;"), false);

                    return;
                }
            }
        }

        wzdTargetPage->Disable();
        wzdSrcTypePage->Disable();
        wzdTextFilePage->Disable();
        wzdArchFilePage->Disable();
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
        wzdSrcODBCPage->Disable();
#endif
        wzdSrcPgSQLPage->Disable();
        wzdFileSettingPage->Disable();
        wzdFileOptsPage->Disable();
        wzdColsSettingPage->Disable();
        wzdDestLogPage->Disable();

        m_impstartdt = wxDateTime::UNow();

        m_procstatus = IMPRUNNING;
        btnImpStart->SetLabel(_("&Pause"));
        btnImpStop->Enable();

        wzdctrlbtn = (wxButton*)FindWindowById(wxID_BACKWARD);
        wzdctrlbtn->Disable();
        wzdctrlbtn = (wxButton*)FindWindowById(wxID_FORWARD);
        wzdctrlbtn->Disable();

        m_imperrnum = 0;
        m_improwsnum = 0;
        m_skiprowsnum = 0;
        m_transrowsnum = 0;
        RandomSavepointName(m_imptransname);

        wxLog::GetActiveTarget()->SetLogLevel(LOG_NONE);
    }
    else if (m_procstatus==IMPPAUSED)
    {
        m_imppausedts += wxDateTime::UNow() - m_imppausedt;

        m_procstatus = IMPRUNNING;
        btnImpStart->SetLabel(_("&Pause"));
        wzdctrlbtn = (wxButton*)FindWindowById(wxID_BACKWARD);
        wzdctrlbtn->Disable();
        wzdctrlbtn = (wxButton*)FindWindowById(wxID_FORWARD);
        wzdctrlbtn->Disable();

        wxLog::GetActiveTarget()->SetLogLevel(LOG_NONE);
    }
    else if (m_procstatus==IMPSTOPING)
    {
        if (impmode==IMPORTMODECOPY && m_destfile)
            m_destfile->Write(wxT("\\."));

        if (impdest!=IMPDESTFILE)
        {
            wxLog::GetActiveTarget()->SetLogLevel(LOG_NONE);

            if (m_impconn->ExecuteVoid(wxT("ROLLBACK;"), false))
            {
                txtLogMessages->AppendText(_("Tranction rollbacked."));
                txtLogMessages->AppendText(END_OF_LINE);
                if (m_logfile)
                {
                    m_logfile->Write(_("Tranction rollbacked."));
                    m_logfile->Write(END_OF_LINE);
                }
                m_procstatus = IMPSTOPED;
            }
            else
            {
                txtLogMessages->AppendText(_("Failed to rollback transaction. ") + m_impconn->GetLastError());
                if (m_logfile)
                    m_logfile->Write(_("Failed to rollback transaction. ") + m_impconn->GetLastError());

                m_procstatus = IMPERRORSTOP;
            }

            wxLog::GetActiveTarget()->SetLogLevel(settings->GetLogLevel());
        }
        else
            m_procstatus = IMPSTOPED;

        if (m_destfile)
        {
            m_destfile->Close();
            delete m_destfile;
            m_destfile = NULL;
        }

        if (m_logfile)
        {
            m_logfile->Close();
            delete m_logfile;
            m_logfile = NULL;
        }

        return;
    }

    int fmtstyle = (int)chcFormatStyle->GetClientData(chcFormatStyle->GetSelection());

    bool skipemptyline;
    int skipfirstrows;
    int imponlyrows;
    if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
    {
        skipemptyline = chkFileSkipEmptyRow->GetValue();
        skipfirstrows = spinFileSkipFirstRows->GetValue();
        if (rdbFileImpAllRows->GetValue())
            imponlyrows = 0;
        else
            imponlyrows = spinFileImpOnlyRows->GetValue();
    }
    else
    {
        skipemptyline = false;
        skipfirstrows = false;
    }

    unsigned int commitevery = spinCommitEvery->GetValue();
    unsigned int stoponerror = spinStopOnError->GetValue();

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    SQLSMALLINT stmtcolsnum;
    SQLSMALLINT *odbccoltypes;

    SQLSMALLINT odbccoltype;
    SQLUSMALLINT odbccolidx;
    SQLRETURN odbcsqlrc;
    SQLWCHAR odbcdatabuff[IMPDATABUFFLEN];
    SQLINTEGER indptr, coldatalen;

    SQLWCHAR* coldata;
    SQLWCHAR** odbcrowdata;

    if (m_datasrctype==SRCODBCPAGE)
    {
        stmtcolsnum = m_srcodbc->GetResultColsNum();
        odbccoltypes = m_srcodbc->GetResultColTypes();

        odbcrowdata = new SQLWCHAR*[stmtcolsnum];
        memset(odbcrowdata, NULL, sizeof(SQLWCHAR*)*stmtcolsnum);
    }
#endif

    wxString linedata, logmsg;
    wxString **linevals;
    int valsnum;
    wxDateTime clienttime;
    wxString impdatalist, insstmt, rowlog;
    wxChar imptranssn[32];
    bool needflush = false;
    bool rowdataisok, lineisskipped = false;
    int commres;

    while (true)
    {
        if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
        {
            lineisskipped = false;
            linedata.Empty();
            linevals = NULL;
            if (!m_srcfile->NextLine(linedata, linevals, valsnum))
            {
                if (logerror)
                {
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(m_srcfile->GetErrorMsg());
                        txtLogMessages->AppendText(END_OF_LINE);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(m_srcfile->GetErrorMsg());
                        m_logfile->Write(END_OF_LINE);
                    }
                }

                m_procstatus = IMPERRORSTOP;
                break;
            }

            if (!linevals) // Eof
            {
                if (loginfo)
                {
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(_("Reached EOF."));
                        txtLogMessages->AppendText(END_OF_LINE);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(_("Reached EOF."));
                        m_logfile->Write(END_OF_LINE);
                    }
                }

                break;
            }

            if (skipfirstrows && m_srcfile->GetCurCSVLineNr()<=(size_t)skipfirstrows)
            {
                if (loginfo)
                {
                    logmsg.Printf(_("Skip first %d/%d row(s)"), m_srcfile->GetCurCSVLineNr(), skipfirstrows);
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(logmsg);
                        txtLogMessages->AppendText(END_OF_LINE);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(logmsg);
                        m_logfile->Write(END_OF_LINE);
                    }
                }

                lineisskipped = true;
            }

            if (skipemptyline && linedata.IsEmpty())
            {
                if (loginfo)
                {
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(_("Skip empty row."));
                        txtLogMessages->AppendText(END_OF_LINE);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(_("Skip empty row."));
                        m_logfile->Write(END_OF_LINE);
                    }
                }

                lineisskipped = true;
            }

            if (m_fileoptimpregex && !m_fileoptimpregex->Matches(linedata))
            {
                if (loginfo)
                {
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(_("Skip not match row."));
                        txtLogMessages->AppendText(END_OF_LINE);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(_("Skip not match row."));
                        m_logfile->Write(END_OF_LINE);
                    }
                }

                lineisskipped = true;
            }

            if (m_fileoptstartwith && !linedata.StartsWith(*m_fileoptstartwith))
            {
                if (loginfo)
                {
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(_("Skip not match start with row."));
                        txtLogMessages->AppendText(END_OF_LINE);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(_("Skip not match start with row."));
                        m_logfile->Write(END_OF_LINE);
                    }
                }

                lineisskipped = true;
            }
        }
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
        else if (m_datasrctype==SRCODBCPAGE)
        {
            odbcsqlrc = m_srcodbc->NextResultRow();
            if (odbcsqlrc==SQL_SUCCESS || odbcsqlrc==SQL_SUCCESS_WITH_INFO)
            {
                if (loginfo)
                {
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(_("ODBC: 1 row fetched."));
                        txtLogMessages->AppendText(END_OF_LINE);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(_("ODBC: 1 row fetched."));
                        m_logfile->Write(END_OF_LINE);
                    }
                }
            }
            else if (odbcsqlrc==SQL_NO_DATA)
            {
                if (loginfo)
                {
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(_("ODBC: All rows fetched."));
                        txtLogMessages->AppendText(END_OF_LINE);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(_("ODBC: All rows fetched."));
                        m_logfile->Write(END_OF_LINE);
                    }
                }

                break;
            }
            else
            {
                if (logerror)
                {
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(m_srcodbc->GetErrorMsg());
                        txtLogMessages->AppendText(END_OF_LINE);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(m_srcodbc->GetErrorMsg());
                        m_logfile->Write(END_OF_LINE);
                    }
                }

                m_procstatus = IMPERRORSTOP;
                break;
            }

            for (odbccolidx=0; odbccolidx<stmtcolsnum; odbccolidx++)
            {
                odbccoltype = odbccoltypes[odbccolidx];
                if (odbccoltype==SQL_BINARY || odbccoltype==SQL_VARBINARY || odbccoltype==SQL_LONGVARBINARY
                        || odbccoltype==SQL_BIGINT || odbccoltype==SQL_TINYINT || odbccoltype==SQL_DECIMAL
                        || odbccoltype==SQL_BIT || odbccoltype==SQL_UNKNOWN_TYPE)
                {
                    // Not support yet
                    continue;
                }

                while (true)
                {
                    odbcsqlrc = m_srcodbc->GetResultData(odbccolidx+1, odbcdatabuff, IMPDATABUFFLEN, &indptr);

                    if (odbcsqlrc!=SQL_SUCCESS && odbcsqlrc!=SQL_SUCCESS_WITH_INFO)
                    {
                        if (logerror)
                        {
                            if (logtoscr)
                            {
                                txtLogMessages->AppendText(m_srcodbc->GetErrorMsg());
                                txtLogMessages->AppendText(END_OF_LINE);
                            }
                            if (m_logfile)
                            {
                                m_logfile->Write(m_srcodbc->GetErrorMsg());
                                m_logfile->Write(END_OF_LINE);
                            }
                        }

                        break;
                    }

                    if (indptr==SQL_NO_TOTAL || indptr>(int)(IMPDATABUFFLEN*sizeof(SQLWCHAR)) )
                    {
                        if (odbcrowdata[odbccolidx])
                        {
                            coldata = odbcrowdata[odbccolidx];
                            odbcrowdata[odbccolidx] = new SQLWCHAR[coldatalen + IMPDATABUFFLEN];
                            wcscpy(odbcrowdata[odbccolidx], coldata);
                            wcscat(odbcrowdata[odbccolidx], odbcdatabuff);
                            coldatalen += IMPDATABUFFLEN; 
                            delete[] coldata;
                        }
                        else
                        {
                            coldatalen = IMPDATABUFFLEN*2;
                            odbcrowdata[odbccolidx] = new SQLWCHAR[coldatalen];
                            wcscpy(odbcrowdata[odbccolidx], odbcdatabuff);
                        }

                        continue;
                    }
                    else if (indptr==SQL_NULL_DATA)
                        break;

                    if (odbcrowdata[odbccolidx])
                        wcscat(odbcrowdata[odbccolidx], odbcdatabuff);
                    else
                    {
                        odbcrowdata[odbccolidx] = new SQLWCHAR[wcslen(odbcdatabuff)+1];
                        wcscpy(odbcrowdata[odbccolidx], odbcdatabuff);
                    }
                    break;
                }

                if (odbcsqlrc!=SQL_SUCCESS && odbcsqlrc!=SQL_SUCCESS_WITH_INFO)
                    break;
            }
        }
#endif
        else
            break;

        if (!lineisskipped)
        {
            impdatalist.Empty();
            clienttime = wxDateTime::UNow();
            rowlog.Empty();
        }

        if (lineisskipped);
        else if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
            rowdataisok = GenCSVSQLData(impdatalist, rowlog, linevals, valsnum, clienttime, impmode, fmtstyle);
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
        else if (m_datasrctype==SRCODBCPAGE)
            rowdataisok = GenODBCSQLData(impdatalist, rowlog, odbcrowdata, clienttime, impmode);
#endif
        else
            rowdataisok = false;

        if (needflush)
        {
            if (impmode==IMPORTMODECOPY)
            {
                if (m_impconn->FlushData())
                {
                    if (loginfo)
                    {
                        if (logtoscr)
                        {
                            txtLogMessages->AppendText(_("Data flushed."));
                            txtLogMessages->AppendText(END_OF_LINE);
                        }
                        if (m_logfile)
                        {
                            m_logfile->Write(_("Data flushed."));
                            m_logfile->Write(END_OF_LINE);
                        }
                    }
                }
                else
                {
                    if (logerror)
                    {
                        if (logtoscr)
                            txtLogMessages->AppendText(_("Failed to flush data: ") + m_impconn->GetLastError());
                        if (m_logfile)
                            m_logfile->Write(_("Failed to flush data: ") + m_impconn->GetLastError());
                    }
                    m_procstatus = IMPERRORSTOP;
                    m_imperrnum++;
                    break;
                }
            }
            else
            {
                insstmt.Empty();
                swprintf(imptranssn, 32, wxT("%d"), m_improwsnum-1);

                if (m_impconn->GetAsyncResult())
                {
                    if (loginfo)
                    {
                        if (logtoscr)
                        {
                            txtLogMessages->AppendText(_("Succeeded."));
                            txtLogMessages->AppendText(END_OF_LINE);
                        }
                        if (m_logfile)
                        {
                            m_logfile->Write(_("Succeeded."));
                            m_logfile->Write(END_OF_LINE);
                        }
                    }
                    m_transrowsnum++;
                }
                else
                {
                    if (logerror)
                    {
                        if (logtoscr)
                            txtLogMessages->AppendText(m_impconn->GetLastError());
                        if (m_logfile)
                            m_logfile->Write(m_impconn->GetLastError());
                    }

                    insstmt.Append(wxT("ROLLBACK TO ")).Append(m_imptransname).Append(imptranssn).Append(wxT(';'));
                    m_imperrnum++;
                }

                insstmt.Append(wxT("RELEASE SAVEPOINT ")).Append(m_imptransname).Append(imptranssn).Append(wxT(';'));
                if (commitevery && m_transrowsnum>=commitevery)
                {
                    if (impdest==IMPDESTTEST || impdest==IMPDESTFTEST)
                    {
                        insstmt.Append(wxT("ROLLBACK;"));
                        txtLogMessages->AppendText(_("Rollback and begin new transaction."));
                        txtLogMessages->AppendText(END_OF_LINE);
                        if (loginfo && m_logfile)
                        {
                            m_logfile->Write(_("Rollback and begin new transaction."));
                            m_logfile->Write(END_OF_LINE);
                        }
                    }
                    else
                    {
                        insstmt.Append(wxT("COMMIT;"));
                        txtLogMessages->AppendText(_("Commit and begin new transaction."));
                        txtLogMessages->AppendText(END_OF_LINE);
                        if (loginfo && m_logfile)
                        {
                            m_logfile->Write(_("Commit and begin new transaction."));
                            m_logfile->Write(END_OF_LINE);
                        }
                    }
                    insstmt.Append(wxT("BEGIN TRANSACTION;"));
                    m_transrowsnum = 0;
                }
                if (logstmt)
                {
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(insstmt);
                        txtLogMessages->AppendText(END_OF_LINE);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(insstmt);
                        m_logfile->Write(END_OF_LINE);
                    }
                }
                if (m_impconn->ExecuteVoid(insstmt, false))
                {
                    if (loginfo)
                    {
                        if (logtoscr)
                        {
                            txtLogMessages->AppendText(_("Succeeded."));
                            txtLogMessages->AppendText(END_OF_LINE);
                        }
                        if (m_logfile)
                        {
                            m_logfile->Write(_("Succeeded."));
                            m_logfile->Write(END_OF_LINE);
                        }
                    }
                }
                else
                {
                    if (logerror)
                    {
                        if (!logstmt)
                        {
                            if (logtoscr)
                            {
                                txtLogMessages->AppendText(insstmt);
                                txtLogMessages->AppendText(END_OF_LINE);
                            }
                            if (m_logfile)
                            {
                                m_logfile->Write(insstmt);
                                m_logfile->Write(END_OF_LINE);
                            }
                        }
                        if (logtoscr)
                            txtLogMessages->AppendText(m_impconn->GetLastError());
                        if (m_logfile)
                            m_logfile->Write(m_impconn->GetLastError());
                    }

                    m_procstatus = IMPERRORSTOP;
                    m_imperrnum++;
                    break;
                }
            }

            needflush = false;
        }

        if (lineisskipped);
        else if (rowdataisok)
        {
            if (impmode==IMPORTMODECOPY)
            {
                if (loginfo && impdest!=IMPDESTFILE)
                {
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(_("Transmits COPY data to server:"));
                        txtLogMessages->AppendText(END_OF_LINE);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(_("Transmits COPY data to server:"));
                        m_logfile->Write(END_OF_LINE);
                    }
                }

                impdatalist.Append(END_OF_LINE);

                if (m_destfile)
                    m_destfile->Write(impdatalist);

                if (logstmt)
                {
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(_("COPY data: "));
                        txtLogMessages->AppendText(impdatalist);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(_("COPY data: "));
                        m_logfile->Write(impdatalist);
                    }
                }

                if (impdest==IMPDESTFILE);
                else if ((commres=m_impconn->PutCopyData(impdatalist))==-1)
                {
                    if (logerror)
                    {
                        if (logtoscr)
                            txtLogMessages->AppendText(_("Failed to transmit data: ") + m_impconn->GetLastError());
                        if (m_logfile)
                            m_logfile->Write(_("Failed to transmit data: ") + m_impconn->GetLastError());
                    }
                    m_procstatus = IMPERRORSTOP;
                    m_imperrnum++;
                    break;
                }
                else if (commres==1)
                {
                    if (loginfo)
                    {
                        if (logtoscr)
                        {
                            txtLogMessages->AppendText(_("Succeeded to transmit data."));
                            txtLogMessages->AppendText(END_OF_LINE);
                        }
                        if (m_logfile)
                        {
                            m_logfile->Write(_("Succeeded to transmit data."));
                            m_logfile->Write(END_OF_LINE);
                        }
                    }
                }
                else
                {
                    if (loginfo)
                    {
                        if (logtoscr)
                        {
                            txtLogMessages->AppendText(_("Waiting to transmit COPY data to server."));
                            txtLogMessages->AppendText(END_OF_LINE);
                        }
                        if (m_logfile)
                        {
                            m_logfile->Write(_("Waiting to transmit COPY data to server."));
                            m_logfile->Write(END_OF_LINE);
                        }
                    }
                    needflush = true;
                }

            }
            else
            {
                if (impdest==IMPDESTFILE)
                {
                    insstmt.Empty();
                    insstmt.Append(m_impsqlstmt).Append(impdatalist).Append(wxT(");")).Append(END_OF_LINE);
                    m_destfile->Write(insstmt);
                }
                else
                {
                    swprintf(imptranssn, 32, wxT("%d"), m_improwsnum);

                    insstmt.Empty();
                    insstmt.Append(wxT("SAVEPOINT ")).Append(m_imptransname).Append(imptranssn).Append(wxT(';'));
                    if (logstmt)
                    {
                        if (logtoscr)
                        {
                            txtLogMessages->AppendText(insstmt);
                            txtLogMessages->AppendText(END_OF_LINE);
                        }
                        if (m_logfile)
                        {
                            m_logfile->Write(insstmt);
                            m_logfile->Write(END_OF_LINE);
                        }
                    }
                    if (m_impconn->ExecuteVoid(insstmt, false))
                    {
                        if (loginfo)
                        {
                            if (logtoscr)
                            {
                                txtLogMessages->AppendText(_("Succeeded."));
                                txtLogMessages->AppendText(END_OF_LINE);
                            }
                            if (m_logfile)
                            {
                                m_logfile->Write(_("Succeeded."));
                                m_logfile->Write(END_OF_LINE);
                            }
                        }

                        insstmt.Empty();
                        insstmt.Append(m_impsqlstmt).Append(impdatalist).Append(wxT(");"));

                        if (m_destfile)
                        {
                            m_destfile->Write(insstmt);
                            m_destfile->Write(END_OF_LINE);
                        }
                        if (logstmt)
                        {
                            if (logtoscr)
                            {
                                txtLogMessages->AppendText(insstmt);
                                txtLogMessages->AppendText(END_OF_LINE);
                            }
                            if (m_logfile)
                            {
                                m_logfile->Write(insstmt);
                                m_logfile->Write(END_OF_LINE);
                            }
                        }

                        if (m_impconn->ExecuteVoidAsync(insstmt, false))
                        {
                            if (loginfo)
                            {
                                if (logtoscr)
                                {
                                    txtLogMessages->AppendText(_("Succeeded."));
                                    txtLogMessages->AppendText(END_OF_LINE);
                                }
                                if (m_logfile)
                                {
                                    m_logfile->Write(_("Succeeded."));
                                    m_logfile->Write(END_OF_LINE);
                                }
                            }
                            needflush = true;
                        }
                        else
                        {
                            if (logerror)
                            {
                                if (!logstmt)
                                {
                                    if (logtoscr)
                                    {
                                        txtLogMessages->AppendText(insstmt);
                                        txtLogMessages->AppendText(END_OF_LINE);
                                    }
                                    if (m_logfile)
                                    {
                                        m_logfile->Write(insstmt);
                                        m_logfile->Write(END_OF_LINE);
                                    }
                                }
                                if (logtoscr)
                                    txtLogMessages->AppendText(m_impconn->GetLastError());
                                if (m_logfile)
                                    m_logfile->Write(m_impconn->GetLastError());
                            }
                            m_procstatus = IMPERRORSTOP;
                            m_imperrnum++;
                            break;
                        }
                    }
                    else
                    {
                        if (logerror)
                        {
                            if (!logstmt)
                            {
                                if (logtoscr)
                                {
                                    txtLogMessages->AppendText(insstmt);
                                    txtLogMessages->AppendText(END_OF_LINE);
                                }
                                if (m_logfile)
                                {
                                    m_logfile->Write(insstmt);
                                    m_logfile->Write(END_OF_LINE);
                                }
                            }
                            if (logtoscr)
                                txtLogMessages->AppendText(m_impconn->GetLastError());
                            if (m_logfile)
                                m_logfile->Write(m_impconn->GetLastError());
                        }
                        m_procstatus = IMPERRORSTOP;
                        m_imperrnum++;
                        break;
                    }
                }

            }

        }
        else
        {
            if (logerror)
            {
                if (logtoscr)
                {
                    txtLogMessages->AppendText(rowlog);
                    txtLogMessages->AppendText(END_OF_LINE);
                }
                if (m_logfile)
                {
                    m_logfile->Write(rowlog);
                    m_logfile->Write(END_OF_LINE);
                }
            }
            m_imperrnum++;
        }

        if (lineisskipped)
            m_skiprowsnum++;
        else
            m_improwsnum++;

        if (logproc)
            RefreshProc();

        if (logtoscr)
            if (txtLogMessages->GetLastPosition()>MAXLOGCOUNT)
            {
                txtLogMessages->Freeze();
                txtLogMessages->Remove(0, 2048);
                txtLogMessages->Thaw();
            }

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
        if (m_datasrctype==SRCODBCPAGE)
            for (odbccolidx=0; odbccolidx<stmtcolsnum; odbccolidx++)
                if (odbcrowdata[odbccolidx])
                {
                    delete[] odbcrowdata[odbccolidx];
                    odbcrowdata[odbccolidx] = NULL;
                }
#endif

        if (stoponerror && m_imperrnum>=stoponerror)
            m_procstatus = IMPERRORSTOP;
        else
            wxTheApp->Yield();

        if (impdest!=IMPDESTFILE && (m_procstatus==IMPPAUSING || m_procstatus==IMPSTOPING))
        {
            if (impmode==IMPORTMODECOPY && needflush)
            {
                if (m_impconn->FlushData())
                {
                    if (loginfo)
                    {
                        if (logtoscr)
                        {
                            txtLogMessages->AppendText(_("Data flushed."));
                            txtLogMessages->AppendText(END_OF_LINE);
                        }
                        if (m_logfile)
                        {
                            m_logfile->Write(_("Data flushed."));
                            m_logfile->Write(END_OF_LINE);
                        }
                    }
                }
                else
                {
                    if (logerror)
                    {
                        if (logtoscr)
                            txtLogMessages->AppendText(_("Failed to flush data: ") + m_impconn->GetLastError());
                        if (m_logfile)
                            m_logfile->Write(_("Failed to flush data: ") + m_impconn->GetLastError());
                    }
                }
            }
            else if (impmode==IMPORTMODEINSERT && needflush)
            {
                insstmt.Empty();
                swprintf(imptranssn, 32, wxT("%d"), m_improwsnum-1);

                if (m_impconn->GetAsyncResult())
                {
                    if (loginfo)
                    {
                        if (logtoscr)
                        {
                            txtLogMessages->AppendText(_("Succeeded."));
                            txtLogMessages->AppendText(END_OF_LINE);
                        }
                        if (m_logfile)
                        {
                            m_logfile->Write(_("Succeeded."));
                            m_logfile->Write(END_OF_LINE);
                        }
                    }
                    m_transrowsnum++;
                }
                else
                {
                    if (logerror)
                    {
                        if (logtoscr)
                            txtLogMessages->AppendText(m_impconn->GetLastError());
                        if (m_logfile)
                            m_logfile->Write(m_impconn->GetLastError());
                    }

                    insstmt.Append(wxT("ROLLBACK TO ")).Append(m_imptransname).Append(imptranssn).Append(wxT(';'));
                    m_imperrnum++;
                }

                insstmt.Append(wxT("RELEASE SAVEPOINT ")).Append(m_imptransname).Append(imptranssn).Append(wxT(';'));
                if (logstmt)
                {
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(insstmt);
                        txtLogMessages->AppendText(END_OF_LINE);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(insstmt);
                        m_logfile->Write(END_OF_LINE);
                    }
                }
                if (m_impconn->ExecuteVoid(insstmt, false))
                {
                    if (loginfo)
                    {
                        if (logtoscr)
                        {
                            txtLogMessages->AppendText(_("Succeeded."));
                            txtLogMessages->AppendText(END_OF_LINE);
                        }
                        if (m_logfile)
                        {
                            m_logfile->Write(_("Succeeded."));
                            m_logfile->Write(END_OF_LINE);
                        }
                    }
                }
                else
                {
                    if (logerror)
                    {
                        if (!logstmt)
                        {
                            if (logtoscr)
                            {
                                txtLogMessages->AppendText(insstmt);
                                txtLogMessages->AppendText(END_OF_LINE);
                            }
                            if (m_logfile)
                            {
                                m_logfile->Write(insstmt);
                                m_logfile->Write(END_OF_LINE);
                            }
                        }

                        if (logtoscr)
                            txtLogMessages->AppendText(m_impconn->GetLastError());
                        if (m_logfile)
                            m_logfile->Write(m_impconn->GetLastError());
                    }
                    m_procstatus = IMPERRORSTOP;
                    m_imperrnum++;
                }
            }
        }

        if (m_procstatus==IMPPAUSING)
        {
            m_imppausedt = wxDateTime::UNow();
            wxLog::GetActiveTarget()->SetLogLevel(settings->GetLogLevel());
            btnImpStart->SetLabel(_("&Resume"));

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
            if (m_datasrctype==SRCODBCPAGE)
                delete[] odbcrowdata;
#endif

            if (!logproc)
                RefreshProc();

            wzdctrlbtn = (wxButton*)FindWindowById(wxID_BACKWARD);
            wzdctrlbtn->Enable();
            m_procstatus = IMPPAUSED;
            btnImpStart->Enable();
            btnImpStop->Enable();

            return;
        }
        else if (m_procstatus==IMPERRORSTOP)
        {
            wzdctrlbtn = (wxButton*)FindWindowById(wxID_BACKWARD);
            wzdctrlbtn->Enable();

            break;
        }
        else if (m_procstatus==IMPSTOPING)
        {
            wzdctrlbtn = (wxButton*)FindWindowById(wxID_BACKWARD);
            wzdctrlbtn->Enable();
            wzdctrlbtn = (wxButton*)FindWindowById(wxID_FORWARD);
            wzdctrlbtn->Enable();

            m_procstatus = IMPSTOPED;

            break;
        }
    }

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    if (m_datasrctype==SRCODBCPAGE)
    {
        for (odbccolidx=0; odbccolidx<stmtcolsnum; odbccolidx++)
            if (odbcrowdata[odbccolidx])
                delete[] odbcrowdata[odbccolidx];

        delete[] odbcrowdata;
    }
#endif

    if ((m_procstatus==IMPSTOPED || m_procstatus==IMPRUNNING) &&
                impmode==IMPORTMODECOPY && m_destfile)
        m_destfile->Write(wxT("\\."));

    if (m_procstatus==IMPRUNNING)
    {
        if (impmode==IMPORTMODECOPY)
        {
            if (impdest!=IMPDESTFILE)
            {
                if (needflush)
                {
                    if (m_impconn->FlushData())
                    {
                        if (loginfo)
                        {
                            if (logtoscr)
                            {
                                txtLogMessages->AppendText(_("Data flushed."));
                                txtLogMessages->AppendText(END_OF_LINE);
                            }
                            if (m_logfile)
                            {
                                m_logfile->Write(_("Data flushed."));
                                m_logfile->Write(END_OF_LINE);
                            }
                        }
                    }
                    else
                    {
                        if (logerror)
                        {
                            if (logtoscr)
                                txtLogMessages->AppendText(_("Failed to flush data: ") + m_impconn->GetLastError());
                            if (m_logfile)
                                m_logfile->Write(_("Failed to flush data: ") + m_impconn->GetLastError());
                        }
                    }
                }
                if (m_impconn->PutCopyEnd())
                {
                    txtLogMessages->AppendText(_("COPY command ended."));
                    txtLogMessages->AppendText(END_OF_LINE);
                    if (loginfo && m_logfile)
                    {
                        m_logfile->Write(_("COPY command ended."));
                        m_logfile->Write(END_OF_LINE);
                    }
                }
                else
                {
                    if (logerror)
                    {
                        if (logtoscr)
                            txtLogMessages->AppendText(_("Failed to end copy: ") + m_impconn->GetLastError());
                        if (m_logfile)
                            m_logfile->Write(_("Failed to end copy: ") + m_impconn->GetLastError());
                    }
                }
            }
        }
        else
        {
            if (needflush && impdest!=IMPDESTFILE)
            {
                insstmt.Empty();
                swprintf(imptranssn, 32, wxT("%d"), m_improwsnum-1);

                if (m_impconn->GetAsyncResult())
                {
                    if (loginfo)
                    {
                        if (logtoscr)
                        {
                            txtLogMessages->AppendText(_("Succeeded."));
                            txtLogMessages->AppendText(END_OF_LINE);
                        }
                        if (m_logfile)
                        {
                            m_logfile->Write(_("Succeeded."));
                            m_logfile->Write(END_OF_LINE);
                        }
                    }
                }
                else
                {
                    if (logerror)
                    {
                        if (logtoscr)
                            txtLogMessages->AppendText(m_impconn->GetLastError());
                        if (m_logfile)
                            m_logfile->Write(m_impconn->GetLastError());
                    }
                    insstmt.Append(wxT("ROLLBACK TO ")).Append(m_imptransname).Append(imptranssn).Append(wxT(';'));
                }
                
                insstmt.Append(wxT("RELEASE SAVEPOINT ")).Append(m_imptransname).Append(imptranssn).Append(wxT(';'));
                if (logstmt)
                {
                    if (logtoscr)
                    {
                        txtLogMessages->AppendText(insstmt);
                        txtLogMessages->AppendText(END_OF_LINE);
                    }
                    if (m_logfile)
                    {
                        m_logfile->Write(insstmt);
                        m_logfile->Write(END_OF_LINE);
                    }
                }
                if (m_impconn->ExecuteVoid(insstmt, false))
                {
                    if (loginfo)
                    {
                        if (logtoscr)
                        {
                            txtLogMessages->AppendText(_("Succeeded."));
                            txtLogMessages->AppendText(END_OF_LINE);
                        }
                        if (m_logfile)
                        {
                            m_logfile->Write(_("Succeeded."));
                            m_logfile->Write(END_OF_LINE);
                        }
                    }
                }
                else
                {
                    if (logerror)
                    {
                        if (!logstmt)
                        {
                            if (logtoscr)
                            {
                                txtLogMessages->AppendText(insstmt);
                                txtLogMessages->AppendText(END_OF_LINE);
                            }
                            if (m_logfile)
                            {
                                m_logfile->Write(insstmt);
                                m_logfile->Write(END_OF_LINE);
                            }
                        }
                        if (logtoscr)
                            txtLogMessages->AppendText(m_impconn->GetLastError());
                        if (m_logfile)
                            m_logfile->Write(m_impconn->GetLastError());
                    }
                }
            }
        }
    }

    if (impdest==IMPDESTFILE)
    {
        if (m_procstatus==IMPRUNNING)
            m_procstatus = IMPFINISHED;
    }
    else
    {
        insstmt.Empty();
        if (m_procstatus==IMPSTOPED || m_procstatus==IMPERRORSTOP || impdest==IMPDESTTEST || impdest==IMPDESTFTEST)
        {
            insstmt.Append(wxT("ROLLBACK;"));
            txtLogMessages->AppendText(_("Rollback transaction."));
            txtLogMessages->AppendText(END_OF_LINE);
            if (loginfo && m_logfile)
            {
                m_logfile->Write(_("Rollback transaction."));
                m_logfile->Write(END_OF_LINE);
            }
        }
        else
        {
            insstmt.Append(wxT("COMMIT;"));
            txtLogMessages->AppendText(_("Commit transaction."));
            txtLogMessages->AppendText(END_OF_LINE);
            if (loginfo && m_logfile)
            {
                m_logfile->Write(_("Commit transaction."));
                m_logfile->Write(END_OF_LINE);
            }
        }
        if (logstmt)
        {
            if (logtoscr)
            {
                txtLogMessages->AppendText(insstmt);
                txtLogMessages->AppendText(END_OF_LINE);
            }
            if (m_logfile)
            {
                m_logfile->Write(insstmt);
                m_logfile->Write(END_OF_LINE);
            }
        }
        if (m_impconn->ExecuteVoid(insstmt, false))
        {
            if (m_procstatus==IMPSTOPED || m_procstatus==IMPERRORSTOP || impdest==IMPDESTTEST || impdest==IMPDESTFTEST)
                txtLogMessages->AppendText(_("Tranction rollbacked."));
            else
                txtLogMessages->AppendText(_("Tranction committed."));
            txtLogMessages->AppendText(END_OF_LINE);
            if (loginfo && m_logfile)
            {
                if (m_procstatus==IMPSTOPED || m_procstatus==IMPERRORSTOP || impdest==IMPDESTTEST || impdest==IMPDESTFTEST)
                    m_logfile->Write(_("Tranction rollbacked."));
                else
                    m_logfile->Write(_("Tranction committed."));
                m_logfile->Write(END_OF_LINE);
            }
            if (m_procstatus==IMPRUNNING)
                m_procstatus = IMPFINISHED;
        }
        else
        {
            if (!logstmt)
            {
                if (logtoscr)
                {
                    txtLogMessages->AppendText(insstmt);
                    txtLogMessages->AppendText(END_OF_LINE);
                }
                if (m_logfile)
                {
                    m_logfile->Write(insstmt);
                    m_logfile->Write(END_OF_LINE);
                }
            }
            if (m_procstatus==IMPSTOPED || m_procstatus==IMPERRORSTOP || impdest==IMPDESTTEST || impdest==IMPDESTFTEST)
                txtLogMessages->AppendText(_("Failed to rollback transaction. ") + m_impconn->GetLastError());
            else
                txtLogMessages->AppendText(_("Failed to commit transaction. ") + m_impconn->GetLastError());
            if (logerror && m_logfile)
            {
                if (m_procstatus==IMPSTOPED || m_procstatus==IMPERRORSTOP || impdest==IMPDESTTEST || impdest==IMPDESTFTEST)
                    m_logfile->Write(_("Failed to rollback transaction. ") + m_impconn->GetLastError());
                else
                    m_logfile->Write(_("Failed to commit transaction. ") + m_impconn->GetLastError());
            }

            m_procstatus = IMPERRORSTOP;
        }
    }

    if (m_procstatus==IMPFINISHED)
        gaugeImp->SetValue(gaugeImp->GetRange());

    if (!logproc && m_procstatus!=IMPERRORSTOP && m_procstatus!=IMPSTOPED)
        RefreshProc(false);

    btnImpStart->Disable();
    btnImpStop->Disable();
    if (m_procstatus==IMPFINISHED || m_procstatus==IMPSTOPED || m_procstatus==IMPERRORSTOP)
    {
        wzdctrlbtn = (wxButton*)FindWindowById(wxID_BACKWARD);
        wzdctrlbtn->Enable();
    }
    if (m_procstatus!=IMPERRORSTOP)
    {
        wxButton* btnnxtpg = (wxButton*)FindWindowById(wxID_FORWARD);
        btnnxtpg->Enable();
    }

    wxLog::GetActiveTarget()->SetLogLevel(settings->GetLogLevel());

    if (m_destfile)
    {
        m_destfile->Close();
        delete m_destfile;
        m_destfile = NULL;
    }

    if (m_logfile)
    {
        m_logfile->Close();
        delete m_logfile;
        m_logfile = NULL;
    }


}

void frmImport::RefreshProc(bool incgauge)
{
    wxTimeSpan elapsedspan;
    int avgrowtime;

    stxtInsertedRows->SetLabel(wxString::Format(_("%d row(s) inserted, %d error(s), %d skipped."),
            m_improwsnum-m_imperrnum, m_imperrnum, m_skiprowsnum));
    elapsedspan = wxDateTime::UNow().Subtract(m_impstartdt).Subtract(m_imppausedts);
    avgrowtime = elapsedspan.GetMilliseconds().ToLong()/(m_improwsnum + m_skiprowsnum);
    stxtElapsedTime->SetLabel(elapsedspan.Format(wxT("%H:%M:%S.%l")));
    stxtAvgRowTime->SetLabel(wxString::Format(_("%d ms"), avgrowtime));

    if (m_srcsize==-1)
    {
        if (incgauge)
            gaugeImp->SetValue((m_improwsnum + m_skiprowsnum)%100);
    }
    else
    {
        if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
        {
            if (incgauge)
                gaugeImp->SetValue(m_srcfile->Tell()*1000/m_srcsize);
            stxtRemainingTime->SetLabel(wxTimeSpan::Milliseconds(elapsedspan.GetMilliseconds().ToLong()*((m_srcsize-m_srcfile->Tell())/m_srcfile->Tell())).Format(wxT("%H:%M:%S.%l")));
        }
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
        else if (m_datasrctype==SRCODBCPAGE)
        {
            if (incgauge)
                gaugeImp->SetValue(m_improwsnum*1000/m_srcsize);
            stxtRemainingTime->SetLabel(wxTimeSpan::Milliseconds(avgrowtime*(m_srcsize-m_improwsnum)).Format(wxT("%H:%M:%S.%l")));
        }
#endif
    }
}

bool frmImport::CheckWizardPage()
{
    if (m_procstatus!=IMPPREPARED)
        return true;

    bool isvalid = false;

    int impdest = IMPDESTNONE;
    if (m_curpagenr==DESTLOGPAGE)
        impdest = (int)chcImpDest->GetClientData(chcImpDest->GetSelection());

    switch(m_curpagenr)
    {
    case TARGETPAGE:
        isvalid = true;
        break;
    case SRCTYPEPAGE:
        isvalid = true;
        break;
    case TEXTFILEPAGE:
    case ARCHFILEPAGE:
        if (!m_srcfile->IsOk())
        {
            isvalid = false;
            DispStatusMsg(m_srcfile->GetErrorMsg());
            btnTextFileBrowse->SetFocus();
            break;
        }

        isvalid = true;
        break;
    case SRCPGSQLPAGE:
        isvalid = false;
        break;
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    case SRCODBCPAGE:
        isvalid = false;

        if (!m_srcodbc || !m_srcodbc->IsConnected())
        {
            DispStatusMsg(_("Please connect to ODBC data source."));
            break;
        }

        if (m_srcodbcisok)
            isvalid = true;
        else
            DispStatusMsg(_("Please generate preview data."));

        break;
#endif
    case FILESETTINGPAGE:
        isvalid = true;
        if (!m_srcfile->IsOk())
        {
            isvalid = false;
            DispStatusMsg(m_srcfile->GetErrorMsg());
            rdbFileCharDelimiter->SetFocus();
            break;
        }

        break;
    case FILEOPTSPAGE:
        if (txtFileOptImpRegEx->GetValue().IsEmpty())
        {
            if (m_fileoptimpregex)
            {
                delete m_fileoptimpregex;
                m_fileoptimpregex = NULL;
            }
        }
        else
        {
            if (!m_fileoptimpregex)
                m_fileoptimpregex = new wxRegEx();

            if (!m_fileoptimpregex->Compile(txtFileOptImpRegEx->GetValue()))
            {
                //DispStatusMsg(_("Invalid import regular expression."));
                txtFileOptImpRegEx->SetFocus();
                txtFileOptImpRegEx->SetSelection(-1, -1);
                isvalid = false;
                break;
            }
        }

        if (txtFileOptStartWith->GetValue().IsEmpty())
        {
            if (m_fileoptstartwith)
            {
                delete m_fileoptstartwith;
                m_fileoptstartwith = NULL;
            }
        }
        else
        {
            if (m_fileoptstartwith)
                m_fileoptstartwith->Empty();
            else
                m_fileoptstartwith = new wxString;
            m_fileoptstartwith->Append(txtFileOptStartWith->GetValue());
        }

        if (chkFileCheckTrueFirst->GetValue())
        {
            if (m_boolfalsevals)
            {
                delete m_boolfalsevals;
                m_boolfalsevals = NULL;
            }
            wxString truevals = txtFileOptTrueVals->GetValue().Trim().Trim(false);
            if (truevals.IsEmpty())
            {
                if (m_booltruevals)
                {
                    delete m_booltruevals;
                    m_booltruevals = NULL;
                }
            }
            else
            {
                if (m_booltruevals)
                    m_booltruevals->Clear();
                else
                    m_booltruevals = new wxArrayString();

                wxStringTokenizer truevalstkz(truevals, wxT(","));
                wxString boolstr;
                while ( truevalstkz.HasMoreTokens() )
                {
                    boolstr = truevalstkz.GetNextToken().Trim().Trim(false);
                    if (!boolstr.IsEmpty())
                        m_booltruevals->Add(boolstr);
                }
                if (m_booltruevals->IsEmpty())
                {
                    delete m_booltruevals;
                    m_booltruevals = NULL;
                }
            }
        }
        else
        {
            if (m_booltruevals)
            {
                delete m_booltruevals;
                m_booltruevals = NULL;
            }
            wxString falsevals = txtFileOptFalseVals->GetValue().Trim().Trim(false);
            if (falsevals.IsEmpty())
            {
                if (m_boolfalsevals)
                {
                    delete m_boolfalsevals;
                    m_boolfalsevals = NULL;
                }
            }
            else
            {
                if (m_boolfalsevals)
                    m_boolfalsevals->Clear();
                else
                    m_boolfalsevals = new wxArrayString();

                wxStringTokenizer falsevalstkz(falsevals, wxT(","));
                wxString boolstr;
                while ( falsevalstkz.HasMoreTokens() )
                {
                    boolstr = falsevalstkz.GetNextToken().Trim().Trim(false);
                    if (!boolstr.IsEmpty())
                        m_boolfalsevals->Add(boolstr);
                }
                if (m_boolfalsevals->IsEmpty())
                {
                    delete m_boolfalsevals;
                    m_boolfalsevals = NULL;
                }
            }
        }

        isvalid = true;
        break;
    case COLSETTINGPAGE:
        if (m_colsettingerrnum==-1)
            ParseColsImpSetting();
        if (m_colsettingerrnum)
            DispStatusMsg(_("Please specify import column correctly."));
        else
            isvalid = true;

        if (m_impsqlstmt.IsEmpty())
        {
            bool isfirstcol = true;
            size_t dbcolidx = 0;
            m_impsqlstmt.Append(wxT("COPY ") + m_imptablename + wxT("("));
            for (; dbcolidx<m_tgtcolsnum; dbcolidx++)
                if (m_tgtcolincopy[dbcolidx])
                {
                    if (isfirstcol)
                        isfirstcol = false;
                    else
                        m_impsqlstmt.Append(wxT(", "));
                    m_impsqlstmt.Append(*m_tgtcolsnames[dbcolidx]);
                }
            m_impsqlstmt.Append(wxT(") FROM stdin;"));

            if (isfirstcol)
            {
                DispStatusMsg(_("Please specify import column."));
                isvalid = false;
            }
        }

        break;
    case DESTLOGPAGE:
        if (impdest==IMPDESTCOPY || impdest==IMPDESTINS || impdest==IMPDESTTEST)
            isvalid = true;
        else if ((impdest==IMPDESTFCOPY || impdest==IMPDESTFINS || impdest==IMPDESTFTEST ||
                 impdest==IMPDESTFILE )&& txtDestFilePath->IsEmpty())
        {
            DispStatusMsg(_("Please specify a output file."));
            isvalid = false;
            break;
        }
        else if ((impdest==IMPDESTFCOPY || impdest==IMPDESTFINS || impdest==IMPDESTFTEST || impdest==IMPDESTFILE)
                && txtDestFilePath->IsModified())
        {
            if (OnDestFilePathChange(txtDestFilePath->GetValue()))
            {
                txtDestFilePath->SetModified(false);
                isvalid = true;
            }
            else
            {
                isvalid = false;
                break;
            }
        }
        else
        {
            isvalid = true;
        }

        if (rdbLogToScreen->GetValue())
            isvalid = true;
        else if (txtLogFilePath->IsEmpty())
        {
            DispStatusMsg(_("Please specify a log file."));
            isvalid = false;
        }
        else if (txtLogFilePath->IsModified())
        {
            if (OnLogFilePathChange(txtLogFilePath->GetValue()))
            {
                txtLogFilePath->SetModified(false);
                isvalid = true;
            }
            else
                isvalid = false;
        }
        else
            isvalid = true;

        break;
    case PROCESSPAGE:
        isvalid = true;
        break;
    default:
        isvalid = false;
        break;
    }

    return isvalid;
}

void frmImport::DispStatusMsg(const wxString& statusmsg)
{
    wxMessageBox(statusmsg);
}

void frmImport::GenFilePreview()
{
    if (!m_srcfile->FileIsOk() || !m_srcfile->FileEncodingIsOk())
    {
        if (m_curpagenr==TEXTFILEPAGE)
            txtTextFileCont->SetValue(m_srcfile->GetErrorMsg());
        else if (m_curpagenr==ARCHFILEPAGE)
            txtArchFileCont->SetValue(m_srcfile->GetErrorMsg());

        return;
    }

    if (m_curpagenr==TEXTFILEPAGE)
    {
        txtTextFileCont->Freeze();
        txtTextFileCont->SetValue(m_srcfile->GetFilePreview());
        txtTextFileCont->Thaw();
    }
    else if (m_curpagenr==ARCHFILEPAGE)
    {
        txtArchFileCont->Freeze();
        txtArchFileCont->SetValue(m_srcfile->GetFilePreview());
        txtArchFileCont->Thaw();
    }
}

void frmImport::GenCSVPreview()
{
    size_t uipvrowsnum = spinFilePreviewRows->GetValue();
    size_t sfpvrowsnum = m_srcfile->GetPreviewRowsCount();

    if (!sfpvrowsnum)
        return;

    bool firstrowisheader = m_srcfile->FirstRowIsHeader();

    size_t rowidx = (size_t)0, colidx, gridrowidx = (size_t)0;

    if (firstrowisheader)
    {
        rowidx++;
        if (sfpvrowsnum<uipvrowsnum+1)
            uipvrowsnum = sfpvrowsnum-1;
    }
    else if (sfpvrowsnum<uipvrowsnum)
        uipvrowsnum = sfpvrowsnum;

    gridFilePreview->Freeze();

    int gridxpos, gridypos;
    gridFilePreview->GetViewStart(&gridxpos, &gridypos);

    bool indicatenull = true;//settings->GetIndicateNull();
    const size_t maxcolsnum = m_srcfile->GetColsNum();
    const size_t* colsnum = m_srcfile->GetPreviewRowColsNum();
    const wxString ***rowsvals = m_srcfile->GetPreviewRowsVals();

    ClearGrid(gridFilePreview);
    gridFilePreview->AppendCols(maxcolsnum);
    gridFilePreview->AppendRows(uipvrowsnum);

    const wxArrayString *colsheader = m_srcfile->GetColsHeader();
    for (colidx=(size_t)0; colidx<maxcolsnum; colidx++)
        gridFilePreview->SetColLabelValue(colidx, colsheader->Item(colidx));

    for (gridrowidx=(size_t)0; gridrowidx<uipvrowsnum; rowidx++, gridrowidx++)
    {
        for (colidx=(size_t)0; colidx<maxcolsnum; colidx++)
        {
            if (colidx>=colsnum[rowidx])
            {
                if (indicatenull)
                    for (; colidx<maxcolsnum; colidx++)
                    {
                        gridFilePreview->SetCellValue(gridrowidx, colidx, wxT("<NULL>"));
                        gridFilePreview->SetCellTextColour(gridrowidx, colidx, *wxBLUE);
                    }

                break;
            }

            if (rowsvals[rowidx][colidx])
                gridFilePreview->SetCellValue(gridrowidx, colidx, *rowsvals[rowidx][colidx]);
            else if (indicatenull)
            {
                gridFilePreview->SetCellValue(gridrowidx, colidx, wxT("<NULL>"));
                gridFilePreview->SetCellTextColour(gridrowidx, colidx, *wxBLUE);
            }
        }
    }

    gridFilePreview->FitInside();
    gridFilePreview->Scroll(gridxpos, gridypos);
    gridFilePreview->Thaw();
}

void frmImport::GenColsPreview()
{
    size_t pvrowsnum;
    if (m_datasrctype==SRCPGSQLPAGE)
    {
        pvrowsnum = 0;
    }
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    else if (m_datasrctype==SRCODBCPAGE)
    {
        pvrowsnum = gridODBCPreview->GetNumberRows();
    }
#endif
    else
    {
        pvrowsnum = gridFilePreview->GetNumberRows();
    }

    if (!pvrowsnum)
        return;

    gridColsSampleData->Freeze();
    txtColsSettingLog->Freeze();
    txtColsImpLog->Freeze();

    int gridxpos, gridypos;
    gridColsSampleData->GetViewStart(&gridxpos, &gridypos);

    int impmode = (int)chcImportMode->GetClientData(chcImportMode->GetSelection());
    int fmtstyle = (int)chcFormatStyle->GetClientData(chcFormatStyle->GetSelection());
    bool testimp = chkTestImport->GetValue();
    bool calcpgexp = testimp?false:chkValidateExpr->GetValue();
    wxDateTime tsfromval;

    if (testimp && !m_impconn)
    {
        m_impconn = m_conn->Duplicate();
        if (!m_impconn)
        {
            DispStatusMsg(_("Couldn't create a connection object!"));
            return;
        }
        if (m_impconn->GetStatus()!=PGCONN_OK)
        {
            DispStatusMsg(m_impconn->GetLastError());
            delete m_impconn;
            m_impconn = NULL;
            return;
        }
        m_impconn->SetNonblocking();
    }

    bool rowhaserr, normtrans = true;

    m_datapverr = false;
    size_t dbcolidx, pvgridrowidx = 0, pvgridcolidx, datpvrownr;

    wxDateTime clienttime;
    wxString datalog;

    txtColsImpLog->Clear();
    ClearGrid(gridColsSampleData);

    gridColsSampleData->AppendCols(m_tgtcolsnum);
    gridColsSampleData->AppendRows(pvrowsnum);

    for (dbcolidx=0; dbcolidx<m_tgtcolsnum; dbcolidx++)
        gridColsSampleData->SetColLabelValue(dbcolidx, **(m_tgtcolsnames + dbcolidx));

    wxString tgtcoltype, impdatalist, insstmt;
    exprSnippet *exprsnip;
    wxString cellval, snipval, expresult;
    int commres;
    bool copybolcking = false;

    wxChar tmpspname[16];
    wxChar tmpspsn[8];
    RandomSavepointName(tmpspname);

    if (testimp && !m_impconn->ExecuteVoid(wxT("BEGIN TRANSACTION;"), false))
    {
        txtColsImpLog->AppendText(m_impconn->GetLastError());
        txtColsImpLog->AppendText(END_OF_LINE);
        normtrans = false;
    }

    if (testimp && normtrans && impmode==IMPORTMODECOPY)
    {
        if (m_impsqlstmt.IsEmpty())
        {
            bool isfirstcol = true;
            m_impsqlstmt.Append(wxT("COPY ") + m_imptablename + wxT("("));
            for (dbcolidx=0; dbcolidx<m_tgtcolsnum; dbcolidx++)
                if (m_tgtcolincopy[dbcolidx])
                {
                    if (isfirstcol)
                        isfirstcol = false;
                    else
                        m_impsqlstmt.Append(wxT(", "));
                    m_impsqlstmt.Append(*m_tgtcolsnames[dbcolidx]);
                }
            m_impsqlstmt.Append(wxT(") FROM stdin;"));
        }
        txtColsImpLog->AppendText(_("Try to execute COPY command:"));
        txtColsImpLog->AppendText(END_OF_LINE);
        txtColsImpLog->AppendText(m_impsqlstmt);
        txtColsImpLog->AppendText(END_OF_LINE);

        if (!m_impconn->PutCopyBegin(m_impsqlstmt))
        {
            txtColsImpLog->AppendText(m_impconn->GetLastError());
            normtrans = false;
        }
        else
            txtColsImpLog->AppendText(_("COPY command has been executed successfully."));

        txtColsImpLog->AppendText(END_OF_LINE);
        txtColsImpLog->AppendText(END_OF_LINE);
    }

    if (m_datasrctype==SRCPGSQLPAGE)
    {
    }
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    else if (m_datasrctype==SRCODBCPAGE)
    {
        if (!m_srcodbc->ReexecPrevQuery())
        {
            if (m_srcodbc->HasErrorMsg())
                DispStatusMsg(m_srcodbc->GetErrorMsg());

            return;
        }

        SQLSMALLINT stmtcolsnum = m_srcodbc->GetResultColsNum();
        SQLSMALLINT *odbccoltypes = m_srcodbc->GetResultColTypes();

        SQLUSMALLINT odbcrowidx=0;
        SQLSMALLINT odbccoltype;
        SQLUSMALLINT odbccolidx;
        SQLRETURN odbcsqlrc;
        SQLWCHAR odbcdatabuff[IMPDATABUFFLEN];
        SQLINTEGER indptr;

        SQLWCHAR** odbcrowdata = new SQLWCHAR*[stmtcolsnum];
        memset(odbcrowdata, NULL, sizeof(SQLWCHAR*)*stmtcolsnum);

        while (true)
        {
            odbcsqlrc = m_srcodbc->NextResultRow();
            if (odbcsqlrc==SQL_NO_DATA)
                break;
            else if (odbcsqlrc != SQL_SUCCESS && odbcsqlrc != SQL_SUCCESS_WITH_INFO)
            {
                if (m_srcodbc->HasErrorMsg())
                    DispStatusMsg(m_srcodbc->GetErrorMsg());
                break;
            }

            for (odbccolidx=0; odbccolidx<stmtcolsnum; odbccolidx++)
            {
                odbccoltype = odbccoltypes[odbccolidx];
                if (odbccoltype==SQL_BINARY || odbccoltype==SQL_VARBINARY || odbccoltype==SQL_LONGVARBINARY
                        || odbccoltype==SQL_BIGINT || odbccoltype==SQL_TINYINT || odbccoltype==SQL_DECIMAL
                        || odbccoltype==SQL_BIT || odbccoltype==SQL_UNKNOWN_TYPE)
                {
                    continue;
                }

                odbcsqlrc = m_srcodbc->GetResultData(odbccolidx+1, odbcdatabuff, IMPDATABUFFLEN, &indptr);

                if (odbcsqlrc!=SQL_SUCCESS && odbcsqlrc!=SQL_SUCCESS_WITH_INFO)
                {
                    if (m_srcodbc->HasErrorMsg())
                        DispStatusMsg(m_srcodbc->GetErrorMsg());
                    break;
                }

                if (indptr==SQL_NO_TOTAL || indptr>(int)(IMPDATABUFFLEN*sizeof(SQLWCHAR)))
                {
                    wcscpy(odbcdatabuff + IMPDATABUFFLEN - wcslen(MOREDATADISP) -1, MOREDATADISP);
                    odbcdatabuff[IMPDATABUFFLEN-1] = wxT('\0');
                }
                else if (indptr==SQL_NULL_DATA)
                    continue;

                odbcrowdata[odbccolidx] = new SQLWCHAR[wcslen(odbcdatabuff)+1];
                wcscpy(odbcrowdata[odbccolidx], odbcdatabuff);
            }

            impdatalist.Empty();
            datalog.Empty();
            clienttime = wxDateTime::UNow();
            if (GenODBCSQLData(impdatalist, datalog, odbcrowdata, clienttime, impmode))
            {
                if (impmode==IMPORTMODECOPY)
                {
                    if (testimp && normtrans)
                        txtColsImpLog->AppendText(_("Transmits COPY data to server:"));
                    else
                        txtColsImpLog->AppendText(_("COPY data:"));
                    txtColsImpLog->AppendText(END_OF_LINE);

                    txtColsImpLog->AppendText(impdatalist);
                    txtColsImpLog->AppendText(END_OF_LINE);

                    if (testimp && normtrans)
                    {
                        impdatalist.Append(END_OF_LINE);

                        if (copybolcking)
                        {
                            if (m_impconn->FlushData())
                            {
                                txtColsImpLog->AppendText(_("Data flushed."));
                                txtColsImpLog->AppendText(END_OF_LINE);
                            }
                            else
                            {
                                txtColsImpLog->AppendText(_("Failed to flush data: "));
                                txtColsImpLog->AppendText(m_impconn->GetLastError());
                            }

                            copybolcking = false;
                        }

                        if ((commres=m_impconn->PutCopyData(impdatalist))==-1)
                        {
                            txtColsImpLog->AppendText(_("Failed to transmit data: "));
                            txtColsImpLog->AppendText(m_impconn->GetLastError());
                        }
                        else if (commres==1)
                        {
                            txtColsImpLog->AppendText(_("Succeeded to transmit data."));
                            txtColsImpLog->AppendText(END_OF_LINE);
                        }
                        else
                        {
                            txtColsImpLog->AppendText(_("Transmit is bolcked, waiting for flush..."));
                            txtColsImpLog->AppendText(END_OF_LINE);
                            copybolcking = true;
                        }
                    }
                    txtColsImpLog->AppendText(END_OF_LINE);
                }
                else
                {
                    if (testimp && normtrans && pvgridrowidx)
                    {
                        insstmt.Empty();
                        //_itow(pvgridrowidx-1, tmpspsn, 10);
                        swprintf(tmpspsn, 32, wxT("%d"), pvgridrowidx-1);

                        if (m_impconn->GetAsyncResult())
                        {
                            txtColsImpLog->AppendText(_("Succeeded."));
                            txtColsImpLog->AppendText(END_OF_LINE);
                        }
                        else
                        {
                            txtColsImpLog->AppendText(m_impconn->GetLastError());
                            insstmt.Append(wxT("ROLLBACK TO ")).Append(tmpspname).Append(tmpspsn).Append(wxT(';'));
                        }

                        insstmt.Append(wxT("RELEASE SAVEPOINT ")).Append(tmpspname).Append(tmpspsn).Append(wxT(';'));
                        if (!m_impconn->ExecuteVoid(insstmt, false))
                        {
                            txtColsImpLog->AppendText(m_impconn->GetLastError());
                            txtColsImpLog->AppendText(END_OF_LINE);
                        }
                        txtColsImpLog->AppendText(END_OF_LINE);
                    }

                    txtColsImpLog->AppendText(m_impsqlstmt);
                    txtColsImpLog->AppendText(impdatalist);
                    txtColsImpLog->AppendText(wxT(")"));
                    txtColsImpLog->AppendText(END_OF_LINE);

                    if (testimp && normtrans)
                    {
                        //_itow(pvgridrowidx, tmpspsn, 10);
                        swprintf(tmpspsn, 32, wxT("%d"), pvgridrowidx);

                        insstmt.Empty();
                        insstmt.Append(wxT("SAVEPOINT ")).Append(tmpspname).Append(tmpspsn).Append(wxT(';'));
                        if (m_impconn->ExecuteVoid(insstmt, false))
                        {
                            insstmt.Empty();
                            insstmt.Append(m_impsqlstmt).Append(impdatalist).Append(wxT(");"));

                            if (!m_impconn->ExecuteVoidAsync(insstmt, false))
                                txtColsImpLog->AppendText(m_impconn->GetLastError());
                        }
                        else
                            txtColsImpLog->AppendText(m_impconn->GetLastError());
                    }

                }

            }
            else
            {
                txtColsImpLog->AppendText(datalog);
                txtColsImpLog->AppendText(END_OF_LINE);
                rowhaserr = true;
            }

            for (dbcolidx=0, pvgridcolidx=0; dbcolidx<m_tgtcolsnum; dbcolidx++, pvgridcolidx++)
            {
                tgtcoltype = (*(m_tgtcolstypes+dbcolidx))->Name();
                exprsnip = NULL;

                if (m_colsrccolnr[dbcolidx]==wxNOT_FOUND
                        || !odbcrowdata[m_colsrccolnr[dbcolidx]] || !odbcrowdata[m_colsrccolnr[dbcolidx]][0])
                    GenColsEmptyPreview(pvgridrowidx, pvgridcolidx, dbcolidx,
                            m_colonemptyact[dbcolidx], tgtcoltype, exprsnip, clienttime, rowhaserr);
                else
                {
                    if (m_colfmtarr[dbcolidx])
                    {
                        if (tgtcoltype==wxT("date") && tsfromval.ParseFormat(odbcrowdata[m_colsrccolnr[dbcolidx]], m_colfmtarr[dbcolidx]->GetData()))
                            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, tsfromval.FormatISODate());
                        else if ((tgtcoltype==wxT("timestamp") || tgtcoltype==wxT("timestamp without time zone"))
                                && tsfromval.ParseFormat(odbcrowdata[m_colsrccolnr[dbcolidx]], m_colfmtarr[dbcolidx]->GetData()))
                            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, tsfromval.Format(ANSICTIMESTAMPFMT.GetData()));
                        else if ((tgtcoltype==wxT("timestamptz") || tgtcoltype==wxT("timestamp with time zone"))
                                && tsfromval.ParseFormat(odbcrowdata[m_colsrccolnr[dbcolidx]], m_colfmtarr[dbcolidx]->GetData()))
                            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, tsfromval.Format(ANSICTIMESTAMPTZFMT.GetData()));
                        else if ((tgtcoltype==wxT("time") || tgtcoltype==wxT("time without time zone"))
                                && tsfromval.ParseFormat(odbcrowdata[m_colsrccolnr[dbcolidx]], m_colfmtarr[dbcolidx]->GetData()))
                            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, tsfromval.FormatISOTime());
                        else if ((tgtcoltype==wxT("timetz") || tgtcoltype==wxT("time with time zone"))
                                && tsfromval.ParseFormat(odbcrowdata[m_colsrccolnr[dbcolidx]], m_colfmtarr[dbcolidx]->GetData()))
                            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, tsfromval.Format(ANSICTIMETZFMT.GetData()));
                        else
                        {
                            rowhaserr = true;
                            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, wxT("<ERROR>"));
                            gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxRED);
                        }
                    }
                    else
                        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, odbcrowdata[m_colsrccolnr[dbcolidx]]);
                }

                if (exprsnip)
                {
                    cellval.Empty();
                    while (exprsnip)
                    {
                        if (exprsnip->snippettype==wxNOT_FOUND)
                            cellval.Append(exprsnip->exprsnippet);
                        else
                        {
                            if (odbcrowdata[exprsnip->snippettype])
                            {
                                snipval = *odbcrowdata[exprsnip->snippettype];
                                snipval.Replace(wxT("'"), wxT("''"));
                                cellval.Append(wxT("'")).Append(snipval).Append(wxT("'"));
                            }
                            else
                                cellval.Append(wxT("NULL"));
                        }
                        exprsnip = exprsnip->nextsnip;
                    }
                    if (calcpgexp)
                    {
                        if (ValidatePgExp(cellval, tgtcoltype, expresult))
                        {
                            gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxBLACK);
                            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, expresult);
                        }
                        else
                        {
                            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPERRORDISP);
                            gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxRED);
                            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, cellval);
                            rowhaserr = true;
                        }
                    }
                    else
                        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, cellval);
                }
            }

            for (odbccolidx=0; odbccolidx<stmtcolsnum; odbccolidx++)
                if (odbcrowdata[odbccolidx])
                {
                    delete[] odbcrowdata[odbccolidx];
                    odbcrowdata[odbccolidx] = NULL;
                }

            odbcrowidx++;
            pvgridrowidx++;
            if (odbcrowidx>=pvrowsnum)
                break;
        }

        for (odbccolidx=0; odbccolidx<stmtcolsnum; odbccolidx++)
            if (odbcrowdata[odbccolidx])
            {
                delete[] odbcrowdata[odbccolidx];
                odbcrowdata[odbccolidx] = NULL;
            }

        delete[] odbcrowdata;
    }
#endif
    else
    {
        bool firstrowisheader = chkFileFirstRowIsHeader->GetValue();
        bool chktruefirst = chkFileCheckTrueFirst->GetValue();

        datpvrownr = firstrowisheader?1:0;

        const wxString **csvrows = m_srcfile->GetPreviewLines();
        const size_t *rowcolsnum = m_srcfile->GetPreviewRowColsNum();
        const wxString ***rowvals = m_srcfile->GetPreviewRowsVals();

        if (firstrowisheader)
            pvrowsnum++;

        const wxString *csvline;
        wxString **csvlinevals;
        size_t csvcolnum;
        wxArrayString impdataarr;
        for (; datpvrownr<pvrowsnum; datpvrownr++, pvgridrowidx++)
        {
            csvline = csvrows[datpvrownr];
            csvlinevals = (wxString**)rowvals[datpvrownr];
            csvcolnum = rowcolsnum[datpvrownr];
            rowhaserr = false;

            impdatalist.Empty();
            datalog.Empty();
            clienttime = wxDateTime::UNow();
            if (GenCSVSQLData(impdatalist, datalog, csvlinevals, rowcolsnum[datpvrownr], clienttime, impmode, fmtstyle))
            {
                if (impmode==IMPORTMODECOPY)
                {
                    if (testimp && normtrans)
                        txtColsImpLog->AppendText(_("Transmits COPY data to server:"));
                    else
                        txtColsImpLog->AppendText(_("COPY data:"));
                    txtColsImpLog->AppendText(END_OF_LINE);

                    txtColsImpLog->AppendText(impdatalist);
                    txtColsImpLog->AppendText(END_OF_LINE);

                    if (testimp && normtrans)
                    {
                        // flush data
                        if (copybolcking)
                        {
                            //impdatalist.Append(END_OF_LINE);
                            if (!m_impconn->FlushData())
                            {
                                txtColsImpLog->AppendText(_("Failed to flush data: "));
                                txtColsImpLog->AppendText(m_impconn->GetLastError());
                            }
                            else
                                txtColsImpLog->AppendText(_("Data flushed."));

                            txtColsImpLog->AppendText(END_OF_LINE);
                        }

                        if ((commres=m_impconn->PutCopyData(impdatalist.Append(END_OF_LINE)))==-1)
                        {
                            txtColsImpLog->AppendText(_("Failed to transmit data: "));
                            txtColsImpLog->AppendText(m_impconn->GetLastError());
                        }
                        else if (commres==1)
                            txtColsImpLog->AppendText(_("Succeeded to transmit data."));
                        else
                        {
                            txtColsImpLog->AppendText(_("Transmit is bolcked, waiting for flush..."));
                            copybolcking = true;
                        }
                        txtColsImpLog->AppendText(END_OF_LINE);
                        txtColsImpLog->AppendText(END_OF_LINE);
                    }
                }
                else
                {
                    if (testimp && normtrans && pvgridrowidx)
                    {
                        insstmt.Empty();
                        //_itow(pvgridrowidx-1, tmpspsn, 10);
                        swprintf(tmpspsn, 32, wxT("%d"), pvgridrowidx-1);

                        if (m_impconn->GetAsyncResult())
                        {
                            txtColsImpLog->AppendText(_("Succeeded."));
                            txtColsImpLog->AppendText(END_OF_LINE);
                        }
                        else
                        {
                            txtColsImpLog->AppendText(m_impconn->GetLastError());
                            insstmt.Append(wxT("ROLLBACK TO ")).Append(tmpspname).Append(tmpspsn).Append(wxT(';'));
                        }
                        insstmt.Append(wxT("RELEASE SAVEPOINT ")).Append(tmpspname).Append(tmpspsn).Append(wxT(';'));
                        if (!m_impconn->ExecuteVoid(insstmt, false))
                            txtColsImpLog->AppendText(m_impconn->GetLastError());

                        txtColsImpLog->AppendText(END_OF_LINE);
                    }

                    txtColsImpLog->AppendText(m_impsqlstmt);
                    txtColsImpLog->AppendText(impdatalist);
                    txtColsImpLog->AppendText(wxT(")"));
                    txtColsImpLog->AppendText(END_OF_LINE);

                    if (testimp && normtrans)
                    {
                        //_itow(pvgridrowidx, tmpspsn, 10);
                        swprintf(tmpspsn, 32, wxT("%d"), pvgridrowidx);

                        insstmt.Empty();
                        insstmt.Append(wxT("SAVEPOINT ")).Append(tmpspname).Append(tmpspsn).Append(wxT(';'));

                        if (m_impconn->ExecuteVoid(insstmt, false))
                        {
                            insstmt.Empty();
                            insstmt.Append(m_impsqlstmt).Append(impdatalist).Append(wxT(");"));
                            if (!m_impconn->ExecuteVoidAsync(insstmt, false))
                                txtColsImpLog->AppendText(m_impconn->GetLastError());
                        }
                        else
                            txtColsImpLog->AppendText(m_impconn->GetLastError());
                    }

                }
            }
            else
            {
                txtColsImpLog->AppendText(datalog);
                txtColsImpLog->AppendText(END_OF_LINE);
                rowhaserr = true;
            }

            for (dbcolidx=0, pvgridcolidx=0; dbcolidx<m_tgtcolsnum; dbcolidx++, pvgridcolidx++)
            {
                tgtcoltype = (*(m_tgtcolstypes+dbcolidx))->Name();
                exprsnip = NULL;

                if (m_colsrccolnr[dbcolidx]==wxNOT_FOUND
                        || m_colsrccolnr[dbcolidx]>=(int)csvcolnum || !csvlinevals[m_colsrccolnr[dbcolidx]])
                    GenColsEmptyPreview(pvgridrowidx, pvgridcolidx, dbcolidx,
                        m_colonemptyact[dbcolidx], tgtcoltype, exprsnip, clienttime, rowhaserr);
                else
                {
                    cellval = *csvlinevals[m_colsrccolnr[dbcolidx]];
                    if (m_coltrimmode[dbcolidx]==TRIMALL)
                        cellval.Trim().Trim(false);
                    else if (m_coltrimmode[dbcolidx]==TRIMLEFT)
                        cellval.Trim(false);
                    else if (m_coltrimmode[dbcolidx]==TRIMRIGHT)
                        cellval.Trim();

                    if (cellval.IsEmpty())
                        GenColsEmptyPreview(pvgridrowidx, pvgridcolidx, dbcolidx,
                            m_colonemptyact[dbcolidx], tgtcoltype, exprsnip, clienttime, rowhaserr);
                    else
                    {
                        if (m_colfmtarr[dbcolidx])
                        {
                            if (tgtcoltype==wxT("date") && tsfromval.ParseFormat(cellval.GetData(), m_colfmtarr[dbcolidx]->GetData()))
                                gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, tsfromval.FormatISODate());
                            else if ((tgtcoltype==wxT("timestamp") || tgtcoltype==wxT("timestamp without time zone"))
                                    && tsfromval.ParseFormat(cellval.GetData(), m_colfmtarr[dbcolidx]->GetData()))
                                gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, tsfromval.Format(ANSICTIMESTAMPFMT.GetData()));
                            else if ((tgtcoltype==wxT("timestamptz") || tgtcoltype==wxT("timestamp with time zone"))
                                    && tsfromval.ParseFormat(cellval.GetData(), m_colfmtarr[dbcolidx]->GetData()))
                                gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, tsfromval.Format(ANSICTIMESTAMPTZFMT.GetData()));
                            else if ((tgtcoltype==wxT("time") || tgtcoltype==wxT("time without time zone"))
                                    && tsfromval.ParseFormat(cellval.GetData(), m_colfmtarr[dbcolidx]->GetData()))
                                gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, tsfromval.FormatISOTime());
                            else if ((tgtcoltype==wxT("timetz") || tgtcoltype==wxT("time with time zone"))
                                    && tsfromval.ParseFormat(cellval.GetData(), m_colfmtarr[dbcolidx]->GetData()))
                                gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, tsfromval.Format(ANSICTIMETZFMT.GetData()));
                            else
                            {
                                rowhaserr = true;
                                gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, wxT("<ERROR>"));
                                gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxRED);
                            }
                        }
                        else if ((m_booltruevals || m_boolfalsevals) && tgtcoltype==wxT("boolean"))
                        {
                            if (chktruefirst)
                            {
                                if (m_booltruevals->Index(cellval)!=wxNOT_FOUND)
                                    gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPBOOLTRUEDISP);
                                else
                                    gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPBOOLFALSEDISP);
                            }
                            else
                            {
                                if (m_boolfalsevals->Index(cellval)!=wxNOT_FOUND)
                                    gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPBOOLFALSEDISP);
                                else
                                    gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPBOOLTRUEDISP);
                            }
                        }
                        else
                            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, cellval);
                    }
                }

                if (exprsnip)
                {
                    cellval.Empty();
                    while (exprsnip)
                    {
                        if (exprsnip->snippettype==wxNOT_FOUND)
                            cellval.Append(exprsnip->exprsnippet);
                        else
                        {
                            if (exprsnip->snippettype>=(int)csvcolnum || !csvlinevals[exprsnip->snippettype])
                                cellval.Append(wxT("NULL"));
                            else
                            {
                                snipval = *csvlinevals[exprsnip->snippettype];
                                snipval.Replace(wxT("'"), wxT("''"));
                                cellval.Append(wxT("'")).Append(snipval).Append(wxT("'"));
                            }
                        }
                        exprsnip = exprsnip->nextsnip;
                    }
                    if (calcpgexp)
                    {
                        if (ValidatePgExp(cellval, tgtcoltype, expresult))
                        {
                            gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxBLACK);
                            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, expresult);
                        }
                        else
                        {
                            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPERRORDISP);
                            gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxRED);
                            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, cellval);
                            rowhaserr = true;
                        }
                    }
                    else
                    {
                        gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxBLACK);
                        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, cellval);
                    }
                }

                if (rowhaserr)
                    SetGridRowTextColour(gridColsSampleData, pvgridrowidx, wxRED);
            }

        }

    }

    if (testimp && normtrans && impmode==IMPORTMODECOPY)
    {
        if (copybolcking)
        {
            if (!m_impconn->FlushData())
            {
                txtColsImpLog->AppendText(_("Failed to flush data: "));
                txtColsImpLog->AppendText(m_impconn->GetLastError());
            }
            else
            {
                txtColsImpLog->AppendText(_("Data flushed."));
                txtColsImpLog->AppendText(END_OF_LINE);
            }
        }

        if (m_impconn->PutCopyEnd())
        {        
            txtColsImpLog->AppendText(_("COPY command ended."));
        }
        else
        {
            txtColsImpLog->AppendText(_("Failed to end copy."));
            txtColsImpLog->AppendText(END_OF_LINE);
            txtColsImpLog->AppendText(m_impconn->GetLastError());
        }
    }
    else if (testimp && normtrans && pvgridrowidx && impmode==IMPORTMODEINSERT)
    {
        insstmt.Empty();
        //_itow(pvgridrowidx-1, tmpspsn, 10);
        swprintf(tmpspsn, 32, wxT("%d"), pvgridrowidx-1);

        if (m_impconn->GetAsyncResult())
        {
            txtColsImpLog->AppendText(_("Succeeded."));
            txtColsImpLog->AppendText(END_OF_LINE);
        }
        else
        {
            txtColsImpLog->AppendText(m_impconn->GetLastError());
            insstmt.Append(wxT("ROLLBACK TO ")).Append(tmpspname).Append(tmpspsn).Append(wxT(';'));
        }

        insstmt.Append(wxT("RELEASE SAVEPOINT ")).Append(tmpspname).Append(tmpspsn).Append(wxT(';'));
        if (!m_impconn->ExecuteVoid(insstmt, false))
            txtColsImpLog->AppendText(m_impconn->GetLastError());
    }

    if (testimp && normtrans)
    {
        txtColsImpLog->AppendText(END_OF_LINE);
        if (m_impconn->ExecuteVoid(wxT("ROLLBACK;"), false))
            txtColsImpLog->AppendText(_("Tranction rollbacked."));
        else
        {
            txtColsImpLog->AppendText(_("Failed to rollback tranction:"));
            txtColsImpLog->AppendText(END_OF_LINE);
            txtColsImpLog->AppendText(m_impconn->GetLastError());
        }
    }

    gridColsSampleData->Scroll(gridxpos, gridypos);
    gridColsSampleData->Thaw();
    txtColsSettingLog->Thaw();
    txtColsImpLog->Thaw();
}

void frmImport::GenColsEmptyPreview(size_t pvgridrowidx, size_t pvgridcolidx, size_t dbcolidx,
            int onemptyact, wxString& tgtcoltype, exprSnippet*& exprsnip, wxDateTime& clienttime, bool& haserror)
{
    wxString curcdate = clienttime.FormatISODate();
    wxString curctime = clienttime.FormatISOTime();
    wxString curctimetz = clienttime.Format(ANSICTIMETZFMT);
    wxString curcts = clienttime.Format(ANSICTIMESTAMPFMT);
    wxString curctstz = clienttime.Format(ANSICTIMESTAMPTZFMT);

    if (onemptyact==IMPCONST || onemptyact==IMPEXPR)
    {
        if (m_colexprarr[dbcolidx])
            exprsnip = m_colexprarr[dbcolidx];
        else
        {
            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPNULLDISP);
            gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxBLUE);
        }
    }
    else if (m_colonemptyact[dbcolidx]==IMPEMPTY)
    {
    }
    else if (onemptyact==IMPNULL)
    {
        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPNULLDISP);
        gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxBLUE);
    }
    else if (onemptyact==IMPDEFAULT)
    {
        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPDEFAULTDISP);
        gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxBLUE);
    }
    else if (onemptyact==IMPZERONUMBER)
        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPZERONUMBERDISP);
    else if (onemptyact==IMPZEROINTERVAL)
        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPZEROINTERVALDISP);
    else if (onemptyact==IMPCURTS)
    {
        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPCURTSDISP);
        gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxBLUE);
    }
    else if (onemptyact==IMPCURDATE)
    {
        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPCURDATEDISP);
        gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxBLUE);
    }
    else if (onemptyact==IMPCURTIME)
    {
        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPCURTIMEDISP);
        gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxBLUE);
    }
    else if (onemptyact==IMPLOCALTS)
    {
        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPLOCALTSDISP);
        gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxBLUE);
    }
    else if (onemptyact==IMPLOCALTIME)
    {
        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPLOCALTIMEDISP);
        gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxBLUE);
    }
    else if (onemptyact==IMPCLIENTTIME)
    {
        if (tgtcoltype==wxT("date"))
            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, curcdate);
        else if (tgtcoltype==wxT("timestamp") || tgtcoltype==wxT("timestamp without time zone"))
            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, curcts);
        else if (tgtcoltype==wxT("timestamptz") || tgtcoltype==wxT("timestamp with time zone"))
            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, curctstz);
        else if (tgtcoltype==wxT("time") || tgtcoltype==wxT("time without time zone"))
            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, curctime);
        else if (tgtcoltype==wxT("timetz") || tgtcoltype==wxT("time with time zone"))
            gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, curctimetz);
        else
        {
            gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxRED);
            haserror = true;
        }
    }
    else if (onemptyact==IMPBOOLTRUE)
        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPBOOLTRUEDISP);
    else if (onemptyact==IMPBOOLFALSE)
        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPBOOLFALSEDISP);
    else
    {
        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPNULLDISP);
        gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxRED);
        haserror = true;
    }

    haserror = false;
}

bool frmImport::GenCSVSQLData(wxString& rowdatalist, wxString& rowlog, wxString** linedata,
                           const size_t colnum, wxDateTime& clienttime, const int impmode, const int formatstyle)
{
    if (!linedata)
        return false;

    wxString vallist, tgtcoltype, colval, snipval, expresult;
    exprSnippet *exprsnip;
    bool isfirstcol = true, haserror = false;
    wxDateTime tsfromval;

    size_t dbcolidx, pvgridcolidx;

    wxString curcdate = clienttime.FormatISODate();
    wxString curctime = clienttime.FormatISOTime();
    wxString curctimetz = clienttime.Format(ANSICTIMETZFMT);
    wxString curcts = clienttime.Format(ANSICTIMESTAMPFMT);
    wxString curctstz = clienttime.Format(ANSICTIMESTAMPTZFMT);

    for (dbcolidx=0, pvgridcolidx=0; dbcolidx<m_tgtcolsnum; dbcolidx++)
    {
        tgtcoltype = (*(m_tgtcolstypes+dbcolidx))->Name();
        exprsnip = NULL;

        if (m_colsrccolnr[dbcolidx]==wxNOT_FOUND)
        {
            if (impmode==IMPORTMODECOPY && m_colonemptyact[dbcolidx]==IMPDEFAULT)
                continue;

            if (isfirstcol)
                isfirstcol = false;
            else
            {
                if (impmode==IMPORTMODECOPY)
                    rowdatalist.Append(wxT("\t"));
                else
                    rowdatalist.Append(wxT(", "));
            }

            if (m_colonemptyact[dbcolidx]==IMPCONST || m_colonemptyact[dbcolidx]==IMPEXPR)
            {
                if (m_colexprarr[dbcolidx])
                    exprsnip = m_colexprarr[dbcolidx];
                else
                {
                    if (impmode!=IMPORTMODECOPY)
                        rowdatalist.Append(wxT("NULL"));
                }
            }/*
            else if (m_colonemptyact[dbcolidx]==IMPEXPR)
            {
                if (m_colexprarr[dbcolidx])
                    exprsnip = m_colexprarr[dbcolidx];
                else if (indicatenull)
                {
                    gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPNULLDISP);
                    gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxBLUE);
                }
            }*/
            else if (m_colonemptyact[dbcolidx]==IMPEMPTY)
            {
                if (impmode!=IMPORTMODECOPY)
                    rowdatalist.Append(wxT("''"));
            }
            else if (m_colonemptyact[dbcolidx]==IMPNULL)
            {
                if (impmode==IMPORTMODECOPY)
                    rowdatalist.Append(wxT("\\N"));
                else
                    rowdatalist.Append(wxT("NULL"));
            }
            else if (m_colonemptyact[dbcolidx]==IMPDEFAULT)
                rowdatalist.Append(wxT("DEFAULT"));
            else if (m_colonemptyact[dbcolidx]==IMPZERONUMBER)
                rowdatalist.Append(IMPZERONUMBERDISP);
            else if (m_colonemptyact[dbcolidx]==IMPZEROINTERVAL)
                rowdatalist.Append(IMPZEROINTERVALDISP);
            else if (m_colonemptyact[dbcolidx]==IMPCURTS)
                rowdatalist.Append(IMPCURTSDISP);
            else if (m_colonemptyact[dbcolidx]==IMPCURDATE)
                rowdatalist.Append(IMPCURDATEDISP);
            else if (m_colonemptyact[dbcolidx]==IMPCURTIME)
                rowdatalist.Append(IMPCURTIMEDISP);
            else if (m_colonemptyact[dbcolidx]==IMPLOCALTS)
                rowdatalist.Append(IMPLOCALTSDISP);
            else if (m_colonemptyact[dbcolidx]==IMPLOCALTIME)
                rowdatalist.Append(IMPLOCALTIMEDISP);
            else if (m_colonemptyact[dbcolidx]==IMPCLIENTTIME)
            {
                if (tgtcoltype==wxT("date"))
                    rowdatalist.Append(curcdate);
                else if (tgtcoltype==wxT("timestamp") || tgtcoltype==wxT("timestamp without time zone"))
                    rowdatalist.Append(curcts);
                else if (tgtcoltype==wxT("timestamptz") || tgtcoltype==wxT("timestamp with time zone"))
                    rowdatalist.Append(curctstz);
                else if (tgtcoltype==wxT("time") || tgtcoltype==wxT("time without time zone"))
                    rowdatalist.Append(curctime);
                else if (tgtcoltype==wxT("timetz") || tgtcoltype==wxT("time with time zone"))
                    rowdatalist.Append(curctimetz);
            }
            else
            {
                haserror = true;
                rowdatalist.Append(wxT("<ERROR>"));
            }
        }
        else
        {
            if (isfirstcol)
                isfirstcol = false;
            else
            {
                if (impmode==IMPORTMODECOPY)
                    rowdatalist.Append(wxT("\t"));
                else
                    rowdatalist.Append(wxT(", "));
            }

            if (m_colsrccolnr[dbcolidx]>=(int)colnum || !linedata[m_colsrccolnr[dbcolidx]])
            {
                if (m_colonemptyact[dbcolidx]==IMPERROR)
                {
                    if (haserror)
                        rowlog.Append(END_OF_LINE);
                    rowlog.Append(_("Column: ") + *m_tgtcolsnames[dbcolidx] + _("%s can not be NULL."));
                    haserror = true;
                    rowdatalist.Append(wxT("<NULL>"));
                }
                else if (m_colonemptyact[dbcolidx]==IMPNULL)
                {
                    if (impmode==IMPORTMODECOPY)
                        rowdatalist.Append(wxT("\\N"));
                    else
                        rowdatalist.Append(wxT("NULL"));
                }
                else if (impmode==IMPORTMODEINSERT && m_colonemptyact[dbcolidx]==IMPDEFAULT)
                    rowdatalist.Append(wxT("DEFAULT"));
                else if (m_colonemptyact[dbcolidx]==IMPEMPTY)
                {
                    if (impmode==IMPORTMODECOPY);
                        //rowdatalist.Append(wxT("\"\""));
                    else
                        rowdatalist.Append(wxT("''"));
                }
                else if (m_colonemptyact[dbcolidx]==IMPZERONUMBER)
                    rowdatalist.Append(IMPZERONUMBERDISP);
                else if (m_colonemptyact[dbcolidx]==IMPZEROINTERVAL)
                    rowdatalist.Append(IMPZEROINTERVALDISP);
                else if (m_colonemptyact[dbcolidx]==IMPCONST || m_colonemptyact[dbcolidx]==IMPEXPR)
                {
                    if (m_colexprarr[dbcolidx])
                        exprsnip = m_colexprarr[dbcolidx];
                    else
                    {
                        if (impmode==IMPORTMODECOPY)
                            rowdatalist.Append(wxT("\\N"));
                        else
                            rowdatalist.Append(wxT("NULL"));
                    }
                }
                /*else if (m_colonemptyact[dbcolidx]==IMPEXPR)
                {
                    if (m_colexprarr[dbcolidx])
                        exprsnip = m_colexprarr[dbcolidx];
                    else if (indicatenull)
                    {
                        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, IMPNULLDISP);
                        gridColsSampleData->SetCellTextColour(pvgridrowidx, pvgridcolidx, *wxBLUE);
                    }
                }*/
                else if (m_colonemptyact[dbcolidx]==IMPCURTS)
                    rowdatalist.Append(IMPCURTSDISP);
                else if (m_colonemptyact[dbcolidx]==IMPCURDATE)
                    rowdatalist.Append(IMPCURDATEDISP);
                else if (m_colonemptyact[dbcolidx]==IMPCURTIME)
                    rowdatalist.Append(IMPCURTIMEDISP);
                else if (m_colonemptyact[dbcolidx]==IMPLOCALTS)
                    rowdatalist.Append(IMPLOCALTSDISP);
                else if (m_colonemptyact[dbcolidx]==IMPLOCALTIME)
                    rowdatalist.Append(IMPLOCALTIMEDISP);
                else if (m_colonemptyact[dbcolidx]==IMPCLIENTTIME)
                {
                    if (tgtcoltype==wxT("date"))
                        rowdatalist.Append(curcdate);
                    else if (tgtcoltype==wxT("timestamp") || tgtcoltype==wxT("timestamp without time zone"))
                        rowdatalist.Append(curcts);
                    else if (tgtcoltype==wxT("timestamptz") || tgtcoltype==wxT("timestamp with time zone"))
                        rowdatalist.Append(curctstz);
                    else if (tgtcoltype==wxT("time") || tgtcoltype==wxT("time without time zone"))
                        rowdatalist.Append(curctime);
                    else if (tgtcoltype==wxT("timetz") || tgtcoltype==wxT("time with time zone"))
                        rowdatalist.Append(curctimetz);
                }
                else
                {
                    haserror = true;
                    rowdatalist.Append(wxT("<ERROR>"));
                }
            }
            else
            {
                colval = *linedata[m_colsrccolnr[dbcolidx]];
                if (m_coltrimmode[dbcolidx]==TRIMALL)
                    colval.Trim().Trim(false);
                else if (m_coltrimmode[dbcolidx]==TRIMLEFT)
                    colval.Trim(false);
                else if (m_coltrimmode[dbcolidx]==TRIMRIGHT)
                    colval.Trim();

                if (colval.IsEmpty())
                {
                    if (m_colonemptyact[dbcolidx]==IMPERROR)
                    {
                        if (haserror)
                            rowlog.Append(END_OF_LINE);
                        rowlog.Append(_("Column: ")).Append(*m_tgtcolsnames[m_colsrccolnr[dbcolidx]]);
                        rowlog.Append(_(" can not be empty."));
                        haserror = true;
                        rowdatalist.Append(wxT("<ERROR>"));
                    }
                    else if (impmode==IMPORTMODECOPY && m_colonemptyact[dbcolidx]==IMPDEFAULT)
                        rowdatalist.Append(wxT("DEFAULT"));
                    else if (m_colonemptyact[dbcolidx]==IMPZERONUMBER)
                        rowdatalist.Append(IMPZERONUMBERDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPZEROINTERVAL)
                        rowdatalist.Append(IMPZEROINTERVALDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPEMPTY)
                    {
                        if (impmode==IMPORTMODECOPY);
                            //rowdatalist.Append(wxT("\"\""));
                        else
                            rowdatalist.Append(wxT("''"));
                    }
                    else if (m_colonemptyact[dbcolidx]==IMPCONST || m_colonemptyact[dbcolidx]==IMPEXPR)
                    {
                        if (m_colexprarr[dbcolidx])
                            exprsnip = m_colexprarr[dbcolidx];
                        else
                        {
                            if (impmode==IMPORTMODECOPY)
                                rowdatalist.Append(wxT("\\N"));
                            else
                                rowdatalist.Append(wxT("NULL"));
                        }
                    }
                    /*else if (m_colonemptyact[dbcolidx]==IMPEXPR)
                    {
                        gridColsSampleData->SetCellValue(pvgridrowidx, pvgridcolidx, );
                    }*/
                    else if (m_colonemptyact[dbcolidx]==IMPCURTS)
                        rowdatalist.Append(IMPCURTSDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPCURDATE)
                        rowdatalist.Append(IMPCURDATEDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPCURTIME)
                        rowdatalist.Append(IMPCURTIMEDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPLOCALTS)
                        rowdatalist.Append(IMPLOCALTSDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPLOCALTIME)
                        rowdatalist.Append(IMPLOCALTIMEDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPCLIENTTIME)
                    {
                        if (tgtcoltype==wxT("date"))
                            rowdatalist.Append(curcdate);
                        else if (tgtcoltype==wxT("timestamp") || tgtcoltype==wxT("timestamp without time zone"))
                            rowdatalist.Append(curcts);
                        else if (tgtcoltype==wxT("timestamptz") || tgtcoltype==wxT("timestamp with time zone"))
                            rowdatalist.Append(curctstz);
                        else if (tgtcoltype==wxT("time") || tgtcoltype==wxT("time without time zone"))
                            rowdatalist.Append(curctime);
                        else if (tgtcoltype==wxT("timetz") || tgtcoltype==wxT("time with time zone"))
                            rowdatalist.Append(curctimetz);
                        else
                        {
                            haserror = true;
                            rowdatalist.Append(wxT("<ERROR>"));
                        }
                    }
                    else
                    {
                        haserror = true;
                        rowdatalist.Append(wxT("<ERROR>"));
                    }
                }
                else
                {
                    if (m_colfmtarr[dbcolidx])
                    {
                        if (tgtcoltype==wxT("date") && tsfromval.ParseFormat(colval.GetData(), m_colfmtarr[dbcolidx]->GetData()))
                            rowdatalist.Append(tsfromval.FormatISODate());
                        else if ((tgtcoltype==wxT("timestamp") || tgtcoltype==wxT("timestamp without time zone"))
                                && tsfromval.ParseFormat(colval.GetData(), m_colfmtarr[dbcolidx]->GetData()))
                            rowdatalist.Append(tsfromval.Format(ANSICTIMESTAMPFMT.GetData()));
                        else if ((tgtcoltype==wxT("timestamptz") || tgtcoltype==wxT("timestamp with time zone"))
                                && tsfromval.ParseFormat(colval.GetData(), m_colfmtarr[dbcolidx]->GetData()))
                            rowdatalist.Append(tsfromval.Format(ANSICTIMESTAMPTZFMT.GetData()));
                        else if ((tgtcoltype==wxT("time") || tgtcoltype==wxT("time without time zone"))
                                && tsfromval.ParseFormat(colval.GetData(), m_colfmtarr[dbcolidx]->GetData()))
                            rowdatalist.Append(tsfromval.FormatISOTime());
                        else if ((tgtcoltype==wxT("timetz") || tgtcoltype==wxT("time with time zone"))
                                && tsfromval.ParseFormat(colval.GetData(), m_colfmtarr[dbcolidx]->GetData()))
                            rowdatalist.Append(tsfromval.Format(ANSICTIMETZFMT.GetData()));
                        else
                        {
                            haserror = true;
                            rowdatalist.Append(wxT("<ERROR>"));
                        }
                    }
                    else if ((m_booltruevals || m_boolfalsevals) && tgtcoltype==wxT("boolean"))
                    {
                        if (m_booltruevals)
                        {
                            if (m_booltruevals->Index(colval)!=wxNOT_FOUND)
                                rowdatalist.Append(wxT("true"));
                            else
                                rowdatalist.Append(wxT("false"));
                        }
                        else
                        {
                            if (m_boolfalsevals->Index(colval)!=wxNOT_FOUND)
                                rowdatalist.Append(wxT("false"));
                            else
                                rowdatalist.Append(wxT("true"));
                        }
                    }
                    else
                    {
                        if (impmode==IMPORTMODECOPY)
                        {
                            colval.Replace(wxT("\t"), wxT("\\t"));
                            rowdatalist.Append(colval);
                        }
                        else
                        {
                            colval.Replace(wxT("'"), wxT("''"));
                            rowdatalist.Append(wxT("'"));
                            rowdatalist.Append(colval);
                            rowdatalist.Append(wxT("'"));
                        }
                    }
                }
            }
        }

        if (exprsnip)
        {
            colval.Empty();
            if (m_colonemptyact[dbcolidx]==IMPCONST)
            {
                colval = exprsnip->exprsnippet;
                /*if (impmode==IMPORTMODEINSERT)
                {
                    colval.Replace(wxT("'"), wxT("''"));
                    colval = wxT("'") + colval + wxT("'");
                }*/
            }
            else
                while (exprsnip)
                {
                    if (exprsnip->snippettype==wxNOT_FOUND)
                        colval.Append(exprsnip->exprsnippet);
                    else
                    {
                        if (exprsnip->snippettype>=(int)colnum || !linedata[exprsnip->snippettype])
                            colval.Append(wxT("NULL"));
                        else
                        {
                            snipval = *linedata[exprsnip->snippettype];
                            snipval.Replace(wxT("'"), wxT("''"));
                            colval.Append(wxT("'")).Append(snipval).Append(wxT("'"));
                        }
                    }
                    exprsnip = exprsnip->nextsnip;
                }

            rowdatalist.Append(colval);
        }

        pvgridcolidx++;
    }

    return !haserror;
}

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
bool frmImport::GenODBCSQLData(wxString& rowvals, wxString& rowlog, SQLWCHAR** odbcdata,
                            wxDateTime& clienttime, const int impmode)
{
    wxString vallist, tgtcoltype, snipval, expresult, colval, logmsg;
    exprSnippet *exprsnip;
    bool isfirstcol = true, haserror = false;
    wxDateTime tsfromval;
    SQLSMALLINT odbccoltype;

    size_t dbcolidx, pvgridcolidx;

    wxString curcdate = clienttime.FormatISODate();
    wxString curctime = clienttime.FormatISOTime();
    wxString curctimetz = clienttime.Format(ANSICTIMETZFMT);
    wxString curcts = clienttime.Format(ANSICTIMESTAMPFMT);
    wxString curctstz = clienttime.Format(ANSICTIMESTAMPTZFMT);

    for (dbcolidx=0, pvgridcolidx=0; dbcolidx<m_tgtcolsnum; dbcolidx++)
    {
        tgtcoltype = (*(m_tgtcolstypes+dbcolidx))->Name();
        exprsnip = NULL;

        if (m_colsrccolnr[dbcolidx]==wxNOT_FOUND)
        {
            if (impmode==IMPORTMODECOPY && m_colonemptyact[dbcolidx]==IMPDEFAULT)
                continue;

            if (isfirstcol)
                isfirstcol = false;
            else
            {
                if (impmode==IMPORTMODECOPY)
                    rowvals.Append(wxT("\t"));
                else
                    rowvals.Append(wxT(", "));
            }

            if (m_colonemptyact[dbcolidx]==IMPCONST || m_colonemptyact[dbcolidx]==IMPEXPR)
            {
                if (m_colexprarr[dbcolidx])
                    exprsnip = m_colexprarr[dbcolidx];
                else
                {
                    if (impmode!=IMPORTMODECOPY)
                        rowvals.Append(wxT("NULL"));
                }
            }
            else if (m_colonemptyact[dbcolidx]==IMPEMPTY)
            {
                if (impmode!=IMPORTMODECOPY)
                    rowvals.Append(wxT("''"));
            }
            else if (m_colonemptyact[dbcolidx]==IMPDEFAULT)
                rowvals.Append(wxT("DEFAULT"));
            else if (m_colonemptyact[dbcolidx]==IMPZERONUMBER)
                rowvals.Append(IMPZERONUMBERDISP);
            else if (m_colonemptyact[dbcolidx]==IMPZEROINTERVAL)
                rowvals.Append(IMPZEROINTERVALDISP);
            else if (m_colonemptyact[dbcolidx]==IMPCURTS)
                rowvals.Append(IMPCURTSDISP);
            else if (m_colonemptyact[dbcolidx]==IMPCURDATE)
                rowvals.Append(IMPCURDATEDISP);
            else if (m_colonemptyact[dbcolidx]==IMPCURTIME)
                rowvals.Append(IMPCURTIMEDISP);
            else if (m_colonemptyact[dbcolidx]==IMPLOCALTS)
                rowvals.Append(IMPLOCALTSDISP);
            else if (m_colonemptyact[dbcolidx]==IMPLOCALTIME)
                rowvals.Append(IMPLOCALTIMEDISP);
            else if (m_colonemptyact[dbcolidx]==IMPCLIENTTIME)
            {
                if (tgtcoltype==wxT("date"))
                    rowvals.Append(curcdate);
                else if (tgtcoltype==wxT("timestamp") || tgtcoltype==wxT("timestamp without time zone"))
                    rowvals.Append(curcts);
                else if (tgtcoltype==wxT("timestamptz") || tgtcoltype==wxT("timestamp with time zone"))
                    rowvals.Append(curctstz);
                else if (tgtcoltype==wxT("time") || tgtcoltype==wxT("time without time zone"))
                    rowvals.Append(curctime);
                else if (tgtcoltype==wxT("timetz") || tgtcoltype==wxT("time with time zone"))
                    rowvals.Append(curctimetz);
            }
            else
            {
                haserror = true;
                rowvals.Append(wxT("<ERROR>"));
            }
        }
        else
        {
            if (isfirstcol)
                isfirstcol = false;
            else
            {
                if (impmode==IMPORTMODECOPY)
                    rowvals.Append(wxT("\t"));
                else
                    rowvals.Append(wxT(", "));
            }

            if (odbcdata[m_colsrccolnr[dbcolidx]])
            {
                if (odbcdata[m_colsrccolnr[dbcolidx]][0])
                {
                    odbccoltype = m_srcodbc->GetResultColType(m_colsrccolnr[dbcolidx]);
                    if (odbccoltype==SQL_CHAR || odbccoltype==SQL_VARCHAR || odbccoltype==SQL_LONGVARCHAR
                            || odbccoltype==SQL_WCHAR || odbccoltype==SQL_WVARCHAR || odbccoltype==SQL_WLONGVARCHAR)
                    {
                        colval.Empty();
                        colval.Append(odbcdata[m_colsrccolnr[dbcolidx]]);
                        if (impmode==IMPORTMODEINSERT)
                        {
                            colval.Replace(wxT("'"), wxT("''"));
                            rowvals.Append(wxT('\'')).Append(colval).Append(wxT('\''));
                        }
                        else
                        {
                            colval.Replace(wxT("\\"), wxT("\\\\"));
                            colval.Replace(wxT("\t"), wxT("\\t"));
                            colval.Replace(wxT("\r"), wxT("\\r"));
                            colval.Replace(wxT("\n"), wxT("\\n"));
                            rowvals.Append(colval);
                        }
                    }
                    else if (impmode==IMPORTMODEINSERT && (odbccoltype==SQL_INTERVAL
                            || odbccoltype==SQL_TIMESTAMP || odbccoltype==SQL_DATETIME))
                    {
                        colval.Empty();
                        colval.Append(odbcdata[m_colsrccolnr[dbcolidx]]);
                        rowvals.Append(wxT('\'')).Append(colval).Append(wxT('\''));
                    }
                    else
                        rowvals.Append(odbcdata[m_colsrccolnr[dbcolidx]]);
                }
                else
                {
                    if (m_colonemptyact[dbcolidx]==IMPERROR)
                    {
                        logmsg.Printf(_("Column: %s can not be empty."),
                                *m_tgtcolsnames[m_colsrccolnr[dbcolidx]]);

                        if (haserror)
                            rowlog.Append(END_OF_LINE);
                        rowlog.Append(logmsg);

                        haserror = true;
                        rowvals.Append(wxT("<ERROR>"));
                    }
                    else if (impmode==IMPORTMODECOPY && m_colonemptyact[dbcolidx]==IMPDEFAULT)
                        rowvals.Append(wxT("DEFAULT"));
                    else if (m_colonemptyact[dbcolidx]==IMPZERONUMBER)
                        rowvals.Append(IMPZERONUMBERDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPZEROINTERVAL)
                        rowvals.Append(IMPZEROINTERVALDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPEMPTY)
                    {
                        if (impmode==IMPORTMODECOPY);
                            //rowvals.Append(wxT("\"\""));
                        else
                            rowvals.Append(wxT("''"));
                    }
                    else if (m_colonemptyact[dbcolidx]==IMPCONST || m_colonemptyact[dbcolidx]==IMPEXPR)
                    {
                        if (m_colexprarr[dbcolidx])
                            exprsnip = m_colexprarr[dbcolidx];
                        else
                        {
                            if (impmode==IMPORTMODECOPY)
                                rowvals.Append(wxT("\\N"));
                            else
                                rowvals.Append(wxT("NULL"));
                        }
                    }
                    else if (m_colonemptyact[dbcolidx]==IMPCURTS)
                        rowvals.Append(IMPCURTSDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPCURDATE)
                        rowvals.Append(IMPCURDATEDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPCURTIME)
                        rowvals.Append(IMPCURTIMEDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPLOCALTS)
                        rowvals.Append(IMPLOCALTSDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPLOCALTIME)
                        rowvals.Append(IMPLOCALTIMEDISP);
                    else if (m_colonemptyact[dbcolidx]==IMPCLIENTTIME)
                    {
                        if (tgtcoltype==wxT("date"))
                            rowvals.Append(curcdate);
                        else if (tgtcoltype==wxT("timestamp") || tgtcoltype==wxT("timestamp without time zone"))
                            rowvals.Append(curcts);
                        else if (tgtcoltype==wxT("timestamptz") || tgtcoltype==wxT("timestamp with time zone"))
                            rowvals.Append(curctstz);
                        else if (tgtcoltype==wxT("time") || tgtcoltype==wxT("time without time zone"))
                            rowvals.Append(curctime);
                        else if (tgtcoltype==wxT("timetz") || tgtcoltype==wxT("time with time zone"))
                            rowvals.Append(curctimetz);
                        else
                        {
                            haserror = true;
                            rowvals.Append(wxT("<ERROR>"));
                        }
                    }
                    else
                    {
                        haserror = true;
                        rowvals.Append(wxT("<ERROR>"));
                    }
                }
            }
            else
            {
                if (m_colonemptyact[dbcolidx]==IMPERROR)
                {
                    logmsg.Printf(_("Column: %s can not be NULL."),
                                *m_tgtcolsnames[m_colsrccolnr[dbcolidx]]);

                    if (haserror)
                        rowlog.Append(END_OF_LINE);
                    rowlog.Append(logmsg);

                    haserror = true;
                    rowvals.Append(wxT("<ERROR>"));
                }
                else if (m_colonemptyact[dbcolidx]==IMPNULL)
                {
                    if (impmode==IMPORTMODECOPY)
                        rowvals.Append(wxT("\\N"));
                    else
                        rowvals.Append(wxT("NULL"));
                }
                else if (impmode==IMPORTMODECOPY && m_colonemptyact[dbcolidx]==IMPDEFAULT)
                    rowvals.Append(wxT("DEFAULT"));
                else if (m_colonemptyact[dbcolidx]==IMPEMPTY)
                {
                    if (impmode==IMPORTMODECOPY);
                        //rowvals.Append(wxT("\"\""));
                    else
                        rowvals.Append(wxT("''"));
                }
                else if (m_colonemptyact[dbcolidx]==IMPZERONUMBER)
                    rowvals.Append(IMPZERONUMBERDISP);
                else if (m_colonemptyact[dbcolidx]==IMPZEROINTERVAL)
                    rowvals.Append(IMPZEROINTERVALDISP);
                else if (m_colonemptyact[dbcolidx]==IMPCONST || m_colonemptyact[dbcolidx]==IMPEXPR)
                {
                    if (m_colexprarr[dbcolidx])
                        exprsnip = m_colexprarr[dbcolidx];
                    else
                    {
                        if (impmode==IMPORTMODECOPY)
                            rowvals.Append(wxT("\\N"));
                        else
                            rowvals.Append(wxT("NULL"));
                    }
                }
                else if (m_colonemptyact[dbcolidx]==IMPCURTS)
                    rowvals.Append(IMPCURTSDISP);
                else if (m_colonemptyact[dbcolidx]==IMPCURDATE)
                    rowvals.Append(IMPCURDATEDISP);
                else if (m_colonemptyact[dbcolidx]==IMPCURTIME)
                    rowvals.Append(IMPCURTIMEDISP);
                else if (m_colonemptyact[dbcolidx]==IMPLOCALTS)
                    rowvals.Append(IMPLOCALTSDISP);
                else if (m_colonemptyact[dbcolidx]==IMPLOCALTIME)
                    rowvals.Append(IMPLOCALTIMEDISP);
                else if (m_colonemptyact[dbcolidx]==IMPCLIENTTIME)
                {
                    if (tgtcoltype==wxT("date"))
                        rowvals.Append(curcdate);
                    else if (tgtcoltype==wxT("timestamp") || tgtcoltype==wxT("timestamp without time zone"))
                        rowvals.Append(curcts);
                    else if (tgtcoltype==wxT("timestamptz") || tgtcoltype==wxT("timestamp with time zone"))
                        rowvals.Append(curctstz);
                    else if (tgtcoltype==wxT("time") || tgtcoltype==wxT("time without time zone"))
                        rowvals.Append(curctime);
                    else if (tgtcoltype==wxT("timetz") || tgtcoltype==wxT("time with time zone"))
                        rowvals.Append(curctimetz);
                }
                else
                {
                    haserror = true;
                    rowvals.Append(wxT("<ERROR>"));
                }
            }
        }
        
        if (exprsnip)
        {
            colval.Empty();
            if (m_colonemptyact[dbcolidx]==IMPCONST)
                colval = exprsnip->exprsnippet;
            else
                while (exprsnip)
                {
                    if (exprsnip->snippettype==wxNOT_FOUND)
                        colval.Append(exprsnip->exprsnippet);
                    else
                    {
                        if (odbcdata[exprsnip->snippettype])
                        {
                            snipval = *odbcdata[exprsnip->snippettype];
                            snipval.Replace(wxT("'"), wxT("''"));
                            colval.Append(wxT("'")).Append(snipval).Append(wxT("'"));
                        }
                        else
                            colval.Append(wxT("NULL"));
                    }
                    exprsnip = exprsnip->nextsnip;
                }

            rowvals.Append(colval);
        }

    }

    return !haserror;
}
#endif

void frmImport::InitWizardPage(WizardPageNumber pagenr)
{
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    const SQLWCHAR **infonames;
    wxString **odbcstmt;
#endif

    size_t encidx;

    switch(pagenr)
    {
    case TEXTFILEPAGE:
        if (m_txtfilepginit)
            break;

        // register event
        txtTextFilePath->Connect(wxEVT_KILL_FOCUS, wxFocusEventHandler(frmImport::OnTextFilePathKillFocus), NULL, this );

        chcTextFileEncoding->Clear();
        for ( encidx=0; gs_encodingdescs[encidx]; encidx++)
            chcTextFileEncoding->Append(gs_encodingdescs[encidx]);
        chcTextFileEncoding->SetSelection(0);

        m_txtfilepginit = true;
        break;
    case ARCHFILEPAGE:
        if (m_archfilepginit)
            break;

        // register event
        txtArchFilePath->Connect(wxEVT_KILL_FOCUS, wxFocusEventHandler(frmImport::OnArchFilePathKillFocus), NULL, this );

        chcArchFileEncoding->Clear();
        for ( encidx=0; gs_encodingdescs[encidx]; encidx++)
            chcArchFileEncoding->Append(gs_encodingdescs[encidx]);
        chcArchFileEncoding->SetSelection(0);

        chcArchEntryEncoding->Clear();
        for ( encidx=0; gs_encodingdescs[encidx]; encidx++)
            chcArchEntryEncoding->Append(gs_encodingdescs[encidx]);
        chcArchEntryEncoding->SetSelection(0);

        m_archfilepginit = true;
        break;
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    case SRCODBCPAGE:
        if (m_srcodbcpginit)
            break;

        gridODBCPreview->CreateGrid(0, 0);
        gridODBCPreview->SetRowLabelSize(40);

        lstODBCResultAttribs->AddColumn(_("Column name"), 150);
        lstODBCResultAttribs->AddColumn(_("Type"), 160);

        lstODBCConnInfo->AddColumn(_("Name"), 150);
        lstODBCConnInfo->AddColumn(_("Value"), 160);

        lstODBCTableCols->AddColumn(_("Column name"), 110);
        lstODBCTableCols->AddColumn(_("Type"), 100);
        lstODBCTableCols->AddColumn(_("Type name"), 100);

        size_t nameidx;
        infonames = pgODBC::GetInfoNames();
        for (nameidx=0; infonames[nameidx]; nameidx++)
            lstODBCConnInfo->AppendItem(infonames[nameidx]);

        odbcstmt = new wxString *[2];
        odbcstmt[0] = new wxString;
        odbcstmt[1] = new wxString;
        txtODBCSQLStmt->SetClientData(odbcstmt);

        m_srcodbcpginit = true;
        break;
#endif
    case SRCPGSQLPAGE:
        if (m_srcpgsqlpginit)
            break;

        m_srcpgsqlpginit = true;
        break;
    case FILESETTINGPAGE:
        if (m_filesettingpginit)
            break;

        // register event
        txtFileWidthDelimiter->Connect( wxEVT_KILL_FOCUS, wxFocusEventHandler( frmImport::OnFileFixedWidthKillFocus ), NULL, this );

        cbFileCharDelimiter->Clear();
        cbFileCharDelimiter->Append(wxT(","));
        cbFileCharDelimiter->Append(wxT(";"));
        cbFileCharDelimiter->Append(wxT(":"));
        cbFileCharDelimiter->Append(wxT("|"));
        cbFileCharDelimiter->Append(IMPTABMARK);

        cbFileQuotedBy->Clear();
        cbFileQuotedBy->Append(IMPNONEQUOTEDBY);
        cbFileQuotedBy->Append(wxT("\""));
        cbFileQuotedBy->Append(wxT("'"));

        gridFilePreview->CreateGrid(0, 0);
        gridFilePreview->SetRowLabelSize(40);
        gridFilePreview->EnableEditing(false);

        m_filesettingpginit = true;
        break;
    case FILEOPTSPAGE:
        if (m_fileoptspginit)
            break;

        chkFileSkipEmptyRow->SetValue(true);
        txtFileOptImpRegEx->Clear();
        spinFileSkipFirstRows->SetValue(0);
        rdbFileImpAllRows->SetValue(true);
        spinFileImpOnlyRows->Disable();
        chkFileCheckTrueFirst->SetValue(true);
        chkFileBackSlashQuote->SetValue(false);
        txtFileOptFalseVals->Disable();

        m_fileoptspginit = true;
        break;
    default:
        break;
    }

}

void frmImport::ResetWizardPage(WizardPageNumber pagenr)
{
    size_t entriesnum, entriesnr;
    int impmode, impdest;

    switch(pagenr)
    {
    case TEXTFILEPAGE:
        m_srcfile->SetFileEncoding(gs_encodings[chcTextFileEncoding->GetSelection()]);

        if (m_srcfile->IsOpened())
            txtTextFilePath->ChangeValue(m_srcfile->GetFilePath());
        else
            m_srcfile->Open(txtTextFilePath->GetValue());

        GenFilePreview();

        break;
    case ARCHFILEPAGE:
        m_srcfile->SetFileEncoding(gs_encodings[chcArchFileEncoding->GetSelection()]);
        m_srcfile->SetEntryEncoding(gs_encodings[chcArchEntryEncoding->GetSelection()]);

        if (m_srcfile->IsOpened())
            txtTextFilePath->ChangeValue(m_srcfile->GetFilePath());
        else
            m_srcfile->Open(txtTextFilePath->GetValue());

        chcArchEntryName->Clear();
        chcArchEntryName->Append(wxT(""));

        entriesnum = m_srcfile->GetEntriesNum();
        entriesnr = 0;
        for (; entriesnr<entriesnum; entriesnr++)
        {
            chcArchEntryName->Append(m_srcfile->GetEntryName(entriesnr), m_srcfile->GetEntry(entriesnr));
            if (m_srcfile->GetEntry(entriesnr)==m_srcfile->GetCurrentEntry())
                chcArchEntryName->SetSelection(entriesnr+1);
        }

        GenFilePreview();

        break;
    case FILESETTINGPAGE:
        rdbFileCharDelimiter->SetValue(m_srcfile->IsDelimitedByChar());
        rdbFileWidthDelimiter->SetValue(m_srcfile->IsDelimitedByWidth());

        if (m_srcfile->GetCharDelimiter()==wxT("\t"))
            cbFileCharDelimiter->SetValue(IMPTABMARK);
        else
            cbFileCharDelimiter->SetValue(m_srcfile->GetCharDelimiter());

        cbFileCharDelimiter->Enable(m_srcfile->IsDelimitedByChar());
        txtFileWidthDelimiter->SetValue(m_srcfile->GetWidthDelimiterStr());
        txtFileWidthDelimiter->Enable(m_srcfile->IsDelimitedByWidth());

        if (m_srcfile->GetQuotedBy())
            cbFileQuotedBy->SetValue(m_srcfile->GetQuotedBy());
        else
            cbFileQuotedBy->SetValue(IMPNONEQUOTEDBY);

        chkFileFirstRowIsHeader->SetValue(m_srcfile->FirstRowIsHeader());
        chkFileBackSlashQuote->SetValue(m_srcfile->IsBackSlashQuote());
        chkFileForceLineBreak->SetValue(m_srcfile->IsForceLineBreak());

        ClearGrid(gridFilePreview);
        GenCSVPreview();

        break;
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    case SRCODBCPAGE:
        ClearODBCPreview();
        FetchODBCDSNList();

        chkODBCCustomizedSQL->SetValue(false);
        txtODBCWhereCl->Clear();

        cbODBCDSNNames->SetValue(wxEmptyString);
        cbODBCDSNNames->SetClientData(NULL);
        lstODBCTableCols->DeleteAllItems();
        txtODBCUsername->Clear();
        txtODBCPassword->Clear();
        cbODBCTableNames->SetValue(wxEmptyString);

        cbODBCDSNNames->Enable();
        txtODBCUsername->Enable();
        txtODBCPassword->Enable();
        btnODBCConnect->SetLabel(_("Connect"));

        m_srcodbcisok = false;

        break;
#endif
    case SRCPGSQLPAGE:
        break;
    case FILEOPTSPAGE:
        /*chkFileSkipEmptyRow->SetValue(true);
        txtFileOptImpRegEx->Clear();
        spinFileSkipFirstRows->SetValue(0);
        rdbFileImpAllRows->SetValue(true);
        spinFileImpOnlyRows->Disable();
        chkFileCheckTrueFirst->SetValue(true);
        chkFileBackSlashQuote->SetValue(false);
        txtFileOptFalseVals->Disable();*/

        break;
    case DESTLOGPAGE:
        impmode = (int)chcImportMode->GetClientData(chcImportMode->GetSelection());
        if (chcImpDest->GetCount())
            impdest = (int)chcImpDest->GetClientData(chcImpDest->GetSelection());
        else
            impdest = IMPDESTNONE;

        chcImpDest->Clear();
        if (impmode==IMPORTMODECOPY)
        {
            chcImpDest->Append(IMPDESTCOPYDISP, (void*)IMPDESTCOPY);
            chcImpDest->Append(IMPDESTFCOPYDISP, (void*)IMPDESTFCOPY);
            chcImpDest->Append(IMPDESTTESTDISP, (void*)IMPDESTTEST);
            chcImpDest->Append(IMPDESTFTESTDISP, (void*)IMPDESTFTEST);
            chcImpDest->Append(IMPDESTFILEDISP, (void*)IMPDESTFILE);
        }
        else
        {
            chcImpDest->Append(IMPDESTINSDISP, (void*)IMPDESTINS);
            chcImpDest->Append(IMPDESTFINSDISP, (void*)IMPDESTFINS);
            chcImpDest->Append(IMPDESTTESTDISP, (void*)IMPDESTTEST);
            chcImpDest->Append(IMPDESTFTESTDISP, (void*)IMPDESTFTEST);
            chcImpDest->Append(IMPDESTFILEDISP, (void*)IMPDESTFILE);
        }

        if (impdest==IMPDESTNONE || impdest==IMPDESTCOPY || impdest==IMPDESTINS)
        {
            chcImpDest->SetSelection(0);
            txtDestFilePath->Disable();
            btnDestFileBrowse->Disable();
            chkDestFileAppend->Disable();
        }
        else if (impdest==IMPDESTFCOPY || impdest==IMPDESTFINS)
        {
            chcImpDest->SetSelection(1);
            txtDestFilePath->Enable();
            btnDestFileBrowse->Enable();
            chkDestFileAppend->Enable();
        }
        else if (impdest==IMPDESTTEST)
        {
            chcImpDest->SetSelection(2);
            txtDestFilePath->Disable();
            btnDestFileBrowse->Disable();
            chkDestFileAppend->Disable();
        }
        else if (impdest==IMPDESTFTEST)
        {
            chcImpDest->SetSelection(3);
            txtDestFilePath->Enable();
            btnDestFileBrowse->Enable();
            chkDestFileAppend->Enable();
        }
        else
        {
            chcImpDest->SetSelection(4);
            txtDestFilePath->Enable();
            btnDestFileBrowse->Enable();
            chkDestFileAppend->Enable();
        }

        if (rdbLogToScreen->GetValue())
        {
            txtLogFilePath->Disable();
            btnLogFileBrowse->Disable();
            chkLogFileAppend->Disable();
        }
        else if (rdbLogToFile->GetValue())
        {
            txtLogFilePath->Enable();
            btnLogFileBrowse->Enable();
            chkLogFileAppend->Enable();
        }
        else
        {
            txtLogFilePath->Disable();
            btnLogFileBrowse->Disable();
            chkLogFileAppend->Disable();
            rdbLogToScreen->SetValue(true);
        }

        if (impmode==IMPORTMODECOPY)
            spinCommitEvery->Disable();
        else
            spinCommitEvery->Enable();

        break;
    default:
        break;
    }
}

bool frmImport::FetchTargetColsInfo()
{
    ClearTargetColsInfo();

    wxString sql =
        wxT("SELECT att.attname, format_type(atttypid,NULL) AS typname, tn.nspname as typnspname, att.attndims,\n")
        wxT("  att.atttypmod, (SELECT count(1) FROM pg_type t2 WHERE t2.typname=ty.typname) > 1 AS isdup,\n")
        wxT("  (SELECT count(1) FROM pg_constraint cons WHERE cons.conrelid=att.attrelid\n")
        wxT("  AND cons.contype='p' AND att.attnum = ANY (cons.conkey)) >0 AS ispk, att.attnotnull, att.atthasdef\n")
        wxT("  FROM pg_attribute att\n")
        wxT("  JOIN pg_type ty ON ty.oid=att.atttypid\n")
        wxT("  JOIN pg_namespace tn ON tn.oid=ty.typnamespace\n")
        wxT("  WHERE att.attrelid = ") + m_imptableoidstr + wxT(" AND att.attnum > 0\n")
        wxT("  ORDER BY att.attnum\n");
    pgSet *colinfset = m_conn->ExecuteSet(sql);

    if (colinfset)
    {
        m_tgtcolsnum = colinfset->NumRows();
        if (!m_tgtcolsnum)
        {
            delete colinfset;
            return false;
        }
        m_tgtcolsnames = new wxString *[m_tgtcolsnum];
        m_tgtcolstypes = new pgDatatype *[m_tgtcolsnum];
        m_tgtcolsisnotnull = new bool[m_tgtcolsnum];
        m_tgtcolshasdef = new bool[m_tgtcolsnum];
        m_tgtcolincopy = new bool[m_tgtcolsnum];

        int rowidx = 0;
        pgDatatype *coltype;

        while (!colinfset->Eof())
        {
            coltype = new pgDatatype(colinfset->GetVal(wxT("typnspname")),
                                colinfset->GetVal(wxT("typname")), 
                                colinfset->GetBool(wxT("isdup")),
                                colinfset->GetLong(wxT("attndims")),
                                colinfset->GetLong(wxT("atttypmod")));

            *(m_tgtcolsnames + rowidx) = new wxString();
            **(m_tgtcolsnames + rowidx) = colinfset->GetVal(wxT("attname"));

            if (colinfset->GetBool(wxT("attnotnull")))
                m_tgtcolsisnotnull[rowidx] = true;
            else
                m_tgtcolsisnotnull[rowidx] = false;

            if (colinfset->GetBool(wxT("atthasdef")))
                m_tgtcolshasdef[rowidx] = true;
            else
                m_tgtcolshasdef[rowidx] = false;

            m_tgtcolincopy[rowidx] = false;

            *(m_tgtcolstypes + rowidx++) = coltype;

            colinfset->MoveNext();
        }
        delete colinfset;

        return true;
    }
    else
        return false;
}

void frmImport::ClearTargetColsInfo()
{
    ClearColsImpSetting();

    if (!m_tgtcolsnum)
        return;

    size_t tgtcolinx = (size_t)0;
    for (; tgtcolinx<m_tgtcolsnum; tgtcolinx++)
    {
        delete *(m_tgtcolsnames + tgtcolinx);
        delete *(m_tgtcolstypes + tgtcolinx);
    }

    delete []m_tgtcolsnames;
    m_tgtcolsnames= NULL;
    delete []m_tgtcolstypes;
    m_tgtcolstypes= NULL;
    delete []m_tgtcolsisnotnull;
    m_tgtcolsisnotnull = NULL;
    delete []m_tgtcolshasdef;
    m_tgtcolshasdef = NULL;
    delete []m_tgtcolincopy;
    m_tgtcolincopy = NULL;

    m_tgtcolsnum = 0;
}

void frmImport::InitColSettingPage()
{
    if (m_colsettingpginit)
        return;

    gridColSetting->Freeze();

    // Trim wxGridCellChioceEditor
    pgGridCellChioceEditor *gce_trimcol = NULL;

    chcImportMode->Clear();
    chcImportMode->Append(_("COPY"), (void*)IMPORTMODECOPY);
    chcImportMode->Append(_("INSERT"), (void*)IMPORTMODEINSERT);
    chcImportMode->SetSelection(0);

    chcFormatStyle->Clear();
    chcFormatStyle->Append(_("ISO 8601"), (void*)FORMATSTYLEISO);
    //if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
    //{
        chcFormatStyle->Append(_("ANSI C"), (void*)FORMATSTYLEANSIC);
        chcFormatStyle->Append(_("RFC 822"), (void*)FORMATSTYLERFC);
        chcFormatStyle->Append(_("PostgreSQL"), (void*)FORMATSTYLEPGSQL);
    if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
        chcFormatStyle->SetSelection(1);
    //}
    else
    //{
    //    chcFormatStyle->Disable();
        chcFormatStyle->SetSelection(0);
    //}

    ClearGrid(gridColSetting);

    gridColSetting->AppendRows(m_tgtcolsnum);

    SetGridColReadOnly(gridColSetting, TGTCOLNAMENUM, true, false);
    SetGridColReadOnly(gridColSetting, TGTCOLTYPENUM, true, false);

    gridColSetting->SetColLabelValue(TGTCOLNAMENUM, _("Target column"));
    gridColSetting->SetColLabelValue(TGTCOLTYPENUM, _("Type"));

    if (m_datasrctype==SRCPGSQLPAGE)
    {
        gridColSetting->AppendCols(SRCDBOPTCOLSCT);
        gridColSetting->SetColLabelValue(SRCCOLNAMENUM, _("Source column"));
    }
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    else if (m_datasrctype==SRCODBCPAGE)
    {
        gridColSetting->AppendCols(ODBCOPTCOLSCT);
        gridColSetting->SetColLabelValue(SRCCOLNAMENUM, _("Source column"));
        gridColSetting->SetColLabelValue(ODBCONEMPTYCOLNUM, _("On NULL/empty/unset"));
        gridColSetting->SetColLabelValue(ODBCFMTCOLNUM, _("Format"));
        gridColSetting->SetColLabelValue(ODBCEXPCOLNUM, _("Expression/Constant"));
    }
#endif
    else
    {
        gridColSetting->AppendCols(TEXTFILEOPTCOLSCT);
        gridColSetting->SetColLabelValue(SRCCOLNAMENUM, _("File column"));
        gridColSetting->SetColLabelValue(FILETRIMMODECOLNUM, _("Trim"));
        gridColSetting->SetColLabelValue(FILEONEMPTYCOLNUM, _("On NULL/empty/unset"));
        gridColSetting->SetColLabelValue(FILEFMTCOLNUM, _("Format"));
        gridColSetting->SetColLabelValue(FILEEXPCOLNUM, _("Expression/Constant"));
    }

    size_t rowidx;
    wxString tgtcoltype;

    for (rowidx=(size_t)0; rowidx<m_tgtcolsnum; rowidx++)
    {
        gridColSetting->SetCellValue(rowidx, TGTCOLNAMENUM, **(m_tgtcolsnames + rowidx));
        gridColSetting->SetCellValue(rowidx, TGTCOLTYPENUM, (*(m_tgtcolstypes + rowidx))->FullName());

        if (m_datasrctype==SRCPGSQLPAGE)
        {
        }
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
        else if (m_datasrctype==SRCODBCPAGE)
        {
        }
#endif
        else
        {
            gridColSetting->SetCellEditor(rowidx, FILETRIMMODECOLNUM, new pgGridCellChioceEditor());
            gce_trimcol = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, FILETRIMMODECOLNUM);
            gce_trimcol->Append(TRIMNONEDISP);
            gce_trimcol->Append(TRIMALLDISP);
            gce_trimcol->Append(TRIMLEFTDISP);
            gce_trimcol->Append(TRIMRIGHTDISP);

            gridColSetting->SetCellValue(rowidx, FILETRIMMODECOLNUM, TRIMNONEDISP);
        }

    }

    SetGridColReadOnly(gridColSetting, FILEEXPCOLNUM);

    ResetColSettingSrcColEditor();
    ResetColSettingOnEmptyColEditor();

    //if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
        ResetColSettingFmtColEditor();

    gridColSetting->AutoSizeColumn(TGTCOLNAMENUM);
    gridColSetting->AutoSizeColumn(TGTCOLTYPENUM);
    gridColSetting->AutoSizeColumn(SRCCOLNAMENUM);

    ClearGrid(gridColsSampleData);
    txtColsSettingLog->ChangeValue(wxEmptyString);
    txtColsImpLog->ChangeValue(wxEmptyString);

    ParseColsImpSetting();

    gridColSetting->Thaw();

    m_colsettingpginit = true;
}

void frmImport::ResetColSettingPage()
{
    gridColSetting->Freeze();

    if (m_datasrctype==m_origsrctype || (m_datasrctype==TEXTFILEPAGE && m_origsrctype==ARCHFILEPAGE)
            || (m_origsrctype==TEXTFILEPAGE && m_datasrctype==ARCHFILEPAGE));
    else if (m_origsrctype==SRCPGSQLPAGE)
    {
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
        if (m_datasrctype==SRCODBCPAGE)
        {
        }
        else// import from file
#endif
        {
        }
    }
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    else if (m_origsrctype==SRCODBCPAGE)
    {
        if (m_datasrctype==SRCPGSQLPAGE)
        {
            gridColSetting->InsertCols(FILETRIMMODECOLNUM);
            //gridColSetting->InsertCols(FILEFMTCOLNUM);

            gridColSetting->SetColLabelValue(SRCCOLNAMENUM, _("File column"));
            gridColSetting->SetColLabelValue(FILETRIMMODECOLNUM, _("Trim"));
            //gridColSetting->SetColLabelValue(FILEFMTCOLNUM, _("Format"));
        }
        else// import from file
        {
        }
    }
#endif
    else// import from file
    {
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
        if (m_datasrctype==SRCODBCPAGE)
        {
            //gridColSetting->DeleteCols(FILEFMTCOLNUM);
            gridColSetting->DeleteCols(FILETRIMMODECOLNUM);

            gridColSetting->SetColLabelValue(SRCCOLNAMENUM, _("Source column"));
            gridColSetting->SetColLabelValue(ODBCONEMPTYCOLNUM, _("On NULL/empty/unset"));
            gridColSetting->SetColLabelValue(ODBCEXPCOLNUM, _("Expression/Constant"));
        }
        else// import from pgsql
#endif
        {
        }
    }

    ResetColSettingSrcColEditor(false);
    ResetColSettingOnEmptyColEditor();

    //if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
        ResetColSettingFmtColEditor();

    ClearGrid(gridColsSampleData);
    txtColsSettingLog->ChangeValue(wxEmptyString);
    txtColsImpLog->ChangeValue(wxEmptyString);

    ParseColsImpSetting();

    gridColSetting->Thaw();

}

void frmImport::ResetColSettingSrcColEditor(bool resetsrccol)
{
    pgGridCellChioceEditor *gcesrccol = NULL;

    size_t rowidx;
    wxString tgtcolname, srccolname;

    if (m_datasrctype==SRCPGSQLPAGE)
    {
    }
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    else if (m_datasrctype==SRCODBCPAGE)
    {
        SQLSMALLINT stmtcolsnum = m_srcodbc->GetResultColsNum();
        SQLSMALLINT* stmttypes = m_srcodbc->GetResultColTypes();
        SQLWCHAR** stmtlabels = m_srcodbc->GetResultColLabels();
        SQLSMALLINT coltype, odbccolidx;
        for (rowidx=(size_t)0; rowidx<m_tgtcolsnum; rowidx++)
        {
            gridColSetting->SetCellEditor(rowidx, SRCCOLNAMENUM, new pgGridCellChioceEditor());
            gcesrccol = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, SRCCOLNAMENUM);

            gcesrccol->Append(wxEmptyString);
            for (odbccolidx=0; odbccolidx<stmtcolsnum; odbccolidx++)
            {
                coltype = stmttypes[odbccolidx];
                if (coltype==SQL_BINARY || coltype==SQL_VARBINARY || coltype==SQL_LONGVARBINARY
                        || coltype==SQL_BIGINT || coltype==SQL_TINYINT || coltype==SQL_DECIMAL
                        || coltype==SQL_BIT || coltype==SQL_UNKNOWN_TYPE)
                    continue;
                if (!stmtlabels[odbccolidx])
                    continue;
                gcesrccol->Append(stmtlabels[odbccolidx]);
            }

            tgtcolname = gridColSetting->GetCellValue(rowidx, TGTCOLNAMENUM);
            srccolname = gridColSetting->GetCellValue(rowidx, SRCCOLNAMENUM);

            if (resetsrccol)
            {
                if (gcesrccol->Contains(tgtcolname))
                    gridColSetting->SetCellValue(rowidx, SRCCOLNAMENUM, tgtcolname);
                else if ((int)rowidx<gcesrccol->GetCount()-1)
                    gridColSetting->SetCellValue(rowidx, SRCCOLNAMENUM, gcesrccol->Item(rowidx+1));
                else
                    gridColSetting->SetCellValue(rowidx, SRCCOLNAMENUM, wxEmptyString);
            }
            else if (!srccolname.IsEmpty() && !gcesrccol->Contains(srccolname))
                gridColSetting->SetCellValue(rowidx, SRCCOLNAMENUM, wxEmptyString);

        }
    }
#endif
    else
    {
        for (rowidx=(size_t)0; rowidx<m_tgtcolsnum; rowidx++)
        {
            gridColSetting->SetCellEditor(rowidx, SRCCOLNAMENUM, new pgGridCellChioceEditor());
            gcesrccol = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, SRCCOLNAMENUM);
            gcesrccol->SetItems(m_srcfile->GetColsHeader());
            gcesrccol->Insert(wxEmptyString, 0);

            tgtcolname = gridColSetting->GetCellValue(rowidx, TGTCOLNAMENUM);
            srccolname = gridColSetting->GetCellValue(rowidx, SRCCOLNAMENUM);

            if (resetsrccol)
            {
                if (gcesrccol->Contains(tgtcolname))
                    gridColSetting->SetCellValue(rowidx, SRCCOLNAMENUM, tgtcolname);
                else if ((int)rowidx<gcesrccol->GetCount()-1)
                    gridColSetting->SetCellValue(rowidx, SRCCOLNAMENUM, gcesrccol->Item(rowidx+1));
                else
                    gridColSetting->SetCellValue(rowidx, SRCCOLNAMENUM, wxEmptyString);
            }
            else if (!srccolname.IsEmpty() && !gcesrccol->Contains(srccolname))
                gridColSetting->SetCellValue(rowidx, SRCCOLNAMENUM, wxEmptyString);

        }
    }
}

void frmImport::ResetColSettingOnEmptyColEditor(int dbcolnr)
{
    // This function will reset:
    // Trim column's default value
    // On NULL/empty/unset column's editor & default value

    pgGridCellChioceEditor *gce_onempty;

    int impmode = (int)chcImportMode->GetClientData(chcImportMode->GetSelection());

    int actcolnum;
    if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
        actcolnum = FILEONEMPTYCOLNUM;
    else if (m_datasrctype==SRCODBCPAGE)
        actcolnum = ODBCONEMPTYCOLNUM;
    else
        return;

    size_t fromcolnr, tocolnr;
    if (dbcolnr=wxNOT_FOUND)
    {
        fromcolnr = (size_t)0;
        tocolnr = m_tgtcolsnum;
    }
    else
    {
        fromcolnr = (size_t)dbcolnr;
        tocolnr = (size_t)dbcolnr+1;
    }

    size_t rowidx;
    wxString tgtcoltype, srccolname, onemptyact;
    bool hassrccol;
    for (rowidx=fromcolnr; rowidx<tocolnr; rowidx++)
    {
        tgtcoltype = (*(m_tgtcolstypes+rowidx))->Name();
        srccolname = gridColSetting->GetCellValue(rowidx, SRCCOLNAMENUM);
        hassrccol = !srccolname.IsEmpty();

        onemptyact = gridColSetting->GetCellValue(rowidx, actcolnum);

        if (tgtcoltype==wxT("boolean"))
        {
            if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
                gridColSetting->SetCellValue(rowidx, FILETRIMMODECOLNUM, TRIMALLDISP);

            gridColSetting->SetCellEditor(rowidx, actcolnum, new pgGridCellChioceEditor());
            gce_onempty = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, actcolnum);

            gce_onempty->Append(IMPERRORDISP);
            if ((!hassrccol || impmode==IMPORTMODEINSERT) && (!m_tgtcolsisnotnull[rowidx] || (m_tgtcolsisnotnull[rowidx] && m_tgtcolshasdef[rowidx])))
                gce_onempty->Append(IMPDEFAULTDISP);
            if (!m_tgtcolsisnotnull[rowidx])
                gce_onempty->Append(IMPNULLDISP);
            gce_onempty->Append(IMPBOOLFALSEDISP);
            gce_onempty->Append(IMPBOOLTRUEDISP);
            if (impmode==IMPORTMODEINSERT)
                gce_onempty->Append(IMPEXPRDISP);

            if (onemptyact.IsEmpty() || !gce_onempty->Contains(onemptyact))
            {
                if ((!hassrccol || impmode==IMPORTMODEINSERT) && m_tgtcolshasdef[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPDEFAULTDISP);
                else if (m_tgtcolsisnotnull[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPERRORDISP);
                else
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPNULLDISP);
            }
        }
        else if (tgtcoltype==wxT("integer") || tgtcoltype==wxT("bigint") || tgtcoltype==wxT("smallint") || tgtcoltype==wxT("numeric") ||
            tgtcoltype==wxT("float") || tgtcoltype==wxT("float8") || tgtcoltype==wxT("double precision") ||
            tgtcoltype==wxT("float4") || tgtcoltype==wxT("real"))
        {
            if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
                gridColSetting->SetCellValue(rowidx, FILETRIMMODECOLNUM, TRIMLEFTDISP);

            gridColSetting->SetCellEditor(rowidx, actcolnum, new pgGridCellChioceEditor());
            gce_onempty = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, actcolnum);

            gce_onempty->Append(IMPERRORDISP);
            if ((!hassrccol || impmode==IMPORTMODEINSERT) && (!m_tgtcolsisnotnull[rowidx] || (m_tgtcolsisnotnull[rowidx] && m_tgtcolshasdef[rowidx])))
                gce_onempty->Append(IMPDEFAULTDISP);
            if (!m_tgtcolsisnotnull[rowidx])
                gce_onempty->Append(IMPNULLDISP);
            gce_onempty->Append(IMPZERONUMBERDISP);
            gce_onempty->Append(IMPCONSTDISP);
            if (impmode==IMPORTMODEINSERT)
                gce_onempty->Append(IMPEXPRDISP);

            if (onemptyact.IsEmpty() || !gce_onempty->Contains(onemptyact))
            {
                if ((!hassrccol || impmode==IMPORTMODEINSERT) && m_tgtcolshasdef[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPDEFAULTDISP);
                else if (m_tgtcolsisnotnull[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPERRORDISP);
                else
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPNULLDISP);
            }
        }
        else if (tgtcoltype==wxT("character") || tgtcoltype == wxT("character varying") || tgtcoltype == wxT("text"))
        {
            if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
                gridColSetting->SetCellValue(rowidx, FILETRIMMODECOLNUM, TRIMNONEDISP);

            gridColSetting->SetCellEditor(rowidx, actcolnum, new pgGridCellChioceEditor());
            gce_onempty = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, actcolnum);

            gce_onempty->Append(IMPERRORDISP);
            if ((!hassrccol || impmode==IMPORTMODEINSERT) && (!m_tgtcolsisnotnull[rowidx] || (m_tgtcolsisnotnull[rowidx] && m_tgtcolshasdef[rowidx])))
                gce_onempty->Append(IMPDEFAULTDISP);
            if (!m_tgtcolsisnotnull[rowidx])
                gce_onempty->Append(IMPNULLDISP);
            gce_onempty->Append(IMPEMPTYDISP);
            gce_onempty->Append(IMPCONSTDISP);
            if (impmode==IMPORTMODEINSERT)
                gce_onempty->Append(IMPEXPRDISP);

            if (onemptyact.IsEmpty() || !gce_onempty->Contains(onemptyact))
                gridColSetting->SetCellValue(rowidx, actcolnum, IMPEMPTYDISP);
        }
        else if (tgtcoltype == wxT("date"))
        {
            if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
                gridColSetting->SetCellValue(rowidx, FILETRIMMODECOLNUM, TRIMALLDISP);

            gridColSetting->SetCellEditor(rowidx, actcolnum, new pgGridCellChioceEditor());
            gce_onempty = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, actcolnum);

            gce_onempty->Append(IMPERRORDISP);
            if ((!hassrccol || impmode==IMPORTMODEINSERT) && (!m_tgtcolsisnotnull[rowidx] || (m_tgtcolsisnotnull[rowidx] && m_tgtcolshasdef[rowidx])))
                gce_onempty->Append(IMPDEFAULTDISP);
            if (!m_tgtcolsisnotnull[rowidx])
                gce_onempty->Append(IMPNULLDISP);
            if (impmode==IMPORTMODEINSERT)
                gce_onempty->Append(IMPCURDATEDISP);
            gce_onempty->Append(IMPCLIENTTIMEDISP);
            gce_onempty->Append(IMPCONSTDISP);
            if (impmode==IMPORTMODEINSERT)
                gce_onempty->Append(IMPEXPRDISP);

            if (onemptyact.IsEmpty() || !gce_onempty->Contains(onemptyact))
            {
                if ((!hassrccol || impmode==IMPORTMODEINSERT) && m_tgtcolshasdef[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPDEFAULTDISP);
                else if (m_tgtcolsisnotnull[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPERRORDISP);
                else
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPNULLDISP);
            }
        }
        else if (tgtcoltype == wxT("time") || tgtcoltype == wxT("timetz")
              || tgtcoltype == wxT("time without time zone") || tgtcoltype == wxT("time with time zone"))
        {
            if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
                gridColSetting->SetCellValue(rowidx, FILETRIMMODECOLNUM, TRIMALLDISP);

            gridColSetting->SetCellEditor(rowidx, actcolnum, new pgGridCellChioceEditor());
            gce_onempty = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, actcolnum);

            gce_onempty->Append(IMPERRORDISP);
            if ((!hassrccol || impmode==IMPORTMODEINSERT) && (!m_tgtcolsisnotnull[rowidx] || (m_tgtcolsisnotnull[rowidx] && m_tgtcolshasdef[rowidx])))
                gce_onempty->Append(IMPDEFAULTDISP);
            if (!m_tgtcolsisnotnull[rowidx])
                gce_onempty->Append(IMPNULLDISP);
            gce_onempty->Append(IMPCONSTDISP);
            if (impmode==IMPORTMODEINSERT)
            {
                gce_onempty->Append(IMPCURTIMEDISP);
                gce_onempty->Append(IMPLOCALTIMEDISP);
            }
            gce_onempty->Append(IMPCLIENTTIMEDISP);
            if (impmode==IMPORTMODEINSERT)
                gce_onempty->Append(IMPEXPRDISP);

            if (onemptyact.IsEmpty() || !gce_onempty->Contains(onemptyact))
            {
                if ((!hassrccol || impmode==IMPORTMODEINSERT) && m_tgtcolshasdef[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPDEFAULTDISP);
                else if (m_tgtcolsisnotnull[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPERRORDISP);
                else
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPNULLDISP);
            }
        }
        else if (tgtcoltype == wxT("timestamp") || tgtcoltype == wxT("timestamptz")
              || tgtcoltype == wxT("timestamp without time zone") || tgtcoltype == wxT("timestamp with time zone"))
        {
            if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
                gridColSetting->SetCellValue(rowidx, FILETRIMMODECOLNUM, TRIMRIGHTDISP);

            gridColSetting->SetCellEditor(rowidx, actcolnum, new pgGridCellChioceEditor());
            gce_onempty = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, actcolnum);

            gce_onempty->Append(IMPERRORDISP);
            if (!m_tgtcolsisnotnull[rowidx])
                gce_onempty->Append(IMPNULLDISP);
            if ((!hassrccol || impmode==IMPORTMODEINSERT) && (!m_tgtcolsisnotnull[rowidx] || (m_tgtcolsisnotnull[rowidx] && m_tgtcolshasdef[rowidx])))
                gce_onempty->Append(IMPDEFAULTDISP);
            gce_onempty->Append(IMPCONSTDISP);
            if (impmode==IMPORTMODEINSERT)
            {
                gce_onempty->Append(IMPCURTSDISP);
                gce_onempty->Append(IMPLOCALTSDISP);
            }
            gce_onempty->Append(IMPCLIENTTIMEDISP);
            if (impmode==IMPORTMODEINSERT)
                gce_onempty->Append(IMPEXPRDISP);

            if (onemptyact.IsEmpty() || !gce_onempty->Contains(onemptyact))
            {
                if ((!hassrccol || impmode==IMPORTMODEINSERT) && m_tgtcolshasdef[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPDEFAULTDISP);
                else if (m_tgtcolsisnotnull[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPERRORDISP);
                else
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPNULLDISP);
            }
        }
        else if (tgtcoltype == wxT("interval"))
        {
            if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
                gridColSetting->SetCellValue(rowidx, FILETRIMMODECOLNUM, TRIMRIGHTDISP);

            gridColSetting->SetCellEditor(rowidx, actcolnum, new pgGridCellChioceEditor());
            gce_onempty = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, actcolnum);

            gce_onempty->Append(IMPERRORDISP);
            if (!m_tgtcolsisnotnull[rowidx])
                gce_onempty->Append(IMPNULLDISP);
            if ((!hassrccol || impmode==IMPORTMODEINSERT) && (!m_tgtcolsisnotnull[rowidx] || (m_tgtcolsisnotnull[rowidx] && m_tgtcolshasdef[rowidx])))
                gce_onempty->Append(IMPDEFAULTDISP);
            gce_onempty->Append(IMPZEROINTERVALDISP);
            gce_onempty->Append(IMPCONSTDISP);
            if (impmode==IMPORTMODEINSERT)
                gce_onempty->Append(IMPEXPRDISP);

            if (onemptyact.IsEmpty() || !gce_onempty->Contains(onemptyact))
            {
                if ((!hassrccol || impmode==IMPORTMODEINSERT) && m_tgtcolshasdef[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPDEFAULTDISP);
                else if (m_tgtcolsisnotnull[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPERRORDISP);
                else
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPNULLDISP);
            }
        }
        else
        {
            if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
                gridColSetting->SetCellValue(rowidx, FILETRIMMODECOLNUM, TRIMNONEDISP);

            gridColSetting->SetCellEditor(rowidx, actcolnum, new pgGridCellChioceEditor());
            gce_onempty = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, actcolnum);

            gce_onempty->Append(IMPERRORDISP);
            if ((!hassrccol || impmode==IMPORTMODEINSERT) && (!m_tgtcolsisnotnull[rowidx] || (m_tgtcolsisnotnull[rowidx] && m_tgtcolshasdef[rowidx])))
                gce_onempty->Append(IMPDEFAULTDISP);
            if (!m_tgtcolsisnotnull[rowidx])
                gce_onempty->Append(IMPNULLDISP);
            gce_onempty->Append(IMPCONSTDISP);
            if (impmode==IMPORTMODEINSERT)
                gce_onempty->Append(IMPEXPRDISP);

            if (onemptyact.IsEmpty() || !gce_onempty->Contains(onemptyact))
            {
                if ((!hassrccol || impmode==IMPORTMODEINSERT) && m_tgtcolshasdef[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPDEFAULTDISP);
                else if (m_tgtcolsisnotnull[rowidx])
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPERRORDISP);
                else
                    gridColSetting->SetCellValue(rowidx, actcolnum, IMPNULLDISP);
            }
        }

    }

}

void frmImport::ResetColSettingFmtColEditor()
{
    pgGridCellChioceEditor *gceformatcol;

    int fmtstyle = (int)chcFormatStyle->GetClientData(chcFormatStyle->GetSelection());

    int fmtcolnr;
    if (m_datasrctype==SRCPGSQLPAGE)
        return;
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    else if (m_datasrctype==SRCODBCPAGE)
        fmtcolnr = ODBCFMTCOLNUM;
#endif
    else
        fmtcolnr = FILEFMTCOLNUM;

    size_t rowidx;
    wxString coltype;
    for (rowidx=(size_t)0; rowidx<m_tgtcolsnum; rowidx++)
    {
        coltype = (*(m_tgtcolstypes+rowidx))->Name();

        if (m_datasrctype==SRCPGSQLPAGE)
        {
        }
        else
        {
            if (coltype==wxT("boolean"))
            {
                SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr);
            }
            else if (coltype==wxT("integer") || coltype == wxT("bigint") || coltype == wxT("smallint") || coltype == wxT("numeric") ||
                coltype==wxT("float") || coltype==wxT("float8") || coltype==wxT("double precision") ||
                coltype==wxT("float4") || coltype==wxT("real"))
            {
                SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr);
                // TODO set format
            }
            else if (coltype==wxT("character") || coltype == wxT("character varying") || coltype == wxT("text"))
            {
                SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr);
            }
            else if (coltype == wxT("date"))
            {
                switch(fmtstyle)
                {
                case FORMATSTYLEANSIC:
                    gridColSetting->SetCellEditor(rowidx, fmtcolnr, new pgGridCellChioceEditor(true));
                    gceformatcol = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, fmtcolnr);
                    gceformatcol->Append(wxEmptyString);
                    gceformatcol->Append(wxT("%Y-%m-%d"));
                    gceformatcol->Append(wxT("%Y/%m/%d"));
                    gceformatcol->Append(wxT("%d/%m/%Y"));
                    gceformatcol->Append(wxT("%m/%d/%Y"));
                    gceformatcol->Append(wxT("%d.%m.%Y"));
                    gceformatcol->Append(wxT("%d-%m-%Y"));
                    gceformatcol->Append(wxT("%d-%m-%Y"));
                    gceformatcol->Append(wxT("%b %d, %Y"));

                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr, false);

                    break;
                case FORMATSTYLEPGSQL:
                    gridColSetting->SetCellEditor(rowidx, fmtcolnr, NULL);
                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr, false);

                    break;
                case FORMATSTYLERFC:
                //case FORMATSTYLEISO:
                default:
                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr);

                    break;
                }
            }
            else if (coltype == wxT("time") || coltype == wxT("time without time zone"))
            {
                switch(fmtstyle)
                {
                case FORMATSTYLEANSIC:
                    gridColSetting->SetCellEditor(rowidx, fmtcolnr, new pgGridCellChioceEditor(true));
                    gceformatcol = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, fmtcolnr);
                    gceformatcol->Append(wxEmptyString);
                    gceformatcol->Append(wxT("%H:%M:%S"));
                    gceformatcol->Append(wxT("%H:%M"));
                    gceformatcol->Append(wxT("%I:%M:%S %p"));
                    gceformatcol->Append(wxT("%I:%M %p"));
                    gceformatcol->Append(wxT("%I o'clock %p"));

                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr, false);

                    break;
                case FORMATSTYLEPGSQL:
                    gridColSetting->SetCellEditor(rowidx, fmtcolnr, NULL);
                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr, false);

                    break;
                case FORMATSTYLERFC:
                //case FORMATSTYLEISO:
                default:
                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr);

                    break;
                }
            }
            else if (coltype == wxT("timetz") || coltype == wxT("time with time zone"))
            {
                switch(fmtstyle)
                {
                case FORMATSTYLEANSIC:
                    gridColSetting->SetCellEditor(rowidx, fmtcolnr, new pgGridCellChioceEditor(true));
                    gceformatcol = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, fmtcolnr);
                    gceformatcol->Append(wxEmptyString);
                    gceformatcol->Append(wxT("%H:%M:%S"));
                    gceformatcol->Append(wxT("%H:%M"));
                    gceformatcol->Append(wxT("%H:%M %Z"));
                    gceformatcol->Append(wxT("%I:%M:%S %p"));
                    gceformatcol->Append(wxT("%I:%M %p"));
                    gceformatcol->Append(wxT("%I:%M:%S %p, %Z"));
                    gceformatcol->Append(wxT("%I o'clock %p, %Z"));

                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr, false);

                    break;
                case FORMATSTYLEPGSQL:
                    gridColSetting->SetCellEditor(rowidx, fmtcolnr, NULL);
                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr, false);

                    break;
                case FORMATSTYLERFC:
                //case FORMATSTYLEISO:
                default:
                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr);

                    break;
                }
            }
            else if (coltype == wxT("timestamp") || coltype == wxT("timestamp without time zone"))
            {
                switch(fmtstyle)
                {
                case FORMATSTYLEANSIC:
                    gridColSetting->SetCellEditor(rowidx, fmtcolnr, new pgGridCellChioceEditor(true));
                    gceformatcol = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, fmtcolnr);
                    gceformatcol->Append(wxEmptyString);
                    gceformatcol->Append(wxT("%Y-%m-%d %H:%M:%S"));
                    gceformatcol->Append(wxT("%Y/%m/%d %H:%M:%S"));
                    gceformatcol->Append(wxT("%d/%m/%Y %H:%M:%S"));
                    gceformatcol->Append(wxT("%m/%d/%Y %H:%M:%S"));
                    gceformatcol->Append(wxT("%d.%m.%Y %H:%M:%S"));
                    gceformatcol->Append(wxT("%d-%m-%Y %H:%M:%S"));
                    gceformatcol->Append(wxT("%d-%m-%Y %H:%M:%S"));

                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr, false);

                    break;
                case FORMATSTYLEPGSQL:
                    gridColSetting->SetCellEditor(rowidx, fmtcolnr, NULL);
                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr, false);

                    break;
                case FORMATSTYLERFC:
                //case FORMATSTYLEISO:
                default:
                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr);

                    break;
                }
            }
            else if (coltype == wxT("timestamptz") || coltype == wxT("timestamp with time zone"))
            {
                switch(fmtstyle)
                {
                case FORMATSTYLEANSIC:
                    gridColSetting->SetCellEditor(rowidx, fmtcolnr, new pgGridCellChioceEditor(true));
                    gceformatcol = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, fmtcolnr);
                    gceformatcol->Append(wxEmptyString);
                    gceformatcol->Append(wxT("%Y-%m-%d %H:%M:%S %Z"));
                    gceformatcol->Append(wxT("%Y/%m/%d %H:%M:%S %Z"));
                    gceformatcol->Append(wxT("%d/%m/%Y %H:%M:%S %Z"));
                    gceformatcol->Append(wxT("%m/%d/%Y %H:%M:%S %Z"));
                    gceformatcol->Append(wxT("%d.%m.%Y %H:%M:%S %Z"));
                    gceformatcol->Append(wxT("%d-%m-%Y %H:%M:%S %Z"));
                    gceformatcol->Append(wxT("%d-%m-%Y %H:%M:%S %Z"));

                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr, false);

                    break;
                case FORMATSTYLEPGSQL:
                    gridColSetting->SetCellEditor(rowidx, fmtcolnr, NULL);
                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr, false);

                    break;
                case FORMATSTYLERFC:
                //case FORMATSTYLEISO:
                default:
                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr);

                    break;
                }
            }
            else if (coltype == wxT("interval"))
            {
                switch(fmtstyle)
                {
                case FORMATSTYLEANSIC:
                    gridColSetting->SetCellEditor(rowidx, fmtcolnr, new pgGridCellChioceEditor(true));
                    gceformatcol = (pgGridCellChioceEditor*)gridColSetting->GetCellEditor(rowidx, fmtcolnr);
                    gceformatcol->Append(wxEmptyString);
                    gceformatcol->Append(wxT("%H:%M:%S"));
                    gceformatcol->Append(wxT("%H:%M"));
                    gceformatcol->Append(wxT("%H hour(s)"));

                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr, false);

                    break;
                case FORMATSTYLEPGSQL:
                    gridColSetting->SetCellEditor(rowidx, fmtcolnr, NULL);
                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr, false);

                    break;
                case FORMATSTYLERFC:
                //case FORMATSTYLEISO:
                default:
                    SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr);

                    break;
                }
            }
            else
                SetGridCellReadOnly(gridColSetting, rowidx, fmtcolnr);
        }
    }
}

void frmImport::ParseColsImpSetting(int dbcolnr)
{
    if (dbcolnr==wxNOT_FOUND)
        ClearColsImpSetting();

    if (!m_tgtcolsnum)
        return;

    int impmode = (int)chcImportMode->GetClientData(chcImportMode->GetSelection());
    int formatstyle = (int)chcFormatStyle->GetClientData(chcFormatStyle->GetSelection());

    exprSnippet *cursnip;
    size_t loopstartnr, loopuntilnr;
    if (dbcolnr==wxNOT_FOUND)
    {
        m_colsrccolnr = new int[m_tgtcolsnum];
        if (m_datasrctype!=SRCPGSQLPAGE)
        {
            m_colonemptyact = new int[m_tgtcolsnum];
            m_colfmtarr = new wxString *[m_tgtcolsnum];
            m_colexprarr = new exprSnippet *[m_tgtcolsnum];
        }
        if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
            m_coltrimmode = new int[m_tgtcolsnum];

        if (m_datasrctype==SRCPGSQLPAGE);
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
        else if (m_datasrctype==SRCODBCPAGE)
        {
            m_srcrefnum = new short[m_srcodbc->GetResultColsNum()];
            memset(m_srcrefnum, 0, m_srcodbc->GetResultColsNum()*sizeof(short));
        }
#endif
        else
        {
            m_srcrefnum = new short[m_srcfile->GetColsNum()];
            memset(m_srcrefnum, 0, m_srcfile->GetColsNum()*sizeof(short));
        }

        loopstartnr = 0;
        loopuntilnr = m_tgtcolsnum;

        m_colsettingerrnum = 0;
        m_colsettingiserr = new bool[m_tgtcolsnum];
        memset(m_colsettingiserr, 0, m_tgtcolsnum*sizeof(bool));
    }
    else
    {
        if (m_datasrctype!=SRCPGSQLPAGE)
        {
            if (m_colfmtarr[dbcolnr])
            {
                delete m_colfmtarr[dbcolnr];
                m_colfmtarr[dbcolnr] = NULL;
            }

            if (m_colexprarr[dbcolnr])
            {
                while (true)
                {
                    cursnip = m_colexprarr[dbcolnr]->nextsnip;
                    if (m_colexprarr[dbcolnr]->snippettype!=wxNOT_FOUND)
                        m_srcrefnum[m_colexprarr[dbcolnr]->snippettype]--;
                    delete m_colexprarr[dbcolnr];

                    if (cursnip)
                        m_colexprarr[dbcolnr] = cursnip;
                    else
                        break;
                }
                m_colexprarr[dbcolnr] = NULL;
            }
        }

        loopstartnr = dbcolnr;
        loopuntilnr = dbcolnr + 1;
    }

    size_t dbcolidx;
    wxString tgtcoltype, srccolname, coltrimmode, emptyact, colfmt, colexpr;

    bool isfirstcol = true, origcolerr;
    int wrncnt=0;
    int origsrccolnr = wxNOT_FOUND;

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    SQLSMALLINT stmtcolsnum, *stmttypes;
    SQLWCHAR** stmtlabels;
    if (m_datasrctype==SRCODBCPAGE)
    {
        stmtcolsnum = m_srcodbc->GetResultColsNum();
        stmttypes = m_srcodbc->GetResultColTypes();
        stmtlabels = m_srcodbc->GetResultColLabels();
    }
#endif

    txtColsSettingLog->AppendText(_("Parsing setting ... "));

    if (dbcolnr==wxNOT_FOUND)
    {
        if (impmode==IMPORTMODECOPY)
            m_impsqlstmt.Append(wxT("COPY ") + m_imptablename + wxT("("));
        else
            m_impsqlstmt.Append(wxT("INSERT INTO ") + m_imptablename + wxT("("));

        origcolerr = false;
    }
    else
    {
        txtColsSettingLog->AppendText(_("(Target column"));
        txtColsSettingLog->AppendText(wxString::Format(wxT(" #%d: "),
            dbcolnr) + *m_tgtcolsnames[dbcolnr] + wxT(")"));

        origsrccolnr = m_colsrccolnr[dbcolnr];
        origcolerr = m_colsettingiserr[dbcolnr];
    }

    int expcolnr, onemptycolnr, fmtcolnr;
    for (dbcolidx=loopstartnr; dbcolidx<loopuntilnr; dbcolidx++)
    {
        m_colsettingiserr[dbcolidx] = false;

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
        if (m_datasrctype==SRCODBCPAGE)
        {
            gridColSetting->SetCellBackgroundColour(dbcolidx, m_datasrctype==SRCODBCPAGE?ODBCEXPCOLNUM:FILEEXPCOLNUM, *wxWHITE);

            tgtcoltype = (*(m_tgtcolstypes+dbcolidx))->Name();
            srccolname = gridColSetting->GetCellValue(dbcolidx, SRCCOLNAMENUM);

            if (srccolname.IsEmpty())
                m_colsrccolnr[dbcolidx] = wxNOT_FOUND;
            else
            {
                m_colsrccolnr[dbcolidx] = m_srcodbc->GetResultColNr((SQLWCHAR *)srccolname.wc_str());
                if (m_colsrccolnr[dbcolidx])
                    m_colsrccolnr[dbcolidx]--;
                else
                    m_colsrccolnr[dbcolidx] = wxNOT_FOUND;
            }

            if (m_colsrccolnr[dbcolidx]==wxNOT_FOUND)
                m_colfmtarr[dbcolidx] = NULL;
            else
            {
                if (dbcolnr==wxNOT_FOUND)
                    m_srcrefnum[m_colsrccolnr[dbcolidx]]++;

                if ((formatstyle==FORMATSTYLEANSIC || (impmode==IMPORTMODEINSERT && formatstyle==FORMATSTYLEPGSQL)) &&
                    !pgODBC::IsDateTimeType(stmttypes[m_colsrccolnr[dbcolidx]]) && (tgtcoltype == wxT("date") ||
                        tgtcoltype == wxT("time") || tgtcoltype == wxT("time without time zone") ||
                        tgtcoltype == wxT("timetz") || tgtcoltype == wxT("time with time zone") ||
                        tgtcoltype == wxT("timestamp") || tgtcoltype == wxT("timestamp without time zone") ||
                        tgtcoltype == wxT("timestamptz") || tgtcoltype == wxT("timestamp with time zone")))
                {
                    colfmt = gridColSetting->GetCellValue(dbcolidx, ODBCFMTCOLNUM);
                    if (colfmt.IsEmpty())
                        m_colfmtarr[dbcolidx] = NULL;
                    else
                        m_colfmtarr[dbcolidx] = new wxString(colfmt);
                }
                else if ((formatstyle==FORMATSTYLEANSIC || (impmode==IMPORTMODEINSERT && formatstyle==FORMATSTYLEPGSQL)) &&
                        !pgODBC::IsIntervalType(stmttypes[m_colsrccolnr[dbcolidx]]) && tgtcoltype == wxT("interval"))
                {
                    colfmt = gridColSetting->GetCellValue(dbcolidx, ODBCFMTCOLNUM);
                    if (colfmt.IsEmpty())
                        m_colfmtarr[dbcolidx] = NULL;
                    else
                        m_colfmtarr[dbcolidx] = new wxString(colfmt);
                }
                else
                    m_colfmtarr[dbcolidx] = NULL;
            }

            emptyact = gridColSetting->GetCellValue(dbcolidx, ODBCONEMPTYCOLNUM);

            onemptycolnr = ODBCONEMPTYCOLNUM;
            expcolnr = ODBCEXPCOLNUM;
            fmtcolnr = ODBCFMTCOLNUM;
        }
        else
#endif
        {
            gridColSetting->SetCellBackgroundColour(dbcolidx, m_datasrctype==TEXTFILEPAGE?FILEEXPCOLNUM:FILEEXPCOLNUM, *wxWHITE);

            tgtcoltype = (*(m_tgtcolstypes+dbcolidx))->Name();
            srccolname = gridColSetting->GetCellValue(dbcolidx, SRCCOLNAMENUM);

            if (srccolname.IsEmpty())
                m_colsrccolnr[dbcolidx] = wxNOT_FOUND;
            else
                m_colsrccolnr[dbcolidx] = m_srcfile->GetColPos(srccolname);

            emptyact = gridColSetting->GetCellValue(dbcolidx, FILEONEMPTYCOLNUM);

            onemptycolnr = FILEONEMPTYCOLNUM;
            expcolnr = FILEEXPCOLNUM;
            fmtcolnr = FILEFMTCOLNUM;

            if (m_colsrccolnr[dbcolidx]==wxNOT_FOUND)
            {
                m_coltrimmode[dbcolidx] = TRIMNONE;
                m_colfmtarr[dbcolidx] = NULL;
            }
            else
            {
                if (dbcolnr==wxNOT_FOUND)
                    m_srcrefnum[m_colsrccolnr[dbcolidx]]++;

                coltrimmode = gridColSetting->GetCellValue(dbcolidx, FILETRIMMODECOLNUM);
                if (coltrimmode==TRIMALLDISP)
                    m_coltrimmode[dbcolidx] = TRIMALL;
                else if (coltrimmode==TRIMLEFTDISP)
                    m_coltrimmode[dbcolidx] = TRIMLEFT;
                else if (coltrimmode==TRIMRIGHTDISP)
                    m_coltrimmode[dbcolidx] = TRIMRIGHT;
                else
                    m_coltrimmode[dbcolidx] = TRIMNONE;

                if ((formatstyle==FORMATSTYLEANSIC || (impmode==IMPORTMODEINSERT && formatstyle==FORMATSTYLEPGSQL)) &&
                        (tgtcoltype == wxT("date") || tgtcoltype == wxT("interval") ||
                        tgtcoltype == wxT("time") || tgtcoltype == wxT("time without time zone") ||
                        tgtcoltype == wxT("timetz") || tgtcoltype == wxT("time with time zone") ||
                        tgtcoltype == wxT("timestamp") || tgtcoltype == wxT("timestamp without time zone") ||
                        tgtcoltype == wxT("timestamptz") || tgtcoltype == wxT("timestamp with time zone")))
                {
                    colfmt = gridColSetting->GetCellValue(dbcolidx, FILEFMTCOLNUM);
                    if (colfmt.IsEmpty())
                        m_colfmtarr[dbcolidx] = NULL;
                    else
                        m_colfmtarr[dbcolidx] = new wxString(colfmt);
                }
                else
                    m_colfmtarr[dbcolidx] = NULL;
            }

        }// DATA SOURCE TYPE

        if (emptyact==IMPERRORDISP)
            m_colonemptyact[dbcolidx] = IMPERROR;
        else if (emptyact==IMPNULLDISP)
            m_colonemptyact[dbcolidx] = IMPNULL;
        else if (emptyact==IMPDEFAULTDISP)
            m_colonemptyact[dbcolidx] = IMPDEFAULT;
        else if (emptyact==IMPEMPTYDISP)
            m_colonemptyact[dbcolidx] = IMPEMPTY;
        else if (emptyact==IMPCONSTDISP)
            m_colonemptyact[dbcolidx] = IMPCONST;
        else if (emptyact==IMPEXPRDISP)
            m_colonemptyact[dbcolidx] = IMPEXPR;
        else if (emptyact==IMPCURTSDISP)
            m_colonemptyact[dbcolidx] = IMPCURTS;
        else if (emptyact==IMPCURDATEDISP)
            m_colonemptyact[dbcolidx] = IMPCURDATE;
        else if (emptyact==IMPCURTIMEDISP)
            m_colonemptyact[dbcolidx] = IMPCURTIME;
        else if (emptyact==IMPLOCALTSDISP)
            m_colonemptyact[dbcolidx] = IMPLOCALTS;
        else if (emptyact==IMPLOCALTIMEDISP)
            m_colonemptyact[dbcolidx] = IMPLOCALTIME;
        else if (emptyact==IMPCLIENTTIMEDISP)
            m_colonemptyact[dbcolidx] = IMPCLIENTTIME;
        else if (emptyact==IMPZERONUMBERDISP)
            m_colonemptyact[dbcolidx] = IMPZERONUMBER;
        else if (emptyact==IMPZEROINTERVALDISP)
            m_colonemptyact[dbcolidx] = IMPZEROINTERVAL;
        else if (emptyact==IMPBOOLTRUEDISP)
            m_colonemptyact[dbcolidx] = IMPBOOLTRUE;
        else if (emptyact==IMPBOOLFALSEDISP)
            m_colonemptyact[dbcolidx] = IMPBOOLFALSE;
        else
            m_colonemptyact[dbcolidx] = IMPERROR;

        if (m_colonemptyact[dbcolidx]==IMPCONST || m_colonemptyact[dbcolidx]==IMPEXPR)
            SetGridCellReadOnly(gridColSetting, dbcolidx, expcolnr, false);
        else
            SetGridCellReadOnly(gridColSetting, dbcolidx, expcolnr);

        if (m_colsrccolnr[dbcolidx]==wxNOT_FOUND)
        {
            SetGridCellReadOnly(gridColSetting, dbcolidx, fmtcolnr);
            if (m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE)
            {
                SetGridCellReadOnly(gridColSetting, dbcolidx, FILETRIMMODECOLNUM);
                SetGridCellReadOnly(gridColSetting, dbcolidx, FILEFMTCOLNUM);
            }
        }
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
        else if (m_datasrctype==SRCODBCPAGE)
        {
            if (formatstyle==FORMATSTYLERFC || formatstyle==FORMATSTYLEISO ||
                    (impmode==IMPORTMODECOPY && formatstyle==FORMATSTYLEPGSQL))
                SetGridCellReadOnly(gridColSetting, dbcolidx, ODBCFMTCOLNUM);
            else
            {
                if ((tgtcoltype == wxT("date") ||
                        tgtcoltype == wxT("time") || tgtcoltype == wxT("time without time zone") ||
                        tgtcoltype == wxT("timetz") || tgtcoltype == wxT("time with time zone") ||
                        tgtcoltype == wxT("timestamp") || tgtcoltype == wxT("timestamp without time zone") ||
                        tgtcoltype == wxT("timestamptz") || tgtcoltype == wxT("timestamp with time zone"))
                        && !pgODBC::IsDateTimeType(m_colsrccolnr[dbcolidx]))
                    SetGridCellReadOnly(gridColSetting, dbcolidx, ODBCFMTCOLNUM, false);
                else if (tgtcoltype==wxT("interval") && !pgODBC::IsIntervalType(m_colsrccolnr[dbcolidx]))
                    SetGridCellReadOnly(gridColSetting, dbcolidx, ODBCFMTCOLNUM, false);
                else
                    SetGridCellReadOnly(gridColSetting, dbcolidx, ODBCFMTCOLNUM);
            }
        }
#endif
        else
        {
            SetGridCellReadOnly(gridColSetting, dbcolidx, FILETRIMMODECOLNUM, false);

            if (formatstyle==FORMATSTYLERFC || formatstyle==FORMATSTYLEISO ||
                    (impmode==IMPORTMODECOPY && formatstyle==FORMATSTYLEPGSQL))
                SetGridCellReadOnly(gridColSetting, dbcolidx, FILEFMTCOLNUM);
            else
            {
                if (tgtcoltype == wxT("date") || tgtcoltype == wxT("interval") ||
                        tgtcoltype == wxT("time") || tgtcoltype == wxT("time without time zone") ||
                        tgtcoltype == wxT("timetz") || tgtcoltype == wxT("time with time zone") ||
                        tgtcoltype == wxT("timestamp") || tgtcoltype == wxT("timestamp without time zone") ||
                        tgtcoltype == wxT("timestamptz") || tgtcoltype == wxT("timestamp with time zone"))
                    SetGridCellReadOnly(gridColSetting, dbcolidx, FILEFMTCOLNUM, false);
                else
                    SetGridCellReadOnly(gridColSetting, dbcolidx, FILEFMTCOLNUM);
            }
        }

        if (m_colonemptyact[dbcolidx]==IMPCONST)
        {
            colexpr = gridColSetting->GetCellValue(dbcolidx, expcolnr);

            if (colexpr.IsEmpty())
            {
                m_colexprarr[dbcolidx] = NULL;
                if (m_tgtcolsisnotnull[dbcolidx])
                {
                    if (m_colsrccolnr[dbcolidx]==wxNOT_FOUND)
                    {
                        txtColsSettingLog->AppendText(END_OF_LINE);
                        txtColsSettingLog->AppendText(_("Error"));
                        txtColsSettingLog->AppendText(_(" -- Please specify a constant for NOT NULL column: ") + *m_tgtcolsnames[dbcolidx]);
                        gridColSetting->SetCellBackgroundColour(dbcolidx, expcolnr, *wxRED);

                        m_colsettingiserr[dbcolidx] = true;
                        if (dbcolnr==wxNOT_FOUND || !origcolerr)
                            m_colsettingerrnum++;
                    }
                    else
                    {
                        txtColsSettingLog->AppendText(END_OF_LINE);
                        txtColsSettingLog->AppendText(_("Warning"));
                        txtColsSettingLog->AppendText(_(" -- NOT NULL column maybe need a constant: ") + *m_tgtcolsnames[dbcolidx]);
                        gridColSetting->SetCellBackgroundColour(dbcolidx, expcolnr, YELLOW_COLOR);
                        wrncnt++;
                    }
                }
                else
                    gridColSetting->SetCellBackgroundColour(dbcolidx, expcolnr, *wxWHITE);
            }
            else
            {
                m_colexprarr[dbcolidx] = new exprSnippet(colexpr, wxNOT_FOUND);
                gridColSetting->SetCellBackgroundColour(dbcolidx, expcolnr, *wxWHITE);
            }
        }
        else if (impmode==IMPORTMODEINSERT && m_colonemptyact[dbcolidx]==IMPEXPR)
        {
            colexpr = gridColSetting->GetCellValue(dbcolidx, expcolnr).Trim().Trim(false);

            if (colexpr.IsEmpty())
            {
                m_colexprarr[dbcolidx] = NULL;
                if (m_tgtcolsisnotnull[dbcolidx])
                {
                    if (m_colsrccolnr[dbcolidx]==wxNOT_FOUND)
                    {
                        txtColsSettingLog->AppendText(END_OF_LINE);
                        txtColsSettingLog->AppendText(_("Error"));
                        txtColsSettingLog->AppendText(_(" -- Please specify a expression for NOT NULL column: ") + *m_tgtcolsnames[dbcolidx]);
                        gridColSetting->SetCellBackgroundColour(dbcolidx, expcolnr, *wxRED);

                        m_colsettingiserr[dbcolidx] = true;
                        if (dbcolnr==wxNOT_FOUND || !origcolerr)
                            m_colsettingerrnum++;
                    }
                    else
                    {
                        txtColsSettingLog->AppendText(END_OF_LINE);
                        txtColsSettingLog->AppendText(_("Warning"));
                        txtColsSettingLog->AppendText(_(" -- NOT NULL column maybe need a expression: ") + *m_tgtcolsnames[dbcolidx]);
                        gridColSetting->SetCellBackgroundColour(dbcolidx, expcolnr, YELLOW_COLOR);
                        wrncnt++;
                    }
                }
                else
                    gridColSetting->SetCellBackgroundColour(dbcolidx, expcolnr, *wxWHITE);
            }
            else
            {
                txtColsSettingLog->AppendText(END_OF_LINE);
                txtColsSettingLog->AppendText(_("Parsing user-defined expression for column: ") + colexpr);
                txtColsSettingLog->AppendText(_("Expression: ") + colexpr);
                cursnip = m_colexprarr[dbcolidx] = ParseImportExpr(colexpr);
                while (cursnip)
                {
                    txtColsSettingLog->AppendText(END_OF_LINE);
                    if (cursnip->snippettype==wxNOT_FOUND)
                        txtColsSettingLog->AppendText(wxT("\t") + IMPCONSTDISP + wxT(": ") + cursnip->exprsnippet);
                    else
                    {
                        m_srcrefnum[cursnip->snippettype]++;
                        txtColsSettingLog->AppendText(wxT("\tcolumn: ") + m_srcfile->GetColName(cursnip->snippettype));
                    }

                    cursnip = cursnip->nextsnip;
                }
                txtColsSettingLog->AppendText(END_OF_LINE);
                txtColsSettingLog->AppendText(_("Expression parse completed."));

                gridColSetting->SetCellBackgroundColour(dbcolidx, expcolnr, *wxWHITE);
            }
        }
        else
        {
            m_colexprarr[dbcolidx] = NULL;
            gridColSetting->SetCellBackgroundColour(dbcolidx, expcolnr, *wxWHITE);
        }

        if ((m_colsrccolnr[dbcolidx]==wxNOT_FOUND && m_colonemptyact[dbcolidx]==IMPERROR))
        {
            txtColsSettingLog->AppendText(END_OF_LINE);
            txtColsSettingLog->AppendText(_("Error"));
            txtColsSettingLog->AppendText(_(" -- Can't import any rows since setting error: ") + *m_tgtcolsnames[dbcolidx]);
            gridColSetting->SetCellBackgroundColour(dbcolidx, onemptycolnr, *wxRED);

            m_colsettingiserr[dbcolidx] = true;
            if (dbcolnr==wxNOT_FOUND || !origcolerr)
                m_colsettingerrnum++;
        }
        if (!m_colsettingiserr[dbcolidx])
        {
            gridColSetting->SetCellBackgroundColour(dbcolidx, onemptycolnr, *wxWHITE);
            if (origcolerr)
                m_colsettingerrnum--;
        }

        if (impmode==IMPORTMODEINSERT && dbcolnr==wxNOT_FOUND)
        {
            if (dbcolidx)
                m_impsqlstmt.Append(wxT(", "));
            m_impsqlstmt.Append(*m_tgtcolsnames[dbcolidx]);
        }
        else if (m_colsrccolnr[dbcolidx]==wxNOT_FOUND)
        {
            if (impmode==IMPORTMODECOPY && (m_colonemptyact[dbcolidx]==IMPNULL || m_colonemptyact[dbcolidx]==IMPEMPTY ||
                m_colonemptyact[dbcolidx]==IMPCONST || m_colonemptyact[dbcolidx]==IMPEXPR ||
                m_colonemptyact[dbcolidx]==IMPCLIENTTIME || m_colonemptyact[dbcolidx]==IMPZERONUMBER ||
                m_colonemptyact[dbcolidx]==IMPZEROINTERVAL))
            {
                if (dbcolnr==wxNOT_FOUND)
                {
                    if (isfirstcol)
                        isfirstcol = false;
                    else
                        m_impsqlstmt.Append(wxT(", "));
                    m_impsqlstmt.Append(*m_tgtcolsnames[dbcolidx]);
                }
                m_tgtcolincopy[dbcolidx] = true;
            }
            else
                m_tgtcolincopy[dbcolidx] = false;
        }
        else
        {
            if (impmode==IMPORTMODECOPY)
            {
                if (dbcolnr==wxNOT_FOUND)
                {
                    if (isfirstcol)
                        isfirstcol = false;
                    else
                        m_impsqlstmt.Append(wxT(", "));
                    m_impsqlstmt.Append(*m_tgtcolsnames[dbcolidx]);
                }
                m_tgtcolincopy[dbcolidx] = true;
            }
            else
                m_tgtcolincopy[dbcolidx] = false;
        }
    }//LOOP 1

    txtColsSettingLog->AppendText(END_OF_LINE);
    if (dbcolnr==wxNOT_FOUND)
    {
        txtColsSettingLog->AppendText(_("Import statement:"));
        txtColsSettingLog->AppendText(END_OF_LINE);

        if (impmode==IMPORTMODECOPY)
        {
            m_impsqlstmt.Append(wxT(") FROM stdin;"));
            txtColsSettingLog->AppendText(m_impsqlstmt);
        }
        else
        {
            m_impsqlstmt.Append(wxT(") VALUES("));
            txtColsSettingLog->AppendText(m_impsqlstmt);
            txtColsSettingLog->AppendText(wxT("...)"));
        }

        if (m_colsettingerrnum)
        {
            txtColsSettingLog->AppendText(_("Incorrect import setting."));
            //DispStatusMsg(_("Incorrect import setting."));
        }
    }
    else
    {
        txtColsSettingLog->AppendText(wxString::Format(_("The setting of target column #%d: "),
            dbcolnr) + *m_tgtcolsnames[dbcolnr]);
        txtColsSettingLog->AppendText(_(" has been changed."));

        if (origsrccolnr!=m_colsrccolnr[dbcolnr])
        {
            if (origsrccolnr!=wxNOT_FOUND)
                m_srcrefnum[origsrccolnr]--;
            if (m_colsrccolnr[dbcolnr]!=wxNOT_FOUND)
                m_srcrefnum[m_colsrccolnr[dbcolnr]]++;

            if (impmode==IMPORTMODECOPY)
                m_impsqlstmt.Empty();
        }
    }
    txtColsSettingLog->AppendText(END_OF_LINE);
    txtColsSettingLog->AppendText(END_OF_LINE);

    if (m_colsettingerrnum)
        nbColsPreview->ChangeSelection(1);
}

void frmImport::ClearColsImpSetting()
{
    if (!m_tgtcolsnum)
        return;

    if (m_colsrccolnr)
    {
        delete []m_colsrccolnr;
        m_colsrccolnr = NULL;
    }

    if (m_coltrimmode)
    {
        delete []m_coltrimmode;
        m_coltrimmode = NULL;
    }

    if (m_colonemptyact)
    {
        delete []m_colonemptyact;
        m_colonemptyact = NULL;
    }

    size_t colidx;
    if (m_colfmtarr)
    {
        for (colidx=0; colidx<m_tgtcolsnum; colidx++)
            if (m_colfmtarr[colidx])
                delete m_colfmtarr[colidx];

        delete []m_colfmtarr;
        m_colfmtarr= NULL;
    }

    if (m_colexprarr)
    {
        exprSnippet *csnip, *nsnip;
        for (colidx=0; colidx<m_tgtcolsnum; colidx++)
            if (m_colexprarr[colidx])
            {
                csnip = m_colexprarr[colidx];
                while (true)
                {
                    nsnip = csnip->nextsnip;
                    delete csnip;
                    if (nsnip)
                        csnip = nsnip;
                    else
                        break;
                }
            }

        delete []m_colexprarr;
        m_colexprarr= NULL;
    }

    if (m_srcrefnum)
    {
        delete[] m_srcrefnum;
        m_srcrefnum = NULL;
    }

    m_colsettingerrnum = -1;
    if (m_colsettingiserr)
    {
        delete []m_colsettingiserr;
        m_colsettingiserr = NULL;
    }

    m_impsqlstmt.Empty();
}

frmImport::exprSnippet *frmImport::ParseImportConst(const wxString& origconst, const bool iscopymode)
{
    wxASSERT(origconst.IsEmpty());
    //if (origconst.IsEmpty())
    //    return NULL;

    return new exprSnippet(origconst, wxNOT_FOUND);
/*
    const wxChar *constdata = origconst.GetData();

    if (constdata[0]!=wxT('\''))
    {
        if (origconst.Find(wxT('\''))==wxNOT_FOUND)
            return new exprSnippet(origconst, wxNOT_FOUND);
        else
            return NULL;
    }

    if (constdata[0]==wxT('\'') || !constdata[1])
        return NULL;
    if (constdata[0]==wxT('\'') && constdata[1]==wxT('\'') && !constdata[2])
    {
        if (iscopymode)
            return new exprSnippet(wxEmptyString, wxNOT_FOUND);
        else
            return new exprSnippet(origconst, wxNOT_FOUND);
    }

    int dataidx;

    dataidx = 1;
    while (constdata[dataidx])
    {
        if (constdata[dataidx]==wxT('\''))
        {
            if (constdata[dataidx+1])
            {
                if (constdata[dataidx+1]==wxT('\''))
                {
                    dataidx++;
                    if (!constdata[dataidx+1])
                        return NULL;
                }
                else
                    return NULL;
            }
        }

        dataidx++;
    }

    if (iscopymode)
    {
        wxString copyconst = origconst.Mid(0, dataidx-1);
        copyconst.Replace(wxT("''"), wxT("'"));
        return new exprSnippet(copyconst, wxNOT_FOUND);
    }
    else
        return new exprSnippet(origconst, wxNOT_FOUND);
*/
}

frmImport::exprSnippet *frmImport::ParseImportExpr(const wxString& origexpr)
{
    wxASSERT(!origexpr.IsEmpty());
    //if (origexpr.IsEmpty())
    //    return NULL;
    wxASSERT(m_datasrctype==TEXTFILEPAGE || m_datasrctype==ARCHFILEPAGE || m_datasrctype==SRCODBCPAGE);

    const wxChar *exprdata = origexpr.GetData();

    int dataidx = 0, defidx;

    size_t srccolnum;
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    if (m_datasrctype==SRCODBCPAGE)
        srccolnum = m_srcodbc->GetResultColsNum();
    else
#endif
        srccolnum = m_srcfile->GetColsNum();

    // $(#nnn)
    // $(col_name)
    // escape: $$ $)
    long colpos;
    wxString parsedsnip, colname;
    exprSnippet *exprsnip = NULL, *cursnip = NULL;

    while(exprdata[dataidx])
    {
        if (exprdata[dataidx]==wxT('$'))
        {
            if (exprdata[dataidx+1])
            {
                if (exprdata[dataidx+1]==wxT('$'))
                {
                    parsedsnip.Append(exprdata[dataidx]);
                    dataidx++;
                }
                else if (exprdata[dataidx+1]==wxT('('))
                {
                    defidx = dataidx+2;
                    colname.Empty();
                    while (exprdata[defidx])
                    {
                        if (exprdata[defidx]==wxT('$'))
                        {
                            if (exprdata[defidx+1])
                            {
                                if (exprdata[defidx+1]==wxT('$') ||
                                        //exprdata[defidx+1]==wxT('(') ||
                                        exprdata[defidx+1]==wxT(')'))
                                    colname.Append(exprdata[defidx+1]);
                                else
                                    colname.Append(exprdata[defidx]).Append(exprdata[defidx+1]);

                                defidx++;
                            }
                            else
                            {
                                colname.Empty();
                                break;
                            }
                        }
                        else if (exprdata[defidx]==wxT(')'))
                        {
                            break;
                        }
                        else
                            colname.Append(exprdata[defidx]);

                        defidx++;
                    }

                    if (colname.IsEmpty())
                    {
                        for (; dataidx<defidx; dataidx++)
                            parsedsnip.Append(exprdata[dataidx]);
                        parsedsnip.Append(exprdata[dataidx]);
                    }
                    else
                    {
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
                        if (m_datasrctype==SRCODBCPAGE)
                        {
                            colpos = m_srcodbc->GetResultColNr((SQLWCHAR *)colname.wc_str());
                            if (!colpos)
                                colpos = wxNOT_FOUND;
                            else
                                colpos--;
                        }
                        else
#endif
                            colpos = m_srcfile->GetColPos(colname);
                        if (colpos==wxNOT_FOUND)
                        {
                            if (StrIsInteger(colname) && colname.ToLong(&colpos) && colpos>=0 && (size_t)colpos<srccolnum)
                            {
                                if (!parsedsnip.IsEmpty())
                                {
                                    if (exprsnip)
                                    {
                                        cursnip->nextsnip = new exprSnippet(parsedsnip, wxNOT_FOUND);
                                        cursnip = cursnip->nextsnip;
                                    }
                                    else
                                    {
                                        exprsnip = new exprSnippet(parsedsnip, wxNOT_FOUND);
                                        cursnip = exprsnip;
                                    }

                                    parsedsnip.Empty();
                                }
                                if (exprsnip)
                                {
                                    cursnip->nextsnip = new exprSnippet(wxEmptyString, (int)colpos);
                                    cursnip = cursnip->nextsnip;
                                }
                                else
                                {
                                    exprsnip = new exprSnippet(wxEmptyString, (int)colpos);
                                    cursnip = exprsnip;
                                }

                                dataidx = defidx;
                            }
                            else
                            {
                                for (; dataidx<defidx; dataidx++)
                                    parsedsnip.Append(exprdata[dataidx]);
                                parsedsnip.Append(exprdata[dataidx]);
                            }
                        }
                        else
                        {
                            if (!parsedsnip.IsEmpty())
                            {
                                if (exprsnip)
                                {
                                    cursnip->nextsnip = new exprSnippet(parsedsnip, wxNOT_FOUND);
                                    cursnip = cursnip->nextsnip;
                                }
                                else
                                {
                                    exprsnip = new exprSnippet(parsedsnip, wxNOT_FOUND);
                                    cursnip = exprsnip;
                                }

                                parsedsnip.Empty();
                            }

                            if (exprsnip)
                            {
                                cursnip->nextsnip = new exprSnippet(wxEmptyString, (int)colpos);
                                cursnip = cursnip->nextsnip;
                            }
                            else
                            {
                                exprsnip = new exprSnippet(wxEmptyString, (int)colpos);
                                cursnip = exprsnip;
                            }

                            dataidx = defidx;
                        }
                    }
                }
                else
                {
                    parsedsnip.Append(exprdata[dataidx]).Append(exprdata[dataidx+1]);
                    dataidx++;
                }
            }
            else
            {
                parsedsnip.Append(exprdata[dataidx]);

                if (exprsnip)
                    cursnip->nextsnip = new exprSnippet(parsedsnip, wxNOT_FOUND);
                else
                    exprsnip = new exprSnippet(parsedsnip, wxNOT_FOUND);

                dataidx = 0;
                break;
            }
        }
        else
            parsedsnip.Append(exprdata[dataidx]);

        dataidx++;
    }

    if (dataidx && !parsedsnip.IsEmpty())
    {
        if (exprsnip)
            cursnip->nextsnip = new exprSnippet(parsedsnip, wxNOT_FOUND);
        else
            exprsnip = new exprSnippet(parsedsnip, wxNOT_FOUND);
    }

    return exprsnip;
}

bool frmImport::ValidatePgExp(const wxString& pgexp, const wxString& exptype, wxString& expresult)
{
    if (!m_conn)// should never occur
        return false;

    bool isvalid;

    wxString sqlstmt = wxT("SELECT ") + pgexp + wxT("::") + exptype;
    txtColsSettingLog->AppendText(_("SQL Statement: ") + sqlstmt);
    txtColsSettingLog->AppendText(END_OF_LINE);
    pgSet *colsset = m_conn->ExecuteSet(sqlstmt, false);
    if (colsset)
    {
        if (colsset->Eof())
        {
            txtColsSettingLog->AppendText(_("No result."));
            txtColsSettingLog->AppendText(END_OF_LINE);
            delete colsset;
            isvalid = false;
        }

        if (colsset->NumRows()!=1 || colsset->NumCols()!=1 || colsset->Eof())
        {
            txtColsSettingLog->AppendText(_("The constant/expression must has one column and one row."));
            txtColsSettingLog->AppendText(END_OF_LINE);
            delete colsset;
            isvalid = false;
        }

        if (exptype==wxT("boolean"))
        {
            if (colsset->GetBool(0))
                expresult = IMPBOOLTRUEDISP;
            else
                expresult = IMPBOOLFALSEDISP;
        }
        else
            expresult = colsset->GetVal(0);

        delete colsset;

        isvalid = true;
    }
    else
    {
        wxString errorMsg = m_conn->GetLastError().Trim().Trim(false);
        if (errorMsg.IsEmpty())
        {
            txtColsSettingLog->AppendText(_("Unknown error."));
            txtColsSettingLog->AppendText(END_OF_LINE);
        }
        else
            txtColsSettingLog->AppendText(errorMsg + END_OF_LINE);

        isvalid = false;
    }

    if (isvalid)
        txtColsSettingLog->AppendText(_("Valid expression."));
    else
        txtColsSettingLog->AppendText(_("Invalid expression."));

    txtColsSettingLog->AppendText(END_OF_LINE);
    txtColsSettingLog->AppendText(END_OF_LINE);

    return isvalid;
}

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
void frmImport::FetchODBCDSNList()
{
    cbODBCDSNNames->Clear();
    cbODBCDSNNames->Append(wxEmptyString);

    SQLWCHAR **dsptr;
    wxString errmsg;
    SQLWCHAR **dslist = pgODBC::FetchDataSources(errmsg.GetWriteBuf(256));
    errmsg.UngetWriteBuf();

    if (!dslist && !errmsg.IsEmpty())
    {
        DispStatusMsg(errmsg);
        return;
    }

    for (dsptr=dslist; *dsptr; dsptr++)
    {
        cbODBCDSNNames->Append(*dsptr);
        delete[] (*dsptr);
    }

    delete[] dslist;
}

void frmImport::FetchODBCInfo()
{
    size_t infoidx;

    if (!m_srcodbc || !m_srcodbc->IsConnected())
    {
        const SQLWCHAR **infonames = pgODBC::GetInfoNames();
        for (infoidx=0; infonames[infoidx]; infoidx++)
            lstODBCConnInfo->SetItem(infoidx, 1, wxEmptyString);

        return;
    }

    const SQLWCHAR **infovals = m_srcodbc->GetInfoValues();

    for (infoidx=0; infovals[infoidx]; infoidx++)
        lstODBCConnInfo->SetItem(infoidx, 1, infovals[infoidx]);

}

void frmImport::ClearODBCPreview()
{
    m_srcodbcisok = false;

    gridODBCPreview->Freeze();
    ClearGrid(gridODBCPreview);
    gridODBCPreview->Thaw();
    lstODBCResultAttribs->DeleteAllItems();
}

void frmImport::GenODBCPreview()
{
    if (!m_srcodbc || !m_srcodbc->IsConnected())
        return;

    wxString sqlstmt = txtODBCSQLStmt->GetValue();
    if (sqlstmt.IsEmpty())
    {
        DispStatusMsg(_("Please specify a SQL statement."));
        return;
    }

    gridODBCPreview->Freeze();
    ClearGrid(gridODBCPreview);
    lstODBCResultAttribs->DeleteAllItems();

    if (m_srcodbc->HasStmtRunning())
        m_srcodbc->CloseCurrentStmt();

    if (!m_srcodbc->ExecuteQueryStmt((SQLWCHAR *)sqlstmt.wc_str()))
    {
        DispStatusMsg(m_srcodbc->GetErrorMsg());
        gridODBCPreview->Thaw();
        return;
    }

    SQLSMALLINT stmtcolsnum = m_srcodbc->GetResultColsNum();
    if (!stmtcolsnum)
    {
        DispStatusMsg(_("The result set doesn't contain any column."));
        gridODBCPreview->Thaw();

        if (!m_srcodbc->CloseCurrentStmt())
            DispStatusMsg(m_srcodbc->GetErrorMsg());

        return;
    }

    gridODBCPreview->AppendCols(stmtcolsnum);

    SQLUSMALLINT colidx;

    SQLSMALLINT *coltypes = m_srcodbc->GetResultColTypes();
    SQLWCHAR** stmtlabels = m_srcodbc->GetResultColLabels();

    for (colidx=0; colidx<stmtcolsnum; colidx++)
        lstODBCResultAttribs->AppendItem(stmtlabels[colidx], pgODBC::SQLTypeToName(coltypes[colidx]));
    for (colidx=0; colidx<stmtcolsnum; colidx++)
        gridODBCPreview->SetColLabelValue(colidx, stmtlabels[colidx]);

    int pvrownum = spinODBCPreviewRows->GetValue();

    SQLSMALLINT coltype;
    SQLUSMALLINT rowidx=0;
    SQLRETURN sqlrc;
    SQLWCHAR databuff[IMPDATABUFFLEN];
    SQLINTEGER indptr;
    while (true)
    {
        sqlrc = m_srcodbc->NextResultRow();
        if (sqlrc==SQL_NO_DATA)
            break;
        else if (sqlrc != SQL_SUCCESS && sqlrc != SQL_SUCCESS_WITH_INFO)
        {
            if (m_srcodbc->HasErrorMsg())
                DispStatusMsg(m_srcodbc->GetErrorMsg());
            m_srcodbcisok = false;
            return;
        }

        gridODBCPreview->AppendRows();
        for (colidx=0; colidx<stmtcolsnum; colidx++)
        {
            coltype = coltypes[colidx];
            if (coltype==SQL_BINARY || coltype==SQL_VARBINARY || coltype==SQL_LONGVARBINARY)
            {
                gridODBCPreview->SetCellValue(rowidx, colidx, BINDATADISP);
                gridODBCPreview->SetCellTextColour(rowidx, colidx, *wxRED);
                continue;
            }
            else if (coltype==SQL_BIGINT || coltype==SQL_TINYINT
                || coltype==SQL_DECIMAL || coltype==SQL_BIT)
            {
                gridODBCPreview->SetCellValue(rowidx, colidx, UNKNOWNTYPEDISP);
                gridODBCPreview->SetCellTextColour(rowidx, colidx, *wxRED);
                continue;
            }
            else if (coltype==SQL_UNKNOWN_TYPE)
            {
                gridODBCPreview->SetCellValue(rowidx, colidx, UNSUPTYPEDISP);
                gridODBCPreview->SetCellTextColour(rowidx, colidx, *wxRED);
                continue;
            }

            sqlrc = m_srcodbc->GetResultData(colidx+1, databuff, IMPDATABUFFLEN, &indptr);

            if (sqlrc!=SQL_SUCCESS && sqlrc!=SQL_SUCCESS_WITH_INFO)
            {
                if (m_srcodbc->HasErrorMsg())
                    DispStatusMsg(m_srcodbc->GetErrorMsg());
                m_srcodbcisok = false;
                return;
            }

            if (indptr==SQL_NO_TOTAL || indptr>(int)(IMPDATABUFFLEN*sizeof(SQLWCHAR)))
            {
                wcscpy(databuff + IMPDATABUFFLEN - wcslen(MOREDATADISP) -1, MOREDATADISP);
                databuff[IMPDATABUFFLEN-1] = wxT('\0');
                gridODBCPreview->SetCellValue(rowidx, colidx, databuff);
            }
            else if (indptr==SQL_NULL_DATA)
            {
                gridODBCPreview->SetCellValue(rowidx, colidx, IMPNULLDISP);
                gridODBCPreview->SetCellTextColour(rowidx, colidx, *wxBLUE);
            }
            else
                gridODBCPreview->SetCellValue(rowidx, colidx, databuff);
        }
        rowidx++;
        if (rowidx>=pvrownum)
            break;
    }

    gridODBCPreview->Thaw();

    m_srcodbcisok = true;
}

void frmImport::GenODBCSelectStmt(const SQLWCHAR*** colsinfo)
{
    if (!m_srcodbc || !m_srcodbc->IsConnected())
        return;

    wxString** odbcstmt = (wxString**)txtODBCSQLStmt->GetClientData();
    bool custsql = chkODBCCustomizedSQL->GetValue();
    if (custsql)
        return;

    if (!colsinfo)
    {
        if (custsql)
            btnODBCPreview->Enable(false);
        else
            btnODBCPreview->Enable(true);

        odbcstmt[1]->Empty();
        txtODBCSQLStmt->Clear();
        return;
    }

    odbcstmt[0]->Empty();
    txtODBCSQLStmt->Clear();

    const SQLWCHAR *sqlstmt = m_srcodbc->GetSelectStmt(cbODBCTableNames->GetValue().wc_str(), colsinfo,
                txtODBCWhereCl->GetValue().wc_str());
    if (sqlstmt)
    {
        txtODBCSQLStmt->AppendText(sqlstmt);
        odbcstmt[0]->Append(sqlstmt);
        delete[] sqlstmt;
    }
    else
        txtODBCSQLStmt->Clear();

}

#endif

void frmImport::RandomSavepointName(wxChar* randname, short namelen)
{
    int randidx, randnum;
    for (randidx=0; randidx<namelen; randidx++)
    {
        randnum = rand()%26;
        randname[randidx] = wxT('A') + randnum;
    }
    randname[randidx] = '\0';
}

importFactory::importFactory(menuFactoryList *list, wxMenu *mnu, ctlMenuToolbar *toolbar) : contextActionFactory(list)
{
    mnu->Append(id, _("&Import(preview)"), _("Import data to current table from a data source."));
}

wxWindow *importFactory::StartDialog(frmMain *form, pgObject *obj)
{
    frmImport *frm=new frmImport(form, (pgTable*)obj);
    frm->Go(false);
    return 0;
}


bool importFactory::CheckEnable(pgObject *obj)
{
    if (!obj)
        return false;

    return obj->CanImport();
}
/*
static const wxChar* gs_colournames[] =
{
wxT("AQUAMARINE"),
wxT("BLACK"),
wxT("BLUE"),
wxT("BLUE VIOLET"),
wxT("BROWN"),
wxT("CADET BLUE"),
wxT("CORAL"),
wxT("CORNFLOWER BLUE"),
wxT("CYAN"),
wxT("DARK GREY"),
wxT("DARK GREEN"),
wxT("DARK OLIVE GREEN"),
wxT("DARK ORCHID"),
wxT("DARK SLATE BLUE"),
wxT("DARK SLATE GREY DARK TURQUOISE"),
wxT("DIM GREY"),
wxT("FIREBRICK"),
wxT("FOREST GREEN"),
wxT("GOLD"),
wxT("GOLDENROD"),
wxT("GREY"),
wxT("GREEN"),
wxT("GREEN YELLOW"),
wxT("INDIAN RED"),
wxT("KHAKI"),
wxT("LIGHT BLUE"),
wxT("LIGHT GREY"),
wxT("LIGHT STEEL BLUE"),
wxT("LIME GREEN"),
wxT("MAGENTA"),
wxT("MAROON"),
wxT("MEDIUM AQUAMARINE"),
wxT("MEDIUM BLUE"),
wxT("MEDIUM FOREST GREEN"),
wxT("MEDIUM GOLDENROD"),
wxT("MEDIUM ORCHID"),
wxT("MEDIUM SEA GREEN"),
wxT("MEDIUM SLATE BLUE"),
wxT("MEDIUM SPRING GREEN"),
wxT("MEDIUM TURQUOISE"),
wxT("MEDIUM VIOLET RED"),
wxT("MIDNIGHT BLUE"),
wxT("NAVY"),
wxT("ORANGE"),
wxT("ORANGE RED"),
wxT("ORCHID"),
wxT("PALE GREEN"),
wxT("PINK"),
wxT("PLUM"),
wxT("PURPLE"),
wxT("RED"),
wxT("SALMON"),
wxT("SEA GREEN"),
wxT("SIENNA"),
wxT("SKY BLUE"),
wxT("SLATE BLUE"),
wxT("SPRING GREEN"),
wxT("STEEL BLUE"),
wxT("TAN"),
wxT("THISTLE"),
wxT("TURQUOISE"),
wxT("VIOLET"),
wxT("VIOLET RED"),
wxT("WHEAT"),
wxT("WHITE"),
wxT("YELLOW"),
wxT("YELLOW GREEN"),
NULL
};
*/
