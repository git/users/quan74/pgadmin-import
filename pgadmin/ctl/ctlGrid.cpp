
#include "pgAdmin3.h"

#include "ctl/ctlGrid.h"


void ClearGrid(wxGrid* grid, int fromrow, int fromcol)
{
    if (fromrow>=0)
    {
        int cr = grid->GetNumberRows();
        if (cr>0 && fromrow<cr)
        grid->DeleteRows(fromrow, cr-fromrow);
    }

    if (fromcol>=0)
    {
        int cc = grid->GetNumberCols();
        if (cc>0 && fromcol<cc)
            grid->DeleteCols(fromcol, cc-fromcol);
    }
}

void ClearGridRows(wxGrid* grid, int fromrow) { ClearGrid(grid, fromrow, -1); }
void ClearGridCols(wxGrid* grid, int fromcol) { ClearGrid(grid, -1, fromcol); }

void SetGridColReadOnly(wxGrid* grid, int col, bool isreadonly, bool chgcol)
{
    int ir, cr = grid->GetNumberRows();
    for (ir=0; ir<cr; ir++)
        SetGridCellReadOnly(grid, ir, col, isreadonly, chgcol);
}

void SetGridCellReadOnly(wxGrid* grid, int row, int col, bool isreadonly, bool chgcol)
{
    grid->SetReadOnly(row, col, isreadonly);

    if (!chgcol)
        return;

    if (isreadonly)
        grid->SetCellTextColour(row, col, *wxLIGHT_GREY);
    else
        grid->SetCellTextColour(row, col, *wxBLACK);
}

void SetGridRowTextColour(wxGrid* grid, int row, const wxColour *fcolor)
{
    int ic, cc = grid->GetNumberCols();
    for (ic=0; ic<cc; ic++)
        grid->SetCellTextColour(row, ic, *fcolor);
}

pgGridCellChioceEditor::pgGridCellChioceEditor(bool allowOthers)
: wxGridCellChoiceEditor(0, NULL, allowOthers)
{
}

pgGridCellChioceEditor::pgGridCellChioceEditor(size_t count, const wxString choices[], bool allowOthers)
: wxGridCellChoiceEditor(count, choices, allowOthers)
{
}

pgGridCellChioceEditor::pgGridCellChioceEditor(const wxArrayString& choices, bool allowOthers)
: wxGridCellChoiceEditor(choices, allowOthers)
{
}
