//////////////////////////////////////////////////////////////////////////
//
// pgScript - PostgreSQL Tools
// RCS-ID:      $Id$
// Copyright (C) 2002 - 2009, The pgAdmin Development Team
// This software is released under the BSD Licence
//
//////////////////////////////////////////////////////////////////////////


#ifndef PGSOBJECTS_H_
#define PGSOBJECTS_H_

#include "pgsGenerator.h"
#include "pgsNumber.h"
#include "pgsRecord.h"
#include "pgsString.h"
#include "pgsVariable.h"

#endif /*PGSOBJECTS_H_*/
