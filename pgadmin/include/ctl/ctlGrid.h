
#ifndef CTLGRID_H
#define CTLGRID_H

class pgGridCellChioceEditor;

void ClearGrid(wxGrid* grid, int fromrow=0, int fromcol=0);
void ClearGridRows(wxGrid* grid, int fromrow=0);
void ClearGridCols(wxGrid* grid, int fromcol=0);

void SetGridColReadOnly(wxGrid* grid, int col, bool isreadonly = true, bool chgcol = true);
void SetGridCellReadOnly(wxGrid* grid, int row, int col, bool isreadonly = true, bool chgcol = true);

void SetGridRowTextColour(wxGrid* grid, int row, const wxColour *fcolor);

class pgGridCellChioceEditor : public wxGridCellChoiceEditor
{
public:
    pgGridCellChioceEditor(bool allowOthers);
    pgGridCellChioceEditor(size_t count = 0,
                           const wxString choices[] = NULL,
                           bool allowOthers = false);
    pgGridCellChioceEditor(const wxArrayString& choices,
                           bool allowOthers = false);

public:
    // choices operation
    void Clear() { m_choices.Empty(); }
    int GetCount() { return m_choices.GetCount(); }
    int Index(const wxString& item) { return m_choices.Index(item); }
    bool Contains(const wxString& item) { return m_choices.Index(item)!=wxNOT_FOUND; }
    wxString const Item(int pos) { return m_choices.Item(pos); }
    void SetItems(const wxArrayString *items) { m_choices = *items; }
    int Append(const wxString& item) { m_choices.Add(item); return m_choices.Count()-1; }
    void Insert(const wxString& item, unsigned int pos) { m_choices.Insert(item, pos); }

public:
    // getter and setter
    wxString getPrevCellValue() const { return m_prevcellvalue; }
    void getPrevCellValue(const wxString prevval) { m_prevcellvalue = prevval; }

protected:
    wxString m_prevcellvalue;

};

#endif
