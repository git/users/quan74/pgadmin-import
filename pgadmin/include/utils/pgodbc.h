#ifndef PGODBC_H
#define PGODBC_H

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)

#if defined(__WXMSW__)
#include <sql.h>
#include <sqlext.h>
#elif defined(__WITHIODBC__)
#define BOOL int
#include <sql.h>
#include <sqlext.h>
#undef BOOL
#elif defined(__WITHUNIXODBC__)
#include <sql.h>
#include <sqlext.h>
#endif

class pgODBC
{
public:
    const static int ODBCNAMEBUFFLEN = 256;
    const static size_t ODBCINFONUM = 17;
    const static SQLWCHAR* const ODBCINFONAMES[];
    const static SQLWCHAR* const ODBCTYPENAMES[];

public:
    static SQLWCHAR **FetchDataSources(SQLWCHAR *errmsg = NULL);

    pgODBC(const SQLWCHAR *dsnname=NULL, const SQLWCHAR *dsnuser=NULL, const SQLWCHAR *dsnpwd=NULL);
    //pgODBC(const wxString dsnname, const wxString dsnuser, const wxString dsnpwd);
    ~pgODBC();

    bool Connect();
    bool Connect(const SQLWCHAR *dsnname, const SQLWCHAR *dsnuser, const SQLWCHAR *dsnpwd);
    //bool Connect(const wxString dsnname, const wxString dsnuser, const wxString dsnpwd);
    void Disconnect();
    bool IsConnected() { return m_connected; }

    const static SQLWCHAR** GetInfoNames() { return (const SQLWCHAR **)ODBCINFONAMES; }
    const SQLWCHAR **GetInfoValues() { return (const SQLWCHAR **)m_odbcinfovals; }
    const SQLINTEGER GetTableNum() { return (const SQLINTEGER)m_tablenum; }
    const SQLWCHAR **GetTableList();
    const SQLWCHAR ***GetTableInfo(SQLWCHAR **tableinfo)
            { return GetTableInfo(tableinfo[0], tableinfo[1], tableinfo[2]); }
    const SQLWCHAR ***GetTableInfo(SQLWCHAR *catname, SQLWCHAR *schemname, SQLWCHAR *tablename);
    SQLWCHAR *GetSelectStmt(const SQLWCHAR *qualifiedname,
                                        const SQLWCHAR ***colsinfo,
                                        const SQLWCHAR *wherecl);

    const SQLWCHAR *GetQuotedName(const SQLWCHAR *objname);
    const SQLWCHAR *GetQualifiedName(const SQLWCHAR **tableinfo)
            { return GetQualifiedName(tableinfo[0], tableinfo[1], tableinfo[2]); }
    const SQLWCHAR *GetQualifiedName(const SQLWCHAR *catname,
                                         const SQLWCHAR *schemname,
                                         const SQLWCHAR *tablename);

    const SQLWCHAR *GetDSNName() { return m_dsnnamewc; }

    bool HasErrorMsg() { return m_oerrmsg[0]!=_T('\0'); }
    const SQLWCHAR* GetErrorMsg() { return m_oerrmsg; }

    static const SQLWCHAR* SQLTypeToName(int sqltype);
    static const bool IsDateTimeType(int sqltype) { return sqltype==SQL_DATETIME || sqltype==SQL_TIMESTAMP; }
    static const bool IsIntervalType(int sqltype) { return sqltype==SQL_INTERVAL; }
    static int NumericToWChar(SQL_NUMERIC_STRUCT numericval, SQLWCHAR *strbuff, size_t bufflen);
    static int IntervalToWChar(SQL_INTERVAL_STRUCT intvval, SQLWCHAR *strbuff, size_t bufflen);
    static int DatetimeToWChar(SQL_TIMESTAMP_STRUCT tsval, SQLWCHAR *strbuff, size_t bufflen);

    bool HasStmtRunning() { return m_curhstmt!=NULL; }
    bool ExecuteQueryStmt(SQLWCHAR *stmetext);
    bool ReexecPrevQuery();
    SQLSMALLINT GetResultColsNum() { return m_curstmtcolsnum; }
    SQLLEN GetResultRowsNum();
    SQLSMALLINT* GetResultColTypes() { return m_curstmttypes; }
    SQLWCHAR** GetResultColLabels() { return m_curstmtlabels; }
    SQLSMALLINT GetResultColType(SQLUSMALLINT colnumber);
    SQLWCHAR* GetResultLabel(SQLUSMALLINT colnumber);
    SQLUSMALLINT GetResultColNr(SQLWCHAR* collabel);
    SQLRETURN NextResultRow();
    SQLRETURN GetResultData(SQLUSMALLINT colnumber, SQLWCHAR *databuff, size_t bufflen, SQLINTEGER *indptr);
    bool CloseCurrentStmt();

private:
    SQLWCHAR m_dsnnamewc[SQL_MAX_DSN_LENGTH + 1];
    SQLWCHAR m_dsnuserwc[ODBCNAMEBUFFLEN];
    SQLWCHAR m_dsnpwdwc[ODBCNAMEBUFFLEN];

    // ODBC driver info
    SQLUSMALLINT m_dsnmaxcatnamelen, m_dsnmaxschemnamelen, m_dsnmaxtablenamelen, m_dsnmaxcolnamelen;
    SQLUINTEGER m_sqlconformance, m_maxstatementlen;
    SQLUSMALLINT m_idcase, m_quotedidcase;
    SQLWCHAR m_idquotechars[ODBCNAMEBUFFLEN], m_idspecchars[ODBCNAMEBUFFLEN];
    SQLUSMALLINT m_catalogloc, m_maxtabsinselect, m_maxcolsinselect, m_maxcolsinorderby, m_maxcolsingroupby;
    SQLWCHAR m_catalogname[2], m_catalognameseparator[ODBCNAMEBUFFLEN];

    // ODBC handles & info
	SQLHANDLE m_odbchenv;
    SQLHDBC m_odbchdbc;
    bool m_connected;
    SQLWCHAR **m_odbcinfovals;
    SQLINTEGER m_tablenum;
    SQLWCHAR **m_tablelist;

    // current executing statement info
    SQLHSTMT m_curhstmt;
    SQLWCHAR *m_curstmttext;
    SQLSMALLINT m_curstmtcolsnum;
    SQLSMALLINT *m_curstmttypes;
    SQLWCHAR **m_curstmtlabels;

    // last fetched table info
    SQLWCHAR ***m_tableinfo;

    // error message
    SQLWCHAR m_oerrmsg[ODBCNAMEBUFFLEN];

private:
    void Init();
    void FetchODBCInfo();
    void ClearODBCInfo();
    void ClearTableList();
    void ClearTableInfo();
    void ClearCurrentStmtInfo();

};

#endif

#endif

