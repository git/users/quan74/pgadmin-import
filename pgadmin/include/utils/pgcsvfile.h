#ifndef PGCSVFILE_H
#define PGCSVFILE_H

#include <wx/regex.h>
#include <wx/archive.h>
#include <wx/wfstream.h>
#include <wx/zstream.h>
#include <wx/zipstrm.h>
#include <wx/tarstrm.h>

class pgCSVFile
{
// constants
public:
    // Archive format
    enum ArchiveFormat { ARCHUNKNOWN=0, ARCHZIP=0x1, ARCHTAR=0x2, ARCHGZIP=0x4, ARCHBZIP2=0x8 };
    static const unsigned int MAXENTRYNUM = 127;
    // Unix: LF, Dos: CR LF, Mac: CR
    static const wxChar LF = wxT('\n');//'LF' = 0xA = 10
    static const wxChar CR = wxT('\r');//'CR' = 0xD = 13
    // parse function return value
    enum { cp_errqt=-5, cp_endwithcr, cp_endwithcomma, cp_needmore, cp_donothing, cp_normal };
    // setting relative
	static const wxString DEFAULTCHARDELIMITER;
    static const wxChar DEFAULTQUOTEDBY = wxT('\"');
	static const int NULLWIDTHDELIMITER = -1;
	//static const int ENDOFWIDTHDELIMITER = -2;
    // file relative
    static const size_t ONEKILO = 1024;
    static const size_t ONEMEGA = 1048576;
    // Multiple disc blocks per read will improve i/o performance
    // Especially when read large size file
    // This time it be set 4k, left optimization later
    static const size_t READBLKSZ = ONEKILO*4;
    static const size_t MAXWORDSZ = 4;
    static const size_t READBUFSZ = READBLKSZ + MAXWORDSZ*3;
    static const size_t INITPREVIEWSZ = READBLKSZ;
    // preview lines number
    static const size_t MINPREVIEWROWSNUM =  (size_t)1;
    static const size_t DEFAULTPREVIEWROWSNUM = (size_t)20;
    static const size_t MAXPREVIEWROWSNUM = (size_t)200;
    // function return value
	enum {cf_converr=-7, cf_aropened, cf_invalidenc, cf_nonamespec, cf_notexist, cf_cantopen, cf_notopen, cf_normal};

// constructor & destructor
public:
    pgCSVFile();
#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
    pgCSVFile(const wxString fpath, bool isarch = false);
#else
    pgCSVFile(const wxString fpath);
#endif
    /*pgCSVFile(bool isarch = false);
    pgCSVFile(const wxString fpath,// bool isarchfile = false,
            const wxFontEncoding fileencoding = wxFONTENCODING_SYSTEM,
            const wxString chardlm = DEFAULTCHARDELIMITER,
            const wxChar quotedby = DEFAULTQUOTEDBY);*/
	~pgCSVFile();

// setting relative functions
public:
    wxFileOffset Length() { return m_filesize; }
    wxFileOffset Tell() { return m_fileoffset; }
    bool Eof();

    wxString GetFilePath() { return m_filepath; }
    void SetFileEncoding(wxFontEncoding fileencoding);
    wxFontEncoding GetFileEncoding() { return m_fileencoding; }

	bool IsDelimitedByChar() { return m_delimitedbychar; }
    void SetDelimitedByChar(bool delimitedbychar = true);
	bool IsDelimitedByWidth() { return !m_delimitedbychar; }
    void SetDelimitedByWidth(bool delimitedbywidth = true) { SetDelimitedByChar(!delimitedbywidth); }
	const wxString& GetCharDelimiter() { return m_chardlm; }
	void SetCharDelimiter(wxString chardlm);
	const int* GetWidthDelimiter() { return m_widthdlm; }
	const wxString GetWidthDelimiterStr();
	bool SetWidthDelimiter(wxString widthdlm);
	bool FirstRowIsHeader() { return m_firstrowisheader; }
	void SetFirstRowIsHeader(bool firstrowisheader=false);
    bool IsBackSlashQuote() { return m_backslashquote; }
    void SetBackSlashQuote(bool backslashquote);
    bool IsForceLineBreak() { return m_forcelinebreak; }
    void SetForceLineBreak(bool forcelinebreak = true);
	wxChar GetQuotedBy() { return m_quotedby; }
    void SetQuotedBy(wxChar quotedby);

    size_t GetPreviewRowsNum() { return m_pvrowsnum; }
    void SetPreviewRowsNum(const size_t pvrowsnum = (size_t)0);

    long GetLineNumber() { return m_linenumber; }

// file relative functions
public:
    bool Open() { return Open(m_filepath); }
    bool Open(const wxString fname) { return Open(fname, false); }
    bool Open(const wxString fname, bool isarch);
    bool IsOpened();
#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
    int TestStream(wxInputStream& testinstrm, int strmtype, wxMBConv& strmconv=wxConvLocal);
    void ClearEntryList();
    void LoadEntryList(const wxString* origentryname=NULL);
#endif

#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
    static bool CanHandleArchive() { return true; }
    size_t GetEntriesNum() { return m_entriesnum; }
    wxString GetEntryName(size_t entryidx);
    void* GetEntry(size_t entryidx);
    bool OpenArchiveEntry(size_t entryidx);
    bool OpenArchiveEntry(void* archentry);
    bool CloseArchiveEntry();
    void* GetCurrentEntry() { return m_curarchentry; }
    wxString GetCurrentEntryName();
    void SetEntryEncoding(wxFontEncoding entryencoding);
    wxFontEncoding GetEntryEncoding() { return m_entryencoding; }
    bool IsArchive() { return m_parentinstrm!=NULL; }
#else
    static bool CanHandleArchive() { return false; }
    size_t GetEntriesNum() { return 0; }
    wxString GetEntryName(int entryidx) { return *wxEmptyString; }
    void* GetEntry(int entryidx) { return NULL; }
    bool OpenArchiveEntry(int entryidx) { return false; }
    bool OpenArchiveEntry(void* archentry) { return false; }
    void* GetCurrentEntry() { return NULL; }
    wxString GetCurrentEntryName() { return *wxEmptyString; }
    bool IsArchive() { return false; }
    void SetEntryEncoding(wxFontEncoding fileencoding) {}
    wxFontEncoding GetEntryEncoding() { return wxFONTENCODING_SYSTEM; }
#endif

    bool IsOk() { return m_fileisok && m_fencisok && m_settingisok && (m_delimitedbychar || m_widthdlmisok); }

    bool FileIsOk() { return m_fileisok; }
    bool FileEncodingIsOk() { return m_fencisok; }
    const wxString& GetErrorMsg() { return m_ferrmsg; }
    bool WidthDlmIsOk() { return m_delimitedbychar || m_widthdlmisok; }
	void Close();

    wxFileOffset GenMoreFilePreview(wxString *pvstr = NULL);

    const wxString& GetFilePreview() { return m_pvfilecont; }

    const size_t GetPreviewRowsCount() { return m_pvparsedrowsnum; }
    const size_t GetColsNum() { return m_maxcolsnum; }
    const wxString** GetPreviewLines() { return (const wxString**)m_pvcsvlines; }
    const size_t* GetPreviewRowColsNum() { return (const size_t*)m_pvparsedcolsnum; }
    const wxString*** GetPreviewRowsVals() { return (const wxString***)m_pvparsedrowvals; }
    const wxArrayString* GetColsHeader() { return &m_colheaders; }
    const wxString& GetColName(int colpos) { return m_colheaders.Item(colpos); }
    int GetColPos(const wxString colname) { return m_colheaders.Index(colname); }

    bool EndPreview();
    bool NextLine(wxString& linedata, wxString**& linevals, int &valsnum);
    size_t GetCurFileLineNr() { return m_filelinenr; }
    size_t GetCurCSVLineNr() { return m_csvlinenr; }

private:
    wxFileOffset TellI();
    //void Init();

    void GenHeader();
    bool ValidateSetting();

    size_t GenCSVPreview(const bool isinit = false);
    wxFileOffset MB2str(wxCSConv *txtconv, wxString *bufstr, char* inbuf, const size_t bufos, size_t& datlen);

    wxString **ParseLine(const wxChar *csvdata, size_t *parsedoffset, const bool hasmore,
                    wxString **parsedvals, size_t *curcolnr,
                    wxFileOffset *rsval, wxString *linedata);
    wxString **ParseLine(const wxChar *csvdata, size_t *parsedoffset, const bool hasmore,
                    wxString **parsedvals, size_t *curcolnr, bool *incolparsing, bool *inquotedblk,
                    wxFileOffset *rsval, size_t *filelinenum, wxString *linedata);
    void ClearParsedLine();

    void ClearPreview();
    void ClearFilePreview();
    void ClearCSVPreview();
    void ReparseFilePreview();
    void ParsePreviewLines();

//csv setting
private:
	bool m_delimitedbychar;
	wxString m_chardlm;
    const wxChar* m_chardlmvals;
	int *m_widthdlm;
    size_t m_widthdlmsnum;
    bool m_widthdlmisok;// whether width delimiter is ok
	bool m_firstrowisheader;// the first line is header
    bool m_backslashquote;// backslash-quote style
	wxChar m_quotedby;
    size_t m_pvrowsnum;
    bool m_forcelinebreak;

	static const wxRegEx m_widthdlmregex;

// file relative
private:
	wxString m_filepath;// text file path
	wxFile *m_rawfile;// corresponding file
    wxFileOffset m_filesize;
    wxFileOffset m_baseoffset;
    wxFileOffset m_fileoffset;
    float m_charavglen;

    size_t m_linenumber;// current file line number
    size_t m_csvrownr;
	wxFontEncoding m_fileencoding;// charset encoding
	wxCSConv *m_fileconv;// corresponding file's wxCSConv
    bool m_fileisok;// whether file is ok
    bool m_fencisok;// whether file encoding is ok
    bool m_settingisok;// whether csv setting is ok
    wxString m_ferrmsg;// error message

#if wxUSE_STREAMS && wxUSE_ARCHIVE_STREAMS && (wxUSE_ZIPSTREAM || wxUSE_TARSTREAM)
    wxInputStream *m_parentinstrm;
    size_t m_entriesnum;
#if wxUSE_ZIPSTREAM
    wxZipInputStream *m_zipinstrm;
    wxZipEntry **m_zipentrylist;
#endif
#if wxUSE_TARSTREAM
    wxTarInputStream *m_tarinstrm;
    wxTarEntry **m_tarentrylist;
#endif
    wxArchiveEntry *m_curarchentry;
    wxFontEncoding m_entryencoding;// charset encoding
    wxCSConv *m_entryconv;// corresponding file's wxCSConv
#endif

    wxMemoryBuffer m_pvmembuf;// preview buffer
    size_t m_pvbufoffset;// 
    size_t m_pvdecoffset;// decoded preview buffer offset
    //size_t m_pvcurlinenr;// current preview file line number

    wxString m_pvfilecont;// decoded preview file contents
    size_t m_pvparsedoffset;

    size_t m_pvparsedrowsnum;// preview csv rows number
    wxArrayString m_colheaders;
    wxString **m_pvcsvlines;// decoded preview CSV lines
    size_t *m_pvparsedcolsnum;// col number for each preview row
    size_t m_maxcolsnum;
    wxString ***m_pvparsedrowvals;// parsed preview row

    wxString m_curfilecont;
    size_t m_parsedoffset;
    char m_leftbuf[MAXWORDSZ];
    wxString **m_prevcolvals;
    size_t m_prevcolnum;
    size_t m_filelinenr;// current file line number
    size_t m_csvlinenr;// current csv line number

};

#endif //PGCSVFILE_H
