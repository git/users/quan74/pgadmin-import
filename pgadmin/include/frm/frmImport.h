// Thank Julian Smart & Anthemion Software Ltd.
// for applied a free license of DialogBlocks to design wxWizard UI.
// http://www.anthemion.co.uk/   http://www.dialogblocks.com/

#ifndef FRMIMPORT_H
#define FRMIMPORT_H

#include <wx/notebook.h>
#include <wx/spinctrl.h>
#include <wx/wizard.h>
#include <wx/archive.h>
#include <wx/wfstream.h>
#include <wx/zstream.h>
#include <wx/zipstrm.h>
#include <wx/tarstrm.h>

#include "schema/pgTable.h"
#include "ctl/ctlGrid.h"
#include "utils/pgcsvfile.h"
#include "schema/pgDatatype.h"

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
#include "utils/pgodbc.h"
#endif

class frmImport : public wxWizard
{
public:
    enum WizardPageNumber { INVALIDPAGE = -1, ALLIMPPAGES, TARGETPAGE, SRCTYPEPAGE, TEXTFILEPAGE, ARCHFILEPAGE, SRCODBCPAGE, SRCPGSQLPAGE, FILESETTINGPAGE, FILEOPTSPAGE, COLSETTINGPAGE, DESTLOGPAGE, PROCESSPAGE };
    static const int TGTCOLNAMENUM=0, TGTCOLTYPENUM = 1, SRCCOLNAMENUM = 2;
    static const int ODBCONEMPTYCOLNUM = 3, ODBCFMTCOLNUM = 4, ODBCEXPCOLNUM = 5;
    static const int FILETRIMMODECOLNUM = 3, FILEONEMPTYCOLNUM = 4, FILEFMTCOLNUM = 5, FILEEXPCOLNUM = 6;
    static const int ODBCOPTCOLSCT = ODBCEXPCOLNUM+1, SRCDBOPTCOLSCT = SRCCOLNAMENUM+1, TEXTFILEOPTCOLSCT = FILEEXPCOLNUM+1;
    static const wxString IMPTABMARK, IMPNONEQUOTEDBY;

    enum ImpMode { IMPORTMODECOPY = 1, IMPORTMODEINSERT };
    enum FormatStyle { FORMATSTYLEISO = 1, FORMATSTYLEANSIC, FORMATSTYLERFC, FORMATSTYLEPGSQL };

    enum OnEmptyAct { IMPERROR = 1, IMPNULL, IMPDEFAULT, IMPEMPTY, IMPCONST, IMPEXPR, IMPCURTS, IMPCURDATE, IMPCURTIME, IMPLOCALTS, IMPLOCALTIME, IMPCLIENTTIME, IMPZERONUMBER, IMPZEROINTERVAL, IMPBOOLTRUE, IMPBOOLFALSE };
    static const wxString IMPERRORDISP, IMPNULLDISP, IMPDEFAULTDISP, IMPEMPTYDISP, IMPCONSTDISP, IMPEXPRDISP, IMPIGNORERDISP;
    static const wxString IMPCURTSDISP, IMPCURDATEDISP, IMPCURTIMEDISP;
    static const wxString IMPLOCALTSDISP, IMPLOCALTIMEDISP, IMPCLIENTTIMEDISP;
    static const wxString IMPZERONUMBERDISP, IMPZEROINTERVALDISP;
    static const wxString IMPBOOLTRUEDISP, IMPBOOLFALSEDISP;
    enum TrimAct { TRIMNONE = 1, TRIMALL, TRIMLEFT, TRIMRIGHT };
    static const wxString TRIMNONEDISP, TRIMALLDISP, TRIMLEFTDISP, TRIMRIGHTDISP;
    static const wxString ANSICTIMETZFMT, ANSICTIMESTAMPFMT, ANSICTIMESTAMPTZFMT;
    enum ImpDest { IMPDESTNONE = 0, IMPDESTCOPY, IMPDESTFCOPY, IMPDESTINS, IMPDESTFINS, IMPDESTTEST, IMPDESTFTEST, IMPDESTFILE};
    static const wxString IMPDESTCOPYDISP, IMPDESTFCOPYDISP, IMPDESTINSDISP, IMPDESTFINSDISP, IMPDESTTESTDISP, IMPDESTFTESTDISP, IMPDESTFILEDISP;
    enum ImpGauge { IMPPREPARED, IMPRUNNING, IMPPAUSING, IMPPAUSED, IMPSTOPING, IMPSTOPED, IMPERRORSTOP, IMPFINISHED};

    static const wxColour YELLOW_COLOR;

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    static const short IMPSQLNAMELEN = 256;
    static const short IMPDATABUFFLEN = 1024;
    static const wxString BINDATADISP, UNKNOWNTYPEDISP, UNSUPTYPEDISP, MOREDATADISP;
#endif

public:
    frmImport(frmMain *_form, pgTable *_obj = NULL);
    //frmImport(frmMain *_form, pgConn *_conn, wxString *schemaname, wxString *tableoidstr, wxString *tablename);
    ~frmImport();
    bool Go(bool modal);

private:
    class exprSnippet
    {
    public:
        exprSnippet(wxString sexpr, int stype):exprsnippet(sexpr),snippettype(stype),nextsnip(NULL){}

        wxString exprsnippet;
        // wxNOT_FOUND: constant
        // others: position of source column
        int snippettype;

        exprSnippet *nextsnip;
    };

private:
    // initialize
    void Init();
    // wizard event
    void OnPageChanging(wxWizardEvent& ev);
    void OnWizardCancel(wxWizardEvent& ev);

    // Source Type page event
    void OnSourceType(wxCommandEvent &ev);

    // Text file page event
    void OnTextFilePathKillFocus(wxFocusEvent &ev);
    void OnTextFileBrowse(wxCommandEvent &ev);
    void OnTextFilePathChange(const wxString& fpath);
    void OnTextFileEncoding(wxCommandEvent &ev);
    void OnTextFilePreviewMore(wxCommandEvent &ev);
    // Archive file page event
    void OnArchFileBrowse(wxCommandEvent &ev);
    void OnArchFilePathKillFocus(wxFocusEvent &ev);
    void OnArchFileEncoding(wxCommandEvent &ev);
    void OnArchFileChange(const wxString* fpath=NULL);
    void OnArchEntryName(wxCommandEvent &ev);
    void OnArchEntryEncoding(wxCommandEvent &ev);
    void OnArchFilePreviewMore(wxCommandEvent &ev);
    // File setting page event
    void OnFileDelimiterType(wxCommandEvent &ev);
    void OnFileCharDelimiter(wxCommandEvent &ev);
    void OnFileFixedWidthKillFocus(wxFocusEvent &ev);
    void OnFileQuotedBy(wxCommandEvent &ev);
    void OnFileFirstRowIsHeader(wxCommandEvent &ev);
    void OnFileForceLineBreak(wxCommandEvent &ev);
    void OnFileBackSlashQuote(wxCommandEvent &ev);
    void OnFilePreviewRows(wxSpinEvent &ev);
    //void OnFilePathFixedWidth(wxFocusEvent &ev);

    //wzdODBCPage
#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    //void OnODBCDSNNames(wxCommandEvent &ev);
    void OnODBCReset(wxCommandEvent &ev);
    void OnODBCConnect(wxCommandEvent &ev);
    void OnODBCTableNames(wxCommandEvent &ev);
    //void OnODBCTableNameKillFocus(wxFocusEvent &ev);
    void OnODBCCustomizedSQL(wxCommandEvent &ev);
    void OnODBCWhereCl(wxCommandEvent &ev);
    void OnODBCPreview(wxCommandEvent &ev);
#endif

    // File options page event
    void OnFileOptsImpRows(wxCommandEvent &ev);
    void OnFileOptsCheckTrueFirst(wxCommandEvent &ev);

    // Column page event
    void OnColSettingImportMode(wxCommandEvent &ev);
    void OnColSettingFormatStyle(wxCommandEvent &ev);
    void OnColSettingCellChange(wxGridEvent &ev);
    void OnColSettingImpPreview(wxCommandEvent &ev);
    void OnColSettingTestImp(wxCommandEvent &ev);

    // Destination&log page event
    void OnDestImpDest(wxCommandEvent &ev);
    void OnDestFileBrowse(wxCommandEvent &ev);
    bool OnDestFilePathChange(const wxString& filepath);
    void OnDestLogTo(wxCommandEvent &ev);
    void OnLogFileBrowse(wxCommandEvent &ev);
    bool OnLogFilePathChange(const wxString& filepath);
    void OnLogTypeChange(wxCommandEvent &ev);

    // Process page event
    void OnProcImpStart(wxCommandEvent &ev);
    void OnProcImpStop(wxCommandEvent &ev);

    bool CheckWizardPage();
    void DispStatusMsg(const wxString& statusmsg);
    void InitWizardPage(WizardPageNumber pagenr);
    void ResetWizardPage(WizardPageNumber pagenr);
    void GenFilePreview();
    void GenCSVPreview();
    void GenColsPreview();
    bool GenCSVSQLData(wxString& rowdata, wxString& rowlog, wxString** linedata,
            const size_t colnum, wxDateTime& clienttime, const int importmode, const int formatstyle);
    void GenColsEmptyPreview(size_t pvgridrowidx, size_t pvgridcolidx, size_t dbcolidx,
            int onemptyact, wxString& tgtcoltype, exprSnippet*& exprsnip, wxDateTime& clienttime, bool& haserror);

    bool FetchTargetColsInfo();
    void ClearTargetColsInfo();
    void InitColSettingPage();
    void ResetColSettingPage();
    void ResetColSettingSrcColEditor(bool resetsrccol = true);
    void ResetColSettingOnEmptyColEditor(int dbcolnr = wxNOT_FOUND);
    void ResetColSettingFmtColEditor();
    void ResetColsSettingStatus(int dbcolnr = wxNOT_FOUND);
    void ParseColsImpSetting(int dbcolnr = wxNOT_FOUND);
    void ClearColsImpSetting();

    exprSnippet *ParseImportConst(const wxString& origconst, const bool iscopy = true);
    exprSnippet *ParseImportExpr(const wxString& origexpr);
    bool ValidatePgExp(const wxString& pgexp, const wxString& exptype, wxString& expresult);

    static void RandomSavepointName(wxChar* randname, short namelen=10);

    void ImportData();
    void RefreshProc(bool incgauge=true);

    bool ImportRow(wxString& impdatalist, const int impmode, const int impdest,
                bool& copybolcking, bool logtoscr, bool logtofile,
                bool logerror, bool logwarn, bool logstmt, bool loginfo, bool logproc,
                const int commitevery, wxString& insstmt);

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    void FetchODBCDSNList();
    void FetchODBCInfo();
    void GenODBCSelectStmt(const SQLWCHAR*** colsinfo);
    void GenODBCPreview();
    bool GenODBCSQLData(wxString& rowvals, wxString& rowlog, SQLWCHAR** odbcdata,
            wxDateTime& clienttime, const int impmode);

    wxString SQLTypeToName(int sqltype);
    void ClearODBCPreview();
#endif

private:
    frmMain *m_form;
    pgTable *m_table;

    pgConn *m_conn, *m_impconn;
    wxString m_impschemaname, m_imptableoidstr, m_imptablename;

    WizardPageNumber m_firstpagenr, m_curpagenr, m_origsrctype, m_datasrctype;
    bool m_txtfilepginit, m_archfilepginit, m_srcodbcpginit, m_srcpgsqlpginit, m_filesettingpginit, m_fileoptspginit, m_colsettingpginit, m_destlogpginit;

    pgCSVFile *m_srcfile;
    bool m_datapverr;

    // match regular expression & start with
    wxRegEx *m_fileoptimpregex;
    wxString *m_fileoptstartwith;

    // boolean values
    wxArrayString *m_booltruevals, *m_boolfalsevals;

    // target table columns info
    size_t m_tgtcolsnum;// columns number
    wxString **m_tgtcolsnames;// column name
    pgDatatype **m_tgtcolstypes;// column type
    bool *m_tgtcolsisnotnull, *m_tgtcolshasdef;// *m_colsispk // not null & default value
    bool *m_tgtcolincopy;

    // column import setting
    int *m_colsrccolnr;// source col number(0...n), wxNOT_FOUNT means not set
    int *m_coltrimmode;// trim direction, see enum TrimAct definition
    // when source value is NULL(for Character types)/(NULL or empty)(for other types)/not set
    int *m_colonemptyact;
    wxString **m_colfmtarr;// format
    exprSnippet **m_colexprarr;// expression or constant
    short *m_srcrefnum;
    // import SQL statement
    wxString m_impsqlstmt;

    short m_colsettingerrnum;
    bool *m_colsettingiserr;
    wxString m_colsettingmsg;

#if defined(__WXMSW__) || defined(__WITHIODBC__) || defined(__WITHUNIXODBC__)
    pgODBC *m_srcodbc;
    bool m_srcodbcisok;
    SQLUSMALLINT m_dsnmaxcatnamelen, m_dsnmaxschemnamelen, m_dsnmaxtablenamelen, m_dsnmaxcolnamelen;
#endif

    int m_procstatus;
    wxFile *m_destfile, *m_logfile;
    wxFileOffset m_srcsize;
    unsigned int m_improwsnum, m_skiprowsnum;
    unsigned int m_imperrnum, m_transrowsnum;
    wxChar m_imptransname[16];
    wxChar m_imptranssn[32];
    wxDateTime m_impstartdt, m_imppausedt;
    wxTimeSpan m_imppausedts;

    static const int MAXLOGCOUNT = 16*1024;

    DECLARE_EVENT_TABLE()
};

class importFactory : public contextActionFactory
{
public:
    importFactory(menuFactoryList *list, wxMenu *mnu, ctlMenuToolbar *toolbar);
    wxWindow *StartDialog(frmMain *form, pgObject *obj);
    bool CheckEnable(pgObject *obj);
};

#endif  //FRMIMPORT_H
